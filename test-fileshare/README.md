# 2020-07-02

## Goal

This project was created to detect issued copying files to a NFS share.

## Usage

Copies the files in the sourceBaseDir diretories to the fileShareDir directory.
The "sourceBaseDir" directory must have the thread number suffix. If the "sourceBaseDir" is "sample" and the program is run with 4 threads, the "sample-1", "sample-2", "sample-3" and "sample-4" directories must exist.

```
java -jar test-fileshare-1.0.1-jar-with-dependencies.jar <sourceBaseDir> <fileShareDir> <numberOfThreads> <threadStarupDelayMs>
```

Example:

```
java -jar test-fileshare-1.0.1-jar-with-dependencies.jar sample /home/ajsco/temp/test-fileshare/my-fileshare/ 4 250
```

## TO DO

## Done

## In progress

## Known Problems
