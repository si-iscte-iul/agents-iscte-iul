package pt.iscte.dsi.test.fileShare;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestCopyToFileShare {
	private static final String NL = System.lineSeparator();

	private static final Logger logger = LoggerFactory.getLogger(TestCopyToFileShare.class.getSimpleName());
	private static final Logger loggerDebug = LoggerFactory
			.getLogger(TestCopyToFileShare.class.getSimpleName() + "-debug");

	private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

	public static void main(String[] args) {

//		System.out.println("Hello World!");
//		if (args != null && args.length > 0) {
//			for (int i = 0; i < args.length; i++) {
//				System.out.println("|Arg[" + i + "]: " + args[i]);
//			}
//		}
		if (args == null || args.length != 4) {
			String msg = "You supply 4 arguments" + NL
					+ "<sourceBaseDir> <fileShareDir> <numberOfThreads> <threadStarupDelayMs>" + NL + NL
					+ "Example: mvn exec:java -Dexec.mainClass=\"pt.iscte.dsi.test.fileShare.TestCopyToFileShare\" -Dexec.args=\"sampleDir /home/fenix/storage 2 250\""
					+ NL + "Use: mvnDebug instead of mvn to debug" + NL + NL;
			System.out.println(msg);
			throw new IllegalArgumentException(msg);
		}

		String sourceBaseDir = args[0];
		String fileShareDir = args[1];
		int numberOfThreads = Integer.parseInt(args[2]);
		int threadStarupDelayMs = Integer.parseInt(args[3]);

		// Create threads
		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 0; i < numberOfThreads; i++) {
			Thread thread = new Thread(new CopyThread(sourceBaseDir + "-" + (i + 1), fileShareDir));
			threads.add(thread);
		}

		// Start threads
		for (Thread thread : threads) {
			thread.start();

			try {
				Thread.sleep(threadStarupDelayMs);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// Wait for all thread to finish
		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static class CopyThread implements Runnable {
		private String sourceBaseDir;
		private String fileShareDir;

		public CopyThread(String sourceBaseDir, String fileShareDir) {
			this.sourceBaseDir = sourceBaseDir;
			this.fileShareDir = fileShareDir;
		}

		public void run() {
			// https://attacomsian.com/blog/java-copy-files-from-one-directory-to-another
			try {
				long startDirectory = System.currentTimeMillis();
				log("|Start copy of directory " + sourceBaseDir);

				// source & destination directories
				Path src = Paths.get(sourceBaseDir);
				Path dest = Paths.get(fileShareDir + "/" + sourceBaseDir);

				// create stream for `src`
				Stream<Path> files = Files.walk(src);

				// copy all files and folders from `src` to `dest`
				files.forEach(file -> {
					try {
						log("|Copying file [start] " + file, true);
						long start = System.currentTimeMillis();
						Files.copy(file, dest.resolve(src.relativize(file)), StandardCopyOption.REPLACE_EXISTING);
						long end = System.currentTimeMillis();

//						try {
//							Thread.sleep(25);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}

						long time = end - start;
						log("|Copying file [end] " + file + "|took|" + time + "|ms", true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				});

				// close the stream
				files.close();

				long endDirectory = System.currentTimeMillis();
				long time = endDirectory - startDirectory;
				log("|End copy of directory " + sourceBaseDir + "|" + time + "|ms");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		private void log(String message) {
			log(message, false);
		}

		private void log(String message, boolean debug) {
//			logSystemOut(message);
			if (debug) {
				loggerDebug.debug(message);
			} else {
				logger.info(message);
				loggerDebug.info(message);
			}
		}

		private void logSystemOut(String message) {
			Thread t = Thread.currentThread();
			StringBuilder sb = new StringBuilder();
			sb.append(t.getName());
//			sb.append("-");
//			sb.append(t.getId());
			sb.append("|");
			sb.append(formatter.format(LocalDateTime.now()));
			sb.append("|");
			sb.append(message);
			System.out.println(sb.toString());
		}
	}

}
