# Changelog

## [1.0.1] (2020-07-02)

### Bug Fixes

* None

### Code Refactoring

* Use log4j instead of Java logging

### Features

* None

### BREAKING CHANGES

* None

## [1.0.0] (2020-07-01)

### Bug Fixes

* None

### Code Refactoring

* None

### Features

* None

### BREAKING CHANGES

* None
