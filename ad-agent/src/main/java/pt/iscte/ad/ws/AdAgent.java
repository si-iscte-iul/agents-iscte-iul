package pt.iscte.ad.ws;

import javax.jws.WebService;

import pt.iscte.ad.dto.PinAccountData;
import pt.iscte.ad.exception.InconsistentAccountException;
import pt.iscte.ad.exception.NotExistingAccountException;
import pt.iscte.ad.exception.UnknownErrorException;

/**
 * Active Directory web service interface.
 * 
 * @author Antonio Casqueiro (ajsco@iscte.pt)
 */
@WebService
public interface AdAgent {
    public PinAccountData getUserPinInfo(String username) throws InconsistentAccountException, UnknownErrorException;

    public void updateUserPinInfo(final PinAccountData pinAccountData) throws NotExistingAccountException,
            InconsistentAccountException, UnknownErrorException;
}
