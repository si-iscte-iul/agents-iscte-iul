package pt.iscte.ad.ws;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import pt.iscte.ad.business.AdClient;
import pt.iscte.ad.dto.PinAccountData;
import pt.iscte.ad.exception.InconsistentAccountException;
import pt.iscte.ad.exception.NotExistingAccountException;
import pt.iscte.ad.exception.UnknownErrorException;
import pt.iscte.fenix.util.HostAccessControl;

@WebService(name = "adagent")
// @WebContext(contextRoot = "/ad-agent", transportGuarantee = "CONFIDENTIAL",
// secureWSDLAccess = false)
// @WebContext(contextRoot = "/ad-agent")
@SOAPBinding(style = SOAPBinding.Style.RPC)
// @Remote(AdAgent.class)
// @Stateless
public class AdAgentBean implements AdAgent {

    private final Logger LOGGER;

    @Resource
    private WebServiceContext ctx;

    public AdAgentBean() throws Exception {
        LOGGER = Logger.getLogger(AdAgentBean.class);
    }

    /**
     * Checks if the client is allowed to access this service
     */
    private void validateClientAccess() {
        MessageContext messageContext = ctx.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);
        if (!HostAccessControl.isAllowed(AdAgentBean.class, request)) {
            LOGGER.error("Unknown host: " + HostAccessControl.getRemoteAddress(request) + " tried to access this service");
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
            throw new RuntimeException(HostAccessControl.getRemoteAddress(request) + " is not allowed to access this service");
        }
    }

    @WebMethod
    public PinAccountData getUserPinInfo(String username) throws InconsistentAccountException, UnknownErrorException {
        validateClientAccess();

        LOGGER.info("getUserPinInfo(username: '" + username + "')");
        try {
            return AdClient.getUserPinInfo(username);
        } catch (Exception e) {
            LOGGER.error("Error in getUserPinInfo for user: " + username, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
            throw new UnknownErrorException();
        }
    }

    @WebMethod
    public void updateUserPinInfo(@WebParam(name = "data") PinAccountData data) throws NotExistingAccountException,
            InconsistentAccountException, UnknownErrorException {
        validateClientAccess();

        try {
            AdClient.updateUserPinInfo(data);
        } catch (NotExistingAccountException e) {
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error in getUserPinInfo for user: " + data.getUsername(), e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
            throw new UnknownErrorException();
        }
    }
}
