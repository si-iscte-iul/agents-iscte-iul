package pt.iscte.ad.exception;

import javax.xml.ws.WebFault;

@WebFault
@SuppressWarnings("serial")
public class NotExistingAccountException extends Exception {

    public NotExistingAccountException() {
        super();
    }

}