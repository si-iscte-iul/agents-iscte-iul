package pt.iscte.ad.exception;

import javax.xml.ws.WebFault;

@WebFault
@SuppressWarnings("serial")
public class UnknownErrorException extends Exception {

    public UnknownErrorException() {
        super();
    }

}