package pt.iscte.ad.exception;

import javax.xml.ws.WebFault;

@WebFault
@SuppressWarnings("serial")
public class InconsistentAccountException extends Exception {

    public InconsistentAccountException() {
        super();
    }

    public InconsistentAccountException(String message) {
        super(message);
    }

}