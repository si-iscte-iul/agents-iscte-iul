package pt.iscte.ad.business;

import org.apache.log4j.Logger;

import pt.iscte.ad.dto.PinAccountData;
import pt.iscte.ad.exception.InconsistentAccountException;
import pt.iscte.ad.exception.NotExistingAccountException;
import pt.iscte.ad.exception.UnknownErrorException;
import pt.iscte.ldap.LDAPUser;
import pt.iscte.ldap.LDAPUtilsGeneric;

/**
 * AD agent business logic.
 */
public class AdClient {

    private static final Logger LOGGER = Logger.getLogger(AdClient.class);

    public static PinAccountData getUserPinInfo(String username) throws InconsistentAccountException {
        LDAPUser ldapUser = findByUsername(username);
        if (ldapUser == null) {
            return null;
        }

        return new PinAccountData(ldapUser.getName(), username, ldapUser.getDescription(), ldapUser.getTitle(), ldapUser.getPin());
    }

    private static LDAPUser findByUsername(final String username) {
        final String usernameWithoutDomain;

        if (username != null && username.contains("@")) {
            usernameWithoutDomain = username.split("@")[0];
        } else {
            usernameWithoutDomain = username;
        }

        return LDAPUtilsGeneric.findUserById(usernameWithoutDomain);
    }

    public static void updateUserPinInfo(final PinAccountData pinAccountData) throws NotExistingAccountException,
            InconsistentAccountException, UnknownErrorException {
        LDAPUser ldapUser = findByUsername(pinAccountData.getUsername());
        if (ldapUser == null) {
            throw new NotExistingAccountException();
        }

        String pin = pinAccountData.getPin();
        String workingUnit = pinAccountData.getDescription();
        String groupsEncoded = pinAccountData.getTitle();

        if ("INACTIVATE-PIN".equals(workingUnit)) {
            pin = null;
            workingUnit = null;
            groupsEncoded = null;
        }

        boolean result = LDAPUtilsGeneric.updateLdapUserPinGroupAndUnit(ldapUser, pin, workingUnit, groupsEncoded);
        if (!result) {
            throw new UnknownErrorException();
        }
    }

}