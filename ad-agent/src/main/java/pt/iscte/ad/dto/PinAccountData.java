package pt.iscte.ad.dto;

import java.io.Serializable;

public class PinAccountData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String username;
    private String description;
    private String title;
    private String pin;

    public PinAccountData() {
        super();
    }

    public PinAccountData(String name, String username, String description, String title, String pin) {
        super();
        this.name = name;
        this.username = username;
        this.description = description;
        this.title = title;
        this.pin = pin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

}