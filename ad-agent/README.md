Active directory Agent
==============
Update user pin on active directory.

## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Project dependencies**
You should install firstly the ldap-utils to your local maven repository.

**Configuration**
To configure the application change the *src/main/resources/configuration.properties* file.

**Debug**
To debug define first this environment variable:
export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n"

**Run embedded tomcat**
mvn clean tomcat7:run

**Run embedded tomcat on different port**
mvn clean tomcat7:run -Dmaven.tomcat.port=8082

# Deployment
Generate war
mvn clean package

War is generated on
target/ad-agent-<version>.war
