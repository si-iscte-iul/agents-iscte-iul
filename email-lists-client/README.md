Email Lists Client
========
Invoke Fenix web service and generate files with the lists and users

## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Configuration**
To configure the application change the *src/main/resources/configuration.properties* file.

**Compile**
mvn compile

**Run a main class**
mvn exec:java -Dexec.mainClass="pt.iscte.dsi.emailList.client.EmailListClient" -Dexec.args="false false"

**Debug**
To debug define first this environment variable:
export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n"

