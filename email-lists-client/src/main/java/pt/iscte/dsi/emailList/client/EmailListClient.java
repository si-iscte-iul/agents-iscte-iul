package pt.iscte.dsi.emailList.client;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.FileUtils;

import pt.iscte.agents.emailLists.jaxws.EmailListContact;
import pt.iscte.agents.emailLists.jaxws.EmailListService;

/**
 * This class is the client for the Fénix distribution email list Web Service.
 * 
 * TODO Logging TODO Send email report
 * 
 * @author António Casqueiro
 */
public class EmailListClient {

    private static final String NL = System.getProperty("line.separator");

    private static final Comparator<ResultData> COMPARATOR_BY_EMAIL = new Comparator<ResultData>() {
        public int compare(ResultData o1, ResultData o2) {
            return o1.getEmail().toLowerCase().compareTo(o2.getEmail().toLowerCase());
        }
    };

    private EmailListService service = null;
    private boolean isToWriteName;
    private boolean isToWriteNameInTwoRows;

    public EmailListClient(boolean isToWriteName, boolean isToWriteNameInTwoRows) {
        this.isToWriteName = isToWriteName;
        this.isToWriteNameInTwoRows = isToWriteNameInTwoRows;
    }

    private void init() throws RemoteException {
        service = EmailListClientUtil.getService();
    }

    public static void main(String[] args) throws IOException {
        int errorCode = 0;
        try {
            if (args.length != 2) {
                // Since somehow the message in the exception is not displayed
                // in the console, force it!
                String msg =
                        "You must supply 2 argument " + "'<isToWriteName> <isToWriteNameInTwoRows>" + NL
                                + "Example: ant && ant -f build_run_iscte_client.xml EmailListClient -Dargs='false false'" + NL
                                + NL;
                System.out.println(msg);
                throw new IllegalArgumentException(msg);
            }
            boolean isToWriteName = Boolean.parseBoolean(args[0]);
            boolean isToWriteNameInTwoRows = Boolean.parseBoolean(args[1]);

            EmailListClient client = new EmailListClient(isToWriteName, isToWriteNameInTwoRows);

            client.init();
            List<String> listsToProcess = client.getListsToProcess();

            final int size = listsToProcess.size();
            int count = 1;
            System.out.println("Lists to process: " + size);
            for (String listToProcess : listsToProcess) {
                System.out.println("Processing (" + count + "," + size + "): " + listToProcess);
                client.process(listToProcess);
                count++;
            }
        } catch (Throwable t) {
            t.printStackTrace();
            errorCode = -1;
            if (t instanceof RuntimeException) {
                RuntimeException re = (RuntimeException) t;
                throw re;
            } else {
                throw new RuntimeException(t);
            }
        } finally {
            System.exit(errorCode);
        }
    }

    private void process(String listToProcess) throws RemoteException, IOException {
        List<EmailListContact> result = service.getContacts(listToProcess);
        List<ResultData> data = buildListFromResult(result);
        sort(data);
        writeToFile(listToProcess, data);
    }

    private List<ResultData> buildListFromResult(List<EmailListContact> result) {
        if (result == null || result.size() == 0) {
            return new ArrayList<ResultData>(0);
        }

        List<ResultData> list = new ArrayList<ResultData>();

        for (EmailListContact elem : result) {
            list.add(new ResultData(elem.getUsedName(), elem.getEmail()));
        }

        return list;
    }

    private void sort(List<ResultData> data) {
        Collections.sort(data, COMPARATOR_BY_EMAIL);
    }

    private void writeToFile(String listToProcess, List<ResultData> data) throws IOException {
        String fileName = getFileName(listToProcess);

        List<String> contacts = new ArrayList<String>();
        for (ResultData elem : data) {
            if (isToWriteName) {
                if (isToWriteNameInTwoRows) {
                    contacts.add("# " + elem.getUsername());
                    contacts.add(elem.getEmail());
                    // for (int i = 1; i <= 3; i++) {
                    // contacts.add("alunos." + i +
                    // elem.getEmail().substring(6));
                    // }
                    contacts.add("");
                } else {
                    contacts.add(elem.getUsername() + " " + elem.getEmail());
                }
            } else {
                contacts.add(elem.getEmail());
            }
        }

        FileUtils.writeLines(new File("distributionLists/" + fileName), contacts);
    }

    private String getFileName(String listToProcess) {
        String name = listToProcess.replace('.', '_');
        name = name.substring(0, name.indexOf("@"));

        return name;
    }

    @SuppressWarnings("unchecked")
    private List<String> getListsToProcess() throws IOException {
        List<String> listsFileContent = FileUtils.readLines(new File("lists.txt"));

        // Remove comments and empty lines
        List<String> listsToProcess = new ArrayList<String>();
        for (String line : listsFileContent) {
            if (isToProcess(line)) {
                listsToProcess.add(line.trim());
            }
        }

        return listsToProcess;
    }

    private boolean isToProcess(String line) {
        if (line.trim().length() == 0) {
            // Skip empty line
            return false;
        }

        if (line.trim().startsWith("#")) {
            // Skip commented line
            return false;
        }

        if (line.indexOf("@iscte.pt") == -1) {
            return false;
        }

        return true;
    }

    public static class ResultData {
        private String username;
        private String email;

        public ResultData(String username, String email) {
            super();
            this.username = username;
            this.email = email;
        }

        public String getUsername() {
            return username;
        }

        public String getEmail() {
            return email;
        }

    }

}