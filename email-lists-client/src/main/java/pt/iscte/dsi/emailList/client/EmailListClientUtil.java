package pt.iscte.dsi.emailList.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import pt.iscte.agents.emailLists.jaxws.EmailListService;
import pt.iscte.agents.emailLists.jaxws.EmailListService_Service;
import pt.iscte.dsi.emailList.client.fenixLike.PropertiesManager;

import com.sun.xml.ws.developer.JAXWSProperties;

public class EmailListClientUtil {

    private static final String WSDL_LOCATION = "email.lists.client.wsdl.location";
    private static final String ADDRESS = "email.lists.client.address";
    private static final String CONNECT_TIMEOUT = "email.lists.client.request.timeout";
    private static final String REQUEST_TIMEOUT = "email.lists.client.connect.timeout";

    private final static Logger logger = Logger.getLogger(EmailListClientUtil.class);
    private static URL wsdlLocation = null;

    public static EmailListService getService() throws RemoteException {
        URL wsdlLocation = getWsdlLocation();
        EmailListService_Service service = new EmailListService_Service(wsdlLocation);
        EmailListService webService = service.getPort(EmailListService.class);
        Map<String, Object> context = ((BindingProvider) webService).getRequestContext();
        String address = getProperty(ADDRESS);
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, address);
        String connectTimeoutStr = getProperty(CONNECT_TIMEOUT);
        Integer connectTimeout = new Integer(connectTimeoutStr) * 1000;
        context.put(JAXWSProperties.CONNECT_TIMEOUT, connectTimeout);
        String requestTimeoutStr = getProperty(REQUEST_TIMEOUT);
        Integer requestTimeout = new Integer(requestTimeoutStr) * 1000;
        context.put(JAXWSProperties.REQUEST_TIMEOUT, requestTimeout);
        return webService;
    }

    private static URL getWsdlLocation() throws RemoteException {
        if (wsdlLocation == null) {
            String wsdlLocationStr = getProperty(WSDL_LOCATION);
            try {
                wsdlLocation = new URL(wsdlLocationStr);
            } catch (MalformedURLException e) {
                logger.error("Wrong wsdl location for email list data service ws client.", e);
                e.printStackTrace();
                throw new RemoteException("Email list service web services is down", e);
            }
        }
        return wsdlLocation;
    }

    private static String getProperty(String propertyName) {
        String propertyValue = PropertiesManager.getProperty(propertyName);
        return propertyValue;
    }

}
