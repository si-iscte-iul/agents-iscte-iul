package pt.iscte.google.apps;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GoogleAccountInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String mainAlias;
    private UserType userType;
    private Boolean gmailBlocked;
    private ArrayList<String> aliases;
    private String orgUnitPath;
    private GoogleAccountForwardInfo forwardInfo;

    public GoogleAccountInfo() {
        super();
        this.aliases = new ArrayList<String>();
    }

    public GoogleAccountInfo(GoogleAccountInfo param, UserType userType) {
        this(param.getUsername(), param.getPassword(), param.getFirstName(), param.getLastName(), param.getMainAlias(), param
                .getGmailBlocked(), userType, null);
    }

    public GoogleAccountInfo(String username, String password, String firstName, String lastName, String mainAlias,
            Boolean gmailBlocked, UserType userType, String orgUnitPath) {
        this();
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mainAlias = mainAlias;
        this.gmailBlocked = gmailBlocked;
        this.userType = userType;
        this.orgUnitPath = orgUnitPath;
    }

    public ArrayList<String> getAliases() {
        return aliases;
    }

    public void addAlias(String alias) {
        this.aliases.add(alias);
    }

    // Required to be the getter be on the generated WS client
    public void setAliases(ArrayList<String> aliases) {
        this.aliases = aliases;
    }

    public void addAlias(List<String> alias) {
        this.aliases.addAll(alias);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMainAlias() {
        return mainAlias;
    }

    public void setMainAlias(String mainAlias) {
        this.mainAlias = mainAlias;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Boolean getGmailBlocked() {
        return gmailBlocked;
    }

    public void setGmailBlocked(Boolean gmailBlocked) {
        this.gmailBlocked = gmailBlocked;
    }

    public String getOrgUnitPath() {
        return orgUnitPath;
    }

    public void setOrgUnitPath(String orgUnitPath) {
        this.orgUnitPath = orgUnitPath;
    }

    public GoogleAccountForwardInfo getForwardInfo() {
        return forwardInfo;
    }

    public void setForwardInfo(GoogleAccountForwardInfo forwardInfo) {
        this.forwardInfo = forwardInfo;
    }

    public void print() {
        System.out.println("FirstName: " + getFirstName());
        System.out.println("LastName: " + getLastName());
        System.out.println("MainAlias: " + getMainAlias());
        System.out.println("Password: " + getPassword());
        System.out.println("Username: " + getUsername());
        System.out.println("GmailBlocked: " + getGmailBlocked());
        System.out.println("UserType: " + getUserType());
        System.out.println("OrgUnitPath: " + getOrgUnitPath());

        StringBuilder sb = new StringBuilder();
        for (String alias : aliases) {
            sb.append(alias).append(" || ");
        }
        System.out.println("Alias: " + sb.toString());

        System.out.println("Forward: " + (forwardInfo != null ? forwardInfo : "None"));
    }

}
