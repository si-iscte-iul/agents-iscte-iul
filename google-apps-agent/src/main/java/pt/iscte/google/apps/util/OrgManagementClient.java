package pt.iscte.google.apps.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import pt.iscte.fenix.util.PropertiesManager;
import pt.iscte.google.apps.UserType;
import pt.iscte.google.apps.util.dto.OrganizationUserDTO;
import pt.iscte.google.apps.util.samples.OrgManagementSampleClient;

import com.google.gdata.client.appsforyourdomain.AppsPropertyService;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * Google Apps organization management client.
 */
public class OrgManagementClient extends OrgManagementSampleClient {

    // private static final String ROOT_UNIT = "/";
    private static final String GMAIL_ENABLED_UNIT = "GmailEnabled";
    private static final String GMAIL_DISABLED_UNIT = "GmailDisabled";

    // private static Logger LOGGER =
    // Logger.getLogger(OrgManagementClient.class);

    // ------------------------------------------------------------------------
    private static AppsPropertyService CACHED_INSTANCE;
    private static long ADQUIRED_TIME;

    private static final long ONE_DAY_MS = 86400000;

    /**
     * Check if it has passed more than 1 day.
     * 
     * @param adquiredTime
     *            time
     * @return true if it has passed more than 1 day
     */
    protected boolean hasExpired(long adquiredTime) {
        long now = System.currentTimeMillis();
        if (now - adquiredTime > ONE_DAY_MS) {
            return true;
        }
        return false;
    }

    private synchronized AppsPropertyService getService() throws AuthenticationException {
        if (hasExpired(ADQUIRED_TIME)) {
            // Recycle
            ADQUIRED_TIME = System.currentTimeMillis();
            CACHED_INSTANCE = new AppsPropertyService(OrgManagementClient.class.getName());
            initService(CACHED_INSTANCE);
        }
        return CACHED_INSTANCE;
    }

    private void initService(AppsPropertyService service) throws AuthenticationException {
        final String adminEmail = PropertiesManager.getProperty("admin.email");
        final String password = PropertiesManager.getProperty("admin.password");
        service.setUserCredentials(adminEmail, password);
        System.out.println("Recycling service OrgManagementClient. " + new Date());
    }

    // ------------------------------------------------------------------------

    public OrgManagementClient() throws AuthenticationException {
        this.domain = PropertiesManager.getProperty("domain");
        service = getService();
    }

    public OrganizationUserDTO retrieveOrganizationUserDTO(String orgUserEmail) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        GenericEntry genericEntry = retrieveCustomerId(domain);
        String customerId = genericEntry.getProperty("customerId");
        return retrieveOrganizationUserDTO(customerId, orgUserEmail);
    }

    public OrganizationUserDTO retrieveOrganizationUserDTO(String customerId, String orgUserEmail)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        final GenericEntry genericEntry = retrieveOrganizationUser(customerId, orgUserEmail);
        final OrganizationUserDTO result = new OrganizationUserDTO(customerId, genericEntry);
        return result;
    }

    public void updateOrganizationUser(String username, String newOrgUnitPath) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        GenericEntry genericEntry = retrieveCustomerId(domain);
        String customerId = genericEntry.getProperty("customerId");
        updateOrganizationUser(customerId, username, null, newOrgUnitPath);
    }

    public static Boolean isGmailBlocked(String orgUnitPath) {
        if (orgUnitPath.endsWith(GMAIL_DISABLED_UNIT)) {
            return Boolean.TRUE;
        }
        if (orgUnitPath.endsWith(GMAIL_ENABLED_UNIT)) {
            return Boolean.FALSE;
        }
        return null;
    }

    public static String buildOrgUnit(UserType userType, Boolean gmailBlocked) {
        return userType.getOrgUnit() + "/" + getGmailOrgUnit(gmailBlocked);
    }

    private static String getGmailOrgUnit(Boolean gmailBlocked) {
        if (Boolean.TRUE.equals(gmailBlocked)) {
            return GMAIL_DISABLED_UNIT;
        }
        if (Boolean.FALSE.equals(gmailBlocked)) {
            return GMAIL_ENABLED_UNIT;
        }
        return null;
    }

}
