package pt.iscte.google.apps.util.dto;

import pt.iscte.google.apps.util.samples.gmail.Constants;

import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;

public class ForwardDTO {

    private String username;
    private boolean enable;
    private String forwardTo;
    private String action;

    public ForwardDTO(String username, GenericEntry genericEntry) {
        this.username = username;
        enable = Boolean.valueOf(genericEntry.getProperty(Constants.ENABLE));
        forwardTo = genericEntry.getProperty(Constants.FORWARD_TO);
        action = genericEntry.getProperty(Constants.ACTION);
    }

    public ForwardDTO(String username, boolean enable, String forwardTo, String action) {
        this.username = username;
        this.enable = enable;
        this.forwardTo = forwardTo;
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public boolean isEnable() {
        return enable;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public String getAction() {
        return action;
    }

}
