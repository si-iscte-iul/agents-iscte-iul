package pt.iscte.google.apps;

import javax.jws.WebService;

/**
 * 
 * @author Radoslaw Chmielarz (raczm@iscte.pt)
 */
@WebService
public interface GoogleAppsAgent {
    public boolean authenticate(java.lang.String userName, java.lang.String password);

    public boolean changePassword(java.lang.String userName, java.lang.String password);

    public boolean isUserInGoogle(java.lang.String userName);

    public boolean synchronizeUser(GoogleAccountInfo data);

    public boolean synchronizeSendAs(GoogleAccountInfo data);

    public GoogleAccountInfo getUserInfo(java.lang.String userName);

    public boolean disableAccount(java.lang.String userName);

    public boolean enableAccount(java.lang.String userName);

    public boolean deleteAccount(java.lang.String userName);
}
