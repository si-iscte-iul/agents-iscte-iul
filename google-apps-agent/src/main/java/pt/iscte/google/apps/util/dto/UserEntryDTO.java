package pt.iscte.google.apps.util.dto;

import java.util.HashMap;
import java.util.Map;

import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;

public class UserEntryDTO {

    private static final String USER_EMAIL = "userEmail";
    private static final String PASSWORD = "password";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String SUSPENDED = "isSuspended";

    private String userEmail;
    private String password;
    private String firstName;
    private String lastName;
    private boolean isSuspended;

    public UserEntryDTO(String firstName, String lastName) {
        setFirstName(firstName);
        setLastName(lastName);
    }

    public UserEntryDTO(GenericEntry genericEntry) {
        userEmail = genericEntry.getProperty(USER_EMAIL);
        password = genericEntry.getProperty(PASSWORD);
        firstName = genericEntry.getProperty(FIRST_NAME);
        lastName = genericEntry.getProperty(LAST_NAME);
        setSuspended(Boolean.TRUE.equals(genericEntry.getProperty(SUSPENDED)));
    }

    public Map<String, String> getMap() {
        Map<String, String> map = new HashMap<String, String>();

        map.put(USER_EMAIL, getUserEmail());
        map.put(PASSWORD, getPassword());
        map.put(FIRST_NAME, getFirstName());
        map.put(LAST_NAME, getLastName());
        map.put(SUSPENDED, String.valueOf(isSuspended()));

        return map;
    }

    public Map<String, String> getMapUpdatePersonalData() {
        Map<String, String> map = new HashMap<String, String>();

        map.put(FIRST_NAME, getFirstName());
        map.put(LAST_NAME, getLastName());

        return map;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSuspended(boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public boolean isSuspended() {
        return isSuspended;
    }

}
