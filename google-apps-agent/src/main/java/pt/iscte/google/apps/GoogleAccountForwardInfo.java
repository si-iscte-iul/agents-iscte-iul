package pt.iscte.google.apps;

import java.io.Serializable;

import pt.iscte.google.apps.util.dto.ForwardDTO;

public class GoogleAccountForwardInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private boolean enable;
    private String forwardTo;
    private String action;

    public GoogleAccountForwardInfo() {
        super();
    }

    public GoogleAccountForwardInfo(ForwardDTO forwardInfo) {
        super();
        this.enable = forwardInfo.isEnable();
        this.forwardTo = forwardInfo.getForwardTo();
        this.action = forwardInfo.getAction();
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getForwardTo() {
        return forwardTo;
    }

    public void setForwardTo(String forwardTo) {
        this.forwardTo = forwardTo;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return enable + " | " + forwardTo + " | " + action;
    }

}
