package pt.iscte.google.apps;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import pt.iscte.fenix.util.HostAccessControl;
import pt.iscte.google.apps.util.GmailSettingsClient;
import pt.iscte.google.apps.util.OrgManagementClient;
import pt.iscte.google.apps.util.ProvisioningApiMultiDomainClient;
import pt.iscte.google.apps.util.dto.ForwardDTO;
import pt.iscte.google.apps.util.dto.NicknameEntryDTO;
import pt.iscte.google.apps.util.dto.OrganizationUserDTO;
import pt.iscte.google.apps.util.dto.SendAsDTO;
import pt.iscte.google.apps.util.dto.UserEntryDTO;
import pt.iscte.google.apps.util.samples.gmail.ForwardingAction;

import com.google.gdata.data.appsforyourdomain.AppsForYourDomainErrorCode;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

@WebService(name = "googleappsagent")
public class GoogleAppsAgentBean implements GoogleAppsAgent {

    private final Logger LOGGER;

    @Resource
    private WebServiceContext ctx;

    /**
     * Checks if the client is allowed to access this service
     */
    private void validateClientAccess() {
        MessageContext messageContext = ctx.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);
        if (!HostAccessControl.isAllowed(GoogleAppsAgentBean.class, request)) {
            LOGGER.error("Unknown host: " + HostAccessControl.getRemoteAddress(request) + " tried to access this service");
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
            throw new RuntimeException(HostAccessControl.getRemoteAddress(request) + " is not allowed to access this service");
        }
    }

    public GoogleAppsAgentBean() throws Exception {
        LOGGER = Logger.getLogger(GoogleAppsAgentBean.class);
    }

    /**
     * Logs the user into his or her Google Apps account.
     * 
     * WARNING: this method may fail if the user tries to log in with wrong
     * credentials more than a few times. Then Google will present us with
     * CAPTCHAs (security picture with letters which you have to copy down in
     * order to try to log in a second time) and they can't be handled
     * programmatically.
     * 
     * @param userName
     *            User name (user\@domain.country)
     * @param password
     *            User password.
     * @return A boolean indicating if the user has successfully logged in.
     */
    @WebMethod
    public boolean authenticate(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            return service.checkIfUserCanLogin(userName, password);
        } catch (AuthenticationException e) {
            LOGGER.error("Couldn't login in Google API as user '" + userName + "': " + e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    /**
     * Changes the password for user in Google Apps.
     * 
     * @param userName
     *            User name (user\@domain.country)
     * @param password
     *            User password.
     * @return A boolean indicating if changing of the password was successful.
     */
    @WebMethod
    public boolean changePassword(@WebParam(name = "userName") String userName, @WebParam(name = "password") String password) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            service.changePassword(userName, password);

            return true;
        } catch (Exception e) {
            LOGGER.error("Error in changePassword for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    /**
     * Checks if the user has a Google Apps account.
     * 
     * @param userName
     *            User name (user\@domain.country)
     * @return A boolean indicating if the user has a Google Apps account.
     */
    @WebMethod
    public boolean isUserInGoogle(@WebParam(name = "userName") String userName) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            service.retrieveUser(userName);

            return true;
        } catch (AppsForYourDomainException e) {
            if (e.getErrorCode().compareTo(AppsForYourDomainErrorCode.EntityDoesNotExist) == 0
                    || e.getErrorCode().compareTo(AppsForYourDomainErrorCode.EntityNameNotValid) == 0) {
                LOGGER.debug("User '" + userName + "' doesn't have a Google account: " + e);
            } else {
                LOGGER.error("Error in isUserInGoogle for user: " + userName, e);
                // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
            }
        } catch (Exception e) {
            LOGGER.error("Error in isUserInGoogle for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }
        return false;
    }

    /**
     * Creates/update a user in Google Apps.
     * 
     * @param data
     * @return true if the user was successfully created, false otherwise.
     */
    @WebMethod
    public boolean synchronizeUser(@WebParam(name = "data") GoogleAccountInfo data) {
        validateClientAccess();

        try {
            performSynchronization(data);

            // if there are no exceptions then we assume that the user was
            // created/updated successfully.
            return true;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error in synchronizeUser for user: " + data.getUsername(), e);
            throw e;
        } catch (Exception e) {
            LOGGER.error("Error in synchronizeUser for user: " + data.getUsername(), e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    // ------------------------------------------------------------------------

    private void performSynchronization(GoogleAccountInfo param) throws AuthenticationException, AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        if (param.getUserType() != null) {
            throw new IllegalArgumentException("UserType cannot have data.");
        }
        if (param.getOrgUnitPath() != null) {
            throw new IllegalArgumentException("OrgUnitPath cannot have data.");
        }
        final GoogleAccountInfo data = new GoogleAccountInfo(param, UserType.fromUsername(param.getUsername()));

        ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

        UserEntryDTO userEntry = service.findUser(data.getUsername());
        if (userEntry != null) {
            // Update existing user
            renameUserIfNeeded(service, userEntry, data);
            updatePersonalDataIfNeeded(service, userEntry, data);
            syncAlias(service, userEntry, data);
            syncOrgUnit(userEntry, data);
            syncForward(param, true);
            syncSendAs(param, true);
        } else {
            // Create new user
            service.createUser(data.getUsername(), data.getPassword(), data.getFirstName(), data.getLastName());
            createAlias(service, userEntry, data);
            createOrgUnit(userEntry, data);
            syncForward(param, false);
            syncSendAs(param, false);
        }
    }

    private void syncForward(GoogleAccountInfo param, boolean isUpdate) throws IllegalArgumentException, MalformedURLException,
            ServiceException, IOException {
        final String username = param.getUsername();
        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();

        if (param.getGmailBlocked()) {
            // Forward to "@iul.pt"
            String forwardTo = username.split("@")[0] + "@iul.pt";

            ForwardDTO forwardDTO = new ForwardDTO(username, true, forwardTo, ForwardingAction.DELETE.name());
            serviceGmailSettings.changeForwardingDTO(forwardDTO);
        } else {
            if (isUpdate) {
                // Remove forward to "@iul.pt" if active
                ForwardDTO forwardResultDTO = serviceGmailSettings.retrieveForwardingDTO(username);
                if (forwardResultDTO != null && forwardResultDTO.getForwardTo().endsWith("@iul.pt")) {
                    ForwardDTO forwardParamDTO = new ForwardDTO(username, false, null, null);
                    serviceGmailSettings.changeForwardingDTO(forwardParamDTO);
                }
            }
        }
    }

    private void syncSendAs(GoogleAccountInfo param, boolean isUpdate) throws IllegalArgumentException, MalformedURLException,
            ServiceException, IOException {
        final String username = param.getUsername();
        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();

        if (isUpdate) {
            String currentSendAsAddress = null;

            List<SendAsDTO> result = serviceGmailSettings.retrieveSendAsDTO(username);
            if (result != null) {
                for (SendAsDTO sendAsDTO : result) {
                    if (sendAsDTO.isDefault()) {
                        currentSendAsAddress = sendAsDTO.getAddress();
                    }
                }
            }

            if (currentSendAsAddress != null) {
                if (isExternalEmail(currentSendAsAddress) || currentSendAsAddress.equalsIgnoreCase(param.getMainAlias())) {
                    // External email or unchanged "send as" email address
                    return;
                }
            }
        }

        if (!username.equalsIgnoreCase(param.getMainAlias())) {
            SendAsDTO input =
                    new SendAsDTO(username, param.getFirstName() + " " + param.getLastName(), param.getMainAlias(), "", true,
                            true);
            serviceGmailSettings.createSendAsDTO(input);
        }
    }

    private static boolean isExternalEmail(String email) {
        if (email.endsWith("@iscte.pt") || email.endsWith("@iscte-iul.pt")) {
            return false;
        }
        return true;
    }

    private void createOrgUnit(UserEntryDTO userEntry, GoogleAccountInfo data) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        String newOrgUnitPath = OrgManagementClient.buildOrgUnit(data.getUserType(), data.getGmailBlocked());
        serviceOrgManagement.updateOrganizationUser(data.getUsername(), newOrgUnitPath);
    }

    private void updatePersonalDataIfNeeded(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry,
            GoogleAccountInfo data) throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        // Check if it needs to be updated
        if (!userEntry.getFirstName().equals(data.getFirstName()) || !userEntry.getLastName().equals(data.getLastName())) {
            // Update it
            UserEntryDTO newData = new UserEntryDTO(data.getFirstName(), data.getLastName());
            service.updateUser(data.getUsername(), newData.getMapUpdatePersonalData());
        }
    }

    private void renameUserIfNeeded(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        if (!userEntry.getUserEmail().equals(data.getUsername())) {
            deleteAllAlias(service, userEntry.getUserEmail());
            service.updateEmailAddress(userEntry.getUserEmail(), data.getUsername());
        }
    }

    private void deleteAllAlias(ProvisioningApiMultiDomainClient service, String userEmail) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        List<NicknameEntryDTO> nicknameEntryList = service.retrieveAllUserAliasesDTO(userEmail);
        for (NicknameEntryDTO nicknameEntryDTO : nicknameEntryList) {
            service.deleteAlias(nicknameEntryDTO.getAliasEmail());
        }
    }

    private void syncAlias(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        Set<String> correctAlias = getCorrectAlias(data);

        List<NicknameEntryDTO> nicknameEntryList = service.retrieveAllUserAliasesDTO(data.getUsername());
        List<String> allAlias = new ArrayList<String>();
        for (NicknameEntryDTO nicknameEntry : nicknameEntryList) {
            allAlias.add(nicknameEntry.getAliasEmail());
        }

        // Remove invalid alias
        List<String> invalidAliass = detectInvalidAlias(allAlias, correctAlias, data);
        for (String alias : invalidAliass) {
            service.deleteAlias(alias);
        }

        // Add needed alias
        List<String> newAliass = detectNewAlias(allAlias, correctAlias, data);
        for (String alias : newAliass) {
            service.createAlias(alias, data.getUsername());
        }
    }

    private void createAlias(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        Set<String> correctAlias = getCorrectAlias(data);
        for (String alias : correctAlias) {
            service.createAlias(alias, data.getUsername());
        }
    }

    private Set<String> getCorrectAlias(GoogleAccountInfo data) {
        String username = data.getUsername();
        String usernameNoDomain = username.split("@")[0];

        String mainAlias = data.getMainAlias();
        String mainAliasNoDomain = mainAlias.split("@")[0];

        Set<String> correctAlias = new HashSet<String>();
        correctAlias.add(usernameNoDomain + "@iscte.pt");
        correctAlias.add(mainAliasNoDomain + "@iscte.pt");

        // Always add the iscte-iul.pt alias to ease the share.
        // This way the share does not fail even if the
        // user typed the wrong domain.
        correctAlias.add(usernameNoDomain + "@" + UserType.STUDENT.getDomain());
        correctAlias.add(mainAliasNoDomain + "@" + UserType.STUDENT.getDomain());

        correctAlias.remove(username);
        return correctAlias;
    }

    private List<String> detectNewAlias(List<String> allAlias, Set<String> correctAlias, GoogleAccountInfo data) {
        List<String> newAlias = new ArrayList<String>();
        for (String alias : correctAlias) {
            if (!allAlias.contains(alias)) {
                newAlias.add(alias);
            }
        }

        return newAlias;
    }

    private List<String> detectInvalidAlias(List<String> allAlias, Set<String> correctAlias, GoogleAccountInfo data) {
        List<String> toDeleteAlias = new ArrayList<String>();
        for (String alias : allAlias) {
            if (!correctAlias.contains(alias)) {
                toDeleteAlias.add(alias);
            }
        }

        return toDeleteAlias;
    }

    private void syncOrgUnit(UserEntryDTO userEntry, GoogleAccountInfo data) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        OrganizationUserDTO organizationUser = serviceOrgManagement.retrieveOrganizationUserDTO(data.getUsername());
        String currentOrgUnitPath = organizationUser.getOrgUnitPath();
        String newOrgUnitPath = OrgManagementClient.buildOrgUnit(data.getUserType(), data.getGmailBlocked());
        if (!currentOrgUnitPath.equals(newOrgUnitPath)) {
            serviceOrgManagement.updateOrganizationUser(data.getUsername(), newOrgUnitPath);
        }
    }

    private GoogleAccountInfo getUserDetail(String userName) throws AuthenticationException, AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        ProvisioningApiMultiDomainClient serviceProvisioning = new ProvisioningApiMultiDomainClient();
        UserEntryDTO userEntry = serviceProvisioning.retrieveUserDTO(userName);

        List<NicknameEntryDTO> nicknameEntrys = serviceProvisioning.retrieveAllUserAliasesDTO(userName);
        List<String> alias = new ArrayList<String>();
        for (NicknameEntryDTO nicknameEntry : nicknameEntrys) {
            alias.add(nicknameEntry.getAliasEmail());
        }

        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        OrganizationUserDTO organizationUser = serviceOrgManagement.retrieveOrganizationUserDTO(userName);
        String orgUnitPath = organizationUser.getOrgUnitPath();

        UserType userType = UserType.fromOrgUnit(orgUnitPath);
        Boolean gmailBlocked = OrgManagementClient.isGmailBlocked(organizationUser.getOrgUnitPath());

        GoogleAccountInfo result =
                new GoogleAccountInfo(userName, null, userEntry.getFirstName(), userEntry.getLastName(), null, gmailBlocked,
                        userType, orgUnitPath);
        result.addAlias(alias);

        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();
        ForwardDTO forwardInfo = serviceGmailSettings.retrieveForwardingDTO(userName);
        result.setForwardInfo(new GoogleAccountForwardInfo(forwardInfo));

        return result;
    }

    // ------------------------------------------------------------------------

    /**
     * Get an user google account information.
     * 
     * @param userName
     *            (user\@domain.country)
     * @return user data.
     */
    @WebMethod
    public GoogleAccountInfo getUserInfo(@WebParam(name = "userName") String userName) {

        try {
            LOGGER.info("getUserInfo userName: " + userName);
            GoogleAccountInfo result = getUserDetail(userName);

            return result;

        } catch (AppsForYourDomainException e) {
            if (e.getErrorCode().compareTo(AppsForYourDomainErrorCode.EntityDoesNotExist) == 0
                    || e.getErrorCode().compareTo(AppsForYourDomainErrorCode.EntityNameNotValid) == 0) {
                LOGGER.info("User '" + userName + "' doesn't have a Google account: " + e.getMessage());
            } else {
                LOGGER.error("Error in getUserInfo for user: " + userName, e);
            }

        } catch (Exception e) {
            LOGGER.error("Error in getUserInfo for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return null;
    }

    /**
     * Disables a user account.
     * 
     * @param userName
     *            (user\@domain.country)
     * @return true if the account was successfully disabled, false otherwise.
     */
    @WebMethod
    public boolean disableAccount(@WebParam(name = "userName") String userName) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            service.suspendUser(userName);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in disableAccount for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    /**
     * Enables a user account.
     * 
     * @param userName
     *            (user\@domain.country)
     * @return true if the account was successfully enabled, false otherwise.
     */
    @WebMethod
    public boolean enableAccount(@WebParam(name = "userName") String userName) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            service.restoreUser(userName);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in enableAccount for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    /**
     * Deletes a user account. You have to wait for 5 days before you can use
     * the user name once more.
     * 
     * @param userName
     *            (user\@domain.country)
     * @return true if the account was successfully disabled, false otherwise.
     */
    @WebMethod
    public boolean deleteAccount(@WebParam(name = "userName") String userName) {
        validateClientAccess();

        try {
            ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

            service.deleteUser(userName);
            // if there are no exceptions then we assume that the user was
            // created
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in deleteAccount for user: " + userName, e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }

    /**
     * Synchronizes the "send as" email address.
     * 
     * @param data
     * @return true if the synchronization was successfully, false otherwise.
     */
    @WebMethod
    public boolean synchronizeSendAs(@WebParam(name = "data") GoogleAccountInfo data) {
        validateClientAccess();

        try {
            syncSendAs(data, true);
            return true;
        } catch (Exception e) {
            LOGGER.error("Error in synchronizeSendAs for user: " + data.getUsername(), e);
            // LOGGER.error(LogTriggeringEvent.EMAILSENDTRIGGER);
        }

        return false;
    }
}
