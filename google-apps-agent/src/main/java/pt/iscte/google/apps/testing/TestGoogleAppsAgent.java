package pt.iscte.google.apps.testing;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import pt.iscte.google.apps.GoogleAccountForwardInfo;
import pt.iscte.google.apps.GoogleAccountInfo;
import pt.iscte.google.apps.UserType;
import pt.iscte.google.apps.util.GmailSettingsClient;
import pt.iscte.google.apps.util.OrgManagementClient;
import pt.iscte.google.apps.util.ProvisioningApiMultiDomainClient;
import pt.iscte.google.apps.util.dto.ForwardDTO;
import pt.iscte.google.apps.util.dto.NicknameEntryDTO;
import pt.iscte.google.apps.util.dto.OrganizationUserDTO;
import pt.iscte.google.apps.util.dto.SendAsDTO;
import pt.iscte.google.apps.util.dto.UserEntryDTO;
import pt.iscte.google.apps.util.samples.gmail.ForwardingAction;

import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class TestGoogleAppsAgent {

    private final Logger LOGGER;

    public TestGoogleAppsAgent() throws Exception {
        LOGGER = Logger.getLogger(TestGoogleAppsAgent.class);
    }

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    private void performSynchronization(GoogleAccountInfo param) throws AuthenticationException, AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        if (param.getUserType() != null) {
            throw new IllegalArgumentException("UserType cannot have data.");
        }
        if (param.getOrgUnitPath() != null) {
            throw new IllegalArgumentException("OrgUnitPath cannot have data.");
        }
        final GoogleAccountInfo data = new GoogleAccountInfo(param, UserType.fromUsername(param.getUsername()));

        ProvisioningApiMultiDomainClient service = new ProvisioningApiMultiDomainClient();

        UserEntryDTO userEntry = service.findUser(data.getUsername());
        if (userEntry != null) {
            // Update existing user
            renameUserIfNeeded(service, userEntry, data);
            updatePersonalDataIfNeeded(service, userEntry, data);
            syncAlias(service, userEntry, data);
            syncOrgUnit(userEntry, data);
            syncForward(param, true);
            syncSendAs(param, true);
        } else {
            // Create new user
            service.createUser(data.getUsername(), data.getPassword(), data.getFirstName(), data.getLastName());
            createAlias(service, userEntry, data);
            createOrgUnit(userEntry, data);
            syncForward(param, false);
            syncSendAs(param, false);
        }
    }

    private void syncForward(GoogleAccountInfo param, boolean isUpdate) throws IllegalArgumentException, MalformedURLException,
            ServiceException, IOException {
        final String username = param.getUsername();
        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();

        if (param.getGmailBlocked()) {
            // Forward to "@iul.pt"
            String forwardTo = username.split("@")[0] + "@iul.pt";

            ForwardDTO forwardDTO = new ForwardDTO(username, true, forwardTo, ForwardingAction.DELETE.name());
            serviceGmailSettings.changeForwardingDTO(forwardDTO);
        } else {
            if (isUpdate) {
                // Remove forward to "@iul.pt" if active
                ForwardDTO forwardResultDTO = serviceGmailSettings.retrieveForwardingDTO(username);
                if (forwardResultDTO != null && forwardResultDTO.getForwardTo().endsWith("@iul.pt")) {
                    ForwardDTO forwardParamDTO = new ForwardDTO(username, false, null, null);
                    serviceGmailSettings.changeForwardingDTO(forwardParamDTO);
                }
            }
        }
    }

    private void syncSendAs(GoogleAccountInfo param, boolean isUpdate) throws IllegalArgumentException, MalformedURLException,
            ServiceException, IOException {
        final String username = param.getUsername();
        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();

        if (isUpdate) {
            String currentSendAsAddress = null;

            List<SendAsDTO> result = serviceGmailSettings.retrieveSendAsDTO(username);
            if (result != null) {
                for (SendAsDTO sendAsDTO : result) {
                    if (sendAsDTO.isDefault()) {
                        currentSendAsAddress = sendAsDTO.getAddress();
                    }
                }
            }

            if (currentSendAsAddress != null) {
                if (isExternalEmail(currentSendAsAddress) || currentSendAsAddress.equalsIgnoreCase(param.getMainAlias())) {
                    // External email or unchanged "send as" email address
                    return;
                }
            }
        }

        if (!username.equalsIgnoreCase(param.getMainAlias())) {
            SendAsDTO input =
                    new SendAsDTO(username, param.getFirstName() + " " + param.getLastName(), param.getMainAlias(), "", true,
                            true);
            serviceGmailSettings.createSendAsDTO(input);
        }
    }

    private static boolean isExternalEmail(String email) {
        if (email.endsWith("@iscte.pt") || email.endsWith("@iscte-iul.pt")) {
            return false;
        }
        return true;
    }

    private void createOrgUnit(UserEntryDTO userEntry, GoogleAccountInfo data) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        String newOrgUnitPath = OrgManagementClient.buildOrgUnit(data.getUserType(), data.getGmailBlocked());
        serviceOrgManagement.updateOrganizationUser(data.getUsername(), newOrgUnitPath);
    }

    private void updatePersonalDataIfNeeded(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry,
            GoogleAccountInfo data) throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        // Check if it needs to be updated
        if (!userEntry.getFirstName().equals(data.getFirstName()) || !userEntry.getLastName().equals(data.getLastName())) {
            // Update it
            service.updateUser(data.getUsername(), userEntry.getMapUpdatePersonalData());
        }
    }

    private void renameUserIfNeeded(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        if (!userEntry.getUserEmail().equals(data.getUsername())) {
            deleteAllAlias(service, userEntry.getUserEmail());
            service.updateEmailAddress(userEntry.getUserEmail(), data.getUsername());
        }
    }

    private void deleteAllAlias(ProvisioningApiMultiDomainClient service, String userEmail) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        List<NicknameEntryDTO> nicknameEntryList = service.retrieveAllUserAliasesDTO(userEmail);
        for (NicknameEntryDTO nicknameEntryDTO : nicknameEntryList) {
            service.deleteAlias(nicknameEntryDTO.getAliasEmail());
        }
    }

    private void syncAlias(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        Set<String> correctAlias = getCorrectAlias(data);

        List<NicknameEntryDTO> nicknameEntryList = service.retrieveAllUserAliasesDTO(data.getUsername());
        List<String> allAlias = new ArrayList<String>();
        for (NicknameEntryDTO nicknameEntry : nicknameEntryList) {
            allAlias.add(nicknameEntry.getAliasEmail());
        }

        // Remove invalid alias
        List<String> invalidAliass = detectInvalidAlias(allAlias, correctAlias, data);
        for (String alias : invalidAliass) {
            service.deleteAlias(alias);
        }

        // Add needed alias
        List<String> newAliass = detectNewAlias(allAlias, correctAlias, data);
        for (String alias : newAliass) {
            service.createAlias(alias, data.getUsername());
        }
    }

    private void createAlias(ProvisioningApiMultiDomainClient service, UserEntryDTO userEntry, GoogleAccountInfo data)
            throws AppsForYourDomainException, MalformedURLException, IOException, ServiceException {
        Set<String> correctAlias = getCorrectAlias(data);
        for (String alias : correctAlias) {
            service.createAlias(alias, data.getUsername());
        }
    }

    private Set<String> getCorrectAlias(GoogleAccountInfo data) {
        String username = data.getUsername();
        String usernameNoDomain = username.split("@")[0];

        String mainAlias = data.getMainAlias();
        String mainAliasNoDomain = mainAlias.split("@")[0];

        Set<String> correctAlias = new HashSet<String>();
        correctAlias.add(usernameNoDomain + "@iscte.pt");
        correctAlias.add(mainAliasNoDomain + "@iscte.pt");

        // Always add the iscte-iul.pt alias to ease the share.
        // This way the share does not fail even if the
        // user typed the wrong domain.
        correctAlias.add(usernameNoDomain + "@" + UserType.STUDENT.getDomain());
        correctAlias.add(mainAliasNoDomain + "@" + UserType.STUDENT.getDomain());

        correctAlias.remove(username);
        return correctAlias;
    }

    private List<String> detectNewAlias(List<String> allAlias, Set<String> correctAlias, GoogleAccountInfo data) {
        List<String> newAlias = new ArrayList<String>();
        for (String alias : correctAlias) {
            if (!allAlias.contains(alias)) {
                newAlias.add(alias);
            }
        }

        return newAlias;
    }

    private List<String> detectInvalidAlias(List<String> allAlias, Set<String> correctAlias, GoogleAccountInfo data) {
        List<String> toDeleteAlias = new ArrayList<String>();
        for (String alias : allAlias) {
            if (!correctAlias.contains(alias)) {
                toDeleteAlias.add(alias);
            }
        }

        return toDeleteAlias;
    }

    private void syncOrgUnit(UserEntryDTO userEntry, GoogleAccountInfo data) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        OrganizationUserDTO organizationUser = serviceOrgManagement.retrieveOrganizationUserDTO(data.getUsername());
        String currentOrgUnitPath = organizationUser.getOrgUnitPath();
        String newOrgUnitPath = OrgManagementClient.buildOrgUnit(data.getUserType(), data.getGmailBlocked());
        if (!currentOrgUnitPath.equals(newOrgUnitPath)) {
            serviceOrgManagement.updateOrganizationUser(data.getUsername(), newOrgUnitPath);
        }
    }

    private GoogleAccountInfo getUserDetail(String userName) throws AuthenticationException, AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        ProvisioningApiMultiDomainClient serviceProvisioning = new ProvisioningApiMultiDomainClient();
        UserEntryDTO userEntry = serviceProvisioning.retrieveUserDTO(userName);

        List<NicknameEntryDTO> nicknameEntrys = serviceProvisioning.retrieveAllUserAliasesDTO(userName);
        List<String> alias = new ArrayList<String>();
        for (NicknameEntryDTO nicknameEntry : nicknameEntrys) {
            alias.add(nicknameEntry.getAliasEmail());
        }

        OrgManagementClient serviceOrgManagement = new OrgManagementClient();
        OrganizationUserDTO organizationUser = serviceOrgManagement.retrieveOrganizationUserDTO(userName);
        String orgUnitPath = organizationUser.getOrgUnitPath();

        UserType userType = UserType.fromOrgUnit(orgUnitPath);
        Boolean gmailBlocked = OrgManagementClient.isGmailBlocked(organizationUser.getOrgUnitPath());

        GoogleAccountInfo result =
                new GoogleAccountInfo(userName, null, userEntry.getFirstName(), userEntry.getLastName(), null, gmailBlocked,
                        userType, orgUnitPath);
        result.addAlias(alias);

        GmailSettingsClient serviceGmailSettings = GmailSettingsClient.getService();
        ForwardDTO forwardInfo = serviceGmailSettings.retrieveForwardingDTO(userName);
        result.setForwardInfo(new GoogleAccountForwardInfo(forwardInfo));

        return result;
    }

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    public static void main(String[] args) throws Exception {
        TestGoogleAppsAgent agent = new TestGoogleAppsAgent();

        try {
            String atDomain = "@iscte-iul.pt";
            String username = "teste-103" + atDomain;
            String password = "0xxxxXXXXzzzz1";
            String firstName = "Joaquim";
            String lastName = "Henriques";

            String mainAlias = "Joaquim_HenriquesZZ103@iscte.pt";
//	    String mainAlias = username;
            Boolean gmailBlocked = Boolean.FALSE;
            UserType userType = null;
            String orgUnitPath = null;

            GoogleAccountInfo data =
                    new GoogleAccountInfo(username, password, firstName, lastName, mainAlias, gmailBlocked, userType, orgUnitPath);

            agent.performSynchronization(data);

            GoogleAccountInfo result = agent.getUserDetail(data.getUsername());
            result.print();

            // GmailSettingsClient serviceGmailSettings =
            // GmailSettingsClient.getService();
            // {
            // List<SendAsDTO> result =
            // serviceGmailSettings.retrieveSendAsDTO("ajsco@iscte.pt");
            // print(result);
            // }
            // {
            // SendAsDTO input = new SendAsDTO("ajsco@iscte.pt", "YAHOO",
            // "acasq_pub@yahoo.co.uk", "ajsco@iscte.pt", true, true);
            // SendAsDTO input = new SendAsDTO("ajsco@iscte.pt", "A2",
            // "ajsco@iscte.pt", "ajsco@iscte.pt", true, true);
            // SendAsDTO input = new SendAsDTO("ajsco@iscte.pt", "A3",
            // "Antonio.Casqueiro@iscte.pt", "ajsco@iscte.pt", true, false);
            // SendAsDTO input = new SendAsDTO("ajsco@iscte.pt", "A4",
            // "Antonio.Casqueiro@iscte.pt", "", true, false);
            // serviceGmailSettings.createSendAsDTO(input);
            // }
            // {
            // List<SendAsDTO> result =
            // serviceGmailSettings.retrieveSendAsDTO("ajsco@iscte.pt");
            // print(result);
            // }
            // {
            // List<SendAsDTO> result =
            // serviceGmailSettings.retrieveSendAsDTO("a26000@iscte.pt");
            // print(result);
            // }
            // {
            // List<SendAsDTO> result =
            // serviceGmailSettings.retrieveSendAsDTO("a26000@iscte-iul.pt");
            // print(result);
            // }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void print(List<SendAsDTO> result) {
        if (result != null) {
            for (SendAsDTO sendAsDTO : result) {
                System.out.println(sendAsDTO);
            }
        } else {
            System.out.println("No result");
        }
    }

}
