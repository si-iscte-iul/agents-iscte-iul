package pt.iscte.google.apps.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.iscte.fenix.util.PropertiesManager;
import pt.iscte.google.apps.util.dto.NicknameEntryDTO;
import pt.iscte.google.apps.util.dto.UserEntryDTO;
import pt.iscte.google.apps.util.samples.ProvisioningApiMultiDomainSampleClient;

import com.google.gdata.client.appsforyourdomain.AppsPropertyService;
import com.google.gdata.client.appsforyourdomain.UserService;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainErrorCode;
import com.google.gdata.data.appsforyourdomain.AppsForYourDomainException;
import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * Google Apps provisioning client.
 */
public class ProvisioningApiMultiDomainClient extends ProvisioningApiMultiDomainSampleClient {

    private static final List<String> DOMAIN_TO_CHECK = Arrays.asList("iscte.pt", "iscte-iul.pt", "iul.pt");

    // ------------------------------------------------------------------------
    private static AppsPropertyService CACHED_INSTANCE;
    private static long ADQUIRED_TIME;

    private static final long ONE_DAY_MS = 86400000;

    /**
     * Check if it has passed more than 1 day.
     * 
     * @param adquiredTime
     *            time
     * @return true if it has passed more than 1 day
     */
    protected boolean hasExpired(long adquiredTime) {
        long now = System.currentTimeMillis();
        if (now - adquiredTime > ONE_DAY_MS) {
            return true;
        }
        return false;
    }

    private synchronized AppsPropertyService getService() throws AuthenticationException {
        if (hasExpired(ADQUIRED_TIME)) {
            // Recycle
            ADQUIRED_TIME = System.currentTimeMillis();
            CACHED_INSTANCE = new AppsPropertyService(ProvisioningApiMultiDomainClient.class.getName());
            initService(CACHED_INSTANCE);
        }
        return CACHED_INSTANCE;
    }

    private void initService(AppsPropertyService service) throws AuthenticationException {
        final String adminEmail = PropertiesManager.getProperty("admin.email");
        final String password = PropertiesManager.getProperty("admin.password");
        service.setUserCredentials(adminEmail, password);
        System.out.println("Recycling service ProvisioningApiMultiDomainClient. " + new Date());
    }

    // ------------------------------------------------------------------------

    public ProvisioningApiMultiDomainClient() throws AuthenticationException {
        this.domain = PropertiesManager.getProperty("domain");
        service = getService();
    }

    public List<NicknameEntryDTO> retrieveAllUserAliasesDTO(String userEmail) throws AppsForYourDomainException,
            MalformedURLException, IOException, ServiceException {
        List<GenericEntry> genericEntries = super.retrieveAllUserAliases(userEmail);

        List<NicknameEntryDTO> result = new ArrayList<NicknameEntryDTO>();
        for (GenericEntry genericEntry : genericEntries) {
            result.add(new NicknameEntryDTO(genericEntry));
        }

        return result;
    }

    public UserEntryDTO retrieveUserDTO(String email) throws AppsForYourDomainException, MalformedURLException, IOException,
            ServiceException {
        GenericEntry genericEntry = super.retrieveUser(email);

        UserEntryDTO userEntryDTO = new UserEntryDTO(genericEntry);
        return userEntryDTO;
    }

    public UserEntryDTO findUser(String username) throws MalformedURLException, IOException, ServiceException {
        try {
            return retrieveUserDTO(username);
        } catch (AppsForYourDomainException e) {
            if (AppsForYourDomainErrorCode.EntityDoesNotExist != e.getErrorCode()
                    && AppsForYourDomainErrorCode.EntityNameNotValid != e.getErrorCode()) {
                throw e;
            }
        }

        String userNoDomain = username.split("@")[0];
        String usedDomain = username.split("@")[1];
        List<String> domainsToCheck = new ArrayList<String>(DOMAIN_TO_CHECK);
        domainsToCheck.remove(usedDomain);

        for (String domain : domainsToCheck) {
            try {
                return retrieveUserDTO(userNoDomain + "@" + domain);
            } catch (AppsForYourDomainException e) {
                if (AppsForYourDomainErrorCode.EntityDoesNotExist != e.getErrorCode()
                        && AppsForYourDomainErrorCode.EntityNameNotValid != e.getErrorCode()) {
                    throw e;
                }
            }
        }

        return null;
    }

    public void suspendUser(String userName) throws AppsForYourDomainException, MalformedURLException, IOException,
            ServiceException {
        Map<String, String> updatedAttributes = new HashMap<String, String>();
        updatedAttributes.put("isSuspended", "true");
        super.updateUser(userName, updatedAttributes);
    }

    public void restoreUser(String userName) throws AppsForYourDomainException, MalformedURLException, IOException,
            ServiceException {
        Map<String, String> updatedAttributes = new HashMap<String, String>();
        updatedAttributes.put("isSuspended", "false");
        super.updateUser(userName, updatedAttributes);
    }

    public void changePassword(String userName, String password) throws AppsForYourDomainException, MalformedURLException,
            IOException, ServiceException {
        Map<String, String> updatedAttributes = new HashMap<String, String>();
        updatedAttributes.put("password", password);
        super.updateUser(userName, updatedAttributes);
    }

    /**
     * Checks if a user can successfully log into Google Apps.
     * 
     * @param userEmail
     *            The user e-mail address.
     * @param userPassword
     *            The user password.
     * @throws AuthenticationException
     *             In case the user credentials are incorrect, or we were
     *             introduced with CAPTCHA.
     */
    public boolean checkIfUserCanLogin(String userEmail, String userPassword) {
        UserService oridinaryUserService = new UserService("TestAuthentication");

        try {
            oridinaryUserService.setUserCredentials(userEmail, userPassword);
        } catch (AuthenticationException e) {
            return false;
        }

        return true;
    }
}
