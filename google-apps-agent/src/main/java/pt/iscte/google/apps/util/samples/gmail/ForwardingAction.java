package pt.iscte.google.apps.util.samples.gmail;

public enum ForwardingAction {
    KEEP, ARCHIVE, DELETE;
}
