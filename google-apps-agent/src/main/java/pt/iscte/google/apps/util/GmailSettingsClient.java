package pt.iscte.google.apps.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import pt.iscte.fenix.util.PropertiesManager;
import pt.iscte.google.apps.util.dto.ForwardDTO;
import pt.iscte.google.apps.util.dto.SendAsDTO;
import pt.iscte.google.apps.util.samples.gmail.Constants;
import pt.iscte.google.apps.util.samples.gmail.GmailSettingsService;

import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;
import com.google.gdata.data.appsforyourdomain.generic.GenericFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

/**
 * Google Apps email settings client.
 */
public class GmailSettingsClient extends GmailSettingsService {

    private GmailSettingsClient(String applicationName, String domain, String username, String password)
            throws AuthenticationException {
        super(applicationName, domain, username, password);
    }

    // ------------------------------------------------------------------------
    private static GmailSettingsClient CACHED_INSTANCE;
    private static long ADQUIRED_TIME;

    private static final long ONE_DAY_MS = 86400000;

    /**
     * Check if it has passed more than 1 day.
     * 
     * @param adquiredTime
     *            time
     * @return true if it has passed more than 1 day
     */
    private static boolean hasExpired(long adquiredTime) {
        long now = System.currentTimeMillis();
        if (now - adquiredTime > ONE_DAY_MS) {
            return true;
        }
        return false;
    }

    public static synchronized GmailSettingsClient getService() throws AuthenticationException {
        if (hasExpired(ADQUIRED_TIME)) {
            // Recycle
            ADQUIRED_TIME = System.currentTimeMillis();

            final String domain = PropertiesManager.getProperty("domain");
            final String adminEmail = PropertiesManager.getProperty("admin.email");
            final String password = PropertiesManager.getProperty("admin.password");
            final String adminEmailNoDomain = adminEmail.split("@")[0];

            CACHED_INSTANCE = new GmailSettingsClient(GmailSettingsClient.class.getName(), domain, adminEmailNoDomain, password);
            System.out.println("Recycling service GmailClient. " + new Date());
        }
        return CACHED_INSTANCE;
    }

    // ------------------------------------------------------------------------

    /**
     * Retrieves mail forwarding settings.
     * 
     * @param username
     *            the username
     * 
     * @see GmailSettingsService#retrieveForwarding(String)
     */
    public ForwardDTO retrieveForwardingDTO(String username) throws IllegalArgumentException, IOException, ServiceException {
        GenericEntry forwardingEntry = retrieveSettingsEntry(username, Constants.FORWARDING);
        if (forwardingEntry != null) {
            return new ForwardDTO(username, forwardingEntry);
        }
        return null;
    }

    /**
     * Changes forwarding settings.
     * 
     * @param data
     *            forward information.
     * 
     * @see GmailSettingsService#changeForwarding(List, boolean, String, String)
     */
    public void changeForwardingDTO(ForwardDTO data) throws IllegalArgumentException, ServiceException, MalformedURLException,
            IOException {
        super.changeForwarding(Arrays.asList(data.getUsername()), data.isEnable(), data.getForwardTo(), data.getAction());
    }

    public List<SendAsDTO> retrieveSendAsDTO(String username) throws IllegalArgumentException, IOException, ServiceException {
        GenericFeed sendAsFeed = retrieveSettingsFeed(username, Constants.SEND_AS);
        if (sendAsFeed != null) {
            final List<GenericEntry> sendAsEntries = sendAsFeed.getEntries();
            if (sendAsEntries.size() > 0) {
                List<SendAsDTO> result = new ArrayList<SendAsDTO>();
                for (GenericEntry genericEntry : sendAsEntries) {
                    result.add(new SendAsDTO(username, genericEntry));
                }
                return result;
            }
        }
        return null;
    }

    public void createSendAsDTO(SendAsDTO data) throws IllegalArgumentException, ServiceException, MalformedURLException,
            IOException {
        super.createSendAs(Arrays.asList(data.getUsername()), data.getName(), data.getAddress(), data.getReplyTo(),
                data.isDefault());
    }

}