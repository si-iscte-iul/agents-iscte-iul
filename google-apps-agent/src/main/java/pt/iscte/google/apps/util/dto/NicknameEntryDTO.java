package pt.iscte.google.apps.util.dto;

import java.util.HashMap;
import java.util.Map;

import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;

public class NicknameEntryDTO {
    private static final String ALIAS_EMAIL = "aliasEmail";
    private static final String USER_EMAIL = "userEmail";

    private String aliasEmail;
    private String userEmail;

    public NicknameEntryDTO(GenericEntry genericEntry) {
        aliasEmail = genericEntry.getProperty(ALIAS_EMAIL);
        userEmail = genericEntry.getProperty(USER_EMAIL);
    }

    public Map<String, String> getMap() {
        Map<String, String> map = new HashMap<String, String>();

        map.put(USER_EMAIL, getUserEmail());
        map.put(ALIAS_EMAIL, getAliasEmail());

        return map;
    }

    public String getAliasEmail() {
        return aliasEmail;
    }

    public void setAliasEmail(String aliasEmail) {
        this.aliasEmail = aliasEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

}
