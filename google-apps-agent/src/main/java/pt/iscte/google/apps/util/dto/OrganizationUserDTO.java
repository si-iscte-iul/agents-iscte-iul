package pt.iscte.google.apps.util.dto;

import java.util.HashMap;
import java.util.Map;

import com.google.gdata.data.appsforyourdomain.generic.GenericEntry;

public class OrganizationUserDTO {
    private static final String ORG_USER_EMAIL = "orgUserEmail";
    private static final String ORG_UNIT_PATH = "orgUnitPath";

    private String customerId;
    private String orgUserEmail;
    private String orgUnitPath;

    public OrganizationUserDTO(String customerId, GenericEntry genericEntry) {
        this.customerId = customerId;
        orgUserEmail = genericEntry.getProperty(ORG_USER_EMAIL);
        orgUnitPath = genericEntry.getProperty(ORG_UNIT_PATH);
    }

    public Map<String, String> getMap() {
        Map<String, String> map = new HashMap<String, String>();

        map.put(ORG_USER_EMAIL, getOrgUserEmail());
        map.put(ORG_UNIT_PATH, getOrgUnitPath());

        return map;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrgUserEmail() {
        return orgUserEmail;
    }

    public void setOrgUserEmail(String orgUserEmail) {
        this.orgUserEmail = orgUserEmail;
    }

    public String getOrgUnitPath() {
        return orgUnitPath;
    }

    public void setOrgUnitPath(String orgUnitPath) {
        this.orgUnitPath = orgUnitPath;
    }

}
