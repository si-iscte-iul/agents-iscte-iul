package pt.iscte.logging;

import org.apache.commons.logging.LogFactory;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.TriggeringEventEvaluator;

/**
 * This {@link TriggeringEventEvaluator} implementation sends an email log every
 * time the internal count reaches the {@value #BUFFER_SIZE} or when the
 * explicit message {@value #EMAILSENDTRIGGER} is received over the log
 * 
 * @author jpereira - Linkare TI modified by Benjamin Sick
 * 
 */
public class LogTriggeringEvent implements TriggeringEventEvaluator {

    /**
     * The internal count
     */
    private int count = 0;

    /**
     * The maximum size of the buffer before an automatic sending of the email
     * is triggered
     */
    private static final int BUFFER_SIZE = 100;

    /**
     * The message that triggers the sending of the email
     */
    public final static String EMAILSENDTRIGGER = "This error is just to trigger the sending of the last email.";

    /**
     * Required no-arg public constructor for apache log4j integration
     */
    public LogTriggeringEvent() {
    }

    /**
     * returns true either because the current logging event rendered message
     * equals the {@link #EMAILSENDTRIGGER} ({@value #EMAILSENDTRIGGER} )
     * message or the internal count reaches {@link #BUFFER_SIZE} {@value #BUFFER_SIZE}
     * 
     * It always increments the internal counter
     * 
     * When the messages are sent, this class additionally sends an reference
     * number in the email
     * 
     * @see org.apache.log4j.spi.TriggeringEventEvaluator#isTriggeringEvent(org.apache.log4j.spi.LoggingEvent)
     */
    public boolean isTriggeringEvent(LoggingEvent event) {
        count++;
        if (event.getRenderedMessage() != null && event.getRenderedMessage().equals(EMAILSENDTRIGGER)) {
            LogFactory.getLog(event.getLoggerName()).info("Last logging email #" + (count / BUFFER_SIZE) + 1);
            return true;
        } else if (count % BUFFER_SIZE == 0) {
            LogFactory.getLog(event.getLoggerName()).info("Logging email number #" + (count / BUFFER_SIZE));
            return true;
        }
        return false;
    }

}
