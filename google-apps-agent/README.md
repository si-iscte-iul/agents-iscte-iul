Google Apps Agent
==============
Fenix invokes web services on this agent for provision, authentication and changing password.
## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Configuration**
To configure the application change the *src/main/resources/configuration.properties* file.

**Run embedded tomcat**
To run application using an embedded tomcat use
mvn tomcat7:run

**Run embedded tomcat on different port**
On different port use
mvn tomcat7:run -Dmaven.tomcat.port=8081

Test using
http://localhost:8081/google-apps-agent

# Deployment
Generate war
mvn clean package

War is generated on
target/ad-agent-<version>.war

