package pt.iscte.dsi.macaddress.client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pt.iscte.agents.macAddressRegistry.jaxws.MacAddressBean;
import pt.iscte.agents.macAddressRegistry.jaxws.MacAddressRegistryService;
import pt.iscte.dsi.macaddress.client.util.MacAddressRegistryClientUtil;
import pt.iscte.dsi.macaddress.client.util.PropertiesManager;

/**
 * This class is the client for the Fénix mac address Web Service.
 * 
 * TODO Logging TODO Send email report
 * 
 * @author Paulo Zenida (currently maintained by António Casqueiro)
 */
public class MacAddressRegistryClient {
    private static final String FILE_OUTPUT_DIR_KEY = "file.output.dir";
    private static final String FILE_OUTPUT_DIR = PropertiesManager.getProperty(FILE_OUTPUT_DIR_KEY);

    private MacAddressRegistryService service = null;

    private void init() throws RemoteException {
        service = MacAddressRegistryClientUtil.getService();
    }

    private void processNonStudents() throws IOException {
        List<ResultData> list = buildListFromResult(service.getNonStudentMacAddresses());

        final File nonStudentsForIscteNetwork = new File(FILE_OUTPUT_DIR, "non-students-iscte-network.txt");
        final BufferedWriter writerForIscteNetwork = new BufferedWriter(new FileWriter(nonStudentsForIscteNetwork));
        final File nonStudentsForStudentsNetwork = new File(FILE_OUTPUT_DIR, "non-students-students-network.txt");
        final BufferedWriter writerForStudentsNetwork = new BufferedWriter(new FileWriter(nonStudentsForStudentsNetwork));
        for (final ResultData resultData : list) {
            writeEntryToFile(resultData, writerForIscteNetwork);
            writeEntryToFile(resultData, writerForStudentsNetwork);
        }
        writerForIscteNetwork.close();
        writerForStudentsNetwork.close();
    }

    private void processStudents() throws IOException {
        List<ResultData> list = buildListFromResult(service.getStudentMacAddresses());

        final File studentsForStudentsNetwork = new File(FILE_OUTPUT_DIR, "students-students-network.txt");
        final BufferedWriter writerForStudentsNetwork = new BufferedWriter(new FileWriter(studentsForStudentsNetwork));
        for (final ResultData resultData : list) {
            writeEntryToFile(resultData, writerForStudentsNetwork);
        }
        writerForStudentsNetwork.close();
    }

    private void writeEntryToFile(final ResultData resultData, final BufferedWriter writer) throws IOException {
        String line =
                "host " + resultData.getUsername() + " { hardware ethernet " + resultData.getAddress().toUpperCase() + "; }\n";
        writer.write(line);
    }

    private List<ResultData> buildListFromResult(List<MacAddressBean> result) {
        if (result == null || result.size() == 0) {
            return new ArrayList<ResultData>(0);
        }

        List<ResultData> list = new ArrayList<ResultData>();

        if (result != null) {
            for (MacAddressBean elem : result) {
                list.add(new ResultData(elem.getUsername(), elem.getAddress()));
            }
        }

        return list;
    }

    // ------------------------------------------------------------------------
    public static class ResultData {
        private String username;
        private String address;

        public ResultData(String username, String address) {
            super();
            this.username = username;
            this.address = address;
        }

        public String getUsername() {
            return username;
        }

        public String getAddress() {
            return address;
        }
    }

    // ------------------------------------------------------------------------

    public static void main(String[] args) throws IOException {
        MacAddressRegistryClient client = new MacAddressRegistryClient();
        client.init();
        client.processNonStudents();
        client.processStudents();
    }
}