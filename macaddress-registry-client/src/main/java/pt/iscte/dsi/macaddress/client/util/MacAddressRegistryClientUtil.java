package pt.iscte.dsi.macaddress.client.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import pt.iscte.agents.macAddressRegistry.jaxws.MacAddressRegistryService;
import pt.iscte.agents.macAddressRegistry.jaxws.MacAddressRegistryService_Service;

import com.sun.xml.ws.developer.JAXWSProperties;

public class MacAddressRegistryClientUtil {

    private static final String WSDL_LOCATION = "mac.address.registry.ws.client.wsdl.location";
    private static final String ADDRESS = "mac.address.registry.ws.client.address";
    private static final String CONNECT_TIMEOUT = "mac.address.registry.ws.client.request.timeout";
    private static final String REQUEST_TIMEOUT = "mac.address.registry.ws.client.connect.timeout";

    private final static Logger logger = Logger.getLogger(MacAddressRegistryClientUtil.class);
    private static URL wsdlLocation = null;

    public static MacAddressRegistryService getService() throws RemoteException {
        URL wsdlLocation = getWsdlLocation();
        MacAddressRegistryService_Service service = new MacAddressRegistryService_Service(wsdlLocation);
        MacAddressRegistryService webService = service.getPort(MacAddressRegistryService.class);
        Map<String, Object> context = ((BindingProvider) webService).getRequestContext();
        String address = getProperty(ADDRESS);
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, address);
        String connectTimeoutStr = getProperty(CONNECT_TIMEOUT);
        Integer connectTimeout = new Integer(connectTimeoutStr) * 1000;
        context.put(JAXWSProperties.CONNECT_TIMEOUT, connectTimeout);
        String requestTimeoutStr = getProperty(REQUEST_TIMEOUT);
        Integer requestTimeout = new Integer(requestTimeoutStr) * 1000;
        context.put(JAXWSProperties.REQUEST_TIMEOUT, requestTimeout);
        return webService;
    }

    private static URL getWsdlLocation() throws RemoteException {
        if (wsdlLocation == null) {
            String wsdlLocationStr = getProperty(WSDL_LOCATION);
            try {
                wsdlLocation = new URL(wsdlLocationStr);
            } catch (MalformedURLException e) {
                logger.error("Wrong wsdl location for dsi not student data service ws client.", e);
                e.printStackTrace();
                throw new RemoteException("Dsi not student data service web services is down", e);
            }
        }
        return wsdlLocation;
    }

    private static String getProperty(String propertyName) {
        String propertyValue = PropertiesManager.getProperty(propertyName);
        return propertyValue;
    }

}
