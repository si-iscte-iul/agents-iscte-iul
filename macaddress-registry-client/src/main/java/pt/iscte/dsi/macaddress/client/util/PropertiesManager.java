package pt.iscte.dsi.macaddress.client.util;

import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {
    private static final Properties properties = new Properties();
    static {
        loadProperties(properties, "/configuration.properties");
    }

    public static String getProperty(String s) {
        return properties.getProperty(s);
    }

    public static int getIntegerProperty(String s) {
        String value = getProperty(s);
        return Integer.parseInt(value);
    }

    public static Properties loadProperties(String string) {
        return properties;
    }

    public static void loadProperties(Properties properties, String filename) {
        try {
            properties.load(PropertiesManager.class.getResourceAsStream(filename));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
