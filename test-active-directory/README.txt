Ficheiros:
1- build.properties - Ficheiro de configuração do programa de teste.
2- README.txt - Ficheiro de instruções (este ficheiro).
3- test-active-directory.jar - Programa compilado.
4- test-active-directory.zip - Código fonte do programa (projecto Eclipse).

Configuração:
0- Copiar os ficheiros "TestActiveDirectory.jar" e "build.properties" para outro directório.
Alterar apenas os ficheiros desse directório para preservar os originais.
1- Editar o ficheiro "build.properties"
2- Configurar o valor das propriedade "repeat", "user.username" e "user.password".
3- Substituir o valor o texto "THE-PASSWORD-REPLACE-ME" pela palavra-passe respectiva.

Executar teste:
Na linha de comando escrever "java -cp TestActiveDirectory.jar test.ad.TestActiveDirectory"
