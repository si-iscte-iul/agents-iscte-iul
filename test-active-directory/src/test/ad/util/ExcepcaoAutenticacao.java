package test.ad.util;

/**
 * F�nix project class.
 */
public class ExcepcaoAutenticacao extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for ExcepcaoAutenticacao.
	 */
	public ExcepcaoAutenticacao() {
		super();
	}

	/**
	 * Constructor for ExcepcaoAutenticacao.
	 * 
	 * @param message
	 */
	public ExcepcaoAutenticacao(String message) {
		super(message);
	}

	/**
	 * Constructor for ExcepcaoAutenticacao.
	 * 
	 * @param message
	 * @param cause
	 */
	public ExcepcaoAutenticacao(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for ExcepcaoAutenticacao.
	 * 
	 * @param cause
	 */
	public ExcepcaoAutenticacao(Throwable cause) {
		super(cause);
	}

}