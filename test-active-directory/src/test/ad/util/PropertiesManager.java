package test.ad.util;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Simple class to read the properties file.
 * 
 * @author Antonio Casqueiro
 */
public class PropertiesManager {
    private static Properties props = new Properties();
    static {
        try {
            props.load(new FileReader("build.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getProperty(String key) {
        return props.getProperty(key);
    }

}
