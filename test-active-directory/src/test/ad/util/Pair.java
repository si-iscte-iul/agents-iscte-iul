package test.ad.util;

/**
 * F�nix project class.
 */
public class Pair<K, V> {

	private K key;
	private V value;

	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public K getFirst() {
		return key;
	}

	public V getSecond() {
		return value;
	}

	@Override
	public int hashCode() {
		return (getFirst() == null ? super.hashCode() : getFirst().hashCode())
				+ (getSecond() == null ? super.hashCode() : getSecond()
						.hashCode());
	}

	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Pair)) {
			return false;
		}
		@SuppressWarnings("rawtypes")
		final Pair otherPair = (Pair) other;
		return (otherPair.getFirst() == this.getFirst() || otherPair.getFirst()
				.equals(this.getFirst()))
				&& (otherPair.getSecond() == this.getSecond() || otherPair
						.getSecond().equals(this.getSecond()));
	}

	@Override
	public String toString() {
		return "Pair(" + getKey() + ", " + getValue() + ")";
	}

}