package test.ad.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Logger;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class LdapUtils {

    private static final Logger logger = Logger.getLogger(LdapUtils.class.getSimpleName());
//    private static int STACK_LENGTH = BaseAuthenticationAction.STACK_LENGTH;

    private static final String DESCRIPTION = "description";

    private static final String QUOTE = "\"";

    private static final String USER_ACCOUNT_CONTROL = "userAccountControl";

    private static final String LDAP_USERS_SUFFIX_KEY = "ldap.users.suffix";

    private static final String LDAP_DC_PATH_KEY = "ldap.dc.path";

    private static final String LDAP_INITIAL_CONTEXT_FACTORY_KEY = "ldap.initial.context.factory";

    private static final String LDAP_PROVIDER_URL_KEY = "ldap.provider.url";

    private static final String LDAP_SECURITY_AUTHENTICATION_KEY = "ldap.security.authentication";

    private static final String LDAP_ADMIN_PASS_KEY = "ldap.admin.pass";

    private static final String LDAP_ADMIN_USER_KEY = "ldap.admin.user";

    private static final String LDAP2_USERS_SUFFIX_KEY = "ldap2.users.suffix";

    private static final String LDAP2_DC_PATH_KEY = "ldap2.dc.path";

    private static final String LDAP2_PROVIDER_URL_KEY = "ldap2.provider.url";

    private static final String LDAP2_ADMIN_PASS_KEY = "ldap2.admin.pass";

    private static final String LDAP2_ADMIN_USER_KEY = "ldap2.admin.user";

    // private static final String LDAP3_USERS_SUFFIX =
    // "ldap3.users.suffix";

    private static final String LDAP3_DC_PATH_KEY = "ldap3.dc.path";

    private static final String LDAP3_PROVIDER_URL_KEY = "ldap3.provider.url";

    private static final String LDAP3_ADMIN_PASS_KEY = "ldap3.admin.pass";

    private static final String LDAP3_ADMIN_USER_KEY = "ldap3.admin.user";

    private static final String BAD_AUTHENTICATION = "bad.authentication";

    private static final String UNICODE_PWD = "unicodePwd";

    private static final String UTF_16LE = "UTF-16LE";

    private static final String DISTINGUISHED_NAME = "distinguishedname";

    private static final String USER_PRINCIPAL_NAME = "userprincipalname";

    private static final String SAM_ACCOUNT_NAME = "sAMAccountName";

    private static final String LDAP_USERS_SUFFIX;

    private static final String LDAP_DC_PATH;

    // private static final String LDAP_GROUP_USERS_PATH;

    // private static final String LDAP_GROUP_USERS;

    private static final String LDAP_INITIAL_CONTEXT_FACTORY;

    private static final String LDAP_PROVIDER_URL;

    private static final String LDAP_SECURITY_AUTHENTICATION;

    private static final String LDAP_ADMIN_PASS;

    private static final String LDAP_ADMIN_USER;

    private static final String LDAP2_USERS_SUFFIX;

    private static final String LDAP2_DC_PATH;

    // private static final String LDAP2_GROUP_USERS_PATH;

    private static final String LDAP2_PROVIDER_URL;

    private static final String LDAP2_ADMIN_PASS;

    private static final String LDAP2_ADMIN_USER;

    // private static final String LDAP3_USERS_SUFFIX =
    // "ldap3.users.suffix";

    private static final String LDAP3_DC_PATH;

    // private static final String LDAP3_GROUP_USERS_PATH;

    private static final String LDAP3_PROVIDER_URL;

    private static final String LDAP3_ADMIN_PASS;

    private static final String LDAP3_ADMIN_USER;

    private static final String CHECK_LDAP1_ENABLED;

    private static final String CHECK_LDAP2_ENABLED;

    private static final String CHECK_LDAP3_ENABLED;

    static {
        LDAP_USERS_SUFFIX = PropertiesManager.getProperty(LDAP_USERS_SUFFIX_KEY);
        LDAP_DC_PATH = PropertiesManager.getProperty(LDAP_DC_PATH_KEY);
        // LDAP_GROUP_USERS_PATH =
        // PropertiesManager.getProperty(LDAP_GROUP_USERS_PATH_KEY);
        // LDAP_GROUP_USERS =
        // PropertiesManager.getProperty(LDAP_GROUP_USERS_KEY);
        LDAP_INITIAL_CONTEXT_FACTORY = PropertiesManager.getProperty(LDAP_INITIAL_CONTEXT_FACTORY_KEY);
        LDAP_PROVIDER_URL = PropertiesManager.getProperty(LDAP_PROVIDER_URL_KEY);
        LDAP_SECURITY_AUTHENTICATION = PropertiesManager.getProperty(LDAP_SECURITY_AUTHENTICATION_KEY);
        LDAP_ADMIN_PASS = PropertiesManager.getProperty(LDAP_ADMIN_PASS_KEY);
        LDAP_ADMIN_USER = PropertiesManager.getProperty(LDAP_ADMIN_USER_KEY);
        LDAP2_USERS_SUFFIX = PropertiesManager.getProperty(LDAP2_USERS_SUFFIX_KEY);
        LDAP2_DC_PATH = PropertiesManager.getProperty(LDAP2_DC_PATH_KEY);
        // LDAP2_GROUP_USERS_PATH =
        // PropertiesManager.getProperty(LDAP2_GROUP_USERS_PATH_KEY);
        LDAP2_PROVIDER_URL = PropertiesManager.getProperty(LDAP2_PROVIDER_URL_KEY);
        LDAP2_ADMIN_PASS = PropertiesManager.getProperty(LDAP2_ADMIN_PASS_KEY);
        LDAP2_ADMIN_USER = PropertiesManager.getProperty(LDAP2_ADMIN_USER_KEY);
        LDAP3_DC_PATH = PropertiesManager.getProperty(LDAP3_DC_PATH_KEY);
        // LDAP3_GROUP_USERS_PATH =
        // PropertiesManager.getProperty(LDAP3_GROUP_USERS_PATH_KEY);
        LDAP3_PROVIDER_URL = PropertiesManager.getProperty(LDAP3_PROVIDER_URL_KEY);
        LDAP3_ADMIN_PASS = PropertiesManager.getProperty(LDAP3_ADMIN_PASS_KEY);
        LDAP3_ADMIN_USER = PropertiesManager.getProperty(LDAP3_ADMIN_USER_KEY);

        CHECK_LDAP1_ENABLED = PropertiesManager.getProperty("ldap.check.enabled");
        CHECK_LDAP2_ENABLED = PropertiesManager.getProperty("ldap2.check.enabled");
        CHECK_LDAP3_ENABLED = PropertiesManager.getProperty("ldap3.check.enabled");
    }

    private static void authenticate(final String username, final String password, final String ldapProviderUrl)
            throws ExcepcaoAutenticacao {

        if (password == null || password.length() == 0) {
            logger.info("Empty password " + password + " for user " + username);
            throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION);
        }

        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, username, password);

        try {
            LdapContext ctx = new InitialLdapContext(env, null);
            ctx.close();
        } catch (AuthenticationException ae) {
            throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION + ": " + username);
        } catch (NamingException e) {
            logger.warning(String.format("Error authenticating on ldap: %s", e.toString()));
            throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION + ": " + username);
        }
    }

    private static void unlockAccount(final String ldapProviderUrl, final String ldapDcPath, final String adminName,
            final String adminPassword, final String usernameWithSuffix) {
        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, adminName, adminPassword);

        @SuppressWarnings("rawtypes")
        NamingEnumeration answer = null;
        LdapContext ctx = null;

        final String usernameWithoutSuffix = getUsernameNoDomain(usernameWithSuffix);

        try {
            // Create the initial directory context
            ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { DISTINGUISHED_NAME, USER_PRINCIPAL_NAME };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + SAM_ACCOUNT_NAME + "=" + usernameWithoutSuffix + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;
            // initialize counter to total the results
            int totalResults = 0;

            // Search for objects using the filter
            answer = ctx.search(searchBase, searchFilter, searchCtls);

            String usernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                totalResults++;
                usernameFound =
                        sr.getAttributes().get(DISTINGUISHED_NAME).toString().substring((DISTINGUISHED_NAME + ": ").length());
            }

            if (totalResults > 1) {
                // Something if wrong, the user exists with more than one domain
                throw new RuntimeException("error.problem.sAMAaccountName.notUnique");
            }

            if (usernameFound != null) {
                // Set password is a LDAP modify operation:

                ModificationItem[] mods = new ModificationItem[2];

                // Enable account
                int UF_NORMAL_ACCOUNT = 0x0200;
                int UF_DONT_EXPIRE_PASSWD = 0x10000;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute("userAccountControl", Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD)));
                // Unlock account
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("lockoutTime", "0"));

                // Perform the update
                ctx.modifyAttributes(usernameFound, mods);
            }
        } catch (NamingException e) {
            e.printStackTrace();
            throw new RuntimeException("error.problem.resetting.password", e);
        } finally {
            if (answer != null) {
                try {
                    answer.close();
                } catch (NamingException e) {
                    throw new RuntimeException("error.problem.closing.NamingEnumeration");
                }
            }
            if (ctx != null) {
                try {
                    ctx.close();
                } catch (NamingException e) {
                    throw new RuntimeException("error.problem.closing.InitialLdapContext");
                }
            }
        }
    }

    public static void authenticateInAllLdaps(final String username, final String password) throws ExcepcaoAutenticacao {
        boolean isAuthenticated = false;
        final DebugChronometer dc = new DebugChronometer();

        if (!isAuthenticated && checkHorusLdap()) {
            try {
                authenticateInHorusLdap(username, password);
                isAuthenticated = true;
            } catch (ExcepcaoAutenticacao e) {
            }
            dc.measure("authenticateInHorusLdap");
        }

        if (!isAuthenticated && checkDc1Ldap()) {
            try {
                authenticateInDc1Ldap(username, password);
                isAuthenticated = true;
            } catch (ExcepcaoAutenticacao e) {
            }
            dc.measure("authenticateInDc1Ldap");
        }

        if (!isAuthenticated && checkIscteNtLdap()) {
            try {
                authenticateInIscteNt(username, password);
                isAuthenticated = true;
            } catch (ExcepcaoAutenticacao e) {
            }
            dc.measure("authenticateInIscteNt");
        }

//        if (!isAuthenticated && LdapConfig.STUDENTS.check()) {
//            try {
//                authenticateInAdStudents(person, username, password);
//                isAuthenticated = true;
//            } catch (ExcepcaoAutenticacao e) {
//            }
//            dc.measure("authenticateInAdStudents");
//        }
//
//        if (!isAuthenticated && LdapConfig.NOT_STUDENTS.check()) {
//            try {
//                authenticateInAdNotStudents(person, username, password);
//                isAuthenticated = true;
//            } catch (ExcepcaoAutenticacao e) {
//            }
//            dc.measure("authenticateInAdNotStudents");
//        }

        logger.finest("authenticateInAllLdaps time for user: " + username + " | " + dc.reportSingleLine());

        if (!isAuthenticated) {
            throw new ExcepcaoAutenticacao();
        }
    }

    public static void unlockAccountInHorusLdap(final String username) throws NamingException {
        if (!checkHorusLdap()) {
            return;
        }

        unlockAccount(LDAP_PROVIDER_URL, LDAP_DC_PATH, LDAP_ADMIN_USER, LDAP_ADMIN_PASS, username);
    }

    public static boolean isUsernameInHorusLdap(final String username) throws NamingException {
        if (!checkHorusLdap()) {
            return false;
        }

        return isPersonInLdap(LDAP_PROVIDER_URL, LDAP_DC_PATH, LDAP_ADMIN_USER, LDAP_ADMIN_PASS, username);
    }

    public static void unlockAccountInDc1Ldap(final String username) throws NamingException {
        if (!checkDc1Ldap()) {
            return;
        }

        unlockAccount(LDAP2_PROVIDER_URL, LDAP2_DC_PATH, LDAP2_ADMIN_USER, LDAP2_ADMIN_PASS, username);
    }

    public static boolean isUsernameInDc1Ldap(final String username) throws NamingException {
        if (!checkDc1Ldap()) {
            return false;
        }

        return isPersonInLdap(LDAP2_PROVIDER_URL, LDAP2_DC_PATH, LDAP2_ADMIN_USER, LDAP2_ADMIN_PASS, username);
    }

    public static void unlockAccountInIscteNtLdap(final String username) throws NamingException {
        if (!checkIscteNtLdap()) {
            return;
        }

        unlockAccount(LDAP3_PROVIDER_URL, LDAP3_DC_PATH, LDAP3_ADMIN_USER, LDAP3_ADMIN_PASS, username);
    }

    public static boolean isUsernameInIscteNtLdap(final String username) throws NamingException {
        if (!checkIscteNtLdap()) {
            return false;
        }

        return isPersonInLdap(LDAP3_PROVIDER_URL, LDAP3_DC_PATH, LDAP3_ADMIN_USER, LDAP3_ADMIN_PASS, username);
    }

    public static void authenticateInIscteNt(final String username, final String password) throws ExcepcaoAutenticacao {
        if (!checkIscteNtLdap()) {
            return;
        }
        try {
            authenticate(username, password, LDAP3_PROVIDER_URL);
        } catch (ExcepcaoAutenticacao e) {
            throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION + ". Failed Authentication in " + LDAP3_PROVIDER_URL);
        }
    }

    public static void authenticateInHorusLdap(final String username, final String password) throws ExcepcaoAutenticacao {
        if (!checkHorusLdap()) {
            return;
        }

        try {
            authenticate(username, password, LDAP_PROVIDER_URL);
        } catch (ExcepcaoAutenticacao e) {
            throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION + ". Failed Authentication in " + LDAP_PROVIDER_URL);
        }
    }

    public static void authenticateInDc1Ldap(final String username, final String password) throws ExcepcaoAutenticacao {
        if (!checkDc1Ldap()) {
            return;
        }

        try {
            // xxxxxx@iscte.pt
            authenticate(username, password, LDAP2_PROVIDER_URL);
        } catch (ExcepcaoAutenticacao e) {
            try {
                // xxxxxx@iscte-iul.pt
                authenticate(username.replaceAll(LDAP_USERS_SUFFIX, LDAP2_USERS_SUFFIX), password, LDAP2_PROVIDER_URL);
            } catch (ExcepcaoAutenticacao e1) {
                throw new ExcepcaoAutenticacao(BAD_AUTHENTICATION + ". Failed Authentication in " + LDAP2_PROVIDER_URL);
            }
        }
    }

    /**
     * This method is used in the ChangePassword service and in the
     * GenerateNewPasswordService.
     * 
     * @param username
     * @param password
     * @throws Exception
     */
    public static List<ServiceTypeEnum> changePasswordInAllLdaps(final String username, String password) {
        final List<ServiceTypeEnum> successServices = new ArrayList<ServiceTypeEnum>();

        if (checkDc1Ldap()) {
            logger.info(username + ": Changing password in Dc1");
            LdapUtils.changePasswordInDc1Ldap(username, password);
            logger.info(username + ": Changed successfully password in Dc1");
        }

        if (checkHorusLdap()) {
            logger.info(username + ": Changing password in Horus");
            LdapUtils.changePasswordInHorusLdap(username, password);
            logger.info(username + ": Changed successfully password in Horus");
        }

        if (checkIscteNtLdap()) {
            logger.info(username + ": Changing password in NT");
            LdapUtils.changePasswordInRedeNtLdap(username, password);
            logger.info(username + ": Changed successfully password in NT");
        }

        return successServices;
    }

    public static void changePasswordInHorusLdap(final String username, String password) {
        if (!checkHorusLdap()) {
            return;
        }
        System.out.println(username + "|" + password);
        changePasswordInLdap(LDAP_PROVIDER_URL, LDAP_DC_PATH, LDAP_ADMIN_USER, LDAP_ADMIN_PASS, username, password);
    }

    public static void changePasswordInRedeNtLdap(final String username, String password) {
        if (!checkIscteNtLdap()) {
            return;
        }

        changePasswordInLdap(LDAP3_PROVIDER_URL, LDAP3_DC_PATH, LDAP3_ADMIN_USER, LDAP3_ADMIN_PASS, username, password);
    }

    public static void changePasswordInDc1Ldap(final String username, String password) {
        if (!checkDc1Ldap()) {
            return;
        }

        changePasswordInLdap(LDAP2_PROVIDER_URL, LDAP2_DC_PATH, LDAP2_ADMIN_USER, LDAP2_ADMIN_PASS, username, password);
    }

    public static boolean isPersonInLdap(final String ldapProviderUrl, final String ldapDcPath, final String adminName,
            final String adminPassword, final String usernameWithSuffix) throws NamingException {
        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, adminName, adminPassword);

        @SuppressWarnings("rawtypes")
        NamingEnumeration answer = null;
        LdapContext ctx = null;

        final String usernameWithoutSuffix = getUsernameNoDomain(usernameWithSuffix);
        String searchFilter = "(&(objectClass=user)(" + SAM_ACCOUNT_NAME + "=" + usernameWithoutSuffix + "))";

        try {
            // Create the initial directory context
            ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { DISTINGUISHED_NAME, USER_PRINCIPAL_NAME };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            answer = ctx.search(searchBase, searchFilter, searchCtls);

            return answer.hasMoreElements();
        } finally {
            if (answer != null) {
                answer.close();
            }
            if (ctx != null) {
                ctx.close();
            }
        }
    }

    public static boolean isCnInLdap(final String ldapProviderUrl, final String ldapDcPath, final String adminName,
            final String adminPassword, final String name) throws NamingException {
        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, adminName, adminPassword);

        @SuppressWarnings("rawtypes")
        NamingEnumeration answer = null;
        LdapContext ctx = null;

        try {
            // Create the initial directory context
            ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { DISTINGUISHED_NAME, USER_PRINCIPAL_NAME };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(cn=" + name + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            answer = ctx.search(searchBase, searchFilter, searchCtls);

            return answer.hasMoreElements();
        } finally {
            if (answer != null) {
                answer.close();
            }
            if (ctx != null) {
                ctx.close();
            }
        }
    }

    private static void changePasswordInLdap(final String ldapProviderUrl, final String ldapDcPath, final String adminName,
            final String adminPassword, final String usernameWithSuffix, String password) {
        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, adminName, adminPassword);

        @SuppressWarnings("rawtypes")
        NamingEnumeration answer = null;
        LdapContext ctx = null;

        final String usernameWithoutSuffix = getUsernameNoDomain(usernameWithSuffix);

        try {
            // Create the initial directory context
            ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { DISTINGUISHED_NAME, USER_PRINCIPAL_NAME };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + SAM_ACCOUNT_NAME + "=" + usernameWithoutSuffix + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // initialize counter to total the results
            int totalResults = 0;

            // Search for objects using the filter
            answer = ctx.search(searchBase, searchFilter, searchCtls);

            String usernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                totalResults++;
                usernameFound =
                        sr.getAttributes().get(DISTINGUISHED_NAME).toString().substring((DISTINGUISHED_NAME + ": ").length());
            }

            if (totalResults > 1) {
                // Something if wrong, the user exists with more than one domain
                throw new RuntimeException("error.problem.sAMAaccountName.notUnique");
            }

            if (usernameFound != null) {
                // Set password is a LDAP modify operation:
                ModificationItem[] mods = new ModificationItem[3];

                // Enable account
                int UF_NORMAL_ACCOUNT = 0x0200;
                int UF_DONT_EXPIRE_PASSWD = 0x10000;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute("userAccountControl", Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD)));
                // Unlock account
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute("lockoutTime", "0"));

                // Replace the "unicododePwd" attribute with a new value.
                // Password must be both Unicode (UTF-16LE) and a quoted string.
                String newQuotedPassword = QUOTE + password + QUOTE;
                byte[] newUnicodePassword = newQuotedPassword.getBytes(UTF_16LE);

                mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(UNICODE_PWD, newUnicodePassword));

                // Perform the update
                ctx.modifyAttributes(usernameFound, mods);
            }
        } catch (NamingException e) {
            e.printStackTrace();
            throw new RuntimeException("error.problem.resetting.password", e);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException("error.problem.encoding.password", e);
        } finally {
            if (answer != null) {
                try {
                    answer.close();
                } catch (NamingException e) {
                    throw new RuntimeException("error.problem.closing.NamingEnumeration");
                }
            }
            if (ctx != null) {
                try {
                    ctx.close();
                } catch (NamingException e) {
                    throw new RuntimeException("error.problem.closing.InitialLdapContext");
                }
            }
        }
    }

    private static Hashtable<String, String> initEnvironment(final String ldapProviderUrl, final String adminName,
            final String adminPassword) {
        final Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_INITIAL_CONTEXT_FACTORY);
        env.put(Context.PROVIDER_URL, ldapProviderUrl);
        env.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_AUTHENTICATION);

        env.put(Context.SECURITY_PRINCIPAL, adminName);
        env.put(Context.SECURITY_CREDENTIALS, adminPassword);

        // Configure connect timeout to be 5 seconds
        env.put("com.sun.jndi.ldap.connect.timeout", "5000");

        // Configure read timeout to 10 seconds
        env.put("com.sun.jndi.ldap.read.timeout", "10000");

        // Cipher communication data
        env.put(Context.SECURITY_PROTOCOL, "ssl");
        return env;
    }

    private static Pair<Integer, String> getUserAccountControlAndDescription(final String ldapProviderUrl,
            final String ldapDcPath, final String adminName, final String adminPassword, String usernameWithSuffix)
            throws NamingException {
        final Hashtable<String, String> env = initEnvironment(ldapProviderUrl, adminName, adminPassword);

        @SuppressWarnings("rawtypes")
        NamingEnumeration answer = null;
        LdapContext ctx = null;

        final String usernameWithoutSuffix = getUsernameNoDomain(usernameWithSuffix);

        try {
            // Create the initial directory context
            ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { DESCRIPTION, USER_ACCOUNT_CONTROL };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + SAM_ACCOUNT_NAME + "=" + usernameWithoutSuffix + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            answer = ctx.search(searchBase, searchFilter, searchCtls);

            // Loop through the search results
            if (answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attribute attr1 = sr.getAttributes().get("userAccountControl");
                Attribute attr2 = sr.getAttributes().get("description");
                return new Pair<Integer, String>(Integer.parseInt(attr1.get().toString()),
                        attr2 != null ? attr2.get().toString() : "");
            } else {
                return new Pair<Integer, String>(-1, "");
            }

        } catch (NamingException e) {
            e.printStackTrace();
            throw new RuntimeException("error.problem.resetting.password", e);
        } finally {
            if (answer != null) {
                answer.close();
            }
            if (ctx != null) {
                ctx.close();
            }
        }
    }

    public static boolean isAccountDisabledInHorus(String userName) throws NamingException {
        if (!checkHorusLdap()) {
            return false;
        }
        final Pair<Integer, String> pair =
                getUserAccountControlAndDescription(LDAP_PROVIDER_URL, LDAP_DC_PATH, LDAP_ADMIN_USER, LDAP_ADMIN_PASS, userName);
        int num = getDigitHex(pair.getKey(), 0);
        return isAccountDisabled(pair, num);
    }

    public static boolean isAccountDisabledInIscteNt(String userName) throws NamingException {
        if (!checkIscteNtLdap()) {
            return false;
        }
        final Pair<Integer, String> pair = getUserAccountControlAndDescription(LDAP3_PROVIDER_URL, LDAP3_DC_PATH,
                LDAP3_ADMIN_USER, LDAP3_ADMIN_PASS, userName);
        int num = getDigitHex(pair.getKey(), 0);
        return isAccountDisabled(pair, num);
    }

    public static boolean isAccountDisabledInDC1(String userName) throws NamingException {
        if (!checkDc1Ldap()) {
            return false;
        }
        final Pair<Integer, String> pair = getUserAccountControlAndDescription(LDAP2_PROVIDER_URL, LDAP2_DC_PATH,
                LDAP2_ADMIN_USER, LDAP2_ADMIN_PASS, userName);
        int num = getDigitHex(pair.getKey(), 0);
        return isAccountDisabled(pair, num);
    }

    private static int getDigitHex(int value, int offset) {
        char[] arr = Integer.toHexString(value).toCharArray();
        offset = arr.length - 1 - offset;
        int intValue = Integer.parseInt(String.valueOf(arr[offset]));
        return intValue;
    }

    private static boolean isAccountDisabled(final Pair<Integer, String> pair, int num) {
        num = num - 2;
        boolean result = (num == 9);
        result = result || (num == 8);
        result = result || (num == 1);
        result = result || (num == 0);
        return result && pair.getValue() != null && ("Desactivado automaticamente pelo sistema da DSI".equals(pair.getValue())
                || "Desactivado automaticamente pelo sistema do CI".equals(pair.getValue()));
    }

    /**
     * 
     * @return Returns true if an access to the horus LDAP should be performed.
     *         It returns false otherwise. This method returns the property set
     *         in the build configuration file, where one specifies if this
     *         should be enabled or not.
     */
    private static boolean checkHorusLdap() {
        return CHECK_LDAP1_ENABLED == null || "true".equals(CHECK_LDAP1_ENABLED);
    }

    /**
     * 
     * @return Returns true if an access to the dc1 LDAP should be performed. It
     *         returns false otherwise. This method returns the property set in
     *         the build configuration file, where one specifies if this should
     *         be enabled or not.
     */
    private static boolean checkDc1Ldap() {
        return CHECK_LDAP2_ENABLED == null || "true".equals(CHECK_LDAP2_ENABLED);
    }

    /**
     * 
     * @return Returns true if an access to the isctent LDAP should be
     *         performed. It returns false otherwise. This method returns the
     *         property set in the build configuration file, where one specifies
     *         if this should be enabled or not.
     */
    private static boolean checkIscteNtLdap() {
        return CHECK_LDAP3_ENABLED == null || "true".equals(CHECK_LDAP3_ENABLED);
    }

    /**
     * 
     * @return Returns true at least one LDAP should be checked.
     */
    public static boolean checkLdaps() {
        return checkHorusLdap() || checkDc1Ldap() || checkIscteNtLdap();
//                || LdapConfig.STUDENTS.check() || LdapConfig.NOT_STUDENTS.check();
    }

//    // ------------------------------------------------------------------------
//    // The code below belongs depends on LdapUtilsGeneric that manages the new Active Directories
//    // ------------------------------------------------------------------------
//
//    public static void authenticateInAdStudents(final Person person, final String username, final String password)
//            throws ExcepcaoAutenticacao {
//        final LdapConfig ldapConfig = LdapConfig.STUDENTS;
//        if (!ldapConfig.check()) {
//            return;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(person.getUsername());
//        try {
//            boolean success = LDAPUtilsGeneric.isPasswordCorrectBySamAccountName(usernameWithoutSuffix, password, ldapConfig);
//            if (!success) {
//                throw new ExcepcaoAutenticacao();
//            }
//        } catch (NamingException e) {
//            throw new ExcepcaoAutenticacao(e);
//        }
//    }
//
//    public static void changePasswordInAdStudents(final String username, final String password) {
//        final LdapConfig ldapConfig = LdapConfig.STUDENTS;
//        if (!ldapConfig.check()) {
//            return;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(username);
//        LDAPUtilsGeneric.changePasswordInLdapBySamAccounName(usernameWithoutSuffix, password, ldapConfig);
//    }
//
//    public static boolean isUsernameInAdStudents(final Person person, final String username) throws NamingException {
//        final LdapConfig ldapConfig = LdapConfig.STUDENTS;
//        if (!ldapConfig.check()) {
//            return false;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(person.getUsername());
//        return LDAPUtilsGeneric.isUsernameInLdapBySamAccountName(usernameWithoutSuffix, ldapConfig)
//                && !LDAPUtilsGeneric.isAccountDisabledBySamAccountName(usernameWithoutSuffix, ldapConfig);
//    }
//
//    public static void authenticateInAdNotStudents(final Person person, final String username, final String password)
//            throws ExcepcaoAutenticacao {
//        final LdapConfig ldapConfig = LdapConfig.NOT_STUDENTS;
//        if (!ldapConfig.check()) {
//            return;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(person.getUsername());
//        try {
//            boolean success = LDAPUtilsGeneric.isPasswordCorrectBySamAccountName(usernameWithoutSuffix, password, ldapConfig);
//            if (!success) {
//                throw new ExcepcaoAutenticacao();
//            }
//        } catch (NamingException e) {
//            throw new ExcepcaoAutenticacao(e);
//        }
//    }
//
//    public static void changePasswordInAdNotStudents(final String username, final String password) {
//        final LdapConfig ldapConfig = LdapConfig.NOT_STUDENTS;
//        if (!ldapConfig.check()) {
//            return;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(username);
//        LDAPUtilsGeneric.changePasswordInLdapBySamAccounName(usernameWithoutSuffix, password, ldapConfig);
//    }
//
//    public static boolean isUsernameInAdNotStudents(final Person person, final String username) throws NamingException {
//        final LdapConfig ldapConfig = LdapConfig.NOT_STUDENTS;
//        if (!ldapConfig.check()) {
//            return false;
//        }
//
//        final String usernameWithoutSuffix = User.getUsernameNoDomain(person.getUsername());
//        return LDAPUtilsGeneric.isUsernameInLdapBySamAccountName(usernameWithoutSuffix, ldapConfig)
//                && !LDAPUtilsGeneric.isAccountDisabledBySamAccountName(usernameWithoutSuffix, ldapConfig);
//    }
//    // ------------------------------------------------------------------------

    private static final String getUsernameNoDomain(final String username) {
        if (username != null && username.contains("@")) {
            String usernameNoDomain = username.split("@")[0];
            return usernameNoDomain;
        }
        return username;
    }

}