package test.ad.util;

/**
 * F�nix project class.
 */
public enum ServiceTypeEnum {

	HORUS, DC1, ISCTENT, NEFTIS, BLACKBOARD, INDEG, GOOGLE;

	public String getName() {
		return name();
	}

}