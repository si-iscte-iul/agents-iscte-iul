package test.ad.util;

import java.util.ArrayList;
import java.util.List;

/**
 * This class can be used to take snapshots of the time taken for a particular
 * area of code to execute.
 * 
 * @author Ant�nio Casqueiro
 */
public class DebugChronometer {
	private long basetime;
	private long lastMeasuredTime;
	private int sequenceId;
	private List<Pair<String, Long>> list = new ArrayList<Pair<String, Long>>();

	private static final String NL = System.getProperty("line.separator");

	public DebugChronometer() {
		basetime = System.currentTimeMillis();
		lastMeasuredTime = basetime;
	}

	/**
	 * Record a sample. It uses an auto-increment identifier.
	 */
	public void measure() {
		list.add(new Pair<String, Long>(String.valueOf(sequenceId++), getTime()));
	}

	/**
	 * Record a sample.
	 * 
	 * @param message
	 *            identifier of the sample
	 */
	public void measure(String message) {
		list.add(new Pair<String, Long>(message, getTime()));
	}

	private Long getTime() {
		long now = System.currentTimeMillis();
		Long time = new Long(now - lastMeasuredTime);
		lastMeasuredTime = now;
		return time;
	}

	/**
	 * Return a <code>String</code> with information about the time measures.
	 * 
	 * @return formated output
	 */
	public String report() {
		StringBuilder sb = new StringBuilder();
		sb.append(NL);
		sb.append("----");
		sb.append(NL);
		for (Pair<String, Long> elem : list) {
			sb.append(elem.getFirst()).append(":").append(elem.getSecond())
					.append(" ms").append(NL);
		}
		sb.append("-Total:").append(getTotalTime()).append("ms-");
		sb.append(NL);
		return sb.toString();
	}

	/**
	 * Return a <code>String</code> with information about the time measures.
	 * 
	 * @return formated output
	 */
	public String reportSingleLine() {
		StringBuilder sb = new StringBuilder();
		for (Pair<String, Long> elem : list) {
			sb.append(elem.getFirst()).append(":").append(elem.getSecond())
					.append(" ms").append(" || ");
		}
		sb.append("-Total:").append(getTotalTime()).append("ms-");
		return sb.toString();
	}

	/**
	 * Return the elapsed time.
	 * 
	 * @return time from start to last measure in milliseconds.
	 */
	public long getTotalTime() {
		return lastMeasuredTime - basetime;
	}

}
