package test.ad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.NamingException;

import test.ad.util.DebugChronometer;
import test.ad.util.ExcepcaoAutenticacao;
import test.ad.util.LdapUtils;
import test.ad.util.Pair;
import test.ad.util.PropertiesManager;
import test.ad.util.ServiceTypeEnum;

/**
 * Test active directory authentication class.
 * 
 * @author Antonio Casqueiro
 */
public class TestActiveDirectory {
    private static String NL = System.lineSeparator();

    private static final Logger logger = Logger.getLogger(TestActiveDirectory.class.getName());

    /**
     * Constructor. Initialize log level.
     */
    public TestActiveDirectory() {
        for (Handler handler : logger.getHandlers()) {
            handler.setLevel(Level.FINE);
        }
        logger.setLevel(Level.FINE);

        for (Handler handler : logger.getParent().getHandlers()) {
            handler.setLevel(Level.FINE);
        }
        logger.getParent().setLevel(Level.FINE);
    }

    /**
     * The execution starts here.
     * 
     * @param args 0- Change password flag. If true changes the password, otherwise checks if the user has the provided password.
     */
    public static void main(String[] args) {
        final int times = Integer.parseInt(PropertiesManager.getProperty("repeat"));
        final String username = PropertiesManager.getProperty("user.username");
        final String password = PropertiesManager.getProperty("user.password");
//        args = new String[] { "true" };
//        args = new String[] { "false" };
        if (args.length != 1) {
            // Since somehow the message in the exception is not displayed
            // in the console, force it!
            String msg = "You must supply 1 argument " + "'<changePassword> " + NL
                    + "Example to Only check if the password is it's the right one: java -cp TestActiveDirectory.jar test.ad.TestActiveDirectory false"
                    + NL + "Example to Change the password: java -cp TestActiveDirectory.jar test.ad.TestActiveDirectory true"
                    + NL + NL;
            System.out.println(msg);
            throw new IllegalArgumentException(msg);
        }

        boolean changePassword = Boolean.parseBoolean(args[0]);
        for (int i = 1; i <= times; i++) {
            if (changePassword) {
                TestActiveDirectory.changePassword(username, password, i);
            } else {
                TestActiveDirectory.checkPassword(username, password, i);
            }
        }
    }

    /**
     * Simple test method. Not being used in this test.
     * 
     * @param username
     *            the user login
     * @param password
     *            the user password
     * @param interation
     *            number of try
     * @return not relevant for testing
     */
    @SuppressWarnings("unused")
    private void simpleTest(final String username, final String password, final int interation) {
        try {
            logger.info("Itera��o " + interation);
            LdapUtils.authenticateInAllLdaps(username, password);
            logger.info("Sucesso " + interation);
        } catch (ExcepcaoAutenticacao e) {
            e.printStackTrace();
            logger.info("Falhou " + interation);
        }
    }

    /**
     * F�nix check password uniformization method.<br/>
     * I've disabled the POP, BLACKBOARD 8.1, INDEG AND GOOGLE calls.
     * 
     * @param username
     *            the user login
     * @param password
     *            the user password
     * @param iteration
     *            number of try
     * @return not relevant for testing
     */
    private static Map<String, Pair<List<String>, List<String>>> checkPassword(final String username, final String password,
            final int iteration) {
        logger.info("Start iteration " + iteration);

        final Map<String, Pair<List<String>, List<String>>> wrongPasswordsMap =
                new HashMap<String, Pair<List<String>, List<String>>>();
        final Set<String> usernamesToVerify = new HashSet<String>();

        usernamesToVerify.add(username);

        final DebugChronometer dc = new DebugChronometer();

        for (String usernameToVerify : usernamesToVerify) {
            List<String> wrongPasswordServices = new ArrayList<String>();
            List<String> servicesWithoutAccount = new ArrayList<String>();
            try {
                if (LdapUtils.isUsernameInHorusLdap(usernameToVerify) && !LdapUtils.isAccountDisabledInHorus(usernameToVerify)) {
                    try {
                        LdapUtils.unlockAccountInHorusLdap(usernameToVerify);
                        LdapUtils.authenticateInHorusLdap(usernameToVerify, password);
                        logger.info("success|authenticateInHorusLdap");
                    } catch (ExcepcaoAutenticacao e) {
                        logger.info("failed|authenticateInHorusLdap");
                        wrongPasswordServices.add(ServiceTypeEnum.HORUS.getName());
                    } catch (Exception e) {
                        logger.info("exception|authenticateInHorusLdap");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInHorusLdap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.authenticateInHorusLdap");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.HORUS.getName());
                }

                if (LdapUtils.isUsernameInDc1Ldap(usernameToVerify) && !LdapUtils.isAccountDisabledInDC1(usernameToVerify)) {
                    try {
                        LdapUtils.unlockAccountInDc1Ldap(usernameToVerify);
                        LdapUtils.authenticateInDc1Ldap(usernameToVerify, password);
                        logger.info("success|authenticateInDc1Ldap");
                    } catch (ExcepcaoAutenticacao e) {
                        wrongPasswordServices.add(ServiceTypeEnum.DC1.getName());
                        logger.info("failed|authenticateInDc1Ldap");
                    } catch (Exception e) {
                        logger.info("exception|authenticateInDc1Ldap");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInDc1Ldap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.authenticateInDc1Ldap");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.DC1.getName());
                }
                if (LdapUtils.isUsernameInIscteNtLdap(usernameToVerify)
                        && !LdapUtils.isAccountDisabledInIscteNt(usernameToVerify)) {
                    try {
                        LdapUtils.unlockAccountInIscteNtLdap(usernameToVerify);
                        LdapUtils.authenticateInIscteNt(usernameToVerify, password);
                        logger.info("success|authenticateInIscteNt");
                    } catch (ExcepcaoAutenticacao e) {
                        logger.info("failed|authenticateInIscteNt");
                        wrongPasswordServices.add(ServiceTypeEnum.ISCTENT.getName());
                    } catch (Exception e) {
                        logger.info("exception|authenticateInIscteNt");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInIscteNtLdap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.authenticateInIscteNt");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.ISCTENT.getName());
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }

            wrongPasswordsMap.put(usernameToVerify,
                    new Pair<List<String>, List<String>>(wrongPasswordServices, servicesWithoutAccount));
        }

        logger.info("End iteration " + iteration + " time for user: " + username + " | " + dc.reportSingleLine());

        return wrongPasswordsMap;
    }

    /**
     * Fenix change password in all AD's.
     * 
     * @param username
     *            the user login
     * @param password
     *            the user password
     * @param iteration
     *            number of try
     * @return not relevant for testing
     */
    private static Map<String, Pair<List<String>, List<String>>> changePassword(final String username, final String password,
            final int iteration) {
        logger.info("Start iteration " + iteration);

        final Map<String, Pair<List<String>, List<String>>> wrongPasswordsMap =
                new HashMap<String, Pair<List<String>, List<String>>>();
        final Set<String> usernamesToVerify = new HashSet<String>();

        usernamesToVerify.add(username);

        final DebugChronometer dc = new DebugChronometer();

        for (String usernameToVerify : usernamesToVerify) {
            List<String> wrongPasswordServices = new ArrayList<String>();
            List<String> servicesWithoutAccount = new ArrayList<String>();
            try {
                if (LdapUtils.isUsernameInHorusLdap(usernameToVerify) && !LdapUtils.isAccountDisabledInHorus(usernameToVerify)) {
                    try {
                        logger.fine("before|changePasswordInHorusLdap");
                        LdapUtils.changePasswordInHorusLdap(usernameToVerify, password);
                        logger.info("after|changePasswordInHorusLdap");
                    } catch (Exception e) {
                        logger.info("exception|changePasswordInHorusLdap");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInHorusLdap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.changePasswordInHorusLdap");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.HORUS.getName());
                }

                if (LdapUtils.isUsernameInDc1Ldap(usernameToVerify) && !LdapUtils.isAccountDisabledInDC1(usernameToVerify)) {
                    try {
                        logger.fine("before|changePasswordInDc1Ldap");
                        LdapUtils.changePasswordInDc1Ldap(usernameToVerify, password);
                        logger.info("after|changePasswordInDc1Ldap");
                    } catch (Exception e) {
                        logger.info("exception|changePasswordInDc1Ldap");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInDc1Ldap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.changePasswordInDc1Ldap");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.DC1.getName());
                }
                if (LdapUtils.isUsernameInIscteNtLdap(usernameToVerify)
                        && !LdapUtils.isAccountDisabledInIscteNt(usernameToVerify)) {
                    try {
                        logger.info("before|changePasswordInRedeNtLdap");
                        LdapUtils.changePasswordInRedeNtLdap(usernameToVerify, password);
                        logger.info("after|changePasswordInRedeNtLdap");
                    } catch (Exception e) {
                        logger.info("exception|changePasswordInRedeNtLdap");
                        e.printStackTrace();
                    } finally {
//                        try {
//                            LdapUtils.unlockAccountInIscteNtLdap(usernameToVerify);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                    }
                    dc.measure("LdapUtils.changePasswordInRedeNtLdap");
                    logger.fine(dc.reportSingleLine());
                } else {
                    servicesWithoutAccount.add(ServiceTypeEnum.ISCTENT.getName());
                }
            } catch (NamingException e) {
                e.printStackTrace();
            }

            wrongPasswordsMap.put(usernameToVerify,
                    new Pair<List<String>, List<String>>(wrongPasswordServices, servicesWithoutAccount));
        }

        logger.info("End iteration " + iteration + " time for user: " + username + " | " + dc.reportSingleLine());

        return wrongPasswordsMap;
    }

}
