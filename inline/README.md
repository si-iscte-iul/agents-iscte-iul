Web app that display tickets from inline.
==============
Display the tickets from inline web service.

## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Configuration**
To configure the application change the *src/main/resources/configuration.properties* file.

**Debug**
To debug define first this environment variable:
export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n"

**Run embedded tomcat**
mvn clean tomcat6:run

**Run embedded tomcat on different port**
mvn clean tomcat6:run -Dmaven.tomcat.port=8082

# Deployment
Generate war
mvn clean package

War is generated on
target/inline-webapp-<version>.war

# Dev Deployment to tomcat
Put the following configuration, on tomcat, within the conf/tomcat-users.xml file.
<tomcat-users>
 <role rolename="manager"/>
 <role rolename="admin"/>
 <user username="admin" password="password" roles="admin,manager,manager-script,manager-status,manager-gui"/>
</tomcat-users>

Then restart tomcat
mvn clean package tomcat6:deploy
mvn clean package tomcat6:redeploy

