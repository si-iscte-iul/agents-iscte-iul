package pt.iscte.myticket.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;

public class PropertiesManager {
    private static final Logger logger = Logger.getLogger(PropertiesManager.class);

    private static final Properties properties = new Properties();
    static {
        loadProperties(properties, "/configuration.properties");
    }

    public static String getProperty(String s) {
        return properties.getProperty(s);
    }

    public static int getIntegerProperty(String s) {
        String value = getProperty(s);
        return Integer.parseInt(value);
    }

    public static Properties loadProperties(String string) {
        return properties;
    }

    public static void loadProperties(Properties properties, String filename) {
        try {
            InputStream s = PropertiesManager.class.getResourceAsStream(filename);
            InputStreamReader isr = new InputStreamReader(s, Charset.forName("UTF-8"));
            properties.load(isr);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getProperty(String property, String defaultValueInCaseOfMissing) {
        String value = getProperty(property);
        if (Strings.isNullOrEmpty(value)) {
            logger.error(String.format("Configuration not found for property: %s", property));
            return defaultValueInCaseOfMissing;
        } else {
            return value;
        }
    }

    public static int getIntegerProperty(String property, int defaultValueInCaseOfMissing) {
        String value = getProperty(property);
        if (Strings.isNullOrEmpty(value)) {
            logger.error(String.format("Configuration not found for property: %s", property));
            return defaultValueInCaseOfMissing;
        } else {
            return Integer.parseInt(value);
        }
    }
}
