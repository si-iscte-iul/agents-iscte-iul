package pt.iscte.myticket.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

/**
 * Represents all the data to be displayed on view.
 *
 * @author Ivo Branco
 *
 */
public class Board {
    private static final SimpleDateFormat CREATION_DATE_STR_FORMAT = new SimpleDateFormat("HH:mm:ss");
    private List<Ticket> tickets;
    private List<Service> services;
    private Date lastUpdateDate;

    public List<Ticket> getTickets() {
        return tickets;
    }

    public List<Ticket> getTicketsToDisplay() {
        List<Service> servicesOrderedByLastTicketDate = Service.comparatorByLastTicketDate().reverse().sortedCopy(this.services);
        return Lists.newArrayList(Iterables.transform(servicesOrderedByLastTicketDate, Service.serviceToLastTicket()));
    }

    public void update(List<Ticket> tickets) {
        this.lastUpdateDate = new Date();
        this.tickets = tickets;
        updateServices();
    }

    private void updateServices() {
        this.services = new ArrayList<Service>();
        for (Ticket ticket : this.tickets) {
            String serviceName = ticket.getServiceName();
            Service service = Iterables.find(this.services, Service.serviceByName(serviceName), null);
            if (service == null) {
                service = new Service(serviceName);
                this.services.add(service);
            }
            service.addTicket(ticket);
        }
    }

    public String getLastUpdateDateAsSrt() {
        return CREATION_DATE_STR_FORMAT.format(this.lastUpdateDate);
    }

    public Date getLastUpdateDate() {
        return this.lastUpdateDate;
    }
}
