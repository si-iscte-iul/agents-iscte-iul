package pt.iscte.myticket.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pt.iscte.myticket.util.PropertiesManager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Ordering;

/**
 * Represents a {@link Ticket} on a Queue.
 *
 * <p>
 * Example: number 10, for desk 01, to 1st Cycle queue and 'Academic Service' service.
 * </p>
 *
 * @author Ivo Branco
 *
 */
public class Ticket {

    private static final String DISPLAY_CLIENT_REFRESH_SECONDS = "display.client.refresh.seconds";
    private static final String DESK = "AttendanceTableName";
    private static final String NUMBER = "AttendanceDescription";
    /**
     * Service destiny
     */
    private static final String SERVICE_LETTER = "ToAttendanceServiceDescription"; // "AttendanceServiceDescription"
    private static final String DATE = "Date";

    private String serviceName;
    private String number;
    private String desk;
    private Date date;

    public String getDesk() {
        return this.desk;
    }

    public String getServiceName() {
        return serviceName;
    }

    private void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getNumber() {
        return number;
    }

    private void setNumber(String number) {
        this.number = number;
    }

    private void setDesk(String desk) {
        this.desk = desk;
    }

    public Ticket addAttribute(String attributeName, String value) {
        if (SERVICE_LETTER.equals(attributeName)) {
            setServiceName(value);
        } else if (NUMBER.equals(attributeName)) {
            setNumber(value);
        } else if (DESK.equals(attributeName)) {
            setDesk(value);
        } else if (DATE.equals(attributeName)) {
            setDate(value);
        }
        return this;
    }

    private void setDate(String value) {
        try {
            this.date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(value);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    @JsonIgnore
    public Long getHighlightTimeStamp() {
        Integer milis = PropertiesManager.getIntegerProperty(DISPLAY_CLIENT_REFRESH_SECONDS, 5) * 1000;
        return getDate().getTime() + milis;
    }

    public Date getDate() {
        return date;
    }

    public static Ordering<Ticket> comparatorByDate() {
        return new Ordering<Ticket>() {
            @Override
            public int compare(Ticket left, Ticket right) {
                return left.getDate().compareTo(right.getDate());
            }
        };
    }

    public static Ordering<Ticket> comparatorByQueueName() {
        return new Ordering<Ticket>() {
            @Override
            public int compare(Ticket left, Ticket right) {
                return left.getServiceName().compareTo(right.getServiceName());
            }
        };
    }
}
