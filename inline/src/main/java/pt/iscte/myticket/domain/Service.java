package pt.iscte.myticket.domain;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Ordering;

public class Service {

    private String name;
    private Set<Ticket> tickets;

    public Service(String name) {
        this.name = name;
        this.tickets = new HashSet<Ticket>();
    }

    public Ticket getLastTicket() {
        if (!this.tickets.isEmpty()) {
            return Ticket.comparatorByDate().max(this.tickets);
        } else {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public static Predicate<Service> serviceByName(final String name) {
        return new Predicate<Service>() {
            @Override
            public boolean apply(Service s) {
                return name.equals(s.getName());
            }
        };
    }

    public void addTicket(Ticket ticket) {
        this.tickets.add(ticket);
    }

    public static Ordering<Service> comparatorByLastTicketDate() {
        return new Ordering<Service>() {
            @Override
            public int compare(Service left, Service right) {
                return Ticket.comparatorByDate().compare(left.getLastTicket(), right.getLastTicket());
            }
        };
    }

    public static Function<Service, Ticket> serviceToLastTicket() {
        return new Function<Service, Ticket>() {
            @Override
            public Ticket apply(Service s) {
                return s.getLastTicket();
            }
        };
    }
}
