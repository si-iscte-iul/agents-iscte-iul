package pt.iscte.myticket;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import pt.iscte.myticket.domain.Board;
import pt.iscte.myticket.domain.Ticket;
import pt.iscte.myticket.util.PropertiesManager;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class MyticketService {
    private static final Logger logger = Logger.getLogger(MyticketService.class);
    private static final String DISPLAY_CACHE_SECONDS = "display.cache.seconds";
    private static final String DISPLAY_FILE_LINK = "display.file.link.calls";

    //////////
    // Singleton - begin
    private static MyticketService myticketService;

    public static MyticketService getInstance() {
        if (myticketService == null) {
            init();
        }
        return myticketService;
    }

    private static synchronized void init() {
        if (myticketService == null) {
            myticketService = new MyticketService();
        }
    }

    // Singleton - end
    //////////

    //////////
    // Cache - begin
    private static final String CACHE_KEY = "Ticket";
    private LoadingCache<String, Board> boardCache = CacheBuilder.newBuilder() //
            .expireAfterWrite(PropertiesManager.getIntegerProperty(DISPLAY_CACHE_SECONDS), TimeUnit.SECONDS) //
            .build(new CacheLoader<String, Board>() {
                @Override
                public Board load(String key) {
                    return createOrUpdateBoard();
                }
            });
    private Board board;

    private Board createOrUpdateBoard() {
        if (this.board == null) {
            this.board = new Board();
        }
        this.board.update(getTickets());
        return this.board;
    }

    public Board getBoard() {
        return boardCache.getUnchecked(CACHE_KEY);
    }

    private List<Ticket> getTickets() {
        try {
            logger.debug("Getting tickets - Before invoking inline");

            URL website = new URL(PropertiesManager.getProperty(DISPLAY_FILE_LINK));
            final InputStream is = website.openStream();

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(is);

            // normalize text representation
            doc.getDocumentElement().normalize();
            NodeList notificationEvents = doc.getElementsByTagName("NotificationEvent");

            List<Ticket> tickets = new ArrayList<Ticket>();
            for (int i = 0; i < notificationEvents.getLength(); i++) {
                Node ticketAsNode = notificationEvents.item(i);

                NamedNodeMap attributes = ticketAsNode.getAttributes();

                Ticket ticket = new Ticket();
                for (int j = 0; j < attributes.getLength(); j++) {
                    Node attribute = attributes.item(j);
                    String attributeName = attribute.getNodeName();
                    String value = attribute.getNodeValue();

                    ticket.addAttribute(attributeName, value);
                }
                tickets.add(ticket);
            }
            logger.debug("Getting tickets - done.");
            return tickets;
        } catch (RemoteException e) {
            e.printStackTrace();
            logger.error("Remote error", e);
            return Collections.emptyList();
        } catch (IOException e) {
            e.printStackTrace();
            logger.error("IO exception", e);
            return Collections.emptyList();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            logger.error("Error parsing xml document", e);
            return Collections.emptyList();
        } catch (SAXException e) {
            e.printStackTrace();
            logger.error("Error parsing xml document (SAXException)", e);
            return Collections.emptyList();
        }
    }

}
