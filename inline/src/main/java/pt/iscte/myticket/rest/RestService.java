package pt.iscte.myticket.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import pt.iscte.myticket.MyticketService;
import pt.iscte.myticket.domain.Board;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.jaxrs.annotation.JacksonFeatures;

@Path("/tickets")
public class RestService {

    @JacksonFeatures(serializationEnable = { SerializationFeature.INDENT_OUTPUT },
            serializationDisable = { SerializationFeature.WRITE_DATES_AS_TIMESTAMPS })
    @GET
    @Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
    public Board getTrackInJSON() {
        return MyticketService.getInstance().getBoard();
    }
}
