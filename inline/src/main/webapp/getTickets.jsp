<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="pt.iscte.myticket.util.PropertiesManager"%>
<%@ page import="pt.iscte.myticket.MyticketService"%>

<%
	request.setAttribute("board", MyticketService.getInstance().getBoard());
    request.setAttribute("tickets", MyticketService.getInstance().getBoard().getTicketsToDisplay());
    Long previousUpdateDate = request.getParameter("previousUpdateDate") != null ? Long.valueOf(request.getParameter("previousUpdateDate")) : null;
%>
<div id="ticketBoardLastUpdateDate" class="ticketBoardLastUpdateDate" data-ticketBoardLastUpdateDateTimestamp="${board.lastUpdateDate.time}"><span>Data última actualização</span> / <span>Last update date</span>: ${board.lastUpdateDateAsSrt}</div>
<h2>
	Quadro (últimas chamadas) / <span class="langEN">Board (last calls)</span>
</h2>
<div class="myticketSiteLink">myticket.iscte-iul.pt</div>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th>Serviço <br /> <span class="langEN">Service</span></th>
		<th>Número <br /> <span class="langEN">Number</span></th>
		<th>Mesa <br /> <span class="langEN">Desk</span></th>
	</tr>
	<tbody>
		<c:forEach var="ticket" items="${tickets}">
			<tr>
				<c:set var="future" value="${ticket.highlightTimeStamp}" />
                <%
                String newTicketCss = previousUpdateDate != null && previousUpdateDate < ((Long)pageContext.getAttribute("future")) ? "newTicket" : "oldTicket";
                %>
				<td class="ticketNumber <%= newTicketCss %>">${ticket.number}</td>
				<td class="ticketDesk <%= newTicketCss %>">${ticket.desk}</td>
				<td class="ticketQueue">
					<span>
						${ticket.serviceName}
					</span>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
