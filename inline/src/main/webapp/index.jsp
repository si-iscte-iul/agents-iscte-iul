<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="pt.iscte.myticket.util.PropertiesManager"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link rel="stylesheet" type="text/css" href="css/main.css" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/images/favicon.ico" type="image/x-icon" />
<script src="js/jquery/2.0.3/jquery.min.js"></script>
<jsp:include page="googleAnalytics.jsp" />
<script>
function reloadTickets() {
	var previousUpdateDate = $("#ticketBoardLastUpdateDate").attr("data-ticketBoardLastUpdateDateTimestamp");
	$.ajax({
		url: "getTickets.jsp",
		data : {previousUpdateDate : previousUpdateDate},
		success: function(data) {
			$( "#tickets" ).html(data);
		}, error: function() {
			$("#tickets").html("<p class='error'><span class='langPT'>Erro a ler os tickets</span> / <span class='langEN'>Error getting the tickets</span></p>");
		}
	});
}
window.setInterval(reloadTickets, <%= PropertiesManager.getIntegerProperty("display.client.refresh.seconds", 5) * 1000 %>);

window.onload=reloadTickets;
</script>

</head>
<body>
	<div class="container">
		<div class="logo">
			<img alt="ISCTE-IUL Tickets"
				src="<%=request.getContextPath()%>/images/logo_tickets.png" />
		</div>

		<div id="tickets" class="tickets">
			<p class="info">Actualizando Quadro / Updating Board</p>
		</div>

	</div>
</body>
</html>
