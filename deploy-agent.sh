#!/bin/bash
# -xv
#
# -------------------------------------
# Disable SVN, because Google Code was discontinued and now we are using GIT in BitBucket.
# Antonio Casqueiro - 2015-09-17
# -------------------------------------
#
# Fenix Agents Deploy Script
# Author: Ivo Branco 
# Company: ISCTE-IUL (www.iscte-iul.pt)
# Contact: ivo.branco@iscte-iul.pt
# 
# Based on work of:
# Fenix Deploy Script
# Author: Paulo Abrantes 
# Company: QUB-IT (www.qub-it.com)
# Contact: paulo.abrantes@qub-it.com
#
# Script's arguments: revision to deploy, if no arguments are 
# supplied the most recent version is used.
# Please take your time to configure the variables below
#
# 25 - Jun - 2013
#
# Arguments:
#	1. agent project name like 'google-apps-agent'
#	2. revision (optional)

trap cleanup 1 2 3 6

cleanup()
{
	echo "Caught Signal ... cleaning up."
	if [ pgrep monit  -ne 0 ]
	then
		sudo su -c "/etc/init.d/monit start"
	fi
	echo "Done cleanup ... quitting."
	exit 1
}

######################################
# CONFIGURATION VARIABLES START HERE #
######################################
#
# Agents Source Variables
SOURCE_BASE_DIR=/srv/agentes/agents-source
PROJ=$1
SOURCE_PROJ_DIR=$SOURCE_BASE_DIR/$PROJ
SOURCE_PROJ_TARGET_DIR=${SOURCE_PROJ_DIR}/target
#
# Configuration Variables
CONF_BASE_DIR=/srv/agentes/agents-conf
CONF_PROPERTIES_FILE=$CONF_BASE_DIR/$PROJ/configuration.properties
DEPLOY_LOG_DIR=$CONF_BASE_DIR/$PROJ
#
# Tomcat variables
TOMCAT_VERSION=tomcat7
TOMCAT_DIR=/var/lib/${TOMCAT_VERSION}
TOMCAT_WAR_DEPLOY_NAME=$PROJ.war
TOMCAT_WAR_DEPLOY_EXPLODED=$PROJ
TOMCAT_WAR_PATH=$TOMCAT_DIR/webapps/$TOMCAT_WAR_DEPLOY_NAME
TOMCAT_WAR_EXPLODED_PATH=$TOMCAT_DIR/webapps/${TOMCAT_WAR_DEPLOY_EXPLODED}
#
# Svn Variables
SVN_SERVER=fenix-iscte-iul.googlecode.com
SVN_PATH=svn/agents
SVN_USERNAME=
SVN_REVISION=$2
#
####################################
# CONFIGURATION VARIABLES END HERE #
####################################
#
#
# Unless you're pretty sure about what you're doing
# DO NOT edit anything below this comment.
#

function updateOrCreateCheckout {
	ARGS="";
		if [ -n "$SVN_REVISION" ]; then
		ARGS="-r $SVN_REVISION";
	fi
	if [ ! -d $SOURCE_BASE_DIR ]; then
		svn co https://$SVN_USERNAME@$SVN_SERVER/$SVN_PATH $SOURCE_BASE_DIR $ARGS;
	else 
		cd $SOURCE_PROJ_DIR;
		svn update $ARGS;
	fi
	checkProjectSourceDirectory;
}

function stopAS {
	# Desactivate monit, otherwise it will start tomcat
	sudo su -c "/etc/init.d/monit stop"
	sudo su -c "/etc/init.d/${TOMCAT_VERSION} stop"
}

function deployInAS {
	SOURCE_WAR_PATH=`findWarPath`
	# cp is correct!
	sudo su -c "cp $TOMCAT_WAR_PATH $TOMCAT_WAR_PATH.old";
	sudo su -c "rm -rf $TOMCAT_WAR_EXPLODED_PATH"
	sudo su -c "cp $SOURCE_WAR_PATH $TOMCAT_WAR_PATH";
	sudo su -c "unzip -q $SOURCE_WAR_PATH -d $TOMCAT_WAR_EXPLODED_PATH";
}

function headShotCatalina {
	sudo su -c "pkill -9 -f catalina"
}

function startAS {
	# Let's make sure that no catalina 
	# zombie of defunct processes are 
	# still haunting our server 
	headShotCatalina;
	
	# Wait 10 seconds
	sleep 10;

	sudo su -c "/etc/init.d/${TOMCAT_VERSION} start"

	# Reactivate monit as a watchdog
	sudo su -c "/etc/init.d/monit start"
}

function configure {
	cp $CONF_PROPERTIES_FILE $SOURCE_PROJ_DIR/src/main/resources/configuration.properties;
}

function tearDownConfiguration {
	rm $SOURCE_PROJ_DIR/src/main/resources/configuration.properties; 
}

function createWar {
	cd $SOURCE_PROJ_DIR;
	mvn clean package
	SOURCE_WAR_PATH=`findWarPath`
	if [ -z ${SOURCE_WAR_PATH} ];
	then
		echo "[ERROR]: War was not created! Empty. Exiting..."
		exit 0
	fi
	if [ ! -f ${SOURCE_WAR_PATH} ];
	then
		echo "[ERROR]: War was not created! Exiting..."
		exit 0
	fi
}

function logDeploy {
	VERSION=`svn info $SOURCE_PROJ_DIR | grep "Revision:" | cut -d ':' -f2`;
	DATE=`date +"%m-%d-%Y %H:%M"`;
	echo "Deploying revision ${VERSION} - ${DATE}" >>  $DEPLOY_LOG_DIR/deploy.log
}

function findWarPath {
	SOURCE_WAR_PATH=`find $SOURCE_PROJ_TARGET_DIR/*.war | head -1`
	echo $SOURCE_WAR_PATH
} 

function checkRequiredParams {
	if [ -z $PROJ ];
	then
		echo "[ERROR]: Project name argument is required..."
		exit 0
	fi
}

function checkProjectSourceDirectory {
	if [ ! -d $SOURCE_PROJ_DIR ];
	then
		echo "[ERROR]: Project directory not found... ${SOURCE_PROJ_DIR}"
		exit 0
	fi
}

function deploy {
	checkRequiredParams;
#	updateOrCreateCheckout;
	configure;	
	createWar;
	stopAS;
	deployInAS;
#	logDeploy;
	startAS;
	tearDownConfiguration;
} 

deploy $SVN_REVISION;

echo 0

