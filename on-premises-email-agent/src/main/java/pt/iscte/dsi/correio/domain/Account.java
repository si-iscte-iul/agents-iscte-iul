package pt.iscte.dsi.correio.domain;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CalendarDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.QuotaDefaultValue;
import pt.iscte.dsi.correio.aop.rules.required.Required;
import pt.iscte.dsi.correio.domain.entity.PostfixAlias;
import pt.iscte.dsi.correio.domain.entity.PostfixMailbox;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public abstract class Account implements Comparable<Account>, TranslatableToPersistentEntity {

    @Required
    private String name;

    private String quota = new QuotaDefaultValue().getDefaultValue();

    @Required
    private Address address;

    private Set<Address> legacyAddresses = new HashSet<Address>();

    private boolean isActive;

    private Calendar createDate = new CalendarDefaultValue().getDefaultValue();

    private Calendar changeDate = new CalendarDefaultValue().getDefaultValue();

    protected Account() {
    }

    protected Account(@Required final String name, final String quota, final @DefaultValue(
            calculator = CalendarDefaultValue.class) Calendar createDate, final @DefaultValue(
            calculator = CalendarDefaultValue.class) Calendar changeDate, @Required final Address address) {
        this.name = name;
        this.quota = quota;
        this.isActive = true;
        this.createDate = createDate;
        this.changeDate = changeDate;
        setAddress(address);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the quota
     */
    public String getQuota() {
        return quota;
    }

    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(String quota) {
        this.quota = quota;
    }

    /**
     * @return the createDate
     */
    public Calendar getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the changeDate
     */
    public Calendar getChangeDate() {
        return changeDate;
    }

    /**
     * @param changeDate
     *            the changeDate to set
     */
    public void setChangeDate(Calendar changeDate) {
        this.changeDate = changeDate;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
        this.address.setAccount(this);
    }

    public void setAddress(final String address) {
        this.address = new Address(address, null, null, null, isActive(), true);
        this.address.setAccount(this);
    }

    /**
     * @return the legacyAddresses
     */
    public Set<Address> getLegacyAddresses() {
        return legacyAddresses;
    }

    /**
     * @param legacyAddresses
     *            the legacyAddresses to set
     */
    public void setLegacyAddresses(Set<Address> legacyAddresses) {
        this.legacyAddresses = legacyAddresses;
    }

    public boolean hasLegacyAlias() {
        return getLegacyAddresses() != null && !getLegacyAddresses().isEmpty();
    }

    /**
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
        if (getAddress() != null) {
            getAddress().setActive(isActive);
        }
        for (final Address address : getLegacyAddresses()) {
            address.setActive(isActive);
        }
    }

    private void addLegacyAlias(final Address address) {
        getLegacyAddresses().add(address);
        address.setAccount(this);
    }

    /**
     * 
     * @param legacyAlias
     */
    public void addLegacyAlias(final String legacyAlias) {
        final Address address = new Address(legacyAlias, null, null, null, isActive(), false);
        addLegacyAlias(address);
    }

    /**
     * 
     * @param legacyAlias
     */
    public void addLegacyAlias(final PostfixAlias legacyAlias) {
        final Address address = new Address(legacyAlias.getAddress(), legacyAlias.getComments(), null, null, isActive(), false);
        addLegacyAlias(address);
    }

    protected void addLegacyAliases(final PostfixMailbox postfixMailbox) {
        for (final PostfixAlias postfixAlias : postfixMailbox.getPostfixAliases()) {
            if (postfixAlias.isLegacy()) {
                addLegacyAlias(postfixAlias);
            }
        }
    }

    /**
     * 
     * @param legacyAlias
     */
    public void removeLegacyAlias(final String legacyAlias) {
        if (hasLegacyAlias()) {
            final Address address = new Address(legacyAlias, null, null, null, isActive(), false);
            getLegacyAddresses().remove(address);
            address.removeAccount();
        }
    }

    public boolean isPersonalAccount() {
        return false;
    }

    @Override
    public String toString() {
        return "Name=" + getName() + ",Quota=" + getQuota();
    }

    public int compareTo(Account other) {
        return getAddress().compareTo(other.getAddress());
    }

    public void translate() {
        throw new UnsupportedOperationException();
    }
}