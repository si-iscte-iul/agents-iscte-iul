package pt.iscte.dsi.correio.aop.em;

import org.apache.log4j.Logger;

import pt.iscte.dsi.correio.utils.EntityManagerLocator;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @author Paulo Zenida
 * @author Ivo.Branco@iscte.pt
 * 
 */
public aspect EntityManagerRegistrator {

    private static final Logger logger = Logger.getLogger(EntityManagerRegistrator.class);

    declare parents : (@javax.jws.WebService *) implements EntityManageable;

    pointcut publicCall(): execution(public * (@javax.jws.WebService *).*(..));

    before(final EntityManageable entityManageable) :
	publicCall() &&
	target(entityManageable) {
	EntityManager entityManager = EntityManagerLocator.createEntityManager();
	EntityTransaction t = entityManager.getTransaction();
	if (!t.isActive()) {
	    t.begin();
	    logger.info("Creating transaction");
	} else {
	    logger.info("Reusing transaction");
	}
    }

    after() returning(Object o) : publicCall() {
	logger.info("Commiting");
	EntityManager entityManager = EntityManagerLocator.getCurrentEntityManager();
	EntityTransaction t = entityManager.getTransaction();
	t.commit();
    }

    after() throwing(Exception e) : publicCall() {
	logger.error("Rolling back", e);
	EntityTransaction t = EntityManagerLocator.getEntityTransaction();
	if (t.isActive()) {
	    t.rollback();
	}
    }

}