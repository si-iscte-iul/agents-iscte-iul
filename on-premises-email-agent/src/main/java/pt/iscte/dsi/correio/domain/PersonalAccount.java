package pt.iscte.dsi.correio.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;

import pt.iscte.dsi.correio.aop.persistence.translator.Translate;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CalendarDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.PasswordDefaultValue;
import pt.iscte.dsi.correio.aop.rules.required.Required;
import pt.iscte.dsi.correio.domain.entity.PostfixAlias;
import pt.iscte.dsi.correio.domain.entity.PostfixMailbox;
import pt.iscte.dsi.correio.domain.entity.RecordType;
import pt.iscte.dsi.correio.dto.AddressData;
import pt.iscte.dsi.correio.dto.PersonalAccountData;
import pt.iscte.dsi.correio.ejb.exception.AccountAlreadyExistingException;
import pt.iscte.dsi.correio.ejb.exception.NotExistingAccountException;
import pt.iscte.dsi.correio.utils.EntityManagerLocator;

public class PersonalAccount extends Account {

    @Required
    private String username;

    private String password = new PasswordDefaultValue().getDefaultValue();

    private Set<Address> forwards = new TreeSet<Address>();

    private Set<Address> aliases = new TreeSet<Address>();

    private boolean isToLeaveLocalCopy = false;

    private boolean isArchived = false;

    public PersonalAccount() {
    }

    private PersonalAccount(@Required final String name, final String quota, final @DefaultValue(
            calculator = CalendarDefaultValue.class) Calendar createDate, final @DefaultValue(
            calculator = CalendarDefaultValue.class) Calendar changeDate, @Required final Address address,
            @Required final String username, final String password) {
        super(name, quota, createDate, changeDate, address);
        this.username = username;
        this.password = password;
    }

    /**
     * 
     * @param accountData
     */
    private PersonalAccount(@Required PersonalAccountData accountData) {
        updateFrom(accountData);
        addAlias(accountData.getUsername());
    }

    /**
     * 
     * @param accountData
     */
    private PersonalAccount(@Required PostfixMailbox postfixMailbox) {
        this(postfixMailbox.getName(), postfixMailbox.getQuota(), postfixMailbox.getCreateDate(), postfixMailbox.getChangeDate(),
                new Address(postfixMailbox.getAddress()), postfixMailbox.getUsername(), postfixMailbox.getPassword());
        setToLeaveLocalCopy(postfixMailbox.isToLeaveLocalCopy());
        addAliases(postfixMailbox);
        addForwards(postfixMailbox);
        addLegacyAliases(postfixMailbox);
        setIsArchived(postfixMailbox.isArchived());
    }

    private void updateFrom(final PersonalAccountData accountData) {
        setQuota(accountData.getQuota());
        setName(accountData.getName());

        setUsername(accountData.getUsername());
        setPassword(accountData.getPassword());
        setToLeaveLocalCopy(accountData.isToLeaveLocalCopy());
        setAddress(accountData.getAddress().getAddress());
        setAliasesFromData(accountData.getAliases());
        setLegacyAddressesFromData(accountData.getLegacyAliases());
        setForwardsFromData(accountData.getForwards());
        // setCannonicalAliasesFromData(accountData.getForwards());
        setActive(accountData.isActive());
        setIsArchived(accountData.isArchived());
    }

    private void setAliasesFromData(Set<AddressData> aliases) {
        for (final AddressData addressData : aliases) {
            addAlias(new Address(addressData));
        }
    }

    private void setForwardsFromData(Set<AddressData> forwards) {
        for (final AddressData addressData : forwards) {
            addForward(new Address(addressData));
        }
    }

    private void setLegacyAddressesFromData(Set<AddressData> forwards) {
        for (final AddressData addressData : forwards) {
            addLegacyAddress(new Address(addressData));
        }
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the forwards
     */
    public Set<Address> getForwards() {
        return forwards;
    }

    /**
     * @param forwards
     *            the forwards to set
     */
    public void setForwards(Set<Address> forwards) {
        this.forwards = forwards;
    }

    /**
     * @return the aliases
     */
    public Set<Address> getAliases() {
        return aliases;
    }

    public Set<Address> getCannonicalAliases() {
        final Set<Address> result = new TreeSet<Address>();
        for (final Address address : getAliases()) {
            if (address.isCannonical()) {
                result.add(address);
            }
        }
        return result;
    }

    /**
     * @param aliases
     *            the aliases to set
     */
    public void setAliases(Set<Address> aliases) {
        this.aliases = aliases;
    }

    /**
     * @return the isToLeaveLocalCopy
     */
    public boolean isToLeaveLocalCopy() {
        return isToLeaveLocalCopy;
    }

    /**
     * @param isToLeaveLocalCopy
     *            the isToLeaveLocalCopy to set
     */
    public void setToLeaveLocalCopy(boolean isToLeaveLocalCopy) {
        this.isToLeaveLocalCopy = isToLeaveLocalCopy;
    }

    /**
     * @return the isArchived
     */
    public boolean isArchived() {
        return isArchived;
    }

    /**
     * @param isArchived
     *            the isArchived to set
     */
    public void setIsArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    /**
     * 
     * @param alias
     */
    public void addAlias(final String alias) {
        addAlias(new Address(alias, null, null, null, isActive(), false));
    }

    /**
     * 
     * @param address
     */
    public void addAlias(final Address address) {
        getAliases().add(address);
        address.setAccount(this);
    }

    /**
     * 
     * @param postfixAlias
     */
    private void addAlias(final PostfixAlias postfixAlias) {
        Address address = new Address(postfixAlias);
        getAliases().add(address);
        address.setAccount(this);
    }

    /**
     * 
     * @param alias
     */
    public void removeAlias(final String alias) {
        if (hasAlias()) {
            Address address = new Address(alias, null, null, null, isActive(), false);
            getAliases().remove(address);
            address.removeAccount();
        }
    }

    public boolean hasAlias() {
        return getAliases() != null && !getAliases().isEmpty();
    }

    /**
     * @param address
     */
    private void addLegacyAddress(final Address address) {
        getLegacyAddresses().add(address);
        address.setAccount(this);
    }

    // /**
    // * @param address
    // */
    // private void addCannonicalAlias(final Address address) {
    // getCannonicalAliases().add(address);
    // address.setAccount(this);
    // }

    /**
     * If a forward is added to the account, by default, no local copy should be
     * available.
     * 
     * @param address
     */
    private void addForward(final Address address) {
        getForwards().add(address);
        address.setAccount(this);
    }

    /**
     * 
     * @param legacyAddress
     */
    public void addLegacyAddress(final String legacyAddress) {
        final Address address = new Address(legacyAddress, null, null, null, isActive(), false);
        addForward(address);
    }

    /**
     * If a forward is added to the account, by default, make no local copy be
     * available.
     * 
     * @param forward
     */
    public void addForward(final String forward) {
        final Address address = new Address(forward, null, null, null, isActive(), false);
        addForward(address);
    }

    /**
     * 
     * @param forward
     */
    public void addForward(final PostfixAlias forward) {
        final Address address = new Address(forward);
        addForward(address);
    }

    /**
     * @param legacyAddress
     */
    public void removeLegacyAddress(final String legacyAddress) {
        if (hasLegacyAddress()) {
            final Address address = new Address(legacyAddress, null, null, null, isActive(), false);
            getLegacyAddresses().remove(address);
            address.removeAccount();
        }
    }

    /**
     * When a forward is removed and the account has no more forwards, then it
     * is supposed to make a local copy be available.
     * 
     * @param forward
     */
    public void removeForward(final String forward) {
        if (hasForward()) {
            final Address address = new Address(forward, null, null, null, isActive(), false);
            getForwards().remove(address);
            address.removeAccount();
            if (hasOnlyUsernameForward()) {
                removeForward(getUsername());
            }
            if (!hasForward()) {
                this.isToLeaveLocalCopy = false;
            }
        }
    }

    private boolean hasOnlyUsernameForward() {
        if (getForwards() != null && getForwards().size() == 1) {
            for (final Address address : getForwards()) {
                if (address.isCannonical()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean hasLegacyAddress() {
        return getLegacyAddresses() != null && !getLegacyAddresses().isEmpty();
    }

    public boolean hasForward() {
        return getForwards() != null && !getForwards().isEmpty();
    }

    @Override
    public boolean isPersonalAccount() {
        return true;
    }

    public void enable() {
        setActive(true);
    }

    public void disable() {
        setActive(false);
    }

    private void addAliases(final PostfixMailbox postfixMailbox) {
        for (final PostfixAlias postfixAlias : postfixMailbox.getPostfixAliases()) {
            if (postfixAlias.isAlias() && !postfixAlias.isMainAddress()) {
                addAlias(postfixAlias);
            }
        }
    }

    private void addForwards(final PostfixMailbox postfixMailbox) {
        for (final PostfixAlias postfixAlias : postfixMailbox.getPostfixAliases()) {
            if (postfixAlias.isForward()) {
                if (postfixAlias.getGoto().contains(",")) {
                    final String[] forwards = postfixAlias.getGoto().split(",");
                    for (final String forward : forwards) {
                        if (!forward.equals(getUsername())) {
                            addForward(forward);
                        }
                    }
                } else {
                    addForward(postfixAlias);
                }
            }
        }
    }

    public boolean hasCorrespondingAddress(PostfixAlias postfixAlias) {
        final RecordType recordType = postfixAlias.getRecordType();
        switch (recordType) {
        case ALIAS:
            if (postfixAlias.isMainAddress()) {
                final Address address = getAddress();
                return address.getAddress().equals(postfixAlias.getAddress());
            } else {
                return getAlias(postfixAlias.getAddress()) != null;
            }
        case FORWARD:
            return !getForwards().isEmpty();
        case FORWARD_WITH_COPY:
            return !getForwards().isEmpty();
        case LEGACY:
            return getLegacyAddress(postfixAlias.getAddress()) != null;
        default:
            return true;
        }
    }

    @Override
    public String toString() {
        return super.toString() + ",Username=" + getUsername() + ",Password=" + getPassword();
    }

    /**
     * This method translates a personal account into persisted entities. In
     * this case, the corresponding postfix mailbox and the postfix aliases.
     */
    @Override
    public void translate() {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        System.out.println("========================3 " + em.hashCode());
        PostfixMailbox postfixMailbox = PostfixMailbox.findByUsername(username);
        if (postfixMailbox == null) {
            postfixMailbox = new PostfixMailbox(this);
        } else {
            postfixMailbox.update(this);
        }
//	em.merge(postfixMailbox);
    }

    // static methods
    /**
     * 
     * @return PersonalAccount
     */
    public static PersonalAccount findByUsername(final String username) {
        final PostfixMailbox postfixMailbox = PostfixMailbox.findByUsername(username);
        if (postfixMailbox == null) {
            return null;
        }
        final PersonalAccount account = createFromMailbox(postfixMailbox);
        return account;
    }

    @Translate
    public static PersonalAccount create(final PersonalAccountData accountData) throws AccountAlreadyExistingException {
        if (findByUsername(accountData.getUsername()) != null) {
            throw new AccountAlreadyExistingException(accountData.getUsername());
        }
        final PersonalAccount personalAccount = new PersonalAccount(accountData);
        return personalAccount;
    }

    @Translate
    public static PersonalAccount disable(final String username) throws NotExistingAccountException {
        PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount == null) {
            throw new NotExistingAccountException(username);
        }
        personalAccount.disable();
        return personalAccount;
    }

    @Translate
    public static PersonalAccount enable(final String username) throws NotExistingAccountException {
        PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount == null) {
            throw new NotExistingAccountException(username);
        }
        personalAccount.enable();
        return personalAccount;
    }

    public static Collection<PersonalAccount> findAll() {
        final Collection<PostfixMailbox> postfixMailboxes = PostfixMailbox.findAll();
        final Collection<PersonalAccount> result = new ArrayList<PersonalAccount>();
        for (final PostfixMailbox postfixMailbox : postfixMailboxes) {
            result.add(createFromMailbox(postfixMailbox));
        }
        return result;
    }

    @Translate
    public static PersonalAccount addForward(String username, String address, final boolean isToLeaveLocalCopy)
            throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount != null) {
            personalAccount.setToLeaveLocalCopy(isToLeaveLocalCopy);
            personalAccount.addForward(address);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(username);
        }
    }

    @Translate
    public static PersonalAccount removeForward(String username, String address) throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount != null) {
            personalAccount.removeForward(address);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(username);
        }
    }

    @Translate
    public static PersonalAccount update(PersonalAccountData accountData) throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(accountData.getUsername());
        if (personalAccount != null) {
            personalAccount.updateFrom(accountData);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(accountData.getUsername());
        }
    }

    public Address getAlias(final String address) {
        for (final Address a : getAliases()) {
            if (a.getAddress().equals(address)) {
                return a;
            }
        }
        return null;
    }

    public Address getLegacyAddress(final String address) {
        for (final Address a : getLegacyAddresses()) {
            if (a.getAddress().equals(address)) {
                return a;
            }
        }
        return null;
    }

    private static PersonalAccount createFromMailbox(final PostfixMailbox postfixMailbox) {
        return new PersonalAccount(postfixMailbox);
    }

    @Translate
    public static PersonalAccount update(PersonalAccount personalAccount) {
        return personalAccount;
    }

    public static PersonalAccountData getPersonalAccountData(String username) {
        return PostfixMailbox.getPersonalAccountData(username);
    }

    @Translate
    public static PersonalAccount addAlias(String username, String address) throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount != null) {
            personalAccount.addAlias(address);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(username);
        }
    }

    @Translate
    public static PersonalAccount removeAlias(String username, String address) throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount != null) {
            personalAccount.removeAlias(address);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(username);
        }
    }

    @Translate
    public static PersonalAccount changeIsArchivedFlag(String username, boolean archive) throws NotExistingAccountException {
        final PersonalAccount personalAccount = findByUsername(username);
        if (personalAccount != null) {
            personalAccount.setIsArchived(archive);
            return personalAccount;
        } else {
            throw new NotExistingAccountException(username);
        }
    }
}