package pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValueCalculator;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class QuotaDefaultValue extends DefaultValueCalculator<String> {

    @Override
    public String getDefaultValue() {
        return "1000000000S";
    }
}