package pt.iscte.dsi.correio.ejb.personalaccount;

import java.util.Collection;

import javax.jws.WebService;

import pt.iscte.dsi.correio.domain.PersonalAccount;
import pt.iscte.dsi.correio.dto.PersonalAccountData;
import pt.iscte.dsi.correio.ejb.exception.AccountAlreadyExistingException;
import pt.iscte.dsi.correio.ejb.exception.NotExistingAccountException;
import pt.iscte.dsi.correio.ejb.exception.UsernameAsAliasAccountException;

@WebService
public interface PersonalAccountWS {

    public PersonalAccount create(final PersonalAccountData accountData) throws AccountAlreadyExistingException;

    public PersonalAccount disable(final String username) throws NotExistingAccountException;

    public PersonalAccount enable(final String username) throws NotExistingAccountException;

    public PersonalAccount update(final PersonalAccount personalAccount) throws NotExistingAccountException;

    public PersonalAccount find(final String username);

    public Collection<PersonalAccount> findAll();

    public PersonalAccount addForward(final String username, final String address, final boolean isToLeaveLocalCopy)
            throws NotExistingAccountException;

    public PersonalAccount removeForward(final String username, final String address) throws NotExistingAccountException;

    public void addAlias(final String username, final String address) throws NotExistingAccountException;

    public void removeAlias(final String username, final String address) throws NotExistingAccountException;

    public PersonalAccountData getPersonalAccountData(final String username) throws NotExistingAccountException;

    public void synchronizeOnPermiseEmail(final String username, final String name, final String mainEmailAddress,
            final boolean isGmailActive) throws NotExistingAccountException, UsernameAsAliasAccountException;

    public void fixAlias(final String username);

}