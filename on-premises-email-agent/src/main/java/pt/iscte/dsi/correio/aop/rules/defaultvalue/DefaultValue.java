package pt.iscte.dsi.correio.aop.rules.defaultvalue;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DefaultValue {

    public Class<? extends DefaultValueCalculator<? extends Object>> calculator();
}