package pt.iscte.dsi.correio.dto;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.PasswordDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.QuotaDefaultValue;
import pt.iscte.dsi.correio.aop.rules.required.Required;
import pt.iscte.dsi.correio.domain.entity.PostfixMailbox;

/**
 * 
 * @author Paulo Zenida
 * 
 */
@XmlRootElement
public class PersonalAccountData {

    @Required
    private String name;

    @Required
    private String username;

    private String quota = new QuotaDefaultValue().getDefaultValue();

    private String password = new PasswordDefaultValue().getDefaultValue();

    private boolean isActive = true;

    private AddressData address;

    private Set<AddressData> forwards = new HashSet<AddressData>();

    private Set<AddressData> legacyAliases = new HashSet<AddressData>();

    private Set<AddressData> aliases = new HashSet<AddressData>();

    private boolean isToLeaveLocalCopy;

    private boolean isArchived;

    public PersonalAccountData() {
    }

    /**
     * @param name
     * @param username
     * @param quota
     * @param password
     * @param address
     * @param isToLeaveLocalCopy
     * @param isArchived
     */
    public PersonalAccountData(@Required final String name, @Required final String username, final String quota,
            final String password, final AddressData address, final boolean isToLeaveLocalCopy, final boolean isArchived) {
        this(name, username, quota, password, address, isToLeaveLocalCopy, isArchived, null, null, null);
    }

    /**
     * @param name
     * @param username
     * @param quota
     * @param password
     * @param address
     * @param isToLeaveLocalCopy
     * @param isArchived
     * @param forwards
     * @param legacyAliases
     * @param aliases
     */
    public PersonalAccountData(@Required final String name, @Required final String username, final String quota,
            final String password, final AddressData address, final boolean isToLeaveLocalCopy, final boolean isArchived,
            final Set<AddressData> forwards, final Set<AddressData> legacyAliases, final Set<AddressData> aliases) {
        super();
        this.name = name;
        this.username = username;
        this.quota = quota;
        this.password = password;
        this.address = address;
        this.isToLeaveLocalCopy = isToLeaveLocalCopy;
        this.isArchived = isArchived;
        if (forwards != null)
            this.forwards = forwards;
        if (legacyAliases != null)
            this.legacyAliases = legacyAliases;
        if (aliases != null)
            this.aliases = aliases;
    }

    public PersonalAccountData(@Required final PostfixMailbox postfixMailbox) {
        this(postfixMailbox.getName(), postfixMailbox.getUsername(), postfixMailbox.getQuota(), postfixMailbox.getPassword(),
                new AddressData(postfixMailbox.getAddress().getAddress()), postfixMailbox.isToLeaveLocalCopy(), postfixMailbox
                        .isArchived());
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the quota
     */
    public String getQuota() {
        return quota;
    }

    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(String quota) {
        this.quota = quota;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the address
     */
    public AddressData getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(AddressData address) {
        this.address = address;
    }

    /**
     * @return the forwards
     */
    public Set<AddressData> getForwards() {
        return forwards;
    }

    /**
     * @param forwards
     *            the forwards to set
     */
    public void setForwards(Set<AddressData> forwards) {
        this.forwards = forwards;
    }

    /**
     * @return the legacyAliases
     */
    public Set<AddressData> getLegacyAliases() {
        return legacyAliases;
    }

    /**
     * @param legacyAliases
     *            the legacyAliases to set
     */
    public void setLegacyAliases(Set<AddressData> legacyAliases) {
        this.legacyAliases = legacyAliases;
    }

    /**
     * @return the aliases
     */
    public Set<AddressData> getAliases() {
        return aliases;
    }

    /**
     * @param aliases
     *            the aliases to set
     */
    public void setAliases(Set<AddressData> aliases) {
        this.aliases = aliases;
    }

    /**
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the isToLeaveLocalCopy
     */
    public boolean isToLeaveLocalCopy() {
        return isToLeaveLocalCopy;
    }

    /**
     * @param isToLeaveLocalCopy
     *            the isToLeaveLocalCopy to set
     */
    public void setToLeaveLocalCopy(boolean isToLeaveLocalCopy) {
        this.isToLeaveLocalCopy = isToLeaveLocalCopy;
    }

    /**
     * @return the isArchived
     */
    public boolean isArchived() {
        return isArchived;
    }

    /**
     * @param isArchived
     *            the isArchived to set
     */
    public void setIsArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    @Override
    public String toString() {
        return "Name=" + getName() + ",Username=" + getUsername() + ",Password=" + getPassword() + ",Address="
                + getAddress().getAddress() + ",isArchived=" + isArchived();
    }
}