package pt.iscte.dsi.correio.aop.rules.defaultvalue;

import org.aspectj.lang.annotation.SuppressAjWarnings;

import java.lang.annotation.Annotation;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * @author Paulo Zenida
 * 
 */
public aspect DefaultValueEnforcer {

    private static final String METHOD_EXECUTION = "method-execution";

    private static final String CONSTRUCTOR_EXECUTION = "constructor-execution";

    pointcut defaultValueParameter1(Object parameter) : (execution(* *(@DefaultValue (*), ..)) || execution(*.new(@DefaultValue (*), ..)) ) && args(parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter2(Object parameter) : (execution(* *(*, @DefaultValue (*), ..)) || execution(*.new(*, @DefaultValue (*), ..))) && args(*, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter3(Object parameter) : (execution(* *(*, *, @DefaultValue (*), ..)) || execution(*.new(*, *, @DefaultValue (*), ..))) && args(*, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter4(Object parameter) : (execution(* *(*, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, @DefaultValue (*), ..))) && args(*, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter5(Object parameter) : (execution(* *(*, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter6(Object parameter) : (execution(* *(*, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter7(Object parameter) : (execution(* *(*, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter8(Object parameter) : (execution(* *(*, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter9(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter10(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter11(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter12(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter13(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter14(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter15(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter16(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter17(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter18(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter19(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter20(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter21(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter22(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter23(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter24(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    pointcut defaultValueParameter25(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, @DefaultValue (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, *, parameter, ..) && if(parameter == null);

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter1(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 0);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter2(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 1);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter3(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 2);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter4(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 3);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter5(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 4);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter6(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 5);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter7(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 6);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter8(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 7);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter9(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 8);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter10(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 9);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter11(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 10);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter12(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 11);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter13(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 12);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter14(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 13);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter15(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 14);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter16(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 15);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter17(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 16);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter18(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 17);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter19(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 18);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter20(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 19);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter21(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 20);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter22(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 21);
	return proceed(newParameter);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter23(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 22);
	return proceed(newParameter);
    }
    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter24(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 23);
	return proceed(newParameter);
    }
    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(Object parameter) : defaultValueParameter25(parameter) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 24);
	return proceed(newParameter);
    }
    
    
    
    @SuppressAjWarnings("adviceDidNotMatch")
    Object around(final Object parameter, final DefaultValue defaultValue) : set(@DefaultValue * *..*.*) && args(parameter) && if(parameter == null) && @annotation(defaultValue) {
	Object newParameter = enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter,
		defaultValue);
	return proceed(newParameter, defaultValue);
    }

    private Object enforce(final JoinPoint joinPoint, final JoinPoint.StaticPart enclosingJoinPoint,
	    final Object parameter, final int index) {
	DefaultValue defaultValue = null;
	if (joinPoint.getKind().equals(METHOD_EXECUTION)) {
	    defaultValue = getDefaultValueFrom((MethodSignature) joinPoint.getSignature(), index);
	} else if (joinPoint.getKind().equals(CONSTRUCTOR_EXECUTION)) {
	    defaultValue = getDefaultValueFrom((ConstructorSignature) joinPoint.getSignature(), index);
	}
	return enforce(joinPoint, enclosingJoinPoint, parameter, defaultValue);
    }

    private Object enforce(final JoinPoint joinPoint, final JoinPoint.StaticPart enclosingJoinPoint,
	    final Object parameter, final DefaultValue defaultValue) {
	final Class<? extends DefaultValueCalculator<? extends Object>> defaultValueCalculatorClass = defaultValue
		.calculator();
	try {
	    final DefaultValueCalculator<? extends Object> defaultValueCalculator = defaultValueCalculatorClass
		    .newInstance();
	    return defaultValueCalculator.getDefaultValue();
	} catch (InstantiationException e) {
	    e.printStackTrace();
	} catch (IllegalAccessException e) {
	    e.printStackTrace();
	}
	return parameter;
    }

    private DefaultValue getDefaultValueFrom(final MethodSignature signature, final int index) {
	final Annotation[] annotations = signature.getMethod().getParameterAnnotations()[index];
	for (final Annotation annotation : annotations) {
	    if (annotation.annotationType() == DefaultValue.class) {
		return (DefaultValue) annotation;
	    }
	}
	return null;
    }

    private DefaultValue getDefaultValueFrom(final ConstructorSignature signature, final int index) {
	final Annotation[] annotations = signature.getConstructor().getParameterAnnotations()[index];
	for (final Annotation annotation : annotations) {
	    if (annotation.annotationType() == DefaultValue.class) {
		return (DefaultValue) annotation;
	    }
	}
	return null;
    }
}