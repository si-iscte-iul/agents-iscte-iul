package pt.iscte.dsi.correio.domain.entity;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public enum RecordType {
    REVIEW, TO_REMOVE, LEGACY, DISTRIBUTION_LIST, ALIAS, FORWARD, OFICIAL_LIST, FORWARD_WITH_COPY;

    public String getName() {
        return name();
    }

    public static final RecordType createAliasType(String aliasType) {
        if (aliasType.equals(RecordType.REVIEW.getName())) {
            return RecordType.REVIEW;
        }
        if (aliasType.equals(RecordType.TO_REMOVE.getName())) {
            return RecordType.TO_REMOVE;
        }
        if (aliasType.equals(RecordType.LEGACY.getName())) {
            return RecordType.LEGACY;
        }
        if (aliasType.equals(RecordType.DISTRIBUTION_LIST.getName())) {
            return RecordType.DISTRIBUTION_LIST;
        }
        if (aliasType.equals(RecordType.ALIAS.getName())) {
            return RecordType.ALIAS;
        }
        if (aliasType.equals(RecordType.FORWARD.getName())) {
            return RecordType.FORWARD;
        }
        if (aliasType.equals(RecordType.OFICIAL_LIST.getName())) {
            return RecordType.OFICIAL_LIST;
        }
        if (aliasType.equals(RecordType.FORWARD_WITH_COPY.getName())) {
            return RecordType.FORWARD_WITH_COPY;
        }
        throw new IllegalArgumentException("Unknown type: " + aliasType);
    }
}