package pt.iscte.dsi.correio.domain;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlTransient;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CalendarDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CommentsDefaultValue;
import pt.iscte.dsi.correio.aop.rules.required.BypassRequired;
import pt.iscte.dsi.correio.aop.rules.required.Required;
import pt.iscte.dsi.correio.domain.entity.PostfixAlias;
import pt.iscte.dsi.correio.dto.AddressData;

public class Address implements Comparable<Address> {

    @Required
    private String address;

    private String comments = new CommentsDefaultValue().getDefaultValue();

    private Calendar createDate = Calendar.getInstance();

    private Calendar changeDate = Calendar.getInstance();

    private boolean isActive;

    private boolean isMain = false;

    @Required
    private Account account;

    Address() {
    }

    /**
     * 
     * @param addressData
     */
    Address(@Required AddressData addressData) {
        this(addressData.getAddress(), "", null, null, true, true);
    }

    /**
     * 
     * @param address
     * @param comments
     * @param createDate
     * @param changeDate
     * @param isActive
     * @param isMain
     */
    Address(@Required final String address, final @DefaultValue(calculator = CommentsDefaultValue.class) String comments,
            final @DefaultValue(calculator = CalendarDefaultValue.class) Calendar createDate, final @DefaultValue(
                    calculator = CalendarDefaultValue.class) Calendar changeDate, final boolean isActive, final boolean isMain) {
        super();
        init(address, comments, createDate, changeDate, isActive, isMain);
    }

    Address(final @Required PostfixAlias postfixAlias) {
        if (postfixAlias.isForward()) {
            init(postfixAlias.getGoto(), postfixAlias.getComments(), postfixAlias.getCreateDate(), postfixAlias.getChangeDate(),
                    postfixAlias.getActive(), postfixAlias.isMainAddress());
        } else {
            init(postfixAlias.getAddress(), postfixAlias.getComments(), postfixAlias.getCreateDate(),
                    postfixAlias.getChangeDate(), postfixAlias.getActive(), postfixAlias.isMainAddress());
        }
    }

    private void init(final String address, final String comments, final Calendar createDate, final Calendar changeDate,
            final boolean isActive, final boolean isMain) {
        this.address = address;
        this.comments = comments;
        this.createDate = createDate;
        this.changeDate = changeDate;
        this.isActive = isActive;
        this.isMain = isMain;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the createDate
     */
    public Calendar getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the changeDate
     */
    public Calendar getChangeDate() {
        return changeDate;
    }

    /**
     * @param changeDate
     *            the changeDate to set
     */
    public void setChangeDate(Calendar changeDate) {
        this.changeDate = changeDate;
    }

    /**
     * @return the isActive
     */
    public boolean isActive() {
        return isActive;
    }

    /**
     * @param isActive
     *            the isActive to set
     */
    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the isMain
     */
    public boolean isMain() {
        return isMain;
    }

    /**
     * @param isMain
     *            the isMain to set
     */
    public void setMain(boolean isMain) {
        this.isMain = isMain;
    }

    /**
     * The annotation <code>XmlTransient</code> cuts the bi-directionality for
     * the account when in the context of Web Services. When this annotation is
     * not present, a bidirectional relation between two entities result in a
     * cycle detection in the object graph when generating the XML for the Web
     * Services.
     * 
     * @return
     */
    @XmlTransient
    public Account getAccount() {
        return account;
    }

    /**
     * @param account
     *            the account to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * 
     */
    @BypassRequired
    public void removeAccount() {
        this.account = null;
    }

    public void update(AddressData addressData) {
        setAddress(addressData.getAddress());
        setComments(addressData.getComments());
    }

    public boolean isCannonical() {
        return getAccount() != null && getAccount().isPersonalAccount()
                && ((PersonalAccount) getAccount()).getUsername().equals(getAddress());
    }

    public int compareTo(Address other) {
        return getAddress().toLowerCase().compareTo(other.getAddress().toLowerCase());
    }
}