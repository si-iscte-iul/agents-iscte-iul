package pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValueCalculator;
import pt.iscte.dsi.correio.domain.entity.AccountType;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class AccountTypeDefaultValue extends DefaultValueCalculator<AccountType> {

    @Override
    public AccountType getDefaultValue() {
        return AccountType.ACCOUNT;
    }
}