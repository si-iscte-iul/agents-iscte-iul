package pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator;

import java.util.Calendar;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValueCalculator;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class CalendarDefaultValue extends DefaultValueCalculator<Calendar> {

    @Override
    public Calendar getDefaultValue() {
        return Calendar.getInstance();
    }
}