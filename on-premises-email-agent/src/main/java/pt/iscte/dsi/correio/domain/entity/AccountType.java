package pt.iscte.dsi.correio.domain.entity;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public enum AccountType {
    ACCESS_TO_SHARE, SHARE, REVIEW, OTRS, ACCOUNT, SYSTEM;
}