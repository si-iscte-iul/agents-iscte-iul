package pt.iscte.dsi.correio.domain.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import pt.iscte.dsi.correio.aop.em.Persistable;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.AccessedByDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.AccountTypeDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CalendarDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CommentsDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.DomainDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.HomeDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.PasswordExpireDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.QuotaDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.ShareDefaultValue;
import pt.iscte.dsi.correio.aop.rules.required.Required;
import pt.iscte.dsi.correio.domain.Address;
import pt.iscte.dsi.correio.domain.PersonalAccount;
import pt.iscte.dsi.correio.dto.PersonalAccountData;
import pt.iscte.dsi.correio.utils.EntityManagerLocator;

/**
 * 
 * @author Paulo Zenida
 * 
 */
@Entity
@Table(name = "mailbox")
@NamedQueries({
        @NamedQuery(name = "findAllMailboxes", query = "SELECT m FROM PostfixMailbox m where m.domain = 'iscte.pt'"),
        @NamedQuery(
                name = "getPersonalAccountDataByUsername",
                query = "SELECT new pt.iscte.dsi.correio.dto.PersonalAccountData(m) FROM PostfixMailbox m inner join m.postfixAliases a where m.domain = 'iscte.pt' and m.username = :username") })
public class PostfixMailbox implements Serializable {
    @Id
    private String username;

    private String password;

    private String name;

    private String home;

    private String maildir;

    private String quota;

    private String domain;

    @Column(nullable = true, name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createDate = new CalendarDefaultValue().getDefaultValue();

    @Column(name = "change_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar changeDate = new CalendarDefaultValue().getDefaultValue();

    private boolean active;

    @Column(name = "passwd_expire")
    private String passwdExpire;

    private int uid;

    private int gid;

    @Column(nullable = true, name = "is_student")
    private Boolean isStudent;

    @Column(name = "is_shared")
    private boolean isShared;

    @Column(name = "is_access_to_share")
    private boolean isAccessToShare;

    private String share;

    @Column(name = "accessed_by")
    private String accessedBy;

    @Column(name = "is_archived")
    private boolean isArchived;

    @Column(nullable = true, name = "state_change_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar stateChangeDate = new CalendarDefaultValue().getDefaultValue();

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String comments;

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = PostfixAlias.class, mappedBy = "postfixMailbox")
    @OrderBy("address ASC")
    private List<PostfixAlias> postfixAliases;

    private static final long serialVersionUID = 1L;

    public PostfixMailbox() {
        super();
    }

    /**
     * @param username
     * @param password
     * @param name
     * @param home
     * @param maildir
     * @param quota
     * @param domain
     * @param createDate
     * @param changeDate
     * @param active
     * @param passwdExpire
     * @param uid
     * @param gid
     * @param isStudent
     * @param isShared
     * @param isAccessToShare
     * @param share
     * @param accessedBy
     * @param isArchived
     * @param stateChangeDate
     * @param comments
     * @param accountType
     */
    @Persistable
    public PostfixMailbox(String username, String password, String name,
            @DefaultValue(calculator = HomeDefaultValue.class) String home, String maildir, @DefaultValue(
                    calculator = QuotaDefaultValue.class) String quota,
            @DefaultValue(calculator = DomainDefaultValue.class) String domain, @DefaultValue(
                    calculator = CalendarDefaultValue.class) Calendar createDate, @DefaultValue(
                    calculator = CalendarDefaultValue.class) Calendar changeDate, boolean active, @DefaultValue(
                    calculator = PasswordExpireDefaultValue.class) String passwdExpire, int uid, int gid, Boolean isStudent,
            boolean isShared, boolean isAccessToShare, @DefaultValue(calculator = ShareDefaultValue.class) String share,
            @DefaultValue(calculator = AccessedByDefaultValue.class) String accessedBy, boolean isArchived, @DefaultValue(
                    calculator = CalendarDefaultValue.class) Calendar stateChangeDate, @DefaultValue(
                    calculator = CommentsDefaultValue.class) String comments, @DefaultValue(
                    calculator = AccountTypeDefaultValue.class) AccountType accountType) {
        super();
        this.username = username;
        this.password = password;
        this.name = name;
        this.home = home;

        if (maildir == null) {
            maildir = domain + "/" + username.split("@")[0] + "/Maildir/";
        }
        this.maildir = maildir;

        this.quota = quota;
        this.domain = domain;
        this.createDate = createDate;
        this.changeDate = changeDate;
        this.active = active;
        this.passwdExpire = passwdExpire;
        this.uid = uid;
        this.gid = gid;
        this.isStudent = isStudent;
        this.isShared = isShared;
        this.isAccessToShare = isAccessToShare;
        this.share = share;
        this.accessedBy = accessedBy;
        this.isArchived = isArchived;
        this.stateChangeDate = stateChangeDate;
        this.comments = comments;
        this.accountType = accountType;
    }

    public PostfixMailbox(String username, String password, String name, String quota) {
        this(username, password, name, "", "", quota, "iscte.pt", Calendar.getInstance(), Calendar.getInstance(), true, "Y", 109,
                109, false, false, false, "", "", false, Calendar.getInstance(), "", AccountType.ACCOUNT);
    }

    public PostfixMailbox(final PersonalAccount personalAccount) {
        this(personalAccount.getUsername(), personalAccount.getPassword(), personalAccount.getName(), null, null, personalAccount
                .getQuota(), null, personalAccount.getCreateDate(), personalAccount.getChangeDate(), personalAccount.isActive(),
                null, 109, 109, false, false, false, null, null, false, null, null, null);
        createOrUpdateAddress(personalAccount);
        createOrUpdateAliases(personalAccount);
        createOrUpdateLegacyAddresses(personalAccount);
        createOrUpdateForwards(personalAccount);
        removeAliases(personalAccount);
    }

    public void update(PersonalAccount personalAccount) {
        setName(personalAccount.getName());
        setQuota(personalAccount.getQuota());
        setPassword(personalAccount.getPassword());
        setActive(personalAccount.isActive());
        setArchived(personalAccount.isArchived());
        setCreateDate(personalAccount.getCreateDate());
        setChangeDate(personalAccount.getChangeDate());
        setStateChangeDate(Calendar.getInstance());

        createOrUpdateAddress(personalAccount);
        createOrUpdateAliases(personalAccount);
        createOrUpdateLegacyAddresses(personalAccount);
        createOrUpdateForwards(personalAccount);
        removeAliases(personalAccount);
    }

    private void removeAliases(final PersonalAccount personalAccount) {
        if (getPostfixAliases() != null) {
            final Iterator<PostfixAlias> it = getPostfixAliases().iterator();
            while (it.hasNext()) {
                final PostfixAlias postfixAlias = it.next();
                if (!personalAccount.hasCorrespondingAddress(postfixAlias)) {
                    it.remove();
                    postfixAlias.delete();
                }
            }
        }
    }

    private void createOrUpdateAddress(final PersonalAccount personalAccount) {
        final Address address = personalAccount.getAddress();
        PostfixAlias postfixAddress = PostfixAlias.findAliasByAddress(address.getAddress());
        if (postfixAddress == null) {
            postfixAddress =
                    new PostfixAlias(address.getAddress(), personalAccount.getUsername(), null, address.getCreateDate(),
                            address.getChangeDate(), address.isActive(), address.getComments(), RecordType.ALIAS,
                            address.isMain(), this);
        } else {
            postfixAddress.update(address);
        }
    }

    private void createOrUpdateAliases(final PersonalAccount personalAccount) {
        final Collection<Address> aliases = personalAccount.getAliases();
        for (final Address alias : aliases) {
            if (alias.isCannonical()) {
                continue;
            }
            PostfixAlias postfixAlias = PostfixAlias.findAliasByAddress(alias.getAddress());
            if (postfixAlias == null) {
                postfixAlias =
                        new PostfixAlias(alias.getAddress(), personalAccount.getUsername(), null, alias.getCreateDate(),
                                alias.getChangeDate(), alias.isActive(), alias.getComments(), RecordType.ALIAS, alias.isMain(),
                                this);
            } else {
                postfixAlias.update(alias);
            }
        }
    }

    private void createOrUpdateLegacyAddresses(final PersonalAccount personalAccount) {
        final Collection<Address> legacyAddresses = personalAccount.getLegacyAddresses();
        for (final Address legacyAddress : legacyAddresses) {
            PostfixAlias postfixAddress = PostfixAlias.findAliasByAddress(legacyAddress.getAddress());
            if (postfixAddress == null) {
                postfixAddress =
                        new PostfixAlias(legacyAddress.getAddress(), personalAccount.getUsername(), null,
                                legacyAddress.getCreateDate(), legacyAddress.getChangeDate(), legacyAddress.isActive(),
                                legacyAddress.getComments(), RecordType.LEGACY, legacyAddress.isMain(), this);
            } else {
                postfixAddress.update(legacyAddress);
            }
        }
    }

    private void createOrUpdateForwards(final PersonalAccount personalAccount) {
        final String concatenatedForwardAddress = getConcatenatedForwardAddress(personalAccount);
        if (concatenatedForwardAddress != null) {
            final RecordType recordType =
                    personalAccount.isToLeaveLocalCopy() ? RecordType.FORWARD_WITH_COPY : RecordType.FORWARD;
            final Address forward = personalAccount.getForwards().iterator().next();
            PostfixAlias postfixForward = PostfixAlias.findForwardAlias(personalAccount.getUsername());
            if (postfixForward == null) {
                postfixForward =
                        new PostfixAlias(getUsername(), concatenatedForwardAddress, "iscte.pt", forward.getCreateDate(),
                                forward.getChangeDate(), forward.isActive(), forward.getComments(), recordType, forward.isMain(),
                                this);
            } else {
                postfixForward.update(forward);
                postfixForward.updateForward(concatenatedForwardAddress, recordType);
            }
        }
    }

    private String getConcatenatedForwardAddress(final PersonalAccount personalAccount) {
        String result = personalAccount.isToLeaveLocalCopy() ? (getUsername() + ",") : "";
        for (final Address forward : personalAccount.getForwards()) {
            result += forward.getAddress() + ",";
        }
        return (result == null || result.length() == 0) ? null : result.substring(0, result.length() - 1);
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the home
     */
    public String getHome() {
        return home;
    }

    /**
     * @param home
     *            the home to set
     */
    public void setHome(String home) {
        this.home = home;
    }

    /**
     * @return the maildir
     */
    public String getMaildir() {
        return maildir;
    }

    /**
     * @param maildir
     *            the maildir to set
     */
    public void setMaildir(String maildir) {
        this.maildir = maildir;
    }

    /**
     * @return the quota
     */
    public String getQuota() {
        return quota;
    }

    /**
     * @param quota
     *            the quota to set
     */
    public void setQuota(String quota) {
        this.quota = quota;
    }

    /**
     * @return the domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain
     *            the domain to set
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return the createDate
     */
    public Calendar getCreateDate() {
        return createDate;
    }

    /**
     * @param createDate
     *            the createDate to set
     */
    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    /**
     * @return the changeDate
     */
    public Calendar getChangeDate() {
        return changeDate;
    }

    /**
     * @param changeDate
     *            the changeDate to set
     */
    public void setChangeDate(Calendar changeDate) {
        this.changeDate = changeDate;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active
     *            the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the passwdExpire
     */
    public String getPasswdExpire() {
        return passwdExpire;
    }

    /**
     * @param passwdExpire
     *            the passwdExpire to set
     */
    public void setPasswdExpire(String passwdExpire) {
        this.passwdExpire = passwdExpire;
    }

    /**
     * @return the uid
     */
    public int getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public void setUid(int uid) {
        this.uid = uid;
    }

    /**
     * @return the gid
     */
    public int getGid() {
        return gid;
    }

    /**
     * @param gid
     *            the gid to set
     */
    public void setGid(int gid) {
        this.gid = gid;
    }

    /**
     * @return the isStudent
     */
    public Boolean isStudent() {
        return isStudent;
    }

    /**
     * @param isStudent
     *            the isStudent to set
     */
    public void setStudent(Boolean isStudent) {
        this.isStudent = isStudent;
    }

    /**
     * @return the isShared
     */
    public boolean isShared() {
        return isShared;
    }

    /**
     * @param isShared
     *            the isShared to set
     */
    public void setShared(boolean isShared) {
        this.isShared = isShared;
    }

    /**
     * @return the isAccessToShare
     */
    public boolean isAccessToShare() {
        return isAccessToShare;
    }

    /**
     * @param isAccessToShare
     *            the isAccessToShare to set
     */
    public void setAccessToShare(boolean isAccessToShare) {
        this.isAccessToShare = isAccessToShare;
    }

    /**
     * @return the share
     */
    public String getShare() {
        return share;
    }

    /**
     * @param share
     *            the share to set
     */
    public void setShare(String share) {
        this.share = share;
    }

    /**
     * @return the accessedBy
     */
    public String getAccessedBy() {
        return accessedBy;
    }

    /**
     * @param accessedBy
     *            the accessedBy to set
     */
    public void setAccessedBy(String accessedBy) {
        this.accessedBy = accessedBy;
    }

    /**
     * @return the isArchived
     */
    public boolean isArchived() {
        return isArchived;
    }

    /**
     * @param isArchived
     *            the isArchived to set
     */
    public void setArchived(boolean isArchived) {
        this.isArchived = isArchived;
    }

    /**
     * @return the stateChangeDate
     */
    public Calendar getStateChangeDate() {
        return stateChangeDate == null ? Calendar.getInstance() : stateChangeDate;
    }

    /**
     * @param stateChangeDate
     *            the stateChangeDate to set
     */
    public void setStateChangeDate(Calendar stateChangeDate) {
        this.stateChangeDate = stateChangeDate;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the accountType
     */
    public AccountType getAccountType() {
        return accountType;
    }

    /**
     * @param accountType
     *            the accountType to set
     */
    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    /**
     * @return the postfixAliases
     */
    public List<PostfixAlias> getPostfixAliases() {
        return postfixAliases;
    }

    /**
     * @param postfixAliases
     *            the postfixAliases to set
     */
    public void setPostfixAliases(List<PostfixAlias> postfixAliases) {
        this.postfixAliases = postfixAliases;
    }

    @Override
    public String toString() {
        return "Name=" + getName() + ",Username=" + getUsername();
    }

    // static methods
    public static PostfixMailbox findByUsername(final String username) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        return em.find(PostfixMailbox.class, username);
    }

    @SuppressWarnings("unchecked")
    public static Collection<PostfixMailbox> findAll() {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        return em.createNamedQuery("findAllMailboxes").getResultList();
    }

    public PostfixAlias getAddress() {
        for (final PostfixAlias postfixAlias : getPostfixAliases()) {
            if (postfixAlias.getRecordType() == RecordType.ALIAS && postfixAlias.isMainAddress()) {
                return postfixAlias;
            }
        }
        return null;
    }

    public Set<PostfixAlias> getAliases() {
        final Set<PostfixAlias> result = new HashSet<PostfixAlias>();
        for (final PostfixAlias postfixAlias : getPostfixAliases()) {
            if (postfixAlias.getRecordType() == RecordType.ALIAS && !postfixAlias.isMainAddress()) {
                result.add(postfixAlias);
            }
        }
        return result;
    }

    public Set<PostfixAlias> getLegacyAddresses() {
        final Set<PostfixAlias> result = new HashSet<PostfixAlias>();
        for (final PostfixAlias postfixAlias : getPostfixAliases()) {
            if (postfixAlias.getRecordType() == RecordType.LEGACY) {
                result.add(postfixAlias);
            }
        }
        return result;
    }

    public Set<PostfixAlias> getForwards() {
        final Set<PostfixAlias> result = new HashSet<PostfixAlias>();
        for (final PostfixAlias postfixAlias : getPostfixAliases()) {
            if (postfixAlias.getRecordType() == RecordType.FORWARD
                    || postfixAlias.getRecordType() == RecordType.FORWARD_WITH_COPY) {
                result.add(postfixAlias);
            }
        }
        return result;
    }

    public Set<PostfixAlias> getCannonicalAliases() {
        final Set<PostfixAlias> result = new HashSet<PostfixAlias>();
        for (final PostfixAlias postfixAlias : getPostfixAliases()) {
            if (postfixAlias.getRecordType() == RecordType.ALIAS && postfixAlias.isCannonical()) {
                result.add(postfixAlias);
            }
        }
        return result;
    }

    public PostfixAlias getAlias(@Required final String address) {
        for (final PostfixAlias postfixAlias : getAliases()) {
            if (address.equals(postfixAlias.getAddress())) {
                return postfixAlias;
            }
        }
        return null;
    }

    public PostfixAlias getLegacyAddress(@Required final String address) {
        for (final PostfixAlias postfixAlias : getLegacyAddresses()) {
            if (address.equals(postfixAlias.getAddress())) {
                return postfixAlias;
            }
        }
        return null;
    }

    public PostfixAlias getForward(@Required final String address) {
        for (final PostfixAlias postfixAlias : getForwards()) {
            if (address.equals(postfixAlias.getAddress())) {
                return postfixAlias;
            }
        }
        return null;
    }

    public boolean isToLeaveLocalCopy() {
        for (final PostfixAlias forward : getForwards()) {
            if (forward.isToLeaveLocalCopy()) {
                return true;
            }
        }
        return false;
    }

    // @PreRemove
    public void preRemove() {
        System.out.println("PRE REMOVING MAILBOX...");
        final Iterator<PostfixAlias> it = getPostfixAliases().iterator();
        while (it.hasNext()) {
            System.out.println("REMOVING ALIAS " + it.next());
            it.remove();
        }
    }

    public void deletePostfixAliases() {
        // final EntityManager em =
        // EntityManagerLocator.getCurrentEntityManager();
        System.out.println("PRE REMOVING MAILBOX...");
        final Iterator<PostfixAlias> it = getPostfixAliases().iterator();
        while (it.hasNext()) {
            final PostfixAlias postfixAlias = it.next();
            System.out.println("REMOVING ALIAS " + postfixAlias);
            postfixAlias.delete();
            it.remove();
        }
        // em.clear();
    }

    public static PersonalAccountData getPersonalAccountData(String username) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        @SuppressWarnings("unchecked")
        final List<Object> objects =
                em.createNamedQuery("getPersonalAccountDataByUsername").setParameter("username", username).getResultList();
        for (final Object object : objects) {
            System.out.println("object " + object + " of class " + object.getClass());
        }

        return null;
    }
}