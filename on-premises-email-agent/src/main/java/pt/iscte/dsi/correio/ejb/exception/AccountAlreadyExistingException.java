package pt.iscte.dsi.correio.ejb.exception;

public class AccountAlreadyExistingException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private final static String DEFAULT_MESSAGE = "Account already existing";

    private String username;

    public AccountAlreadyExistingException() {
        this(DEFAULT_MESSAGE);
    }

    public AccountAlreadyExistingException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountAlreadyExistingException(String username) {
        super(DEFAULT_MESSAGE + ": " + username);
        this.username = username;
    }

    public AccountAlreadyExistingException(Throwable cause) {
        super(cause);
    }

    public String getUsername() {
        return username;
    }
}