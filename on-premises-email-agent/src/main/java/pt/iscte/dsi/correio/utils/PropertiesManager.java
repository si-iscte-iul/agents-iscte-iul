package pt.iscte.dsi.correio.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertiesManager {

    public PropertiesManager() {
    }

    public static Properties loadProperties(String fileName) throws IOException {
        Properties properties = new Properties();
        loadProperties(properties, fileName);
        return properties;
    }

    public static void loadProperties(Properties properties, String fileName) throws IOException {
        java.io.InputStream inputStream = instance.getClass().getResourceAsStream(fileName);
        if (inputStream != null)
            properties.load(inputStream);
    }

    private static final PropertiesManager instance = new PropertiesManager();
}
