package pt.iscte.dsi.correio.ejb.exception;

public class NotExistingAccountException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private final static String DEFAULT_MESSAGE = "Not existing account";

    public NotExistingAccountException() {
        this(DEFAULT_MESSAGE);
    }

    public NotExistingAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotExistingAccountException(String address) {
        super(DEFAULT_MESSAGE + ": " + address);
    }

    public NotExistingAccountException(Throwable cause) {
        super(cause);
    }
}