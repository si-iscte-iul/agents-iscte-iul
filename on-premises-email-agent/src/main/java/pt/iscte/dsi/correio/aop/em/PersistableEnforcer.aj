package pt.iscte.dsi.correio.aop.em;

import org.aspectj.lang.annotation.SuppressAjWarnings;

import pt.iscte.dsi.correio.utils.EntityManagerLocator;

public aspect PersistableEnforcer {

    declare error : call(public void javax.persistence.EntityManager.persist(..)) && !within(PersistableEnforcer) : "You cannot invoke persist here";

    declare error : execution(@Persistable (@javax.persistence.Entity *..*).new()) : "You cannot declare @Entity no-arg constructor @Persistable";

    /**
     * If we use execution, no object can be captured in the
     * <code>returning()</code> part. Therefore, I must access the newly created
     * object instante directly through the
     * <code>thisJoinPoint.getTarget()</code> variable.
     */
    @SuppressAjWarnings("adviceDidNotMatch")
    after() returning() : execution(@Persistable (@javax.persistence.Entity *..*).new(..)) {
	EntityManagerLocator.getCurrentEntityManager().persist(thisJoinPoint.getTarget());
    }
}