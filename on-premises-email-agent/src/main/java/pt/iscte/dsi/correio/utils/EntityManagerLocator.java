package pt.iscte.dsi.correio.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

/**
 * @author Paulo Zenida
 * @author Ivo.Branco@iscte.pt
 * 
 */
public class EntityManagerLocator {

    private static final Logger logger = Logger.getLogger(EntityManagerLocator.class);

    private static final String PERSISTENCE_UNIT_NAME = "agentes-correio";
    private static EntityManagerFactory entityManagerFactory;
    private static ThreadLocal<EntityManager> entityManagerRegistry = new ThreadLocal<EntityManager>();

    private static EntityManagerFactory getOrCreateEntityManagerFactory() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            logger.info("Creating entity manager factory");
        }
        return entityManagerFactory;
    }

    public static EntityManager createEntityManager() {
        EntityManager entityManager = getOrCreateEntityManagerFactory().createEntityManager();
        logger.info("Creating Entity Manager");
        setCurrentEntityManager(entityManager);
        return entityManager;
    }

    public static EntityManager getCurrentEntityManager() {
        EntityManager entityManager = entityManagerRegistry.get();
        if (entityManager != null) {
            return entityManager;
        }
        return createEntityManager();
    }

    public static void setCurrentEntityManager(final EntityManager em) {
        entityManagerRegistry.set(em);
    }

    public static EntityTransaction getEntityTransaction() {
        EntityTransaction t = getCurrentEntityManager().getTransaction();
        return t;
    }

}