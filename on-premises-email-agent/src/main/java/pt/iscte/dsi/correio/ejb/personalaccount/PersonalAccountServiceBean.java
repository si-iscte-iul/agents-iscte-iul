package pt.iscte.dsi.correio.ejb.personalaccount;

import java.util.Collection;
import java.util.Set;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.xml.ws.WebServiceContext;

import pt.iscte.dsi.correio.aop.em.EntityManageable;
import pt.iscte.dsi.correio.domain.Address;
import pt.iscte.dsi.correio.domain.PersonalAccount;
import pt.iscte.dsi.correio.dto.PersonalAccountData;
import pt.iscte.dsi.correio.ejb.exception.AccountAlreadyExistingException;
import pt.iscte.dsi.correio.ejb.exception.NotExistingAccountException;
import pt.iscte.dsi.correio.ejb.exception.UsernameAsAliasAccountException;
import pt.iscte.dsi.correio.utils.EntityManagerLocator;
import pt.iscte.dsi.correio.utils.HostAccessControl;
//import javax.ejb.Stateless;
//import javax.ejb.TransactionAttribute;
//import javax.ejb.TransactionAttributeType;

/**
 * @author Paulo Zenida
 * 
 */
//@Stateless(name = "PersonalAccountService")
@WebService(endpointInterface = "pt.iscte.dsi.correio.ejb.personalaccount.PersonalAccountWS")
public class PersonalAccountServiceBean implements PersonalAccountWS, EntityManageable {
    @Resource
    WebServiceContext wsCtx;

    public PersonalAccountServiceBean() {
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount create(PersonalAccountData accountData) throws AccountAlreadyExistingException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.create(accountData);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount disable(final String username) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.disable(username);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount enable(final String username) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.enable(username);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount find(String username) {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.findByUsername(username);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public Collection<PersonalAccount> findAll() {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.findAll();
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount update(PersonalAccount personalAccount) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            @SuppressWarnings("unused")
            final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
            return PersonalAccount.update(personalAccount);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount addForward(final String username, final String address, final boolean isToLeaveLocalCopy)
            throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.addForward(username, address, isToLeaveLocalCopy);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccount removeForward(final String username, final String address) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.removeForward(username, address);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public void addAlias(final String username, final String address) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            final PersonalAccount personalAccount = find(username);
            if (personalAccount != null) {
                personalAccount.addAlias(address);
            } else {
                throw new NotExistingAccountException(username);
            }
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public void removeAlias(final String username, final String address) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            final PersonalAccount personalAccount = find(username);
            if (personalAccount != null) {
                personalAccount.removeAlias(address);
            } else {
                throw new NotExistingAccountException(username);
            }
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public PersonalAccountData getPersonalAccountData(String username) throws NotExistingAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            return PersonalAccount.getPersonalAccountData(username);
        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }
    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public void synchronizeOnPermiseEmail(final String username, final String name, final String mainEmailAddress,
            final boolean isGmailActive) throws NotExistingAccountException, UsernameAsAliasAccountException {
        if (HostAccessControl.isAllowed(getClass(), wsCtx)) {
            PersonalAccount personalAccount = PersonalAccount.findByUsername(username);
            if (personalAccount == null) {
                throw new NotExistingAccountException();
            }

            String[] usernameParts = username.split("@");
            String forwardGoogleAddress = usernameParts[0] + "@iscte-iul.pt";
            String iulDropboxAddress = usernameParts[0] + "@iul.pt";

            // Check if it has an invalid alias
            if (personalAccount.getUsername().equalsIgnoreCase(personalAccount.getAddress().getAddress())) {
                throw new UsernameAsAliasAccountException();
            }

            // Update main alias if needed
            String mainEmailAddressToStore = mainEmailAddress;
            if (mainEmailAddress.equalsIgnoreCase(personalAccount.getUsername())) {
                mainEmailAddressToStore = getTemporaryAlias(username);
            }
            if (!mainEmailAddressToStore.equalsIgnoreCase(personalAccount.getAddress().getAddress())) {
                personalAccount.getAddress().setAddress(mainEmailAddressToStore);
            }

            try {
                boolean hasIulDropbox = hasIulDropbox(personalAccount, iulDropboxAddress);

                if (isGmailActive) {
                    boolean hasGoogleForward = hasGoogleForward(personalAccount, forwardGoogleAddress);
                    if (!hasGoogleForward) {
//			PersonalAccount.addForward(username, forwardGoogleAddress, false);
                        personalAccount.setToLeaveLocalCopy(false);
                        personalAccount.addForward(forwardGoogleAddress);
                    }
                    removeOtherForwardsIfNeeded(personalAccount, forwardGoogleAddress);

                    if (hasIulDropbox) {
//			PersonalAccount.removeAlias(username, iulDropboxAddress);
                        personalAccount.removeAlias(iulDropboxAddress);
                    }
                } else {
                    boolean hasGoogleForward = hasGoogleForward(personalAccount, forwardGoogleAddress);
                    if (hasGoogleForward) {
//			PersonalAccount.removeForward(username, forwardGoogleAddress);
                        personalAccount.removeForward(forwardGoogleAddress);
                    }

                    if (!hasIulDropbox) {
//			PersonalAccount.addAlias(username, iulDropboxAddress);
                        personalAccount.addAlias(iulDropboxAddress);
                    }
                }
                // Synchronize is_archived flag
//		PersonalAccount.changeIsArchivedFlag(username, isGmailActive);
                personalAccount.setIsArchived(isGmailActive);

                PersonalAccount.update(personalAccount);

//		personalAccount.setIsArchived(isGmailActive);
//		PersonalAccount.update(personalAccount);
            } catch (NotExistingAccountException e) {
                throw new RuntimeException(e);
            }

        } else {
            throw new RuntimeException(HostAccessControl.getRemoteAddress(wsCtx) + " is not allowed to access this service");
        }

    }

//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @WebMethod
    public void fixAlias(final String username) {
        PersonalAccount personalAccount = find(username);
        if (personalAccount != null) {
            if (personalAccount.getUsername().equalsIgnoreCase(personalAccount.getAddress().getAddress())) {
                String newAlias = getTemporaryAlias(username);
                personalAccount.getAddress().setAddress(newAlias);
                PersonalAccount.update(personalAccount);
            }
        }
    }

    private String getTemporaryAlias(final String username) {
        String[] usernameParts = username.split("@");
        return usernameParts[0] + "_TEMP@" + usernameParts[1];
    }

    private boolean hasGoogleForward(PersonalAccount personalAccount, String forwardAddress) {
        Set<Address> forwards = personalAccount.getForwards();

        for (Address address : forwards) {
            if (address.getAddress().equalsIgnoreCase(forwardAddress)) {
                return true;
            }
        }

        return false;
    }

    private void removeOtherForwardsIfNeeded(PersonalAccount personalAccount, String forwardAddress)
            throws NotExistingAccountException {
        Set<Address> forwards = personalAccount.getForwards();

        for (Address address : forwards) {
            if (!address.getAddress().equalsIgnoreCase(forwardAddress)) {
//		PersonalAccount.removeForward(personalAccount.getUsername(), address.getAddress());
                personalAccount.removeForward(address.getAddress());
            }
        }
    }

    private boolean hasIulDropbox(PersonalAccount personalAccount, String iulDropboxAddress) {
        Set<Address> aliass = personalAccount.getAliases();

        for (Address address : aliass) {
            if (address.getAddress().equalsIgnoreCase(iulDropboxAddress)) {
                return true;
            }
        }

        return false;
    }
}