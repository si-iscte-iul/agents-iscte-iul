package pt.iscte.dsi.correio.domain;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public interface TranslatableToPersistentEntity {

    public void translate();
}