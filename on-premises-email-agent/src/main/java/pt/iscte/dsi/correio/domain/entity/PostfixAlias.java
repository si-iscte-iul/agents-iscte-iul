package pt.iscte.dsi.correio.domain.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NoResultException;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import pt.iscte.dsi.correio.aop.em.Persistable;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CalendarDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.CommentsDefaultValue;
import pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator.DomainDefaultValue;
import pt.iscte.dsi.correio.domain.Address;
import pt.iscte.dsi.correio.utils.EntityManagerLocator;

/**
 * 
 * @author Paulo Zenida
 * 
 */
@Entity
@Table(name = "alias")
@NamedQueries({
        @NamedQuery(name = "findAliasesByUsername",
                query = "SELECT a FROM PostfixAlias a where a.domain = 'iscte.pt' and a.postfixMailbox.username = :username"),
        @NamedQuery(name = "findAliasesByGoto",
                query = "SELECT a FROM PostfixAlias a where a.domain = 'iscte.pt' and a.goto_ like :goto"),
        @NamedQuery(
                name = "findAliasByUsernameAndAddress",
                query = "SELECT a FROM PostfixAlias a where a.domain = 'iscte.pt' and a.postfixMailbox.username = :username and a.address = :address"),
        @NamedQuery(
                name = "findMainAddressByUsername",
                query = "SELECT a FROM PostfixAlias a where a.domain = 'iscte.pt' and a.postfixMailbox.username = :username and a.isMainAddress = 1"),
        @NamedQuery(name = "findAllAliases", query = "SELECT a FROM PostfixAlias a"),
        @NamedQuery(
                name = "findForwardAlias",
                query = "SELECT a FROM PostfixAlias a where a.postfixMailbox.username = :username and (a.recordType = pt.iscte.dsi.correio.domain.entity.RecordType.FORWARD or a.recordType = pt.iscte.dsi.correio.domain.entity.RecordType.FORWARD_WITH_COPY)") })
public class PostfixAlias implements Serializable {

    @Id
    private String address;

    @Column(name = "goto")
    @Lob
    private String goto_;

    private String domain;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createDate = new CalendarDefaultValue().getDefaultValue();

    @Column(name = "change_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar changeDate = new CalendarDefaultValue().getDefaultValue();

    private boolean active;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    private String comments;

    @Column(name = "record_type")
    @Enumerated(EnumType.STRING)
    private RecordType recordType;

    @Column(name = "is_main_address")
    private boolean isMainAddress;

    @ManyToOne(optional = false)
    @JoinColumn(name = "username", nullable = false)
    private PostfixMailbox postfixMailbox;

    private static final long serialVersionUID = 1L;

    public PostfixAlias() {
        super();
    }

    /**
     * @param address
     * @param goto_
     * @param domain
     * @param createDate
     * @param changeDate
     * @param active
     * @param comments
     * @param recordType
     * @param isMainAddress
     * @param postfixMailbox
     */
    @Persistable
    public PostfixAlias(String address, String goto_, @DefaultValue(calculator = DomainDefaultValue.class) String domain,
            @DefaultValue(calculator = CalendarDefaultValue.class) Calendar createDate, @DefaultValue(
                    calculator = CalendarDefaultValue.class) Calendar changeDate, boolean active, @DefaultValue(
                    calculator = CommentsDefaultValue.class) String comments, RecordType recordType, boolean isMainAddress,
            PostfixMailbox postfixMailbox) {
        super();
        this.address = address;
        this.goto_ = goto_;
        this.domain = domain;
        this.createDate = createDate;
        this.changeDate = changeDate;
        this.active = active;
        this.comments = comments;
        this.recordType = recordType;
        this.isMainAddress = isMainAddress;
        this.postfixMailbox = postfixMailbox;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoto() {
        return this.goto_;
    }

    public void setGoto(String goto_) {
        this.goto_ = goto_;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Calendar getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = createDate;
    }

    public Calendar getChangeDate() {
        return this.changeDate;
    }

    public void setChangeDate(Calendar changeDate) {
        this.changeDate = changeDate;
    }

    public boolean getActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public RecordType getRecordType() {
        return this.recordType;
    }

    public void setRecordType(RecordType recordType) {
        this.recordType = recordType;
    }

    /**
     * @return the isMainAddress
     */
    public boolean isMainAddress() {
        return isMainAddress;
    }

    /**
     * @return the postfixMailbox
     */
    public PostfixMailbox getPostfixMailbox() {
        return postfixMailbox;
    }

    /**
     * @param postfixMailbox
     *            the postfixMailbox to set
     */
    public void setPostfixMailbox(PostfixMailbox postfixMailbox) {
        this.postfixMailbox = postfixMailbox;
    }

    /**
     * @param isMainAddress
     *            the isMainAddress to set
     */
    public void setMainAddress(boolean isMainAddress) {
        this.isMainAddress = isMainAddress;
    }

    public boolean isForward() {
        return getRecordType() == RecordType.FORWARD || getRecordType() == RecordType.FORWARD_WITH_COPY;
    }

    public boolean isAlias() {
        return getRecordType() == RecordType.ALIAS;
    }

    public boolean isLegacy() {
        return getRecordType() == RecordType.LEGACY;
    }

    public String getUsername() {
        return getPostfixMailbox() == null ? null : getPostfixMailbox().getUsername();
    }

    public boolean isCannonical() {
        return getAddress().equals(getUsername());
    }

    @Override
    public String toString() {
        return "Address=" + getAddress() + ",Goto=" + getGoto();
    }

    // static methods
    @SuppressWarnings("unchecked")
    public static List<PostfixAlias> findByUsername(final String username) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        return em.createNamedQuery("findAliasesByUsername").setParameter("username", username).getResultList();
    }

    public static PostfixAlias findMainAddressByUsername(final String username) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        return (PostfixAlias) em.createNamedQuery("findMainAddressByUsername").setParameter("username", username)
                .getSingleResult();
    }

    public static PostfixAlias findForwardAlias(final String username) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        try {
            return (PostfixAlias) em.createNamedQuery("findForwardAlias").setParameter("username", username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public static PostfixAlias findAliasByAddress(final String address) {
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        return em.find(PostfixAlias.class, address);
    }

    public void update(Address address) {
//	setAddress(address.getAddress());
        if (address.getComments() != null && address.getComments().length() != 0) {
            setComments(address.getComments());
        }
    }

    public void updateForward(final String address, RecordType recordType) {
        setGoto(address);
        setRecordType(recordType);
    }

    public boolean isToLeaveLocalCopy() {
        return getRecordType() == RecordType.FORWARD_WITH_COPY;
    }

    public void delete() {
        this.postfixMailbox = null;
        final EntityManager em = EntityManagerLocator.getCurrentEntityManager();
        em.remove(this);
    }

    // @PreRemove
    public void preRemove() {
        System.out.println("MAKING POSTFIX MAILBOX BE NULL.....");
        this.postfixMailbox = null;
    }
}