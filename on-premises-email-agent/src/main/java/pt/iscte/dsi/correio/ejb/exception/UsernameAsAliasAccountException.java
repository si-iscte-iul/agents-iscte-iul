package pt.iscte.dsi.correio.ejb.exception;

public class UsernameAsAliasAccountException extends Exception {

    private static final long serialVersionUID = 1L;

    private final static String DEFAULT_MESSAGE = "Username as alias";

    public UsernameAsAliasAccountException() {
        this(DEFAULT_MESSAGE);
    }

    public UsernameAsAliasAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public UsernameAsAliasAccountException(String address) {
        super(DEFAULT_MESSAGE + ": " + address);
    }

    public UsernameAsAliasAccountException(Throwable cause) {
        super(cause);
    }
}