package pt.iscte.dsi.correio.aop.rules.required;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.SuppressAjWarnings;

/**
 * @author Paulo Zenida
 * 
 */
public aspect RequiredEnforcer {

    // ==================== Pointcuts: Fields ====================
    private pointcut requiredSets(final Object argument) : set(@Required * *..*.*) && args(argument) && if(argument == null);

    private pointcut cflowOfBypassingRequired() : cflowbelow(execution(@BypassRequired * *..*(..))) || cflowbelow(execution(@BypassRequired *..new(..)));

    // ==================== Pointcuts: Parameters ====================
    private pointcut requiredParameter1(Object parameter) : (execution(* *(@Required (*), ..)) || execution(*.new(@Required (*), ..))) && args(parameter, ..);

    private pointcut requiredParameter2(Object parameter) : (execution(* *(*, @Required (*), ..)) || execution(*.new(*, @Required (*), ..))) && args(*, parameter, ..);

    private pointcut requiredParameter3(Object parameter) : (execution(* *(*, *, @Required (*), ..)) || execution(*.new(*, *, @Required (*), ..))) && args(*, *, parameter, ..);

    private pointcut requiredParameter4(Object parameter) : (execution(* *(*, *, *, @Required (*), ..)) || execution(*.new(*, *, *, @Required (*), ..))) && args(*, *, *, parameter, ..);

    private pointcut requiredParameter5(Object parameter) : (execution(* *(*, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, @Required (*), ..))) && args(*, *, *, *, parameter, ..);

    private pointcut requiredParameter6(Object parameter) : (execution(* *(*, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, parameter, ..);

    private pointcut requiredParameter7(Object parameter) : (execution(* *(*, *, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, *, parameter, ..);

    private pointcut requiredParameter8(Object parameter) : (execution(* *(*, *, *, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, *, *, parameter, ..);

    private pointcut requiredParameter9(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, *, *, *, parameter, ..);

    private pointcut requiredParameter10(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, *, *, *, *, parameter, ..);

    private pointcut requiredParameter11(Object parameter) : (execution(* *(*, *, *, *, *, *, *, *, *, *, @Required (*), ..)) || execution(*.new(*, *, *, *, *, *, *, *, *, *, @Required (*), ..))) && args(*, *, *, *, *, *, *, *, *, *, parameter, ..);

    // ==================== Advices: Parameters ====================
    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter1(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 1);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter2(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 2);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter3(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 3);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter4(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 4);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter5(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 5);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter6(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 6);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter7(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 7);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter8(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 8);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter9(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 9);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter10(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 10);
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    before(Object parameter) : requiredParameter11(parameter) {
	enforce(thisJoinPoint, thisEnclosingJoinPointStaticPart, parameter, 11);
    }

    // ==================== Advices: Fields ====================
    @SuppressAjWarnings("adviceDidNotMatch")
    before(final Object argument) : requiredSets(argument) && !cflowOfBypassingRequired() {
	throw new IllegalArgumentException("Field cannot be null in " + thisJoinPoint + ".");
    }

    // ==================== Methods ====================
    private void enforce(JoinPoint joinPoint, JoinPoint.StaticPart enclosingJoinPoint, Object parameter,
	    int parameterNumber) {
	if (parameter == null)
	    throw new IllegalArgumentException("Required parameter " + parameterNumber + " is null in "
		    + joinPoint + " called from " + enclosingJoinPoint + ".");
    }
}