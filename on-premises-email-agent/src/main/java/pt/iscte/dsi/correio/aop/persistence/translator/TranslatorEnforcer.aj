package pt.iscte.dsi.correio.aop.persistence.translator;

import org.aspectj.lang.annotation.SuppressAjWarnings;

import pt.iscte.dsi.correio.domain.TranslatableToPersistentEntity;

public aspect TranslatorEnforcer {

    declare error :
	execution(@Translate public static !TranslatableToPersistentEntity+ TranslatableToPersistentEntity+.*(..)) :
	    "Static methods marked with @Translate must return an instance of a TranslatableToPersistentEntity type";

    declare error :
	execution(@Translate * !TranslatableToPersistentEntity+.*(..)) :
	    "The @Translate annotation can be used only for types implementing the interface TranslatableToPersistentEntity.";

    declare error : 
	call(public void TranslatableToPersistentEntity.translate()) && 
	!within(TranslatorEnforcer) : 
	    "You should not invoke directly translate() here. Use the @Translate annotation in the method instead.";

    @SuppressAjWarnings("adviceDidNotMatch")
    after(final TranslatableToPersistentEntity domainObject) returning : 
	execution(@Translate * TranslatableToPersistentEntity+.*(..)) &&
	this(domainObject) {
	domainObject.translate();
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    after() returning(final TranslatableToPersistentEntity domainObject) : 
	execution(@Translate public static TranslatableToPersistentEntity+ TranslatableToPersistentEntity+.*(..)) {
	domainObject.translate();
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    after(final TranslatableToPersistentEntity domainObject) returning : 
	execution(@Translate * TranslatableToPersistentEntity+.*(..)) &&
	this(domainObject) {
	domainObject.translate();
    }

    @SuppressAjWarnings("adviceDidNotMatch")
    after(final TranslatableToPersistentEntity domainObject) : 
	execution(@Translate * TranslatableToPersistentEntity+.*(..)) && target(domainObject) {
	domainObject.translate();
    }
}