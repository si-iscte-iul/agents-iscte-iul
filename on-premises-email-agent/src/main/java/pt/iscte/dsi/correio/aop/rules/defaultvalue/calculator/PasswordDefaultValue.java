package pt.iscte.dsi.correio.aop.rules.defaultvalue.calculator;

import pt.iscte.dsi.correio.aop.rules.defaultvalue.DefaultValueCalculator;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class PasswordDefaultValue extends DefaultValueCalculator<String> {

    @Override
    public String getDefaultValue() {
        return "password_inicial";
    }
}