package pt.iscte.dsi.correio.dto;

import pt.iscte.dsi.correio.aop.rules.required.Required;

public class AddressData {

    @Required
    private String address;

    private String comments;

    /**
     * 
     */
    public AddressData() {
        super();
    }

    /**
     * @param address
     */
    public AddressData(String address) {
        this();
        this.address = address;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @param comments
     *            the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }
}