//package pt.iscte.dsi.correio.aop.em;
//
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//
//public interface EntityManageable {
//
//    public EntityManager getEntityManager();
//    
//    static aspect Impl {
//	
//	private EntityManager EntityManageable.em; 
//	
//	declare @field : EntityManager EntityManageable.* : @PersistenceContext(unitName = "agentes-correio");
//	
//	public EntityManager EntityManageable.getEntityManager() {
//	    return em;
//	}
//    }
//}