create temporary table alias_to_create as select username, name
FROM mailbox
WHERE domain = 'iscte.pt'
    and username NOT LIKE '%-por-%'
    and account_type = 'ACCOUNT'
    and LENGTH(username) < 16
    and REPLACE(LEFT(username, LENGTH(username)-9), '.', '#') NOT LIKE '%#%'
    AND username NOT IN (
        SELECT goto
        FROM alias
    );

-- FIX data
INSERT INTO alias (address, goto, domain, create_date, change_date, active, comments, record_type, username, is_main_address, version)
SELECT CONCAT(LEFT(username, LENGTH(username)-9), '_TEMP@iscte.pt'), username, 'iscte.pt', NOW(), NOW(), 1, '', 'ALIAS', username, 1, NULL
FROM alias_to_create
    
drop table alias_to_create;
