UPDATE mailbox
SET create_date = '1900-01-01'
WHERE create_date = '0000-00-00';

UPDATE mailbox
SET state_change_date = '1900-01-01'
WHERE state_change_date = '0000-00-00';

UPDATE mailbox
SET change_date = '1900-01-01'
WHERE change_date = '0000-00-00';