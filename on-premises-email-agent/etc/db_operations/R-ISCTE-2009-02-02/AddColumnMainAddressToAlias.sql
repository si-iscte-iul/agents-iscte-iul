ALTER TABLE alias ADD COLUMN is_main_address TINYINT(1) default 0;

-- Antonio Casqueiro - 2009-12-29
--
-- Since the code requires a mailbox to have an alias, I cannot delete the alias.
-- However, if I leave the ALIAS, the code is not able to create a forward record,
-- since the address would be 'aXXXXX@iscte.pt' and the ALIAS record has the same address
-- and the database has a constraint to prevent duplicate addresses.
--
-- delete from alias where address = goto and domain = 'iscte.pt';
--
--
-- The solution is to create an dummy address for the alias. Since the mail
-- is sent to the mailbox, it will still work.
UPDATE alias a 
SET address = CONCAT(LEFT(address, LENGTH(address)-9), '_TEMP@iscte.pt')
where address = goto and domain = 'iscte.pt'
    and LENGTH(address) < 16
    and REPLACE(LEFT(address, LENGTH(address)-9), '.', '#') NOT LIKE '%#%'


create temporary table main_addresses as select alias.address from alias inner join 
mailbox on mailbox.username = alias.username where alias.domain = 'iscte.pt' group by alias.username having count(*) = 1;
alter table main_addresses add index address(address);
update alias inner join main_addresses on main_addresses.address = alias.address set
alias.is_main_address = 1 where domain = 'iscte.pt';
drop table main_addresses;

create temporary table main_addresses as select alias.address from alias inner join 
mailbox on mailbox.username = alias.username where alias.domain = 'iscte.pt' and goto 
not like '%,%' and alias.is_main_address = 0 group by alias.username having count(*) = 1;
alter table main_addresses add index address(address);
update alias inner join main_addresses on main_addresses.address = alias.address set
alias.is_main_address = 1 where domain = 'iscte.pt';
drop table main_addresses;


update alias set alias.record_type = 'FORWARD_WITH_COPY' where 
address = username and record_type = 'alias' and goto like '%,%';


-- Some individual corrections

update alias set record_type = 'FORWARD_WITH_COPY' where address = 'a23002@iscte.pt';
update alias set record_type = 'FORWARD_WITH_COPY' where address = 'a23168@iscte.pt';
update alias set record_type = 'FORWARD_WITH_COPY' where address = 'a23252@iscte.pt';
update alias set is_main_address = 1 where address = 'Hugo_Manuel_Nunes@iscte.pt';
update alias set is_main_address = 1, record_type = 'FORWARD' where address = 'Pedro_Miguel_Subtil@iscte.pt';
update alias set is_main_address = 1 where address = 'Patricia_Barreiro@iscte.pt';
update alias set is_main_address = 1 where address = 'Miguel_Ganito@iscte.pt';
update alias set is_main_address = 1 where address = 'Mohammad_Carimo@iscte.pt';
update alias set is_main_address = 1 where address = 'Ivo_Rodrigues@iscte.pt';
update alias set is_main_address = 1 where address = 'Marta_Alexandra_Alves@iscte.pt';
update alias set record_type = 'FORWARD' where address = 'acmpj@iscte.pt';


-- I fixed manually some accounts after the following query performed:
-- select alias.* from mailbox inner join alias on alias.username = mailbox.username 
-- where mailbox.username not in (select alias.username from alias where alias.is_main_address = 1) 
-- and account_type = 'ACCOUNT';

update alias set is_main_address = 1 where address = 'cristina.santos@iscte.pt';
update alias set is_main_address = 1 where address = 'ana.jaleco@iscte.pt';
update alias set is_main_address = 1 where address = 'ana.xavier@iscte.pt';
update alias set is_main_address = 1 where address = 'alexandra.fernandes@iscte.pt';
update alias set is_main_address = 1 where address = 'a.freitasmiguel@iscte.pt';
update alias set is_main_address = 1 where address = 'alexandre.almeida@iscte.pt';
update alias set is_main_address = 1 where address = 'antonio.santos@iscte.pt';
update alias set is_main_address = 1 where address = 'anapaula.nazario@iscte.pt';
update alias set is_main_address = 1 where address = 'Raquel.Cruz@iscte.pt';
update alias set is_main_address = 1 where address = 'ana.leitao@iscte.pt';
update alias set is_main_address = 1 where address = 'Ana.Sofia.Henriques@iscte.pt';
update alias set is_main_address = 1 where address = 'bruno.gonca@iscte.pt';
update alias set is_main_address = 1 where address = 'carla.isidro@iscte.pt';
update alias set is_main_address = 1 where address = 'carlos.goncalves@iscte.pt';
update alias set is_main_address = 1 where address = 'clementina.barroso@iscte.pt';
update alias set is_main_address = 1 where address = 'Cristina.Carreira@iscte.pt';
update alias set is_main_address = 1 where address = 'david.bracke@iscte.pt';
update alias set is_main_address = 1 where address = 'duarte.trigueiros@iscte.pt';
update alias set is_main_address = 1 where address = 'decio.telo@iscte.pt';
update alias set is_main_address = 1 where address = 'Elizabeth.Reis@iscte.pt';
update alias set is_main_address = 1 where address = 'Elsa.Coimbra@iscte.pt';
update alias set is_main_address = 1 where address = 'edgar.decca@iscte.pt';
update alias set is_main_address = 1 where address = 'francisco.cercas@iscte.pt';
update alias set is_main_address = 1 where address = 'fernanda.alvim@iscte.pt';
update alias set is_main_address = 1 where address = 'filipe.carreira@iscte.pt';
update alias set is_main_address = 1 where address = 'guilherme.statter@iscte.pt';
update alias set is_main_address = 1 where address = 'semanaromani@iscte.pt';
update alias set is_main_address = 1 where address = 'isabel.freitas@iscte.pt';
update alias set is_main_address = 1 where address = 'Isabel.Santos@iscte.pt';
update alias set is_main_address = 1 where address = 'Maria.Joao.Amante@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.afonso@iscte.pt';
update alias set is_main_address = 1 where address = 'castro.pinto@iscte.pt';
update alias set is_main_address = 1 where address = 'constantino.ferreira@iscte.pt';
update alias set is_main_address = 1 where address = 'eduardo.simoes@iscte.pt';
update alias set is_main_address = 1 where address = 'filipe.verde@iscte.pt';
update alias set is_main_address = 1 where address = 'ferreira.dias@iscte.pt';
update alias set is_main_address = 1 where address = 'ferreira.almeida@iscte.pt';
update alias set is_main_address = 1 where address = 'joao.goncalves.nunes@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.ferreira@iscte.pt';
update alias set is_main_address = 1 where address = 'joao.monteiro@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.serrao@iscte.pt';
update alias set is_main_address = 1 where address = 'crespo.carvalho@iscte.pt';
update alias set is_main_address = 1 where address = 'jorge.l@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.viegas@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.monteiro@iscte.pt';
update alias set is_main_address = 1 where address = 'prostes.da.fonseca@iscte.pt';
update alias set is_main_address = 1 where address = 'jorge.costa@iscte.pt';
update alias set is_main_address = 1 where address = 'jose.maia@iscte.pt';
update alias set is_main_address = 1 where address = 'pedro.almeida@iscte.pt';
update alias set is_main_address = 1 where address = 'joao.pereira@iscte.pt';
update alias set is_main_address = 1 where address = 'joao.silva.batista@iscte.pt';
update alias set is_main_address = 1 where address = 'liliana.silva@iscte.pt';
update alias set is_main_address = 1 where address = 'luis.reto@iscte.pt';
update alias set is_main_address = 1 where address = 'luis.ducla.soares@iscte.pt';
update alias set is_main_address = 1 where address = 'luis.campos@iscte.pt';
update alias set is_main_address = 1 where address = 'larysa.shotropa@iscte.pt';
update alias set is_main_address = 1 where address = 'marcia.antunes@iscte.pt';
update alias set is_main_address = 1 where address = 'marcos.farias@iscte.pt';
update alias set is_main_address = 1 where address = 'mohamed.azzim@iscte.pt';
update alias set is_main_address = 1 where address = 'antonieta.dias@iscte.pt';
update alias set is_main_address = 1 where address = 'maria.rodrigues@iscte.pt';
update alias set is_main_address = 1 where address = 'adelia.andrade@iscte.pt';
update alias set is_main_address = 1 where address = 'Soraya.Genin@iscte.pt';

-- TODO...