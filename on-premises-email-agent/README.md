On Premises Email Agent
==============
Update postfix database

## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

**Configuration**
To configure the application change the *src/main/resources/configuration.properties* file.

**Debug**
To debug define first this environment variable:
export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8003,server=y,suspend=n"

**Run embedded tomcat**
To run application using an embedded tomcat use
mvn tomcat7:run

**Run embedded tomcat on different port**
On different port use
mvn tomcat7:run -Dmaven.tomcat.port=8083

Test using
http://localhost:8083/email-agent/

**Configuration**
To configure the web application change the *src/main/resources/configuration.properties* file.

# Deployment
Generate war
mvn clean package

War is generated on
target/ad-agent-<version>.war

