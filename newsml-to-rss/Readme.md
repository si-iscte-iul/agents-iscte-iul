Web app transforms the news from newsML to rss
==============
This web application transforms the newsML to rss.
It transform each news, that's a xml file, to a single rss file.

# Dev

Cleans, runs the tests and run it on tomcat7 embedded mode on port 8082.
$ mvn clean test tomcat7:run -Dmaven.tomcat.port=8082

## Maven

Tested on latest maven, version 3.0.5

## Eclipse

To generate .project and .classpath files in orther this project can be imported to eclipse, then you should run following command.

$ mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use.

$ mvn eclipse:clean eclipse:eclipse

## Configuration / Profiles

To configure this application for production change the **src/main/resources/configuration.properties** file.
For specific dev properties change the **src/main/resources/dev/configuration.properties**

By default the properties on **src/main/resources/configuration.properties** are for production.
But there is a **dev** profile that permit to override the default ones with **src/main/resources/dev/configuration.properties** file.

## Debug

To debug define first this environment variable:

$ export MAVEN_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8002,server=y,suspend=n"

## Run an embedded tomcat

To run this application quickly on development mode run (on 8080 port).

$ mvn clean tomcat7:run

To run on different port

$ mvn clean tomcat7:run -Dmaven.tomcat.port=8082

## Tests

To run the tests

$ mvn clean test

## Deployment

To Generate the war file to be installed on a web server run:

$ mvn clean package

the war is generated on target/inline-webapp-<version>.war
