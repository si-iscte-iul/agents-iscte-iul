package pt.iscte.newsmltorss.parser;

import java.io.InputStream;
import java.io.Reader;

import groovy.xml.MarkupBuilder;
import groovy.xml.NamespaceBuilder;
import groovy.util.XmlSlurper

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.*;

import pt.iscte.newsmltorss.domain.NewsItem;

/**
 * This class parses a NewsML xml file and returns a NewsItem domain entity.
 */
class NewsMLParser {

    private def outputDir = "."

    private Logger logger = LoggerFactory.getLogger(this.class.name);

    public NewsItem parseNewsML(Reader newsMLReader) {
        logger.debug("Parsing a newsml")
        def content = newsMLReader.text
    	def records = new XmlSlurper().parseText(content)
        def headLine = records.NewsItem.NewsComponent.NewsLines.HeadLine.text()
        def subHeadLine = records.NewsItem.NewsComponent.NewsLines.SubHeadLine.text()
        def fullNews = records.NewsItem.NewsComponent.ContentItem.DataContent.text()
        def publicIdentifier = records.NewsItem.Identification.NewsIdentifier.PublicIdentifier.text()
        def publicDate = records.NewsEnvelope.DateAndTime.text()
        logger.debug("headLine: {}, subHeadLine: {}, fullNews: {}, publicIdentifier: {}", headLine, subHeadLine, fullNews, publicIdentifier, publicDate)
        return new NewsItem(headLine, subHeadLine, fullNews, publicIdentifier, publicDate, "http://www.iscte-iul.pt")
    }

}

