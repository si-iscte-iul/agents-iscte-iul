package pt.iscte.newsmltorss.parser;

import java.io.Writer;

import groovy.xml.StreamingMarkupBuilder;
import groovy.xml.XmlUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pt.iscte.newsmltorss.domain.NewsItem;

/**
 * This class creates a rss given a NewsItem domain entity.
 */
// Component annotation creates a bean automatically
class RssGenerator {

    private String rssChannelTitle;
    private String rssChannelDescription;
    private String rssChannelLink;

    public RssGenerator(String rssChannelTitle, String rssChannelDescription, String rssChannelLink) {
        this.rssChannelTitle = rssChannelTitle;
        this.rssChannelDescription = rssChannelDescription;
        this.rssChannelLink = rssChannelLink;
    }

    private Logger logger = LoggerFactory.getLogger(this.class.name);

    public String generatesRss(List<NewsItem> newsItems) {
        logger.debug "Generating rss file with {} news", newsItems.size()
        def builder = new StreamingMarkupBuilder()
        builder.useDoubleQuotes = true
        def writable = builder.bind {
            rss(version: "2.0") {
                channel {
                    title(this.rssChannelTitle)
                    description(this.rssChannelDescription)
                    link(this.rssChannelLink)
                    newsItems.each { newsItem ->
                        item {
                            title (
                                newsItem.headLine
                            )
                            description (
                                newsItem.subHeadLine
                            )
                            guid (
                                newsItem.publicIdentifier
                            )
                            pubDate (
                                newsItem.publicDateOnRFC822Format
                            )
                            link (
                                newsItem.link
                            )
                        }
                    }
                }
            }
        }
        return writable
    }

}

