package pt.iscte.newsmltorss.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import pt.iscte.newsmltorss.parser.NewsMLParser;
import pt.iscte.newsmltorss.parser.RssGenerator;

/**
 * Registers the spring beans.
 *
 * @author Ivo Branco
 *
 */
//Marks this class as configuration to the spring
@Configuration
//Enables Spring's MVC annotations
@EnableWebMvc
@PropertySources({ @PropertySource(value = "classpath:configuration.properties"),
        @PropertySource(value = "classpath:${deploy.profile}/configuration.properties", ignoreResourceNotFound = true), })
public class Config {

    /**
     * Bean that permit to read properties from property files dynamically.
     *
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    // read properties from property file and put them on the fields.

    @Value("${rss.channel.title}")
    String rssChannelTitle;

    @Value("${rss.channel.description}")
    String rssChannelDescription;

    @Value("${rss.channel.link}")
    String rssChannelLink;

    @Bean
    public RssGenerator rssGenerator() {
        return new RssGenerator(rssChannelTitle, rssChannelDescription, rssChannelLink);
    }

    @Bean
    public NewsMLParser newsMLParser() {
        return new NewsMLParser();
    }
}
