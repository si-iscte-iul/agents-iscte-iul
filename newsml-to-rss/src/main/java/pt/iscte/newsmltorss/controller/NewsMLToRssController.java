package pt.iscte.newsmltorss.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pt.iscte.newsmltorss.domain.NewsItem;
import pt.iscte.newsmltorss.parser.NewsMLParser;
import pt.iscte.newsmltorss.parser.RssGenerator;

/**
 * Web Controller that receives the requests of the rss feed.
 *
 * @author Ivo Branco
 *
 */
@RestController
public class NewsMLToRssController {

    private static final Logger logger = LoggerFactory.getLogger(NewsMLToRssController.class);

    @Autowired
    NewsMLParser newsMLParser;

    @Autowired
    RssGenerator rssGenerator;

    @Value("${newsml.directory}")
    String newsMLDirectory;

    @Value("${newsml.files.encoding:ISO-8859-1}")
    String newsMLFilesEncoding;

    @Value("${rss.max.number.of.news.to.include:#{null}}")
    Integer rssMaxNumberOfNewsToInclude;

    private static final String RSS_FILE_ENCODING = "utf-8";

    @RequestMapping(value = "/", produces = "application/rss+xml; charset=" + RSS_FILE_ENCODING)
    @ResponseBody
    public String home() {
        logger.info("Receiving a request to transform newsml to rss.");
        File folder = new File(newsMLDirectory);
        if (!folder.exists()) {
            logger.error("The folder where the newsml files are placed do not exists {}", newsMLDirectory);
        }
        List<NewsItem> newsItems = readNewsItemsFromFolder(folder, newsMLFilesEncoding);
        List<NewsItem> newsItemsSorted = NewsItem.COMPARATOR_BY_PUBLIC_DATE.reverse().sortedCopy(newsItems);
        List<NewsItem> subNewsItemsSorted = getNewsItemsFirstNItems(newsItemsSorted, rssMaxNumberOfNewsToInclude);
        return rssGenerator.generatesRss(subNewsItemsSorted);
    }

    private static List<NewsItem> getNewsItemsFirstNItems(List<NewsItem> newsItemsSorted, Integer rssMaxNumberOfNewsToInclude) {
        int newsCount = newsItemsSorted.size();
        int to = newsCount;
        if (rssMaxNumberOfNewsToInclude != null) {
            to = Math.min(newsCount, rssMaxNumberOfNewsToInclude);
        }
        List<NewsItem> subNewsItemsSorted = newsItemsSorted.subList(0, to);
        return subNewsItemsSorted;
    }

    private static List<NewsItem> readNewsItemsFromFolder(File folder, String newsMLFilesEncoding) {
        List<NewsItem> newsItems = new ArrayList<NewsItem>();
        for (File file : listFilesForFolder(folder)) {
            String name = file.getName();
            logger.debug("News file: ", name);
            if (name.endsWith(".xml")) {
                InputStream newsMLInputStream;
                try {
                    newsMLInputStream = new FileInputStream(file);
                } catch (FileNotFoundException e) {
                    logger.error("A file not found exception has been thrown", e);
                    throw new RuntimeException(e);
                }
                Reader newsMLReader;
                try {
                    newsMLReader = new InputStreamReader(newsMLInputStream, newsMLFilesEncoding);
                    NewsMLParser newsMLParser = new NewsMLParser();
                    NewsItem newsItem = newsMLParser.parseNewsML(newsMLReader);
                    newsItems.add(newsItem);
                } catch (Exception e) {
                    logger.error("A generic exception has been thrown", e);
                    throw new RuntimeException(e);
                } finally {
                    try {
                        newsMLInputStream.close();
                    } catch (IOException e) {
                        logger.error("Couldn't close the newsml input stream", e);
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        return newsItems;
    }

    private static Set<File> listFilesForFolder(final File folder) {
        Set<File> files = new HashSet<File>();
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory()) {
                files.add(fileEntry);
            }
        }
        return files;
    }

}
