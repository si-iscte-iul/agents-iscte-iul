package pt.iscte.newsmltorss.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Ordering;

/**
 * Represents a news with has a title, a sub
 *
 * @author Ivo Branco
 *
 */
public class NewsItem {

    public static final Ordering<NewsItem> COMPARATOR_BY_PUBLIC_DATE = new Ordering<NewsItem>() {

        @Override
        public int compare(NewsItem left, NewsItem right) {
            return left.getPublicDate().compareTo(right.getPublicDate());
        }

    };

    private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
    private static final SimpleDateFormat RFC_822_DATE_FORMAT = new SimpleDateFormat("E, d MMM yyyy HH:mm:ss Z", Locale.US);
    private static final Logger logger = LoggerFactory.getLogger(NewsItem.class);

    private String headLine;
    private String subHeadLine;
    private String fullNews;
    private String publicIdentifier;
    private Date publicDate;
    private String link;

    public NewsItem(final String headLine, final String subHeadLine, final String fullNews, final String publicIdentifier,
            final String publicDate, final String link) {
        this.headLine = headLine;
        this.subHeadLine = subHeadLine;
        this.fullNews = fullNews;
        this.publicIdentifier = publicIdentifier;
        this.publicDate = null;
        if (publicDate != null) {
            try {
                this.publicDate = INPUT_DATE_FORMAT.parse(publicDate);
            } catch (ParseException e) {
                logger.error("Error parsing date: " + publicDate, e);
            }
        }
        this.link = link;
    }

    public String getHeadLine() {
        return headLine;
    }

    public String getSubHeadLine() {
        return subHeadLine;
    }

    public String getFullNews() {
        return fullNews;
    }

    public String getPublicIdentifier() {
        return publicIdentifier;
    }

    public Date getPublicDate() {
        return publicDate;
    }

    public String getPublicDateOnRFC822Format() {
        if (getPublicDate() != null) {
            return RFC_822_DATE_FORMAT.format(getPublicDate());
        }
        return null;
    }

    public String getLink() {
        return link;
    }
}
