package pt.iscte.newsmltorss.parser;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Scanner;

import org.junit.Test;

import pt.iscte.newsmltorss.domain.NewsItem;

/**
 * Tests the newsMl parser component.
 *
 * @author Ivo Branco
 *
 */
public class TestNewsMLParser {

    private NewsItem parse(String file) {
        InputStream newsMLInputStream = getClass().getResourceAsStream(file);
        Reader newsMLReader;
        try {
            newsMLReader = new InputStreamReader(newsMLInputStream, "ISO-8859-1");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        NewsMLParser newsMLParser = new NewsMLParser();
        NewsItem newsItem = newsMLParser.parseNewsML(newsMLReader);
        return newsItem;
    }

    @Test
    public void testHeadLinePasses() {
        NewsItem newsItem = parse("/examples/17779724.xml");
        assertEquals("LusaVídeo: \"O Chef\" estreia esta semana nas salas nacionais (pré-editado)", newsItem.getHeadLine());
    }

    @Test
    public void testSubHeadLineEmptyValue() {
        NewsItem newsItem = parse("/examples/17779724.xml");
        assertEquals("", newsItem.getSubHeadLine());
    }

    @Test
    public void testFullNewsEmptyValue() {
        NewsItem newsItem = parse("/examples/17779724.xml");
        assertEquals("", newsItem.getFullNews());
    }

    @Test
    public void testHeadLinePasses2() {
        NewsItem newsItem = parse("/examples/17788112.xml");
        assertEquals("Cerca de 50 médicos cubanos começam a trabalhar em Portugal dentro de um a dois meses",
                newsItem.getHeadLine());
    }

    @Test
    public void testSubHeadLinePasses() {
        NewsItem newsItem = parse("/examples/17788112.xml");
        assertEquals(
                "Lisboa, 28 mai (Lusa) -- Cerca de 50 médicos cubanos deverão começar a trabalhar em Portugal dentro de um a dois meses, principalmente nas zonas onde é reconhecida a falta destes profissionais, revelou o secretário de Estado adjunto do ministro da Saúde.",
                newsItem.getSubHeadLine());
    }

    @Test
    public void testFullNewsPasses() {
        NewsItem newsItem = parse("/examples/17788112.xml");
        InputStream expectedInputStream = getClass().getResourceAsStream("/TestNewsMLParser_testSubHeadLinePasses.txt");
        Scanner scanner = new Scanner(expectedInputStream);
        Scanner s = scanner.useDelimiter("\\A");
        String expectedValue = s.hasNext() ? s.next() : "";
        scanner.close();
        assertEquals(expectedValue, newsItem.getFullNews());
    }

}
