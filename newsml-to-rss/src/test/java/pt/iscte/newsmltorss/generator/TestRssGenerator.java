package pt.iscte.newsmltorss.generator;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.Test;

import pt.iscte.newsmltorss.domain.NewsItem;
import pt.iscte.newsmltorss.parser.RssGenerator;

/**
 * Tests the newsMl parser component.
 *
 * @author Ivo Branco
 *
 */
public class TestRssGenerator {

    @Test
    public void testRssGeneration() {
        NewsItem newsItem =
                new NewsItem("An important head line about something", "A short line that describes the news ",
                        "A full description of the news. A full description of the news. A full description of the news.",
                        "12345678", "20140903T142630", "http://www.iscte.pt");
        RssGenerator rssGenerator = new RssGenerator("TITLE", "DESCRIPTION", "http:/www.iscte-iul.pt");
        String actualContent = rssGenerator.generatesRss(Collections.singletonList(newsItem));

        assertTrue("title do not match, actual content is: " + actualContent,
                actualContent.contains("<title>An important head line about something</title>"));

        assertTrue("description do not match",
                actualContent.contains("<description>A short line that describes the news </description>"));
    }

}
