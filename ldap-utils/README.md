Ldap Utils
=========

## Dev

**Maven**
Tested on latest maven, version 3.0.4

**Eclipse**
Generate .project and .classpath files run
mvn eclipse:eclipse

To revert your eclipse configuration and recreate it again use
mvn eclipse:clean eclipse:eclipse

## Deployment

**Install**
Compile and install the jar to the local maven repository using:
mvn clean install

