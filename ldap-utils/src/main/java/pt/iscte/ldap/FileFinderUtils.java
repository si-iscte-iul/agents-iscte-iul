package pt.iscte.ldap;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

public class FileFinderUtils {

    public static void scan(File path, List<File> list, FilenameFilter filter, final String name,
	    final int searchingDepth, final int maximumSearchDepth) throws IOException {

	if (searchingDepth > maximumSearchDepth) {
	    return;
	}

	// Get filtered files in the current path
	File[] files = path.listFiles(filter);

	// Process each filtered entry
	if(files == null) {
	    return;
	}
	
	for (final File file : files) {
	    if(!file.isDirectory()) {
		continue;
	    }
	    if (file.getName().equals(name)) {
		list.add(file);
	    } else {
		scan(file, list, filter, name, searchingDepth + 1, maximumSearchDepth);
	    }
	}
    }
}