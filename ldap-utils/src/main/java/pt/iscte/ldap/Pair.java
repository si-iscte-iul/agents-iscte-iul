package pt.iscte.ldap;

/**
 * A pair is simple aggregation of two values. This class can be used to keep
 * two values together, like a key and value, without depending in any
 * particular data structure.
 * 
 * @author <a href="mailto:goncalo@ist.utl.pt">Goncalo Luiz</a> <br/>
 * <br/>
 * <br/>
 *         Created on 13:27:42,31/Mar/2006
 * @version $Id: Pair.java 34270 2008-01-31 10:11:52Z pacz $
 */
public class Pair<K, V> {

    private K key;
    private V value;

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public K getFirst() {
        return key;
    }

    public V getSecond() {
        return value;
    }

    @Override
    public int hashCode() {
        return (getFirst() == null ? super.hashCode() : getFirst().hashCode())
                + (getSecond() == null ? super.hashCode() : getSecond().hashCode());
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof Pair)) {
            return false;
        }
        final Pair otherPair = (Pair) other;
        return (otherPair.getFirst() == this.getFirst() || otherPair.getFirst().equals(this.getFirst()))
                && (otherPair.getSecond() == this.getSecond() || otherPair.getSecond().equals(this.getSecond()));
    }

    @Override
    public String toString() {
        return "Pair(" + getKey() + ", " + getValue() + ")";
    }

}