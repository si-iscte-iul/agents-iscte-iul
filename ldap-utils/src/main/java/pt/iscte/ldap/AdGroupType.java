package pt.iscte.ldap;

/**
 * The Active Directory group type constants can be found here:
 * http://msdn.microsoft.com/en-us/library/aa772263.aspx
 * 
 * group types from IAds.h:
 * <ul>
 * <li>ADS_GROUP_TYPE_GLOBAL_GROUP = 0x0002</li>
 * <li>ADS_GROUP_TYPE_DOMAIN_LOCAL_GROUP = 0x0004</li>
 * <li>ADS_GROUP_TYPE_LOCAL_GROUP = 0x0004</li>
 * <li>ADS_GROUP_TYPE_UNIVERSAL_GROUP = 0x0008</li>
 * <li>ADS_GROUP_TYPE_SECURITY_ENABLED = 0x80000000</li>
 * </ul>
 */
public enum AdGroupType {
    SECURITY_ENABLED(0x80000000);

    private int code;

    AdGroupType(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
