package pt.iscte.ldap;

public enum LDAPAttribute {

    DESCRIPTION("description"),

    LOCKOUT_TIME("lockoutTime"),

    NAME("name"),

    DEPARTMENT("department"),

    LOGIN_SHELL("loginShell"),

    COMPANY("company"),

    UID_NUMBER("uidNumber"),

    GID_NUMBER("gidNumber"),

    UNIX_HOME_DIRECTORY("unixHomeDirectory"),

    TITLE("title"),

    WWWHOME_PAGE("wWWHomePage"),

    PHYSICAL_DELIVERY_OFFICE_NAME("physicalDeliveryOfficeName"),

    TELEPHONE_NUMBER("telephoneNumber"),

    SCRIPT_PATH("scriptPath"),

    HOME_DIRECTORY("homeDirectory"),

    HOME_DRIVE("homeDrive"),

    MAIL("mail"),

    DISPLAY_NAME("displayName"),

    SN("sn"),

    GIVEN_NAME("givenName"),

    CN("cn"),

    OBJECT_CLASS("objectClass"),

    OBJECT_SID("objectSid"),

    SAM_ACCOUNT_NAME("sAMAccountName"),

    USER_ACCOUNT_CONTROL("userAccountControl"),

    PROFILE_PATH("profilePath"),

    DISTINGUISHED_NAME("distinguishedName"),

    USER_PRINCIPAL_NAME("userPrincipalName"),
    
    JPEG_PHOTO("jpegPhoto"),
    
    MEMBER("member"),
    
    UNICODE_PWD("unicodePwd"),
    
    PAGER("pager"),
	
	EASYVISTA_OU("easyvistaou"),
	
	EASYVISTA_OFFICE("easyvistalocation"),
	
	HOME_PHONE("homePhone"),
    
    IP_PHONE("ipPhone"),
    
    EMPLOYEE_NUMBER("employeeNumber"),
    
    SECOND_EMAIL("secondEmail"),
    
    ID_DOCUMENT_NUMBER("nic"),
    
    STUDENT_ID_ISCTE("StudentIDIscte");

    private String name;

    private LDAPAttribute(final String name) {
	this.name = name;
    }

    public String getAttribute() {
	return name;
    }
}