package pt.iscte.ldap;

/**
 * This class has methods to handle the Active Directory ObjectSid attribute.
 * 
 * <p>
 * Currently it has only one method convertToString.
 * </p>
 * 
 * <p>
 * There source code contains 2 implementations of the method, one from the from Apache Directory Project and another one from a
 * <a href="http://www.jroller.com/eyallupu/entry/java_jndi_how_to_convert">blog<a>.
 * </p>
 * 
 * I've decided <b>not to use</b> the Apache implementation because it depends on <i>commons-codec</i>. Before making this
 * decision, I've tested both implementations and notice that they give the same result for all the users in our EMP Active
 * Directory.
 * 
 * @author Antonio.Casqueiro
 */
public class SidConverter {

//    /**
//     * Code from Apache Directory Project.
//     * 
//     * @see <a href=
//     *      "http://svn.apache.org/repos/asf/directory/studio/trunk/plugins/valueeditors/src/main/java/org/apache/directory/studio/valueeditors/msad/InPlaceMsAdObjectSidValueEditor.java">InPlaceMsAdObjectSidValueEditor.java</a>
//     * @see <a href="http://msdn.microsoft.com/en-us/library/cc230371(PROT.10).aspx">
//     *      http://msdn.microsoft.com/en-us/library/cc230371(PROT.10).aspx</a> for details
//     * @param bytes SID has an array of bytes
//     * @return SID has a String
//     * @author <a href="mailto:dev@directory.apache.org">Apache Directory Project</a>
//     */
//    public static String convertToString(byte[] bytes) {
//        /*
//         * The binary data structure, from http://msdn.microsoft.com/en-us/library/cc230371(PROT.10).aspx:
//         *   byte[0] - Revision (1 byte): An 8-bit unsigned integer that specifies the revision level of the SID structure. This value MUST be set to 0x01.
//         *   byte[1] - SubAuthorityCount (1 byte): An 8-bit unsigned integer that specifies the number of elements in the SubAuthority array. The maximum number of elements allowed is 15.
//         *   byte[2-7] - IdentifierAuthority (6 bytes): A SID_IDENTIFIER_AUTHORITY structure that contains information, which indicates the authority under which the SID was created. It describes the entity that created the SID and manages the account.
//         *               Six element arrays of 8-bit unsigned integers that specify the top-level authority 
//         *               big-endian!
//         *   and then - SubAuthority (variable): A variable length array of unsigned 32-bit integers that uniquely identifies a principal relative to the IdentifierAuthority. Its length is determined by SubAuthorityCount. 
//         *              little-endian!
//         */
//
//        if (bytes == null || bytes.length < 8) {
////            return Messages.getString("InPlaceMsAdObjectSidValueEditor.InvalidSid"); //$NON-NLS-1$
//            return "InPlaceMsAdObjectSidValueEditor.InvalidSid"; //$NON-NLS-1$
//        }
//
//        char[] hex = Hex.encodeHex(bytes);
//        StringBuffer sb = new StringBuffer();
//
//        // start with 'S'
//        sb.append('S');
//
//        // revision
//        int revision = Integer.parseInt(new String(hex, 0, 2), 16);
//        sb.append('-');
//        sb.append(revision);
//
//        // get count
//        int count = Integer.parseInt(new String(hex, 2, 2), 16);
//
//        // check length
//        if (bytes.length != (8 + count * 4)) {
////            return Messages.getString("InPlaceMsAdObjectSidValueEditor.InvalidSid"); //$NON-NLS-1$
//            return "InPlaceMsAdObjectSidValueEditor.InvalidSid"; //$NON-NLS-1$
//        }
//
//        // get authority, big-endian
//        long authority = Long.parseLong(new String(hex, 4, 12), 16);
//        sb.append('-');
//        sb.append(authority);
//
//        // sub-authorities, little-endian
//        for (int i = 0; i < count; i++) {
//            StringBuffer rid = new StringBuffer();
//            for (int k = 3; k >= 0; k--) {
//                rid.append(hex[16 + (i * 8) + (k * 2)]);
//                rid.append(hex[16 + (i * 8) + (k * 2) + 1]);
//            }
//
//            long subAuthority = Long.parseLong(rid.toString(), 16);
//            sb.append('-');
//            sb.append(subAuthority);
//        }
//
//        return sb.toString();
//    }

    /**
     * The original method name was "getSIDAsString".
     * 
     * I've changed it to "convertToString" to match the name of the Apache implementation.
     * 
     * @param SID the ObjectSID in binary
     * @return The SID representation has a String
     * @see http://www.jroller.com/eyallupu/entry/java_jndi_how_to_convert
     */
    public static String convertToString(byte[] SID) {
        // Add the 'S' prefix
        StringBuilder strSID = new StringBuilder("S-");

        // bytes[0] : in the array is the version (must be 1 but might 
        // change in the future)
        strSID.append(SID[0]).append('-');

        // bytes[2..7] : the Authority
        StringBuilder tmpBuff = new StringBuilder();
        for (int t = 2; t <= 7; t++) {
            String hexString = Integer.toHexString(SID[t] & 0xFF);
            tmpBuff.append(hexString);
        }
        strSID.append(Long.parseLong(tmpBuff.toString(), 16));

        // bytes[1] : the sub authorities count
        int count = SID[1];

        // bytes[8..end] : the sub authorities (these are Integers - notice
        // the endian)
        for (int i = 0; i < count; i++) {
            int currSubAuthOffset = i * 4;
            tmpBuff.setLength(0);
            tmpBuff.append(String.format("%02X%02X%02X%02X", (SID[11 + currSubAuthOffset] & 0xFF),
                    (SID[10 + currSubAuthOffset] & 0xFF), (SID[9 + currSubAuthOffset] & 0xFF),
                    (SID[8 + currSubAuthOffset] & 0xFF)));

            strSID.append('-').append(Long.parseLong(tmpBuff.toString(), 16));
        }

        // That's it - we have the SID
        return strSID.toString();
    }
}