package pt.iscte.ldap;

import org.apache.log4j.Logger;

class TimeKeeper extends Thread {

    private static final Logger logger = Logger.getLogger(LDAPUtilsGeneric.class);

    long timeout; // in milliseconds
    long timerStart;
    Thread parent;

    TimeKeeper(long timeout) {
        parent = Thread.currentThread();
        this.timeout = timeout;
        this.start();
    }

    public static void start(long timeout) {
        new TimeKeeper(timeout);
    }

    public void run() {
        timerStart = System.currentTimeMillis();

        long waitTime = timeout;

        while (waitTime > 0) {
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException ie) {
                // ignore and continue
            }
            waitTime = timeout - (System.currentTimeMillis() - timerStart);
        }
        logger.error("TIMER EXCEEDED!");
        parent.interrupt();
    }
}
