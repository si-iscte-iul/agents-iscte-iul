package pt.iscte.ldap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;

import org.apache.log4j.Logger;

/**
 * 
 * This class provides various methods to query and modify data in LDAP servers.
 * 
 * @author Benjamin Sick
 * @author Antonio Casqueiro (maintenance)
 */
public class LDAPUtilsGeneric {

    private static final String QUOTE = "\"";
    private static final String USER = "user";
    private static final String COMMA = ",";
    private static final String UTF_16LE = "UTF-16LE";

    // nowhere documented, try and error
    private static final int MAXIMUM_COMMON_NAME_LENGTH = 64;

    private static final Logger logger = Logger.getLogger(LDAPUtilsGeneric.class);

    private static final String ACCOUNT_ENABLED_BY_FENIX = "Activado pelo sistema Fenix";
    private static final String ACCOUNT_DISABLED_BY_FENIX = "Desactivado pelo sistema Fenix";

    static {
        Configuration.setAttributesToManage(LDAPAttribute.GIVEN_NAME, LDAPAttribute.SN, LDAPAttribute.DISPLAY_NAME,
                LDAPAttribute.DESCRIPTION, LDAPAttribute.HOME_DRIVE, LDAPAttribute.HOME_DIRECTORY, LDAPAttribute.SCRIPT_PATH,
                LDAPAttribute.PROFILE_PATH, LDAPAttribute.MAIL, LDAPAttribute.TELEPHONE_NUMBER,
                LDAPAttribute.PHYSICAL_DELIVERY_OFFICE_NAME, LDAPAttribute.WWWHOME_PAGE, LDAPAttribute.TITLE,
                LDAPAttribute.COMPANY, LDAPAttribute.DEPARTMENT, LDAPAttribute.JPEG_PHOTO, LDAPAttribute.PAGER,
                LDAPAttribute.EASYVISTA_OU, LDAPAttribute.EASYVISTA_OFFICE,

                LDAPAttribute.STUDENT_ID_ISCTE, LDAPAttribute.IP_PHONE,

                LDAPAttribute.ID_DOCUMENT_NUMBER, LDAPAttribute.SECOND_EMAIL, LDAPAttribute.EMPLOYEE_NUMBER

        );
    }

    public static boolean isUsernameInLdapBySamAccountName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return isUsernameInLdap(username, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean isUsernameInLdapByUserPrincipalName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return isUsernameInLdap(username, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean isUsernameInLdap(final String username, final LDAPAttribute ldapAttribute, final LdapConfig ldapConfig)
            throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + username + "))";
        return isPersonInLdap(searchFilter, ldapConfig);
    }

    public static boolean isCnInLdap(final String name, final LdapConfig ldapConfig) throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.CN.getAttribute() + "=" + name + "))";
        return isPersonInLdap(searchFilter, ldapConfig);
    }

    public static boolean isFamilyNameInLdap(final String name, final LdapConfig ldapConfig) throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.SN.getAttribute() + "=" + name + "))";
        return isPersonInLdap(searchFilter, ldapConfig);
    }

    public static boolean isGivenNameInLdap(final String name, final LdapConfig ldapConfig) throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.GIVEN_NAME.getAttribute() + "=" + name + "))";
        return isPersonInLdap(searchFilter, ldapConfig);
    }

    private static boolean isPersonInLdap(final String searchFilter, final LdapConfig ldapConfig) throws NamingException {
        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        // Create the initial directory context
        LdapContext ctx = new InitialLdapContext(env, null);

        SearchControls searchCtls = new SearchControls();

        // Specify the attributes to return
        String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(),
                LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute(), LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() };
        searchCtls.setReturningAttributes(returnedAtts);

        // Specify the search scope
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        // Specify the Base for the search
        String searchBase = ldapDcPath;

        // Search for objects using the filter
        NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
        boolean result = answer.hasMoreElements();

        answer.close();

        ctx.close();

        return result;
    }

    public static boolean isUpdateRequired(final LDAPUser userFromFenix, final LDAPUser userFromLdap) throws NamingException {

        boolean result = false;
        final Set<LDAPAttribute> attributesToManage = Configuration.getAttributesToManage();

        for (final LDAPAttribute attribute : attributesToManage) {
            switch (attribute) {
            case GIVEN_NAME:
                if (userFromFenix.getGivenName() != null && !userFromFenix.getGivenName().equals(userFromLdap.getGivenName())) {
                    logger.debug(attribute + userFromFenix.getGivenName() + userFromLdap.getGivenName());
                    result = true;
                }
                break;
            case SN:
                if (userFromFenix.getFamilyName() != null
                        && !userFromFenix.getFamilyName().equals(userFromLdap.getFamilyName())) {
                    logger.debug(attribute + userFromFenix.getFamilyName() + userFromLdap.getFamilyName());
                    result = true;
                }
                break;
            case DISPLAY_NAME:
                if (userFromFenix.getDisplayName() != null
                        && !userFromFenix.getDisplayName().equals(userFromLdap.getDisplayName())) {
                    logger.debug(attribute + userFromFenix.getDisplayName() + userFromLdap.getDisplayName());
                    result = true;
                }
                break;
            case DESCRIPTION:
                if (userFromFenix.getDescription() != null
                        && !userFromFenix.getDescription().equals(userFromLdap.getDescription())) {
                    logger.debug(attribute + userFromFenix.getDescription() + userFromLdap.getDescription()
                            + userFromFenix.getUsername());
                    result = true;
                }
                break;
            case HOME_DRIVE:
                if (userFromFenix.getHomeDrive() != null && !userFromFenix.getHomeDrive().equals(userFromLdap.getHomeDrive())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case HOME_DIRECTORY:
                if (userFromFenix.getHomeDirectory() != null
                        && !userFromFenix.getHomeDirectory().equals(userFromLdap.getHomeDirectory())) {
                    logger.debug(attribute + userFromFenix.getHomeDirectory() + userFromLdap.getHomeDirectory());
                    result = true;
                }
                break;
            case SCRIPT_PATH:
                if (userFromFenix.getScriptPath() != null
                        && !userFromFenix.getScriptPath().equals(userFromLdap.getScriptPath())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case PROFILE_PATH:
                if (userFromFenix.getProfilePath() != null
                        && !userFromFenix.getProfilePath().equals(userFromLdap.getProfilePath())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case MAIL:
                if (userFromFenix.getInstitutionalEmail() != null
                        && !userFromFenix.getInstitutionalEmail().equals(userFromLdap.getInstitutionalEmail())) {
                    logger.debug(attribute + userFromFenix.getInstitutionalEmail() + userFromLdap.getInstitutionalEmail());
                    result = true;
                }
                break;
            case TELEPHONE_NUMBER:
                if (userFromFenix.getWorkPhone() != null && !userFromFenix.getWorkPhone().equals(userFromLdap.getWorkPhone())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case PHYSICAL_DELIVERY_OFFICE_NAME:
                if (userFromFenix.getPhysicalDeliveryOfficeName() != null
                        && !userFromFenix.getPhysicalDeliveryOfficeName().equals(userFromLdap.getPhysicalDeliveryOfficeName())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case WWWHOME_PAGE:
                if (userFromFenix.getWWWHomePage() != null
                        && !userFromFenix.getWWWHomePage().equals(userFromLdap.getWWWHomePage())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case TITLE:
                if (userFromFenix.getTitle() != null && !userFromFenix.getTitle().equals(userFromLdap.getTitle())) {
                    logger.debug(attribute + userFromFenix.getTitle() + userFromLdap.getTitle());
                    result = true;
                }
                break;
            case UNIX_HOME_DIRECTORY:
                if (userFromFenix.getUnixHomeDirectory() != null
                        && !userFromFenix.getUnixHomeDirectory().equals(userFromLdap.getUnixHomeDirectory())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case GID_NUMBER:
                if (userFromFenix.getGidNumber() != null && !userFromFenix.getGidNumber().equals(userFromLdap.getGidNumber())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case UID_NUMBER:
                if (userFromFenix.getUidNumber() != null && !userFromFenix.getUidNumber().equals(userFromLdap.getUidNumber())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case COMPANY:
                if (userFromFenix.getCompany() != null && !userFromFenix.getCompany().equals(userFromLdap.getCompany())) {
                    logger.debug(attribute + userFromFenix.getCompany() + userFromLdap.getCompany());
                    result = true;
                }
                break;
            case LOGIN_SHELL:
                if (userFromFenix.getLoginShell() != null
                        && !userFromFenix.getLoginShell().equals(userFromLdap.getLoginShell())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case DEPARTMENT:
                if (userFromFenix.getDepartment() != null
                        && !userFromFenix.getDepartment().equals(userFromLdap.getDepartment())) {
                    logger.debug(attribute + userFromFenix.getDepartment() + userFromLdap.getDepartment());
                    result = true;
                }
                break;
            case CN:
                if (userFromFenix.getCommonName() != null
                        && !userFromFenix.getCommonName().equals(userFromLdap.getCommonName())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case JPEG_PHOTO:
                if (userFromFenix.getPersonalPhoto() != null
                        && !Arrays.equals(userFromFenix.getPersonalPhoto(), userFromLdap.getPersonalPhoto())) {
                    logger.debug(attribute);
                    result = true;
                }
                break;
            case EASYVISTA_OU:
                if (userFromFenix.getEasyVistaOu() != null
                        && !userFromFenix.getEasyVistaOu().equals(userFromLdap.getEasyVistaOu())) {
                    logger.debug(attribute + userFromFenix.getEasyVistaOu() + userFromLdap.getEasyVistaOu());
                }
                break;
            case EASYVISTA_OFFICE:
                if (userFromFenix.getEasyVistaOffice() != null
                        && !userFromFenix.getEasyVistaOffice().equals(userFromLdap.getEasyVistaOffice())) {
                    logger.debug(attribute + userFromFenix.getEasyVistaOffice() + userFromLdap.getEasyVistaOffice());
                }
                break;
            case HOME_PHONE:
                if (userFromFenix.getDocumentIdNumber() != null
                        && !userFromFenix.getDocumentIdNumber().equals(userFromLdap.getDocumentIdNumber())) {
                    logger.debug(attribute + userFromFenix.getDocumentIdNumber() + userFromLdap.getEasyVistaOffice());
                }
                break;
            case STUDENT_ID_ISCTE:
                if (userFromFenix.getStudentNumber() != null
                        && !userFromFenix.getStudentNumber().equals(userFromLdap.getStudentNumber())) {
                    logger.debug(attribute + userFromFenix.getStudentNumber() + userFromLdap.getStudentNumber());
                }
                break;

            // Must be update by a specific method
//            case IP_PHONE:
//                if (userFromFenix.getMobilePhoneMFA() != null
//                        && !userFromFenix.getMobilePhoneMFA().equals(userFromLdap.getMobilePhoneMFA())) {
//                    logger.debug(attribute + userFromFenix.getMobilePhoneMFA() + userFromLdap.getMobilePhoneMFA());
//                }
//                break;

            default:
                // Do nothing for the attributes not specified in a case statement
                break;
            }
        }
        return result;
    }

    public static boolean isPasswordCorrectByUserPrincipalName(final String username, final String password,
            final LdapConfig ldapConfig) throws NamingException {
        return isPasswordCorrect(username, password, ldapConfig);
    }

    public static boolean isPasswordCorrectBySamAccountName(final String username, final String password,
            final LdapConfig ldapConfig) throws NamingException {
        return isPasswordCorrect(username + "@" + ldapConfig.getUsernameSuffix(), password, ldapConfig);
    }

    private static boolean isPasswordCorrect(final String username, final String password, final LdapConfig ldapConfig)
            throws NamingException {
        final Hashtable<String, String> envDefault = ldapConfig.getEnvironment();

        final Hashtable<String, String> env = new Hashtable<String, String>();
        env.putAll(envDefault);

        env.put(Context.SECURITY_PRINCIPAL, username);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            // Create the initial directory context, throws AuthException if
            // password wrong
            InitialLdapContext ctx = new InitialLdapContext(env, null);
            ctx.close();

        } catch (AuthenticationException ae) {
            return false;
        }
        return true;
    }

    public static boolean isAccountDisabledBySamAccountName(final String userName, final LdapConfig ldapConfig)
            throws NamingException {
        return isAccountDisabled(userName, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean isAccountDisabledByUserPrincipalName(final String userName, final LdapConfig ldapConfig)
            throws NamingException {
        return isAccountDisabled(userName, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean isAccountDisabled(final String userName, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) throws NamingException {
        if (isUsernameInLdap(userName, ldapAttribute, ldapConfig)) {
            final Pair<Integer, String> pair = getUserAccountControlAndDescription(userName, ldapAttribute, ldapConfig);
            int num = getDigitHex(pair.getKey(), 0);
            return isAccountDisabled(pair, num);
        }
        return true;
    }

    private static boolean isAccountDisabled(final Pair<Integer, String> pair, int num) {
        num = num - 2;
        boolean result = (num == 9);
        result = result || (num == 8);
        result = result || (num == 1);
        result = result || (num == 0);
        return result && pair.getValue() != null && ACCOUNT_DISABLED_BY_FENIX.equals(pair.getValue());
    }

    private static Pair<Integer, String> getUserAccountControlAndDescription(final String username,
            final LDAPAttribute ldapAttribute, final LdapConfig ldapConfig) throws NamingException {

        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] =
                    { LDAPAttribute.DESCRIPTION.getAttribute(), LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + username + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            // Loop through the search results
            if (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                Attribute attr1 = (Attribute) sr.getAttributes().get(LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute());
                Attribute attr2 = (Attribute) sr.getAttributes().get(LDAPAttribute.DESCRIPTION.getAttribute());
                answer.close();
                return new Pair<Integer, String>(Integer.parseInt(attr1.get().toString()),
                        attr2 != null ? attr2.get().toString() : "");
            } else {
                answer.close();
                return new Pair<Integer, String>(-1, "");
            }

        } catch (NamingException e) {
            throw new RuntimeException("Error: Problem reading Account control and description");
        }
    }

    private static int getDigitHex(int value, int offset) {
        char[] arr = Integer.toHexString(value).toCharArray();
        offset = arr.length - 1 - offset;
        int intValue = Integer.parseInt(String.valueOf(arr[offset]));
        return intValue;
    }

    public static ArrayList<LDAPUser> getUsersFromLdapByCn(final String searchAttr, final LdapConfig ldapConfig)
            throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.CN.getAttribute() + "=" + searchAttr + "))";
        return getUsersFromLdap(searchFilter, ldapConfig);
    }

    public static ArrayList<LDAPUser> getUsersFromLdapByFamilyName(final String searchAttr, final LdapConfig ldapConfig)
            throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.SN.getAttribute() + "=" + searchAttr + "))";
        return getUsersFromLdap(searchFilter, ldapConfig);
    }

    public static ArrayList<LDAPUser> getUsersFromLdapByGivenName(final String searchAttr, final LdapConfig ldapConfig)
            throws NamingException {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.GIVEN_NAME.getAttribute() + "=" + searchAttr + "))";
        return getUsersFromLdap(searchFilter, ldapConfig);
    }

    public static ArrayList<LDAPUser> getUsersFromLdapByAll(final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user))";
        return getUsersFromLdap(searchFilter, ldapConfig);
    }

    public static ArrayList<String> getAllUsersReturnOnlySamAccountName(final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user))";
        ArrayList<LDAPUser> ldapUsers = getUsersFromLdap(searchFilter, Arrays.asList(LDAPAttribute.SAM_ACCOUNT_NAME), ldapConfig);

        ArrayList<String> usernames = new ArrayList<String>(ldapUsers.size());
        for (LDAPUser ldapUser : ldapUsers) {
            usernames.add(ldapUser.getUsernameWithoutSuffix());
        }

        return usernames;
    }

    public static ArrayList<LDAPUser> getAllUsersReturnOnlySamAccountNameAndDistinguishedName(final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user))";
        return getUsersFromLdap(searchFilter, Arrays.asList(LDAPAttribute.SAM_ACCOUNT_NAME, LDAPAttribute.DISTINGUISHED_NAME),
                ldapConfig);
    }

    private static ArrayList<LDAPUser> getUsersFromLdap(final String searchFilter, final LdapConfig ldapConfig) {
        return getUsersFromLdap(searchFilter, null, ldapConfig);
    }

    /*
     * Searches for all users in LDAP server and puts them in an ArrayList. Paging
     * is necessary because maximum number of results from LDAP is 1000 objects.
     * 
     * @return ArrayList<LDAPUser> all users in LDAP
     * 
     * @author Benjamin Sick
     */
    private static ArrayList<LDAPUser> getUsersFromLdap(final String searchFilter, List<LDAPAttribute> attributes,
            final LdapConfig ldapConfig) {
        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        ArrayList<LDAPUser> ldapUsers = new ArrayList<LDAPUser>();

        // Specify the Base for the search
        String searchBase = ldapDcPath;

        String commonName = null;
        String name = null;
        String username = null;
        String usernameWithoutSuffix = null;
        String givenName = null;
        String familyName = null;
        String displayName = null;
        String institutionalEmail = null;
        String workPhone = null;
        String description = null;
        String homeDrive = null;
        String homeDirectory = null;
        String scriptPath = null;
        String physicalDeliveryOfficeName = null;
        String wWWHomePage = null;
        String title = null;
        String unixHomeDirectory = null;
        String gidNumber = null;
        String uidNumber = null;
        String company = null;
        String loginShell = null;
        String department = null;
        String distinguishedName = null;
        String profilePath = null;
        byte[] personalPhoto = null;
        String pin = null;
        byte[] objectSid = null;
        String easyVistaOu = null;
        String easyVistaOffice = null;
        String documentIdNumber = null;
        String studentNumber = null;
        String mobilePhoneMDA = null;

        String employeeNumber = null;
        String secondEmail = null;

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            // Create the search controls
            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { "*" };
            if (attributes != null) {
                returnedAtts = attributes.stream().map(a -> a.getAttribute()).toArray(String[]::new);
            }

            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Set the page size and initialize the cookie that we pass back in
            // subsequent pages
            int pageSize = 100;
            byte[] cookie = null;

            // Request the paged results control
            Control[] ctls = new Control[] { new PagedResultsControl(pageSize, Control.CRITICAL) };

            ctx.setRequestControls(ctls);

            int pageNumber = 0;

            NamingEnumeration<SearchResult> results;

            // Search for objects using the filter

            do {
                results = ctx.search(searchBase, searchFilter, searchCtls);

                // loop through the results in each page

                while (results != null && results.hasMoreElements()) {
                    SearchResult sr = results.next();

                    // check all attributes, if they exist, assign them to the
                    // variables
                    Attributes attrs = sr.getAttributes();
                    if (attrs != null) {

                        NamingEnumeration<? extends Attribute> ae;
                        for (ae = attrs.getAll(); ae.hasMore();) {

                            Attribute attr = (Attribute) ae.next();

                            if (LDAPAttribute.HOME_DRIVE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                homeDrive = attr.get().toString();
                            } else if (LDAPAttribute.HOME_DIRECTORY.getAttribute().equalsIgnoreCase(attr.getID())) {
                                homeDirectory = attr.get().toString();
                            } else if (LDAPAttribute.NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                name = attr.get().toString();
                            } else if (LDAPAttribute.GIVEN_NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                givenName = attr.get().toString();
                            } else if (LDAPAttribute.SN.getAttribute().equalsIgnoreCase(attr.getID())) {
                                familyName = attr.get().toString();
                            } else if (LDAPAttribute.MAIL.getAttribute().equalsIgnoreCase(attr.getID())) {
                                institutionalEmail = attr.get().toString();
                            } else if (LDAPAttribute.DISPLAY_NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                displayName = attr.get().toString();
                            } else if (LDAPAttribute.DESCRIPTION.getAttribute().equalsIgnoreCase(attr.getID())) {
                                description = attr.get().toString();
                            } else if (LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                username = attr.get().toString();
                            } else if (LDAPAttribute.CN.getAttribute().equalsIgnoreCase(attr.getID())) {
                                commonName = attr.get().toString();
                            } else if (LDAPAttribute.SCRIPT_PATH.getAttribute().equalsIgnoreCase(attr.getID())) {
                                scriptPath = attr.get().toString();
                            } else if (LDAPAttribute.PHYSICAL_DELIVERY_OFFICE_NAME.getAttribute()
                                    .equalsIgnoreCase(attr.getID())) {
                                physicalDeliveryOfficeName = attr.get().toString();
                            } else if (LDAPAttribute.WWWHOME_PAGE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                wWWHomePage = attr.get().toString();
                            } else if (LDAPAttribute.TITLE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                title = attr.get().toString();
                            } else if (LDAPAttribute.TELEPHONE_NUMBER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                workPhone = attr.get().toString();
                            } else if (LDAPAttribute.UNIX_HOME_DIRECTORY.getAttribute().equalsIgnoreCase(attr.getID())) {
                                unixHomeDirectory = attr.get().toString();
                            } else if (LDAPAttribute.GID_NUMBER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                gidNumber = attr.get().toString();
                            } else if (LDAPAttribute.UID_NUMBER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                uidNumber = attr.get().toString();
                            } else if (LDAPAttribute.COMPANY.getAttribute().equalsIgnoreCase(attr.getID())) {
                                company = attr.get().toString();
                            } else if (LDAPAttribute.LOGIN_SHELL.getAttribute().equalsIgnoreCase(attr.getID())) {
                                loginShell = attr.get().toString();
                            } else if (LDAPAttribute.DEPARTMENT.getAttribute().equalsIgnoreCase(attr.getID())) {
                                department = attr.get().toString();
                            } else if (LDAPAttribute.DISTINGUISHED_NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                distinguishedName = attr.get().toString();
                            } else if (LDAPAttribute.PROFILE_PATH.getAttribute().equalsIgnoreCase(attr.getID())) {
                                profilePath = attr.get().toString();
                            } else if (LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute().equalsIgnoreCase(attr.getID())) {
                                usernameWithoutSuffix = attr.get().toString();
                            } else if (LDAPAttribute.JPEG_PHOTO.getAttribute().equalsIgnoreCase(attr.getID())) {
                                personalPhoto = (byte[]) (attr.get());
                            } else if (LDAPAttribute.PAGER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                pin = attr.get().toString();
                            } else if (LDAPAttribute.OBJECT_SID.getAttribute().equalsIgnoreCase(attr.getID())) {
                                objectSid = (byte[]) (attr.get());
                            } else if (LDAPAttribute.EASYVISTA_OU.getAttribute().equalsIgnoreCase(attr.getID())) {
                                easyVistaOu = attr.get().toString();
                            } else if (LDAPAttribute.EASYVISTA_OFFICE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                easyVistaOffice = attr.get().toString();
                            } else if (LDAPAttribute.STUDENT_ID_ISCTE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                studentNumber = attr.get().toString();
                            } else if (LDAPAttribute.IP_PHONE.getAttribute().equalsIgnoreCase(attr.getID())) {
                                mobilePhoneMDA = attr.get().toString();

                            } else if (LDAPAttribute.ID_DOCUMENT_NUMBER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                documentIdNumber = attr.get().toString();
                            } else if (LDAPAttribute.EMPLOYEE_NUMBER.getAttribute().equalsIgnoreCase(attr.getID())) {
                                employeeNumber = attr.get().toString();
                            } else if (LDAPAttribute.SECOND_EMAIL.getAttribute().equalsIgnoreCase(attr.getID())) {
                                secondEmail = attr.get().toString();
                            }

//                            NamingEnumeration e;
//                            for (e = attr.getAll(); e.hasMore();) {
//                                e.next();
//                            }
//                            e.close();
                        }
                        ae.close();
                        // for every existent user add an LDAPUser object to the
                        // list
                        ldapUsers.add(new LDAPUser(commonName, company, department, null, null, description, displayName,
                                familyName, gidNumber, givenName, homeDirectory, homeDrive, institutionalEmail, loginShell, name,
                                physicalDeliveryOfficeName, scriptPath, title, uidNumber, unixHomeDirectory, username,
                                usernameWithoutSuffix, wWWHomePage, workPhone, distinguishedName, profilePath, personalPhoto, pin,
                                objectSid, easyVistaOu, easyVistaOffice, documentIdNumber, studentNumber, mobilePhoneMDA,
                                employeeNumber, secondEmail));

                    }

                }

                // examine the response controls
                cookie = parseControls(ctx.getResponseControls(), pageNumber);

                pageNumber++;

                // pass the cookie back to the server for the next page
                ctx.setRequestControls(new Control[] { new PagedResultsControl(pageSize, cookie, Control.CRITICAL) });

            } while ((cookie != null) && (cookie.length != 0));

            results.close();
            ctx.close();

        } catch (NamingException e) {
            logger.error("Paged Search failed." + e);
        } catch (java.io.IOException e) {
            logger.error("Paged Search failed." + e);
        }

        // return the list with all ldap users found
        return ldapUsers;

    }

    /*
     * Helper method for paging of queries used by getAllUsersFromLdap()
     */
    private static byte[] parseControls(Control[] controls, int pageNumber) throws NamingException {

        byte[] cookie = null;

        if (controls != null) {

            for (int i = 0; i < controls.length; i++) {
                if (controls[i] instanceof PagedResultsResponseControl) {
                    PagedResultsResponseControl prrc = (PagedResultsResponseControl) controls[i];
                    cookie = prrc.getCookie();
                    // logger.info(">>Parsed page " + pageNumber + "\n");
                }
            }
        }

        return (cookie == null) ? new byte[0] : cookie;
    }

    /**
     * Create an user in the LDAP.
     * 
     * @param ldapUser user data
     * @param password plain text
     * @param ldapUsersOu Organizational Unit (OU) to store the user. If
     *            <code>null</code>, uses the default value configured in
     *            the LdapConfig enum.
     * 
     * @return <code>true</code> if creates, <code>false</code> otherwise
     */
    public static boolean createLdapUser(final LDAPUser ldapUser, final String password, final String ldapUsersOu,
            final LdapConfig ldapConfig) {
        try {
            // Password must be both Unicode (UTF-16LE) and a quoted string.
            String newQuotedPassword = QUOTE + password + QUOTE;
            byte[] newUnicodePassword = newQuotedPassword.getBytes(UTF_16LE);
            return createLdapUser(ldapUser, newUnicodePassword, ldapUsersOu, ldapConfig);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Create an user in the LDAP.
     * 
     * @param ldapUser user data
     * @param password Password must be both in Unicode (UTF-16LE) and between
     *            quotes. Ex: ("\"" + "password" +
     *            "\"").getBytes("UTF-16LE")
     * @param ldapUsersOu Organizational Unit (OU) to store the user. If
     *            <code>null</code>, uses the default value configured in
     *            the LdapConfig enum.
     * @return <code>true</code> if created, <code>false</code> otherwise
     * @throws NamingException
     */
    public static boolean createLdapUser(final LDAPUser ldapUser, final byte[] password, final String ldapUsersOu,
            final LdapConfig ldapConfig) {
        boolean result = false;

        try {
            // logger.info("LdapUtils searchs for Username");
            if (isUsernameInLdap(ldapUser.getUsernameWithoutSuffix(), LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig)) {
                return false;
            }

            String commonName = ldapUser.getCommonName();
            final String username = ldapUser.getUsername();
            final String usernameWithoutSuffix = ldapUser.getUsernameWithoutSuffix();
            // logger.info("LdapUtils searchs for CN");
            if (isCnInLdap(commonName, ldapConfig)) {
                commonName += "(" + usernameWithoutSuffix + ")";
                if (isCnInLdap(commonName, ldapConfig)) {
                    logger.error("Extended common name is also already in Ldap" + commonName);
                    return false;
                }
            }

            commonName = shortenCommonName(commonName, MAXIMUM_COMMON_NAME_LENGTH);

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            String ldapOu = ldapUsersOu != null ? ldapUsersOu : ldapConfig.getOuUsers();

            String distinguishedName = "CN=" + commonName + COMMA + ldapOu + COMMA + ldapDcPath;

            attrs.put(LDAPAttribute.OBJECT_CLASS.getAttribute(), USER);
            attrs.put(LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute(), usernameWithoutSuffix);
            attrs.put(LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute(), username);
            attrs.put(LDAPAttribute.CN.getAttribute(), commonName);
            attrs.put(LDAPAttribute.NAME.getAttribute(), ldapUser.getName());

            // add rest of values
            setupAttributesValues(ldapUser, attrs, false);

            int UF_ACCOUNTDISABLE = 0x0002;
            int UF_PASSWD_NOTREQD = 0x0020;
            int UF_NORMAL_ACCOUNT = 0x0200;
            int UF_PASSWORD_EXPIRED = 0x800000;

            attrs.put(LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(),
                    Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_PASSWORD_EXPIRED + UF_ACCOUNTDISABLE));

            // Perform the creation
            // logger.info("LdapUtils performs the creation");
            ctx.createSubcontext(distinguishedName.toString(), attrs);

            ModificationItem[] mods = new ModificationItem[3];

            // Password must be both in Unicode (UTF-16LE) and as a quoted
            // set password
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(LDAPAttribute.UNICODE_PWD.getAttribute(), password));

            // Enable user and user has to change password
            mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(),
                            Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWORD_EXPIRED)));

            // Unlock account
            mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(LDAPAttribute.LOCKOUT_TIME.getAttribute(), "0"));

            // Perform the update
            // logger.info("LdapUtils performs the update on the new creation");
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();

            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * updateLdapUser is only called if the username already exists, furthermore the
     * ldapUser has the distinguishedName and the commonName of the found record.
     * Therefore to speed up the synchronization, no renaming is supported.
     * 
     * @param ldapUser
     * @param updateIfNull if true, the corresponding LDAP attribute is
     *            erased/cleaned when the LDAPUser attribute value is null.
     *            <p>
     *            Example: ldapUser.getDepartment() == null, then the
     *            Department field in the LDAP will be erased.
     *            <b>Caution:</b> Some fields are really important, like
     *            the HOME_DRIVE, HOME_DIRECTORY, SCRIPT_PATH,
     *            PROFILE_PATH, MAIL, UNIX_HOME_DIRECTORY, GID_NUMBER,
     *            UID_NUMBER, LOGIN_SHELL, so make sure you don't erased
     *            them unless you really know what you are doing!
     *            </p>
     *            The safe way to use this is to 1st call the
     *            LDAPUtilsGeneric.findUserById method to get the current
     *            data from the user, then change the LDAPUser and call
     *            this method.
     * @return
     */
    public static boolean updateLdapUser(final LDAPUser ldapUser, final boolean updateIfNull, final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            // String commonName = ldapUser.getCommonName();
            // if (!isUsernameInLdap(ldapUser.getUsername())) {
            // if (isCnInLdap(commonName)) {
            // commonName += "(" + ldapUser.getUsernameWithoutSuffix() + ")";
            // }
            // }
            // ldapUser.setCommonName(commonName);

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            // final String ldapDcPath = initDcPath();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            setupAttributesValues(ldapUser, attrs, updateIfNull);

            // if(attrs.get(LDAPAttribute.UNICODE_PWD.getAttribute()) != null) {
            // attrs.remove(LDAPAttribute.UNICODE_PWD.getAttribute());
            // changePasswordInLdap(ldapUser.getUsername(), password);
            // }

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            // String ldapGroupUsers = initGroupUsers();
            // String oldName = ldapUser.getDistinguishedName();
            // String newName = "CN=" + ldapUser.getCommonName() + COMMA +
            // ldapGroupUsers + COMMA
            // + ldapDcPath;
            //
            // // Rename ldap user?
            // if (!oldName.equals(newName)) {
            // // Perform the update
            // ctx.rename(oldName, newName);
            // }

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean changePasswordInLdapBySamAccounName(final String username, final String password,
            final LdapConfig ldapConfig) {
        return changePasswordInLdap(username, password, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean changePasswordInLdapByUserPrincipalName(final String username, final String password,
            final LdapConfig ldapConfig) {
        return changePasswordInLdap(username, password, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean changePasswordInLdap(final String username, final String password, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) {
        boolean result = false;

        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(),
                    LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute(), LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + username + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            String usernameFound = null;

            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                usernameFound = sr.getAttributes().get(LDAPAttribute.DISTINGUISHED_NAME.getAttribute()).toString()
                        .substring((LDAPAttribute.DISTINGUISHED_NAME + ": ").length() - 1);
            }

            answer.close();

            if (usernameFound != null) {
                // Set password is a LDAP modify operation:
                ModificationItem[] mods = new ModificationItem[3];

                // Enable account
                int UF_NORMAL_ACCOUNT = 0x0200;
                int UF_DONT_EXPIRE_PASSWD = 0x10000;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(),
                                Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD)));
                // Unlock account
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.LOCKOUT_TIME.getAttribute(), "0"));

                // Password must be both Unicode (UTF-16LE) and a quoted string.
                String newQuotedPassword = QUOTE + password + QUOTE;
                byte[] newUnicodePassword = newQuotedPassword.getBytes(UTF_16LE);

                mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.UNICODE_PWD.getAttribute(), newUnicodePassword));

                // Perform the update
                ctx.modifyAttributes(usernameFound, mods);

                ctx.close();
                result = true;
            }
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean moveAndDeactivateAccountInLdapBySamAccountName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return moveAndDeactivateAccountInLdap(username, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean moveAndDeactivateAccountInLdapByUserPrincipalName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return moveAndDeactivateAccountInLdap(username, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean moveAndDeactivateAccountInLdap(final String username, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) throws NamingException {
        boolean result = false;
        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(), ldapAttribute.getAttribute(),
                    LDAPAttribute.CN.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute + "=" + username + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            String distUsernameFound = null;
//            String usernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                distUsernameFound = sr.getAttributes().get(LDAPAttribute.DISTINGUISHED_NAME.getAttribute()).toString()
                        .substring((LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + ": ").length());
                // usernameFound =
                // sr.getAttributes().get(LDAPAttribute.CN.getAttribute
                // ()).toString()
                // .substring((LDAPAttribute.CN.getAttribute() +
                // ": ").length());
            }

            answer.close();

            if (distUsernameFound != null) { // && !distUsernameFound.contains(
                // "DEACTIVATED")) {

                // Rename account
                // shortenCommonName(usernameFound, MAXIMUM_COMMON_NAME_LENGTH -
                // 12);
                // String ldapGroupUsers = initGroupUsers();
                // String oldName = distUsernameFound;
                // String newName = "CN=" + usernameFound + " DEACTIVATED" + ","
                // + ldapGroupUsers + COMMA
                // + ldapDcPath;
                // // Perform the update
                // ctx.rename(oldName, newName);

                // disable is a LDAP modify operation:
                ModificationItem[] mods = new ModificationItem[2];
                // Disable account
                int UF_DISABLED_ACCOUNT = 0x0002;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
                        LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(), Integer.toString(UF_DISABLED_ACCOUNT)));
                // Set disabled description
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.DESCRIPTION.getAttribute(), ACCOUNT_DISABLED_BY_FENIX));
                // Perform the update
                ctx.modifyAttributes(distUsernameFound, mods);

                // Really deletes!
                // ctx.destroySubcontext(newName);

                ctx.close();
                result = true;

            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean disableAccountInLdapBySamAccountName(final String samAccountName, final LdapConfig ldapConfig) {
        return disableAccountInLdap(samAccountName, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean disableAccountInLdapByUserPrincipalName(final String userPrincipalName, final LdapConfig ldapConfig) {
        return disableAccountInLdap(userPrincipalName, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean disableAccountInLdap(final String value, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) {
        boolean result = false;

        try {
            if (isAccountDisabled(value, ldapAttribute, ldapConfig)) {
                return true;
            }

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(),
                    LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute(), LDAPAttribute.CN.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + value + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            String distUsernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                distUsernameFound = sr.getAttributes().get(LDAPAttribute.DISTINGUISHED_NAME.getAttribute()).toString()
                        .substring((LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + ": ").length());
            }

            answer.close();

            if (distUsernameFound != null) {
                // disable is a LDAP modify operation:
                ModificationItem[] mods = new ModificationItem[2];
                // Disable account
                int UF_DISABLED_ACCOUNT = 0x0002;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(
                        LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(), Integer.toString(UF_DISABLED_ACCOUNT)));
                // Set disabled description
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.DESCRIPTION.getAttribute(), ACCOUNT_DISABLED_BY_FENIX));
                // Perform the update
                ctx.modifyAttributes(distUsernameFound, mods);

                ctx.close();
                result = true;

            }
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static boolean enableAndUnlockAccountInLdapBySamAccountName(final String samAccountName, final LdapConfig ldapConfig) {
        return enableAndUnlockAccountInLdap(samAccountName, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean enableAndUnlockAccountInLdapByUserPrincipalName(final String userPrincipalName,
            final LdapConfig ldapConfig) {
        return enableAndUnlockAccountInLdap(userPrincipalName, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean enableAndUnlockAccountInLdap(final String value, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) {
        boolean result = false;

        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(), ldapAttribute.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + value + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            String distUsernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                distUsernameFound = sr.getAttributes().get(LDAPAttribute.DISTINGUISHED_NAME.getAttribute()).toString()
                        .substring((LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + ": ").length());
            }

            answer.close();

            if (distUsernameFound != null) {
                ModificationItem[] mods = new ModificationItem[3];

                // Enable account
                int UF_NORMAL_ACCOUNT = 0x0200;
                int UF_DONT_EXPIRE_PASSWD = 0x10000;
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.USER_ACCOUNT_CONTROL.getAttribute(),
                                Integer.toString(UF_NORMAL_ACCOUNT + UF_DONT_EXPIRE_PASSWD)));
                // Unlock account
                mods[1] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.LOCKOUT_TIME.getAttribute(), "0"));

                // Mark as enabled by Fenix
                mods[2] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.DESCRIPTION.getAttribute(), ACCOUNT_ENABLED_BY_FENIX));

                // Perform the update
                ctx.modifyAttributes(distUsernameFound, mods);

                ctx.close();
                result = true;
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static ModificationItem[] getModificationItemsFromBasicAttributes(final Attributes attrs) throws NamingException {
        final ModificationItem[] result = new ModificationItem[attrs.size()];
        final NamingEnumeration<String> ids = attrs.getIDs();
        int index = 0;
        while (ids.hasMore()) {
            final String id = ids.next();
            result[index++] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE, attrs.get(id));
        }
        ids.close();
        return result;
    }

    private static void setupAttributesValues(LDAPUser userFromFenix, Attributes attrs, final boolean updateIfNull) {

        final Set<LDAPAttribute> attributesToManage = Configuration.getAttributesToManage();

        for (final LDAPAttribute attribute : attributesToManage) {
            switch (attribute) {
            case GIVEN_NAME:
                if (userFromFenix.getGivenName() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.GIVEN_NAME.getAttribute(), userFromFenix.getGivenName());
                }
                break;
            case SN:
                if (userFromFenix.getFamilyName() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.SN.getAttribute(), userFromFenix.getFamilyName());
                }
                break;
            case DISPLAY_NAME:
                if (userFromFenix.getDisplayName() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.DISPLAY_NAME.getAttribute(), userFromFenix.getDisplayName());
                }
                break;
            case DESCRIPTION:
                if (userFromFenix.getDescription() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.DESCRIPTION.getAttribute(), userFromFenix.getDescription());
                }
                break;
            case HOME_DRIVE:
                if (userFromFenix.getHomeDrive() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.HOME_DRIVE.getAttribute(), userFromFenix.getHomeDrive());
                }
                break;
            case HOME_DIRECTORY:
                if (userFromFenix.getHomeDirectory() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.HOME_DIRECTORY.getAttribute(), userFromFenix.getHomeDirectory());
                }
                break;
            case SCRIPT_PATH:
                if (userFromFenix.getScriptPath() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.SCRIPT_PATH.getAttribute(), userFromFenix.getScriptPath());
                }
                break;
            case PROFILE_PATH:
                if (userFromFenix.getProfilePath() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.PROFILE_PATH.getAttribute(), userFromFenix.getProfilePath());
                }
                break;
            case MAIL:
                if (userFromFenix.getInstitutionalEmail() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.MAIL.getAttribute(), userFromFenix.getInstitutionalEmail());
                }
                break;
            case TELEPHONE_NUMBER:
                if (userFromFenix.getWorkPhone() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.TELEPHONE_NUMBER.getAttribute(), userFromFenix.getWorkPhone());
                }
                break;
            case PHYSICAL_DELIVERY_OFFICE_NAME:
                if (userFromFenix.getPhysicalDeliveryOfficeName() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.PHYSICAL_DELIVERY_OFFICE_NAME.getAttribute(),
                            userFromFenix.getPhysicalDeliveryOfficeName());
                }
                break;
            case WWWHOME_PAGE:
                if (userFromFenix.getWWWHomePage() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.WWWHOME_PAGE.getAttribute(), userFromFenix.getWWWHomePage());
                }
                break;
            case TITLE:
                if (userFromFenix.getTitle() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.TITLE.getAttribute(), userFromFenix.getTitle());
                }
                break;
            case UNIX_HOME_DIRECTORY:
                if (userFromFenix.getUnixHomeDirectory() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.UNIX_HOME_DIRECTORY.getAttribute(), userFromFenix.getUnixHomeDirectory());
                }
                break;
            case GID_NUMBER:
                if (userFromFenix.getGidNumber() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.GID_NUMBER.getAttribute(), userFromFenix.getGidNumber());
                }
                break;
            case UID_NUMBER:
                if (userFromFenix.getUidNumber() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.UID_NUMBER.getAttribute(), userFromFenix.getUidNumber());
                }
                break;
            case COMPANY:
                if (userFromFenix.getCompany() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.COMPANY.getAttribute(), userFromFenix.getCompany());
                }
                break;
            case LOGIN_SHELL:
                if (userFromFenix.getLoginShell() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.LOGIN_SHELL.getAttribute(), userFromFenix.getLoginShell());
                }
                break;
            case DEPARTMENT:
                if (userFromFenix.getDepartment() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.DEPARTMENT.getAttribute(), userFromFenix.getDepartment());
                }
                break;
            case JPEG_PHOTO:
                if (userFromFenix.getPersonalPhoto() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.JPEG_PHOTO.getAttribute(), userFromFenix.getPersonalPhoto());
                }
            case PAGER:
                if (userFromFenix.getPin() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.PAGER.getAttribute(), userFromFenix.getPin());
                }
                break;
            case EASYVISTA_OU:
                if (userFromFenix.getEasyVistaOu() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.EASYVISTA_OU.getAttribute(), userFromFenix.getEasyVistaOu());
                }
                break;
            case EASYVISTA_OFFICE:
                if (userFromFenix.getEasyVistaOffice() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.EASYVISTA_OFFICE.getAttribute(), userFromFenix.getEasyVistaOffice());
                }
                break;

            case ID_DOCUMENT_NUMBER:
                if (userFromFenix.getDocumentIdNumber() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.ID_DOCUMENT_NUMBER.getAttribute(), userFromFenix.getDocumentIdNumber());
                }
                break;

            case STUDENT_ID_ISCTE:
                if (userFromFenix.getStudentNumber() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.STUDENT_ID_ISCTE.getAttribute(), userFromFenix.getStudentNumber());
                }
                break;

            // Must be update by a specific method
//            case IP_PHONE:
//                if (userFromFenix.getMobilePhoneMFA() != null || updateIfNull) {
//                    attrs.put(LDAPAttribute.IP_PHONE.getAttribute(), userFromFenix.getMobilePhoneMFA());
//                }
//                break;

            case EMPLOYEE_NUMBER:
                if (userFromFenix.getEmployeeNumber() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.EMPLOYEE_NUMBER.getAttribute(), userFromFenix.getEmployeeNumber());
                }
                break;

            case SECOND_EMAIL:
                if (userFromFenix.getSecondEmail() != null || updateIfNull) {
                    attrs.put(LDAPAttribute.SECOND_EMAIL.getAttribute(), userFromFenix.getSecondEmail());
                }
                break;

            // case UNICODE_PWD:
            // if (password != null) {
            // attrs.put(LDAPAttribute.UNICODE_PWD.getAttribute(), password);
            // }
            // break;
            default:
                // Do nothing for the attributes not specified in a case statement
                break;
            }
        }
    }

    public static boolean addJpegPhotoBySamAccountName(final String username, final byte[] jpegPhoto, final LdapConfig ldapConfig)
            throws NamingException {
        return addJpegPhoto(username, jpegPhoto, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static boolean addJpegPhotoByUserPrincipalName(final String username, final byte[] jpegPhoto,
            final LdapConfig ldapConfig) throws NamingException {
        return addJpegPhoto(username, jpegPhoto, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static boolean addJpegPhoto(final String username, final byte[] jpegPhoto, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) throws NamingException {
        boolean result = false;

        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.DISTINGUISHED_NAME.getAttribute(), ldapAttribute.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + username + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            String distUsernameFound = null;
            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                distUsernameFound = sr.getAttributes().get(LDAPAttribute.DISTINGUISHED_NAME.getAttribute()).toString()
                        .substring((LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + ": ").length());
            }

            answer.close();

            if (distUsernameFound != null) {
                ModificationItem[] mods = new ModificationItem[1];
                mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                        new BasicAttribute(LDAPAttribute.JPEG_PHOTO.getAttribute(), jpegPhoto));

                // Perform the update
                ctx.modifyAttributes(distUsernameFound, mods);

                ctx.close();
                result = true;
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static byte[] readJpegPhotoFromFile(final String filename) {
        byte[] data = null;
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(filename, "r");
            data = new byte[(int) raf.length()];
            raf.readFully(data);
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    public static boolean writeJpegPhotoToFile(byte[] jpegPhoto, String filename) throws NamingException {
        if (jpegPhoto != null) {
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(new File(filename));
                fos.write(jpegPhoto);
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    public static byte[] getJpegPhotoFromLdapBySamAccountName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return getJpegPhotoFromLdap(username, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    public static byte[] getJpegPhotoFromLdapByUserPrincipalName(final String username, final LdapConfig ldapConfig)
            throws NamingException {
        return getJpegPhotoFromLdap(username, LDAPAttribute.USER_PRINCIPAL_NAME, ldapConfig);
    }

    private static byte[] getJpegPhotoFromLdap(final String username, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) throws NamingException {
        final Hashtable<String, String> env = ldapConfig.getEnvironment();
        final String ldapDcPath = ldapConfig.getDcPath();

        try {
            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the attributes to return
            String returnedAtts[] = { LDAPAttribute.JPEG_PHOTO.getAttribute() };
            searchCtls.setReturningAttributes(returnedAtts);

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // specify the LDAP search filter
            String searchFilter = "(&(objectClass=user)(" + ldapAttribute.getAttribute() + "=" + username + "))";

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            // Loop through the search results
            if (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                Attribute attr = (Attribute) sr.getAttributes().get(LDAPAttribute.JPEG_PHOTO.getAttribute());
                answer.close();

                if (attr != null) {
                    return (byte[]) (attr.get());
                }
            }
            answer.close();

        } catch (NamingException e) {
            throw new RuntimeException("Error: Problem reading Account control and description");
        }
        return null;
    }

    /**
     * Create a group.
     * 
     * @param groupName name of the group. Maximum size 64 characters.
     * @param groupDescription a description
     * 
     * @param groupScope see enum AdGroupScope
     * @param groupType see enum AdGroupType
     * @param displayName name to display in the GUI
     * @param email email address for the distribution list
     * 
     * @param groupsOu Organizational Unit (OU) to store the group. If
     *            <code>null</code>, uses the default value configured
     *            in the LdapConfig enum.
     * 
     * @return <code>true</code> if created, <code>false</code> otherwise
     */
    public static boolean createGroupByName(final String groupName, final String groupDescription, final AdGroupScope groupScope,
            final AdGroupType groupType, final String displayName, final String email, final String ldapGroupsOu,
            final LdapConfig ldapConfig) {
        return createGroupByName(groupName, null, groupDescription, groupScope, groupType, displayName, email, ldapGroupsOu,
                ldapConfig);
    }

    /**
     * Create a group.
     * 
     * @param groupName name of the group. Maximum size 64 characters.
     * @param samAccountName if empty the common name of the group will be used
     * @param groupDescription a description
     * 
     * @param groupScope see enum AdGroupScope
     * @param groupType see enum AdGroupType
     * @param displayName name to display in the GUI
     * @param email email address for the distribution list
     * 
     * @param ldapGroupsOu Organizational Unit (OU) to store the group. If
     *            <code>null</code>, uses the default value configured
     *            in the LdapConfig enum.
     * 
     * @return <code>true</code> if created, <code>false</code> otherwise
     */
    public static boolean createGroupByName(final String groupName, final String samAccountName, final String groupDescription,
            final AdGroupScope groupScope, final AdGroupType groupType, final String displayName, final String email,
            final String ldapGroupsOu, final LdapConfig ldapConfig) {
        checkGroupNameLength(groupName);
        final String ldapDcPath = ldapConfig.getDcPath();
        String ldapOu = ldapGroupsOu != null ? ldapGroupsOu : ldapConfig.getOuGroups();
        String distinguishedGroupName = "CN=" + groupName + COMMA + ldapOu + COMMA + ldapDcPath;
        return createGroupByDistinguishedName(distinguishedGroupName, samAccountName, groupDescription, groupScope, groupType,
                displayName, email, ldapConfig);
    }

    /**
     * Create group by distinguished name.
     * 
     * @param distinguishedGroupName CN=groupName,CN=myOU,DC=dcName,DC=dcDomain.
     *            Example: CN=My group
     *            name,CN=Users,DC=funcionarios,DC=iul
     * @param samAccountName if empty the common name of the group will be
     *            used
     * @param groupDescription a description
     * 
     * @param groupScope see enum AdGroupScope
     * @param groupType see enum AdGroupType
     * @param displayName name to display in the GUI
     * @param email email address for the distribution list
     * 
     * @return <code>true</code> if created, <code>false</code> otherwise
     */
    public static boolean createGroupByDistinguishedName(final String distinguishedGroupName, final String samAccountName,
            final String groupDescription, final AdGroupScope groupScope, final AdGroupType groupType, final String displayName,
            final String email, final LdapConfig ldapConfig) {
        boolean result = false;
        String groupName = (distinguishedGroupName.split(",")[0]).substring(3);
//        String groupNamePreWin2000 = convertToPreWin2000(groupName);

        checkGroupNameLength(groupName);

        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            attrs.put(LDAPAttribute.OBJECT_CLASS.getAttribute(), "group");
            attrs.put(LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute(), samAccountName != null ? samAccountName : groupName);
            attrs.put(LDAPAttribute.CN.getAttribute(), groupName);
//            attrs.put(LDAPAttribute.NAME.getAttribute(), groupNamePreWin2000);
            attrs.put(LDAPAttribute.DESCRIPTION.getAttribute(), groupDescription);
            attrs.put(LDAPAttribute.DISPLAY_NAME.getAttribute(), displayName);
            if (email != null) {
                attrs.put(LDAPAttribute.MAIL.getAttribute(), email);
            }

            int groupTypeEncoded = groupScope.getCode();
//            if (hasUsersFromOtherDomains) {
//                groupType = ADS_GROUP_TYPE_DOMAIN_LOCAL_GROUP;
//            } else {
//                groupType = ADS_GROUP_TYPE_GLOBAL_GROUP;
//            }
            if (groupType != null) {
                groupTypeEncoded = groupTypeEncoded + groupType.getCode();
            }
            attrs.put("groupType", String.valueOf(groupTypeEncoded));

            // Create the context
            ctx.createSubcontext(distinguishedGroupName, attrs);
            logger.debug("Created group: " + distinguishedGroupName);

            ctx.close();

            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

//    private static String convertToPreWin2000(final String groupName) {
//        if (!groupName.contains("[") && !groupName.contains("]") && !groupName.contains("/")) {
//            return groupName;
//        }
//
//        String groupNamePreWin2000 = groupName;
//        groupNamePreWin2000 = groupNamePreWin2000.replace("[", "(");
//        groupNamePreWin2000 = groupNamePreWin2000.replace("]", ")");
//        groupNamePreWin2000 = groupNamePreWin2000.replace("/", "-");
//
//        return groupNamePreWin2000;
//    }

    private static void checkGroupNameLength(final String groupName) {
        if (groupName == null || groupName.trim().length() == 0) {
            throw new IllegalArgumentException("Group name cannot be empty");
        }
        if (groupName.length() > 64) {
            throw new IllegalArgumentException("Group name size cannot have more than 64 characters");
        }
    }

    /**
     * Add an user to a group.
     * 
     * @param userDistinguishedName user distinguished name (ex: CN=Agente
     *            AD,CN=Users,DC=funcionarios,DC=iul)
     * @param groupSamAccountName group ID (ex: ud.si)
     * @return <code>true</code> if creates, <code>false</code> otherwise
     */
    public static boolean addMemberOfByDistinguishedNameAndGroupSamAccountName(final String userDistinguishedName,
            final String groupSamAccountName, final LdapConfig ldapConfig) {
        final String groupDistinguishedName = findGroupById(groupSamAccountName, ldapConfig);
        if (groupDistinguishedName == null) {
            return false;
        }

        return addMemberOfByDistinguishedName(userDistinguishedName, groupDistinguishedName, ldapConfig);
    }

    /**
     * Add an user to a group.
     * 
     * @param userSamAccountName user name without the domain suffix (ex: ajsco)
     * @param groupSamAccountName group ID (ex: ud.si)
     * @return <code>true</code> if creates, <code>false</code> otherwise
     */
    public static boolean addMemberOfBySamAccountName(final String userSamAccountName, final String groupSamAccountName,
            final LdapConfig ldapConfig) {
        LDAPUser ldapUser = findUserById(userSamAccountName, ldapConfig);
        if (ldapUser == null) {
            return false;
        }
        final String userDistinguishedName = ldapUser.getDistinguishedName();

        final String groupDistinguishedName = findGroupById(groupSamAccountName, ldapConfig);
        if (groupDistinguishedName == null) {
            return false;
        }

        return addMemberOfByDistinguishedName(userDistinguishedName, groupDistinguishedName, ldapConfig);
    }

    /**
     * Add an user to a group.
     * 
     * @param distinguishedName user distinguished name (ex: CN=Agente
     *            AD,CN=Users,DC=funcionarios,DC=iul)
     * @param distinguishedGroupName group distinguished name (ex:
     *            CN=GrupoTeste1,CN=Users,DC=funcionarios,DC=iul)
     * @return <code>true</code> if creates, <code>false</code> otherwise
     */
    public static boolean addMemberOfByDistinguishedName(final String userDistinguishedName, final String groupDistinguishedName,
            final LdapConfig ldapConfig) {
        boolean result = false;

        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            final ModificationItem[] mods = new ModificationItem[1];

            mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE,
                    new BasicAttribute(LDAPAttribute.MEMBER.getAttribute(), userDistinguishedName));

            // Perform the update
            ctx.modifyAttributes(groupDistinguishedName, mods);

            ctx.close();
            result = true;
        } catch (NameAlreadyBoundException e) {
            // duplicate is not a problem.
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Remove an user from a group.
     * 
     * @param userSamAccountName user name without the domain suffix (ex: ajsco)
     * @param groupSamAccountName group ID (ex: ud.si)
     * @return <code>true</code> if removed, <code>false</code> otherwise
     */
    public static boolean removeMemberOfBySamAccountName(final String userSamAccountName, final String groupSamAccountName,
            final LdapConfig ldapConfig) {
        LDAPUser ldapUser = findUserById(userSamAccountName, ldapConfig);
        if (ldapUser == null) {
            return false;
        }
        final String userDistinguishedName = ldapUser.getDistinguishedName();

        final String groupDistinguishedName = findGroupById(groupSamAccountName, ldapConfig);
        if (groupDistinguishedName == null) {
            return false;
        }

        return removeMemberOfDistinguishedName(userDistinguishedName, groupDistinguishedName, ldapConfig);
    }

    /**
     * Remove an user from a group.
     * 
     * @param distinguishedName user distinguished name (ex: CN=Agente
     *            AD,CN=Users,DC=funcionarios,DC=iul)
     * @param distinguishedGroupName group distinguished name (ex:
     *            CN=GrupoTeste1,CN=Users,DC=funcionarios,DC=iul)
     * @return <code>true</code> if removed, <code>false</code> otherwise
     */
    public static boolean removeMemberOfDistinguishedName(final String userDistinguishedName, final String groupDistinguishedName,
            final LdapConfig ldapConfig) {
        boolean result = false;

        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            final ModificationItem[] mods = new ModificationItem[1];

            mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
                    new BasicAttribute(LDAPAttribute.MEMBER.getAttribute(), userDistinguishedName));

            // Perform the update
            ctx.modifyAttributes(groupDistinguishedName, mods);
            ctx.close();
            result = true;
        } catch (NameAlreadyBoundException e) {
            // duplicate is not a problem.
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get the groups an user belongs to.
     * 
     * @param username user name without the domain suffix (ex: ajsco)
     * @return distinguish name of the groups
     */
    public static ArrayList<String> getGroupsByUserSamAccountName(final String username, final LdapConfig ldapConfig) {
        // specify the LDAP search filter
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() + "=" + username + "))";
        return getUserGroups(searchFilter, ldapConfig);
    }

    /**
     * Get the groups an user belongs to.
     * 
     * @param username user name with the domain suffix (ex: ajsco@funcionarios.iul)
     * @return distinguish name of the groups
     */
    public static ArrayList<String> getGroupsByUserUserPrincipalName(final String username, final LdapConfig ldapConfig) {
        // specify the LDAP search filter
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.USER_PRINCIPAL_NAME.getAttribute() + "=" + username + "))";
        return getUserGroups(searchFilter, ldapConfig);
    }

    /**
     * Get the groups an user belongs to.
     * 
     * @param searchFilter criteria
     * @return distinguish name of the groups
     */
    private static ArrayList<String> getUserGroups(String searchFilter, final LdapConfig ldapConfig) {
        ArrayList<String> groups = new ArrayList<String>();
        try {

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Specify the attributes to return
            String returnedAtts[] = { "memberOf" };
            searchCtls.setReturningAttributes(returnedAtts);

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();

                // Print out the groups
                Attributes attrs = sr.getAttributes();
                if (attrs != null) {
                    NamingEnumeration<? extends Attribute> ae;
                    for (ae = attrs.getAll(); ae.hasMore();) {
                        Attribute attr = (Attribute) ae.next();
                        NamingEnumeration<?> e;
                        for (e = attr.getAll(); e.hasMore();) {
                            groups.add((String) e.next());
                        }
                        e.close();
                    }
                    ae.close();
                }
            }
            answer.close();
            ctx.close();
            return groups;
        } catch (NamingException e) {
            logger.error("Problem getting groups: " + e);
        }
        return null;
    }

    /**
     * Get the distinguished name of all the groups.
     * 
     * @return distinguished name of the groups
     */
    public static ArrayList<String> getAllGroupsReturnOnlyDistinguishedName(final LdapConfig ldapConfig) {
        return getGroupsByAll(LDAPAttribute.DISTINGUISHED_NAME, ldapConfig);
    }

    /**
     * Get the SamAccountName of all the groups.
     * 
     * @return SamAccountName name of the groups
     */
    public static ArrayList<String> getAllGroupsReturnOnlySamAccountName(final LdapConfig ldapConfig) {
        return getGroupsByAll(LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
    }

    /**
     * Get all the groups.
     * 
     * @param ldapAttribute the attribute to be returned
     * @return ldapAttribute of the groups
     */
    public static ArrayList<String> getGroupsByAll(final LDAPAttribute ldapAttribute, final LdapConfig ldapConfig) {
        // specify the LDAP search filter
        String searchFilter = "(objectClass=group)";
        return getGroupsFetchInBlocks(searchFilter, ldapAttribute, ldapConfig);
    }

    /**
     * Get groups.
     * 
     * @param searchFilter criteria
     * @param ldapAttribute the attribute to be returned
     * 
     * @return LDAP attribute of the groups
     */
    private static ArrayList<String> getGroups(final String searchFilter, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) {
        ArrayList<String> groups = new ArrayList<String>();
        try {

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Specify the attributes to return
            String returnedAtts[] = { "*" };
            searchCtls.setReturningAttributes(returnedAtts);

            // Search for objects using the filter
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            // Loop through the search results
            while (answer.hasMoreElements()) {
                SearchResult sr = answer.next();
                Attribute at = sr.getAttributes().get(ldapAttribute.getAttribute());
                groups.add((String) at.get());
            }
            answer.close();
            ctx.close();
            return groups;
        } catch (NamingException e) {
            logger.error("Problem getting groups: " + e);
        }
        return null;
    }

    /**
     * Get groups.
     * 
     * @param searchFilter criteria
     * @param ldapAttribute the attribute to be returned
     * 
     * @return LDAP attribute of the groups
     */
    private static ArrayList<String> getGroupsFetchInBlocks(final String searchFilter, final LDAPAttribute ldapAttribute,
            final LdapConfig ldapConfig) {
        ArrayList<String> groups = new ArrayList<String>();
        try {

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Set the page size and initialize the cookie that we pass back in
            // subsequent pages
            int pageSize = 100;
            byte[] cookie = null;

            // Request the paged results control
            Control[] ctls = new Control[] { new PagedResultsControl(pageSize, Control.CRITICAL) };

            ctx.setRequestControls(ctls);

            int pageNumber = 0;

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            // Specify the attributes to return
            String returnedAtts[] = { "*" };
            searchCtls.setReturningAttributes(returnedAtts);

            // Search for objects using the filter
            NamingEnumeration<SearchResult> results;

            do {
                results = ctx.search(searchBase, searchFilter, searchCtls);

                // loop through the page result
                while (results != null && results.hasMoreElements()) {
                    SearchResult sr = results.next();
                    Attribute at = sr.getAttributes().get(ldapAttribute.getAttribute());
                    groups.add((String) at.get());
                }

                // examine the response controls
                cookie = parseControls(ctx.getResponseControls(), pageNumber);

                // increment page number
                pageNumber++;

                // pass the cookie back to the server for the next page
                ctx.setRequestControls(new Control[] { new PagedResultsControl(pageSize, cookie, Control.CRITICAL) });
            } while ((cookie != null) && (cookie.length != 0));

            return groups;
        } catch (NamingException e) {
            logger.error("Problem getting groups: " + e);
        } catch (IOException e) {
            logger.error("Problem getting groups: " + e);
        }
        return null;
    }

//    private static Hashtable<String, String> initEnvironment() {
//        final Hashtable<String, String> env = new Hashtable<String, String>();
//
//        env.put(Context.INITIAL_CONTEXT_FACTORY, LDAP_INITIAL_CONTEXT_FACTORY);
//        env.put(Context.PROVIDER_URL, LDAP_PROVIDER_URL);
//        env.put(Context.SECURITY_AUTHENTICATION, LDAP_SECURITY_AUTHENTICATION);
//        env.put(Context.SECURITY_PRINCIPAL, LDAP_ADMIN_USER);
//        env.put(Context.SECURITY_CREDENTIALS, LDAP_ADMIN_PASS);
//        return env;
//    }
//
//    private static String initDcPath() {
//        String dcPath = LDAP_DC_PATH;
//        return dcPath;
//    }
//
//    private static String initGroupUsers() {
//        String ldapGroupUsers = "";
//        ldapGroupUsers = LDAP_GROUP_USERS;
//        return ldapGroupUsers;
//    }

    /**
     * Get the users that belongs to a group.
     * 
     * @param groupId user name with the domain suffix (ex: ud.si)
     * @return distinguish name of the users
     */
    public static ArrayList<String> getGroupUsersByGroupId(String groupId, final LdapConfig ldapConfig) {
        // specify the LDAP search filter
        String searchFilter = "(&(objectClass=group)(" + LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() + "=" + groupId + "))";
        return getGroupUsers(searchFilter, ldapConfig);
    }

//    /**
//     * Get the users that belongs to a group.
//     * 
//     * @param searchFilter criteria
//     * @return distinguish name of the groups
//     */
//    private static ArrayList<String> getGroupUsersBAK(final String searchFilter, final LdapConfig ldapConfig) {
//        ArrayList<String> groups = new ArrayList<String>();
//        try {
//
//            final Hashtable<String, String> env = ldapConfig.getEnvironment();
//            final String ldapDcPath = ldapConfig.getDcPath();
//
//            // Create the initial directory context
//            LdapContext ctx = new InitialLdapContext(env, null);
//
//            SearchControls searchCtls = new SearchControls();
//
//            // Specify the search scope
//            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//
//            // Specify the Base for the search
//            String searchBase = ldapDcPath;
//
//            // Specify the attributes to return
//            String returnedAtts[] = { LDAPAttribute.MEMBER.getAttribute() };
//            searchCtls.setReturningAttributes(returnedAtts);
//
//            // Search for objects using the filter
//            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
//
//            // Loop through the search results
//            while (answer.hasMoreElements()) {
//                SearchResult sr = answer.next();
//                Attribute memberAttribute = sr.getAttributes().get(LDAPAttribute.MEMBER.getAttribute());
//                if (memberAttribute == null) {
//                    continue;
//                }
//
//                NamingEnumeration<?> e;
//                for (e = memberAttribute.getAll(); e.hasMore();) {
//                    groups.add((String) e.next());
//                }
//                e.close();
//            }
//            answer.close();
//            ctx.close();
//            return groups;
//        } catch (NamingException e) {
//            logger.error("Problem getting groups: " + e);
//        }
//        return null;
//    }

    /**
     * Get the users that belongs to a group.
     * 
     * When an Active Directory (AD) attribute has more than N values, the AD
     * returns only a subset at a time, so the search query must be repeated several
     * times in order to get them all.
     * 
     * The member attribute has this behavior, when a group has more than 1500
     * members, so it must be handled using the
     * "member;range=«LOWER_LIMIT»-«UPPER_LIMIT»" syntax. The server returns the
     * «UPPER_LIMIT» as an asterisk "*" when there are no more records to fetch.
     * 
     * @param searchFilter criteria
     * @return distinguish name of the members
     * 
     * @see https://technet.microsoft.com/en-us/library/cc755809(v=ws.10).aspx
     * @see https://msdn.microsoft.com/en-us/library/Aa367017
     */
    private static ArrayList<String> getGroupUsers(final String searchFilter, final LdapConfig ldapConfig) {
        ArrayList<String> groupMembers = new ArrayList<String>();
        try {

            final Hashtable<String, String> env = ldapConfig.getEnvironment();
            final String ldapDcPath = ldapConfig.getDcPath();

            // Create the initial directory context
            LdapContext ctx = new InitialLdapContext(env, null);

            SearchControls searchCtls = new SearchControls();

            // Specify the search scope
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

            // Specify the Base for the search
            String searchBase = ldapDcPath;

            String rangeKey = "member;range=0-1499";
            int lowerLimit = 0;
            int upperLimit = 1499;
            while (rangeKey != null) {
                // Specify the attributes to return
                String returnedAtts[] = { LDAPAttribute.MEMBER.getAttribute(), rangeKey };
                searchCtls.setReturningAttributes(returnedAtts);

                // Search for objects using the filter
                NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

                // Loop through the search results
                while (answer.hasMoreElements()) {
                    SearchResult sr = answer.next();

                    String memberAttributeToUse = LDAPAttribute.MEMBER.getAttribute();
                    String memberRangeAttribute = getMemberRangeAttribute(sr);
                    if (memberRangeAttribute != null) {
                        // Fetch by RANGE mode
                        memberAttributeToUse = memberRangeAttribute;
                        if (!memberRangeAttribute.contains("*")) {
                            // PREPARE NEXT RANGE
                            lowerLimit += 1500;
                            upperLimit += 1500;
                            rangeKey = "member;range=" + lowerLimit + "-" + upperLimit;
                        } else {
                            // STOP
                            rangeKey = null;
                        }
                    } else {
                        // ABORT search by range loop,
                        // since the "member" attribute is enough
                        // to retrieve all the members.
                        rangeKey = null;
                    }

                    Attribute memberAttribute = sr.getAttributes().get(memberAttributeToUse);
                    if (memberAttribute == null) {
                        continue;
                    }

                    NamingEnumeration<?> e;
                    for (e = memberAttribute.getAll(); e.hasMore();) {
                        groupMembers.add((String) e.next());
                    }
                    e.close();
                }
                answer.close();
            }

            ctx.close();
            return groupMembers;
        } catch (NamingException e) {
            logger.error("Problem getting groups: " + e);
        }
        return null;
    }

    private static String getMemberRangeAttribute(SearchResult sr) throws NamingException {
        NamingEnumeration<String> e = null;
        for (e = sr.getAttributes().getIDs(); e.hasMore();) {
            String key = e.next();
            if (key.contains("range")) {
                e.close();
                return key;
            }
        }
        e.close();
        return null;
    }

    /**
     * If the common name is too long it gets abbreviated like this:
     * 
     * Anfvgreg Biufbr Cbvuerbg Dbfvueb
     * 
     * -> Anfvgreg Biufbr C. Dbfvueb
     * 
     * -> Anfvgreg B. C. Dbfvueb
     * 
     * -> A. B. C. Dbfvueb
     * 
     * @param commonName
     * @param length
     * @return
     */
    private static String shortenCommonName(String commonName, int length) {
        int i = 1;
        while (commonName.length() > length) {
            String[] sa = commonName.split(" ");
            if (sa.length - i - 2 < 0) {
                throw new RuntimeException("Distinguished Name is still too long after abbreviating.");
            }
            if (sa[sa.length - i - 2].length() > 2) {
                sa[sa.length - i - 2] = sa[sa.length - i - 2].substring(0, 1) + ".";
            } else {
                sa[sa.length - i - 2] = sa[sa.length - i - 2].substring(0, 1);
            }
            commonName = "";
            for (int j = 0; j < sa.length; j++) {
                commonName += sa[j];
                if (j < sa.length - 1) {
                    commonName += " ";
                }
            }
            i++;
        }
        return commonName;
    }

    /**
     * Changes the PIN and deletes it if it's <code>null</code>.
     * 
     * @param ldapUser
     * @return
     */
    public static boolean updateLdapUserPin(final LDAPUser ldapUser, final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            // Force PIN update. Erase it if it's null
            attrs.put(LDAPAttribute.PAGER.getAttribute(), ldapUser.getPin());

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Changes the PIN, the unit and the groups. Deletes it if it's
     * <code>null</code>.
     * 
     * @param ldapUser
     * @param pin the user printer PIN.
     * @param unit the user billing unit. It's only stored in the description.
     * @param groups the user groups. They are only stored in the title.
     * @return is the update succeed
     */
    public static boolean updateLdapUserPinGroupAndUnit(final LDAPUser ldapUser, final String pin, final String unit,
            final String groups, final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            // Force update, erase it if it's null.
            attrs.put(LDAPAttribute.PAGER.getAttribute(), pin);
            attrs.put(LDAPAttribute.TITLE.getAttribute(), groups);
            attrs.put(LDAPAttribute.DESCRIPTION.getAttribute(), unit);

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static LDAPUser findUserById(final String usernameWithoutDomainSuffix, final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() + "="
                + usernameWithoutDomainSuffix + "))";

        List<LDAPUser> result = getUsersFromLdap(searchFilter, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            throw new IllegalStateException("More than one user found: " + usernameWithoutDomainSuffix);
        }

        return result.get(0);
    }

    public static LDAPUser findUserByDistinguishedName(final String dn, final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + "=" + dn + "))";

        List<LDAPUser> result = getUsersFromLdap(searchFilter, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            // The Distinguished Name is unique so this should not be possible...
            throw new IllegalStateException("More than one user found: " + dn);
        }

        return result.get(0);
    }

    /**
     * Find an user by SID.
     * 
     * @param objectSidAsString the binary attribute ObjectSid converted to a
     *            String. This can be done using the SidConverter
     *            class.
     * @param ldapConfig configuration of the LDAP to perform the operation
     * @return data of the user
     * 
     * @see SidConverter
     */
    public static LDAPUser findUserByObjectSid(final String objectSidAsString, final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=user)(" + LDAPAttribute.OBJECT_SID.getAttribute() + "=" + objectSidAsString + "))";

        List<LDAPUser> result = getUsersFromLdap(searchFilter, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            throw new IllegalStateException("More than one user found: " + objectSidAsString);
        }

        return result.get(0);
    }

    /**
     * Find an user by SID in the ForeignSecurityPrincipal organization unit.
     * 
     * @param objectSidAsString the binary attribute ObjectSid converted to a
     *            String. This can be done using the SidConverter
     *            class.
     * @param ldapConfig configuration of the LDAP to perform the operation
     * @return data of the user
     * 
     * @see SidConverter
     */
    public static LDAPUser findUserByObjectSidInForeignSecurityPrincipal(final String objectSidAsString,
            final LdapConfig ldapConfig) {
        String userDistinguishedName = "CN=" + objectSidAsString + ",CN=ForeignSecurityPrincipals," + ldapConfig.getDcPath();

        String searchFilter = "(&(objectClass=foreignSecurityPrincipal)(" + LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + "="
                + userDistinguishedName + "))";

        List<LDAPUser> result = getUsersFromLdap(searchFilter, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            throw new IllegalStateException("More than one user found: " + userDistinguishedName);
        }

        return result.get(0);
    }

    /**
     * Search for a group <code>distinguishedName</code> using the
     * <code>samAccountName</code>.
     * 
     * @param groupId the group samAccountName
     * @return the group distinguishedName
     */
    public static String findGroupById(final String groupId, final LdapConfig ldapConfig) {
        String searchFilter = "(&(objectClass=group)(" + LDAPAttribute.SAM_ACCOUNT_NAME.getAttribute() + "=" + groupId + "))";

        List<String> result = getGroups(searchFilter, LDAPAttribute.DISTINGUISHED_NAME, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            throw new IllegalStateException("More than one group found: " + groupId);
        }

        return result.get(0);
    }

    /**
     * Search for a group <code>samAccountName</code> using the
     * <code>distinguishedName</code>.
     * 
     * @param distinguishedName the group Distinguished Name
     * @return the group samAccountName
     */
    public static String findGroupByDistinguishedName(final String distinguishedName, final LdapConfig ldapConfig) {
        String searchFilter =
                "(&(objectClass=group)(" + LDAPAttribute.DISTINGUISHED_NAME.getAttribute() + "=" + distinguishedName + "))";

        List<String> result = getGroups(searchFilter, LDAPAttribute.SAM_ACCOUNT_NAME, ldapConfig);
        if (result == null || result.isEmpty()) {
            return null;
        }

        if (result.size() > 1) {
            // The Distinguished Name is unique so this should not be possible...
            throw new IllegalStateException("More than one group found: " + distinguishedName);
        }

        return result.get(0);
    }

    /**
     * Changes the institutional email address and deletes it if it's
     * <code>null</code>.
     * 
     * @param ldapUser
     * @return
     */
    public static boolean updateLdapUserEmailBySamAccountName(final String usernameWithoutDomainSuffix, final String email,
            final LdapConfig ldapConfig) {
        LDAPUser ldapUser = findUserById(usernameWithoutDomainSuffix, ldapConfig);
        return updateLdapUserEmail(ldapUser, email, ldapConfig);
    }

    /**
     * Changes the institutional email address and deletes it if it's
     * <code>null</code>.
     * 
     * @param ldapUser
     * @return
     */
    public static boolean updateLdapUserEmail(final LDAPUser ldapUser, final String email, final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            // Force PIN update. Erase it if it's null
            attrs.put(LDAPAttribute.MAIL.getAttribute(), email);

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Changes the Mobile Phone for MFA and deletes it if it's <code>null</code>.
     * 
     * @param ldapUser Object with the DistinguishedName
     * @param mobilePhoneMFA phone number to use in the Multi-Factor Authentication
     * @return true if updated, false otherwise
     */
    public static boolean updateLdapUserMobilePhoneMFA(final LDAPUser ldapUser, String mobilePhoneMFA,
            final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            // Force Mobile Phone for MFA update. Erase it if it's null
            attrs.put(LDAPAttribute.IP_PHONE.getAttribute(), mobilePhoneMFA);

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Changes the Home Phone and deletes it if it's <code>null</code>.
     * 
     * This method was created because there was a request by the IT Director
     * Joao Oliveira to delete the home phone data that previous was had
     * the ID Document Number.
     * 
     * @param ldapUser Object with the DistinguishedName
     * @param homePhone phone number
     * @return true if updated, false otherwise
     */
    public static boolean updateLdapUserHomePhone(final LDAPUser ldapUser, String homePhone, final LdapConfig ldapConfig) {
        boolean result = false;
        try {
            final Hashtable<String, String> env = ldapConfig.getEnvironment();

            LdapContext ctx = new InitialLdapContext(env, null);

            Attributes attrs = new BasicAttributes(true);

            // Need to use the field distinguishedName
            final String distinguishedName = ldapUser.getDistinguishedName();

            // Erase it if it's null
            attrs.put(LDAPAttribute.HOME_PHONE.getAttribute(), homePhone);

            ModificationItem[] mods = getModificationItemsFromBasicAttributes(attrs);

            // Perform update
            ctx.modifyAttributes(distinguishedName.toString(), mods);

            ctx.close();
            result = true;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
