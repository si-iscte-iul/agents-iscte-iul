package pt.iscte.ldap;

public class LDAPUser {

    private String name;

    private String familyName;

    private String givenName;

    private String institutionalEmail;

    private String username;

    private String usernameWithoutSuffix;

    private String workPhone;

    private String homeDirectory;

    private String homeDrive;

    private String commonName;

    private String displayName;

    private String scriptPath;

    private String description;

    private String physicalDeliveryOfficeName;

    private String wWWHomePage;

    private String title;

    private String unixHomeDirectory;

    private String gidNumber;

    private String uidNumber;

    private String company;

    private String loginShell;

    private String department;

    private String departmentAcronym;

    private String[] groups;

    private String distinguishedName;

    private String profilePath;

    private byte[] personalPhoto;

    private String pin;

    private byte[] objectSid;

    private String easyVistaOu;

    private String easyVistaOffice;

    private String documentIdNumber;

    private String studentNumber;

    private String mobilePhoneMFA;

    private String employeeNumber;

    private String secondEmail;

    public LDAPUser() {

    }

    public LDAPUser(String commonName, String company, String department, String departmentAcronym, String[] groups,
            String description, String displayName, String familyName, String gidNumber, String givenName, String homeDirectory,
            String homeDrive, String institutionalEmail, String loginShell, String name, String physicalDeliveryOfficeName,
            String scriptPath, String title, String uidNumber, String unixHomeDirectory, String username,
            String usernameWithoutSuffix, String wWWHomePage, String workPhone, String distinguishedName, String profilePath,
            byte[] personalPhoto, String pin, byte[] objectSid, String easyVistaOu, String easyVistaOffice,
            String documentIdNumber, String studentNumber, String mobilePhoneMFA, String employeeNumber, String secondEmail) {
        super();
        this.commonName = commonName;
        this.company = company;
        this.department = department;
        this.departmentAcronym = departmentAcronym;
        this.groups = groups;
        this.description = description;
        this.displayName = displayName;
        this.familyName = familyName;
        this.gidNumber = gidNumber;
        this.givenName = givenName;
        this.homeDrive = homeDrive;
        this.institutionalEmail = institutionalEmail;
        this.loginShell = loginShell;
        this.name = name;
        this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
        this.scriptPath = scriptPath;
        this.title = title;
        this.uidNumber = uidNumber;
        this.unixHomeDirectory = unixHomeDirectory;
        this.username = username;
        this.usernameWithoutSuffix = usernameWithoutSuffix;
        this.homeDirectory = homeDirectory;
        this.wWWHomePage = wWWHomePage;
        this.workPhone = workPhone;
        this.distinguishedName = distinguishedName;
        this.profilePath = profilePath;
        this.personalPhoto = personalPhoto;
        this.pin = pin;
        this.objectSid = objectSid;
        this.easyVistaOu = easyVistaOu;
        this.easyVistaOffice = easyVistaOffice;
        this.documentIdNumber = documentIdNumber;
        this.studentNumber = studentNumber;
        this.mobilePhoneMFA = mobilePhoneMFA;
        this.employeeNumber = employeeNumber;
        this.secondEmail = secondEmail;
    }

    public String getUnixHomeDirectory() {
        return unixHomeDirectory;
    }

    public void setUnixHomeDirectory(String unixHomeDirectory) {
        this.unixHomeDirectory = unixHomeDirectory;
    }

    public String getGidNumber() {
        return gidNumber;
    }

    public void setGidNumber(String gidNumber) {
        this.gidNumber = gidNumber;
    }

    public String getUidNumber() {
        return uidNumber;
    }

    public void setUidNumber(String uidNumber) {
        this.uidNumber = uidNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLoginShell() {
        return loginShell;
    }

    public void setLoginShell(String loginShell) {
        this.loginShell = loginShell;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setDepartmentAcronym(String departmentAcronym) {
        this.departmentAcronym = departmentAcronym;
    }

    public String getDepartmentAcronym() {
        return departmentAcronym;
    }

    public String[] getGroups() {
        return groups;
    }

    public void setGroups(String[] groups) {
        this.groups = groups;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWWWHomePage() {
        return wWWHomePage;
    }

    public void setWWWHomePage(String homePage) {
        wWWHomePage = homePage;
    }

    public String getPhysicalDeliveryOfficeName() {
        return physicalDeliveryOfficeName;
    }

    public void setPhysicalDeliveryOfficeName(String physicalDeliveryOfficeName) {
        this.physicalDeliveryOfficeName = physicalDeliveryOfficeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getInstitutionalEmail() {
        return institutionalEmail;
    }

    public void setInstitutionalEmail(String institutionalEmail) {
        this.institutionalEmail = institutionalEmail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getHomeDirectory() {
        return homeDirectory;
    }

    public void setHomeDirectory(String homeDirectory) {
        this.homeDirectory = homeDirectory;
    }

    public String getHomeDrive() {
        return homeDrive;
    }

    public void setHomeDrive(String homeDrive) {
        this.homeDrive = homeDrive;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getScriptPath() {
        return scriptPath;
    }

    public void setScriptPath(String scriptPath) {
        this.scriptPath = scriptPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDistinguishedName() {
        return distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
        this.distinguishedName = distinguishedName;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public void setUsernameWithoutSuffix(String usernameWithoutSuffix) {
        this.usernameWithoutSuffix = usernameWithoutSuffix;
    }

    public String getUsernameWithoutSuffix() {
        return usernameWithoutSuffix;
    }

    public void setPersonalPhoto(byte[] personalPhoto) {
        this.personalPhoto = personalPhoto;
    }

    public byte[] getPersonalPhoto() {
        return personalPhoto;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }

    public void setObjectSid(byte[] objectSid) {
        this.objectSid = objectSid;
    }

    public byte[] getObjectSid() {
        return objectSid;
    }

    public String getEasyVistaOu() {
        return easyVistaOu;
    }

    public void setEasyVistaOu(String easyVistaOu) {
        this.easyVistaOu = easyVistaOu;
    }

    public String getEasyVistaOffice() {
        return easyVistaOffice;
    }

    public void setEasyVistaOffice(String easyVistaOffice) {
        this.easyVistaOffice = easyVistaOffice;
    }

    public String getDocumentIdNumber() {
        return documentIdNumber;
    }

    public void setDocumentIdNumber(String documentIdNumber) {
        this.documentIdNumber = documentIdNumber;
    }

    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getMobilePhoneMFA() {
        return mobilePhoneMFA;
    }

    public void setMobilePhoneMFA(String mobilePhoneMFA) {
        this.mobilePhoneMFA = mobilePhoneMFA;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getSecondEmail() {
        return secondEmail;
    }

    public void setSecondEmail(String secondEmail) {
        this.secondEmail = secondEmail;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LDAPUser)) {
            return false;
        }
        LDAPUser other = (LDAPUser) obj;
        if ((other.getName() == getName() || other.getName() != null && other.getName().equals(getName()))
                && (other.getFamilyName() == getFamilyName()
                        || other.getFamilyName() != null && other.getFamilyName().equals(getFamilyName()))
                && (other.getGivenName() == getGivenName()
                        || other.getGivenName() != null && other.getGivenName().equals(getGivenName()))
                && (other.getInstitutionalEmail() == getInstitutionalEmail()
                        || other.getInstitutionalEmail() != null && other.getInstitutionalEmail().equals(getInstitutionalEmail()))
                && (other.getUsername() == getUsername()
                        || other.getUsername() != null && other.getUsername().equals(getUsername()))
                && (other.getWorkPhone() == getWorkPhone()
                        || other.getWorkPhone() != null && other.getWorkPhone().equals(getWorkPhone()))
                && (other.getHomeDirectory() == getHomeDirectory()
                        || other.getHomeDirectory() != null && other.getHomeDirectory().equals(getHomeDirectory()))
                && (other.getHomeDrive() == getHomeDrive()
                        || other.getHomeDrive() != null && other.getHomeDrive().equals(getHomeDrive()))
                && (other.getCommonName() == getCommonName()
                        || other.getCommonName() != null && other.getCommonName().equals(getCommonName()))
                && (other.getDisplayName() == getDisplayName()
                        || other.getDisplayName() != null && other.getDisplayName().equals(getDisplayName()))
                && (other.getScriptPath() == getScriptPath()
                        || other.getScriptPath() != null && other.getScriptPath().equals(getScriptPath()))
                && (other.getDescription() == getDescription()
                        || other.getDescription() != null && other.getDescription().equals(getDescription()))
                && (other.getPhysicalDeliveryOfficeName() == getPhysicalDeliveryOfficeName()
                        || other.getPhysicalDeliveryOfficeName() != null
                                && other.getPhysicalDeliveryOfficeName().equals(getPhysicalDeliveryOfficeName()))
                && (other.getWWWHomePage() == getWWWHomePage()
                        || other.getWWWHomePage() != null && other.getWWWHomePage().equals(getWWWHomePage()))
                && (other.getTitle() == getTitle() || other.getTitle() != null && other.getTitle().equals(getTitle()))
                && (other.getUnixHomeDirectory() == getUnixHomeDirectory()
                        || other.getUnixHomeDirectory() != null && other.getUnixHomeDirectory().equals(getUnixHomeDirectory()))
                && (other.getGidNumber() == getGidNumber()
                        || other.getGidNumber() != null && other.getGidNumber().equals(getGidNumber()))
                && (other.getUidNumber() == getUidNumber()
                        || other.getUidNumber() != null && other.getUidNumber().equals(getUidNumber()))
                && (other.getCompany() == getCompany() || other.getCompany() != null && other.getCompany().equals(getCompany()))
                && (other.getLoginShell() == getLoginShell()
                        || other.getLoginShell() != null && other.getLoginShell().equals(getLoginShell()))
                && (other.getDepartment() == getDepartment()
                        || other.getDepartment() != null && other.getDepartment().equals(getDepartment()))
                && (other.getDepartmentAcronym() == getDepartmentAcronym()
                        || other.getDepartmentAcronym() != null && other.getDepartmentAcronym().equals(getDepartmentAcronym()))
                && (other.getGroups() == getGroups() || other.getGroups() != null && other.getGroups().equals(getGroups()))
                && (other.getDistinguishedName() == getDistinguishedName()
                        || other.getDistinguishedName() != null && other.getDistinguishedName().equals(getDistinguishedName()))
                && (other.getProfilePath() == getProfilePath()
                        || other.getProfilePath() != null && other.getProfilePath().equals(getProfilePath()))
                && (other.getPin() == getPin() || other.getPin() != null && other.getPin().equals(getPin()))
                && (other.getObjectSid() == getObjectSid()
                        || other.getObjectSid() != null && other.getObjectSid().equals(getObjectSid()))
                && (other.getEasyVistaOu() == getEasyVistaOu()
                        || other.getEasyVistaOu() != null && other.getEasyVistaOu().equals(getEasyVistaOu()))
                && (other.getEasyVistaOffice() == getEasyVistaOffice()
                        || other.getEasyVistaOffice() != null && other.getEasyVistaOffice().equals(getEasyVistaOffice()))

                && (other.getDocumentIdNumber() == getDocumentIdNumber()
                        || other.getDocumentIdNumber() != null && other.getDocumentIdNumber().equals(getDocumentIdNumber()))

                && (other.getStudentNumber() == getStudentNumber()
                        || other.getStudentNumber() != null && other.getStudentNumber().equals(getStudentNumber()))
                && (other.getMobilePhoneMFA() == getMobilePhoneMFA()
                        || other.getMobilePhoneMFA() != null && other.getMobilePhoneMFA().equals(getMobilePhoneMFA()))

                && (other.getEmployeeNumber() == getEmployeeNumber()
                        || other.getEmployeeNumber() != null && other.getEmployeeNumber().equals(getEmployeeNumber()))
                && (other.getSecondEmail() == getSecondEmail()
                        || other.getSecondEmail() != null && other.getSecondEmail().equals(getSecondEmail()))

        ) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Name=" + getName() + ",FamilyName=" + getFamilyName() + ",GivenName=" + getGivenName() + ",E-mail="
                + getInstitutionalEmail() + ",Username=" + getUsername() + ",WorkPhone=" + getWorkPhone() + ",HomeDirectory="
                + getHomeDirectory() + ",HomeDrive=" + getHomeDrive() + ",CommonName=" + getCommonName() + ",DisplayName="
                + getDisplayName() + ",ScriptPath=" + getScriptPath() + ",Description=" + getDescription()
                + ",PhysicalDeliveryOfficeName=" + getPhysicalDeliveryOfficeName() + ",WWWHomePage=" + getWWWHomePage()
                + ",Title=" + getTitle() + ",UnixHomeDirectory=" + getUnixHomeDirectory() + ",GIDNumber=" + getGidNumber()
                + ",UIDNumber=" + getUidNumber() + ",Company=" + getCompany() + ",LoginShell=" + getLoginShell() + ",Department="
                + getDepartment() + ",DepartmentAcronym=" + getDepartmentAcronym() + ",Groups=" + getGroups()
                + ",DistinguishedName=" + getDistinguishedName() + ",ProfilePath=" + getProfilePath() + ",sAmAccountName="
                + getUsernameWithoutSuffix() + ",pin=" + getPin() + ",objectSid=" + getObjectSid() + ",EasyvistaOu="
                + getEasyVistaOu() + ",EasyvistaOffice=" + getEasyVistaOffice()

                + ",DocumentIdNumber=" + getDocumentIdNumber()

                + ",StudentNumber=" + getStudentNumber()

                + ",MobilePhoneMFA=" + getMobilePhoneMFA()

                + ",EmployeeNumber=" + getEmployeeNumber()

                + ",SecondEmail=" + getSecondEmail()

        ;
    }
}