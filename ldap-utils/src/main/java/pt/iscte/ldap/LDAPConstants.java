package pt.iscte.ldap;

public final class LDAPConstants {

    public static final String HOME_DIRECTORY_BASE_PATH = "\\\\areas\\home\\";

    public static final String HOME_DRIVE = "H:";

    public static final String PROFILE_PATH = "\\\\aplicacoes\\profiles\\alunos";

    public static final String SCRIPT_PATH = "batch.bat";

    private LDAPConstants() {
    }
}