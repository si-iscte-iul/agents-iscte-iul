package pt.iscte.ldap;

import java.util.HashSet;
import java.util.Set;

public class Configuration {

    private static Set<LDAPAttribute> attributesToManage = new HashSet<LDAPAttribute>();

    private Configuration() {
    }

    public static void setAttributesToManage(final LDAPAttribute... attributes) {
        for (LDAPAttribute attribute : attributes) {
            attributesToManage.add(attribute);
        }
    }

    public static Set<LDAPAttribute> getAttributesToManage() {
        return attributesToManage;
    }
}