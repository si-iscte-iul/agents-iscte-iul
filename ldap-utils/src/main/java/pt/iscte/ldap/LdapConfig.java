package pt.iscte.ldap;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;

public enum LdapConfig {

    STUDENTS("students"), NOT_STUDENTS("notStudents"), SERVICES("services");

    private String id;
    private boolean check;

    LdapConfig(String id) {
        this.id = id;
        String ldapCheckEnabled = PropertiesManager.getProperty(MessageFormat.format(LDAP_CHECK_ENABLE_KEY, id));
        check = Boolean.parseBoolean(ldapCheckEnabled);
    }

    public Hashtable<String, String> getEnvironment() {
        return MAP_ENV.get(this);
    }

    public String getDcPath() {
        return MAP_OTHER_PROPS.get(this).get(LDAP_DC_PATH_KEY);
    }

    public String getOuUsers() {
        return MAP_OTHER_PROPS.get(this).get(LDAP_OU_USERS_KEY);
    }

    public String getOuGroups() {
        return MAP_OTHER_PROPS.get(this).get(LDAP_OU_GROUPS_KEY);
    }

    public String getUsernameSuffix() {
        return MAP_OTHER_PROPS.get(this).get(LDAP_USERNAME_SUFFIX);
    }

    public boolean check() {
        return check;
    }

    public String getId() {
        return id;
    }

    public static final Map<LdapConfig, Map<String, String>> MAP_OTHER_PROPS = new HashMap<LdapConfig, Map<String, String>>();
    public static final Map<LdapConfig, Hashtable<String, String>> MAP_ENV = new HashMap<LdapConfig, Hashtable<String, String>>();

    // ENVIRONMENT
    private static final String LDAP_INITIAL_CONTEXT_FACTORY_KEY = "ldap.{0}.initial.context.factory";
    private static final String LDAP_PROVIDER_URL_KEY = "ldap.{0}.provider.url";
    private static final String LDAP_SECURITY_AUTHENTICATION_KEY = "ldap.{0}.security.authentication";
    private static final String LDAP_ADMIN_PASS_KEY = "ldap.{0}.admin.pass";
    private static final String LDAP_ADMIN_USER_KEY = "ldap.{0}.admin.user";

    private static final String LDAP_CONNECT_TIMEOUT_KEY = "ldap.{0}.connect.timeout";
    private static final String LDAP_READ_TIMEOUT_KEY = "ldap.{0}.read.timeout";

    private static final String CONNECT_TIMEOUT = "com.sun.jndi.ldap.connect.timeout";
    private static final String READ_TIMEOUT = "com.sun.jndi.ldap.read.timeout";

    // OTHER PROPERTIES
    private static final String LDAP_DC_PATH_KEY = "ldap.{0}.dc.path";
    private static final String LDAP_OU_USERS_KEY = "ldap.{0}.ou.users";
    private static final String LDAP_OU_GROUPS_KEY = "ldap.{0}.ou.groups";
    private static final String LDAP_USERNAME_SUFFIX = "ldap.{0}.username.suffix";
    private static final String LDAP_CHECK_ENABLE_KEY = "ldap.{0}.check.enabled";

    static {
        for (LdapConfig ldapConfig : LdapConfig.values()) {
            String id = ldapConfig.getId();

            // Environment properties
            Hashtable<String, String> env = new Hashtable<String, String>();
            String ldapInitialContextFactory =
                    PropertiesManager.getProperty(MessageFormat.format(LDAP_INITIAL_CONTEXT_FACTORY_KEY, id));
            String ldapProviderUrl = PropertiesManager.getProperty(MessageFormat.format(LDAP_PROVIDER_URL_KEY, id));
            String ldapSecurityAuthentication =
                    PropertiesManager.getProperty(MessageFormat.format(LDAP_SECURITY_AUTHENTICATION_KEY, id));
            String connectTimeout = PropertiesManager.getProperty(MessageFormat.format(LDAP_CONNECT_TIMEOUT_KEY, id));
            String readTimeout = PropertiesManager.getProperty(MessageFormat.format(LDAP_READ_TIMEOUT_KEY, id));
            String ldapAdminUser = PropertiesManager.getProperty(MessageFormat.format(LDAP_ADMIN_USER_KEY, id));
            String ldapAdminPass = PropertiesManager.getProperty(MessageFormat.format(LDAP_ADMIN_PASS_KEY, id));

            env.put(Context.INITIAL_CONTEXT_FACTORY, ldapInitialContextFactory);
            env.put(Context.PROVIDER_URL, ldapProviderUrl);
            env.put(Context.SECURITY_AUTHENTICATION, ldapSecurityAuthentication);
            env.put(CONNECT_TIMEOUT, connectTimeout);
            env.put(READ_TIMEOUT, readTimeout);
            env.put(Context.SECURITY_PRINCIPAL, ldapAdminUser);
            env.put(Context.SECURITY_CREDENTIALS, ldapAdminPass);
            env.put("java.naming.ldap.attributes.binary", "objectSid objectGUID");

            MAP_ENV.put(ldapConfig, env);

            // Other properties
            Map<String, String> otherProps = new Hashtable<String, String>();
            String ldapDcPath = PropertiesManager.getProperty(MessageFormat.format(LDAP_DC_PATH_KEY, id));
            otherProps.put(LDAP_DC_PATH_KEY, ldapDcPath);

            String ldapOuUsers = PropertiesManager.getProperty(MessageFormat.format(LDAP_OU_USERS_KEY, id));
            otherProps.put(LDAP_OU_USERS_KEY, ldapOuUsers);

            String ldapOuGroups = PropertiesManager.getProperty(MessageFormat.format(LDAP_OU_GROUPS_KEY, id));
            otherProps.put(LDAP_OU_GROUPS_KEY, ldapOuGroups);

            String ldapUsernameSuffix = PropertiesManager.getProperty(MessageFormat.format(LDAP_USERNAME_SUFFIX, id));
            otherProps.put(LDAP_USERNAME_SUFFIX, ldapUsernameSuffix);

            MAP_OTHER_PROPS.put(ldapConfig, otherProps);
        }
    }
}
