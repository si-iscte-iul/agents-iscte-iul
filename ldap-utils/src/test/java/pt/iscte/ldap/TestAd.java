package pt.iscte.ldap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestAd {
    public static void main(String[] args) throws Exception {
        PropertiesManager.init(new PropertiesManager.DefaultPropertiesResolver());

////        LDAPUtilsGeneric.updateLdapUserEmailBySamAccountName("ajsco", "Batatas.Fritas@iscte.pt", LdapConfig.NOT_STUDENTS);
//        LDAPUtilsGeneric.updateLdapUserEmailBySamAccountName("ajsco", null, LdapConfig.NOT_STUDENTS);

//        ArrayList<String> users = LDAPUtilsGeneric.getAllUsersReturnOnlySamAccountName(LdapConfig.NOT_STUDENTS);
//        FileUtils.writeLines(new File("all-emp-ad-users.txt"), users);

//        updateEmailsFromFile("all-emp-ad-users-output.txt");

//        testFind("mskee");
//        testFind("vllrs", LdapConfig.NOT_STUDENTS);
//        testObjectSid("S-1-5-21-494209958-961087194-3522339849-6460", LdapConfig.NOT_STUDENTS);
//        testObjectSid("S-1-5-21-494209958-961087194-3522339849-4137", LdapConfig.NOT_STUDENTS);

//        testFind("vllrs", LdapConfig.NOT_STUDENTS);

//        showMembers("_SAS", LdapConfig.SERVICES);
//        showMembers("sas", LdapConfig.NOT_STUDENTS);
//        addMember("_SAS", "S-1-5-21-494209958-961087194-3522339849-6460", LdapConfig.SERVICES);
//      addMember("_GDSI", "S-1-5-21-494209958-961087194-3522339849-4137", LdapConfig.SERVICES);

//      addMember("_GDSI", "CN=vera lucia lourenco rodrigues,OU=iul.users,DC=emp,DC=iul", LdapConfig.SERVICES);

//        LDAPUtilsGeneric.createForeignSecurityPrincipal("S-1-5-21-494209958-961087194-3522339849-6454", LdapConfig.SERVICES);

//        LDAPUser test =
//                LDAPUtilsGeneric.findUserByObjectSid("S-1-5-21-3527456910-2976979000-3113625395-2005", LdapConfig.NOT_STUDENTS);

        LDAPUser test = LDAPUtilsGeneric.findUserById("Ft5siic", LdapConfig.SERVICES);
        showData(test);
        //
        testUpdate();
        //
        LDAPUser test2 = LDAPUtilsGeneric.findUserById("Ft5siic", LdapConfig.SERVICES);
        showData(test2);

//        LDAPUser userTestPhone = LDAPUtilsGeneric.findUserById("ajsco", LdapConfig.SERVICES);
//        LDAPUtilsGeneric.updateLdapUserHomePhone(userTestPhone, null, LdapConfig.SERVICES);

//        boolean test =
//                LDAPUtilsGeneric.createForeignSecurityPrincipal("S-1-5-21-3527456910-2976979000-3113625395-1355",
//                        LdapConfig.SERVICES);
//        System.out.println(test);

//        createGroup();

//        testUpdate();
//        testGetGroupMembers();

//        testFindUserByDistinguishedName("CN=Jessica Lauren Freund,OU=Erasmus_export,OU=import-fenix,DC=iul,DC=intra");
//        testFindGroupByDistinguishedName("CN=VPN_Funcionarios,OU=groups,DC=iul,DC=intra");
//        testFindGroupByDistinguishedName("CN=office365.license.alumni,OU=groups,OU=import-fenix,DC=iul,DC=intra");

        System.out.println("Done");
    }

    private static void testFindGroupByDistinguishedName(final String dn) {
        String samAccountName = LDAPUtilsGeneric.findGroupByDistinguishedName(dn, LdapConfig.SERVICES);
        System.out.println(samAccountName + "|" + dn);
    }

    private static void testFindUserByDistinguishedName(final String dn) {
//        LDAPUser ldapUser = LDAPUtilsGeneric.findUserById("jlfdn", LdapConfig.SERVICES);
//        System.out.println(ldapUser.getUsernameWithoutSuffix() + "|" + ldapUser.getDistinguishedName());

        LDAPUser ldapUser = LDAPUtilsGeneric.findUserByDistinguishedName(dn, LdapConfig.SERVICES);
        System.out.println(ldapUser.getUsernameWithoutSuffix() + "|" + ldapUser.getDistinguishedName());
    }

    private static void testGetGroupMembers() {
        ArrayList<LDAPUser> allUsers =
                LDAPUtilsGeneric.getAllUsersReturnOnlySamAccountNameAndDistinguishedName(LdapConfig.SERVICES);
//        ArrayList<String> allUsers2 = LDAPUtilsGeneric.getAllUsersReturnOnlySamAccountName(LdapConfig.SERVICES);

        List<String> groups = Arrays.asList("students", "all.users.students");
        for (String groupId : groups) {
            List<String> members = LDAPUtilsGeneric.getGroupUsersByGroupId(groupId, LdapConfig.SERVICES);
            System.out.println("|" + groupId + "|" + members.size());
            for (String member : members) {
                LDAPUser ldapUser = findByDistinguishedName(allUsers, member);
                String userId = ldapUser != null ? ldapUser.getUsernameWithoutSuffix() : null;
                System.out.println("|" + groupId + "|" + member + "|" + userId);
            }
        }
    }

    private static LDAPUser findByDistinguishedName(ArrayList<LDAPUser> allUsers, String member) {
        return allUsers.stream().filter(u -> u.getDistinguishedName().equals(member)).findFirst().orElse(null);
    }

    private static void testUpdate() {
        LDAPUser ldapUser = LDAPUtilsGeneric.findUserById("ft5siic", LdapConfig.SERVICES);

//        ldapUser.setName(userDataForExternalService.getFullName());
//        ldapUser.setCommonName(convertToCommonName(userDataForExternalService.getFullName()));
//        ldapUser.setDisplayName(userDataForExternalService.getNickname());
//        ldapUser.setDescription(userDataForExternalService.getDescription());
//        ldapUser.setFamilyName(userDataForExternalService.getFamilyNames());
//        ldapUser.setGivenName(userDataForExternalService.getGivenNames());
//		ldapUser.setDepartment(null);
//		ldapUser.setDescription("zz");
//        ldapUser.setTitle(userDataForExternalService.getUserDepartmentTitle());
//        ldapUser.setWorkPhone("121212");
//        ldapUser.setEasyVistaOu("blaafefwefwefwef");
//        ldapUser.setEasyVistaOffice("etrphktyorhktrphrtkphktrhp1");

//        ldapUser.setDocumentIdNumber(null);
//        ldapUser.setStudentNumber(null);
//        ldapUser.setMobilePhoneMFA("+351921222558z");
//        ldapUser.setEmployeeNumber(null);
//        ldapUser.setSecondEmail(null);

        ldapUser.setDocumentIdNumber("12345678");
        ldapUser.setStudentNumber("200001");
        ldapUser.setMobilePhoneMFA("+351921222558z");
        ldapUser.setEmployeeNumber("9002");
        ldapUser.setSecondEmail("emilio2@sapo.pt");

        LDAPUtilsGeneric.updateLdapUser(ldapUser, true, LdapConfig.SERVICES);

//        LDAPUtilsGeneric.updateLdapUserMobilePhoneMFA(ldapUser, "+351921222777", LdapConfig.SERVICES);
    }

    private static void createGroup() {
        LDAPUtilsGeneric.createGroupByName("potatoesGroupSecurityFalse", null, "FALSE", AdGroupScope.DOMAIN_LOCAL, null,
                "DisplayNome potatoes DLnull", "pot1@horta.com", LdapConfig.STUDENTS.getOuGroups(), LdapConfig.STUDENTS);

        LDAPUtilsGeneric.createGroupByName("potatoesGroupSecurityTrue", null, "TRUE", AdGroupScope.DOMAIN_LOCAL,
                AdGroupType.SECURITY_ENABLED, "DisplayNome potatoes DLse", "pot2@horta.com", LdapConfig.STUDENTS.getOuGroups(),
                LdapConfig.STUDENTS);

        LDAPUtilsGeneric.createGroupByName("potatoesGroupGlobalSecurityFalse", null, "FALSE", AdGroupScope.GLOBAL, null,
                "DisplayNome potatoes Gnull", "pot3@horta.com", LdapConfig.STUDENTS.getOuGroups(), LdapConfig.STUDENTS);

        LDAPUtilsGeneric.createGroupByName("potatoesGroupGlobalSecurityTrue", null, "TRUE", AdGroupScope.GLOBAL,
                AdGroupType.SECURITY_ENABLED, "DisplayNome potatoes Gse", "pot4@horta.com", LdapConfig.STUDENTS.getOuGroups(),
                LdapConfig.STUDENTS);

        LDAPUtilsGeneric.createGroupByName("potatoesGroupUnivDistribList", null, "FALSE", AdGroupScope.UNIVERSAL, null,
                "DisplayNome potatoes Unull", "pot5@horta.com", LdapConfig.STUDENTS.getOuGroups(), LdapConfig.STUDENTS);

        LDAPUtilsGeneric.createGroupByName("potatoesGroupUnivSecurityTrue", null, "TRUE", AdGroupScope.UNIVERSAL,
                AdGroupType.SECURITY_ENABLED, "DisplayNome potatoes Use", "pot6@horta.com", LdapConfig.STUDENTS.getOuGroups(),
                LdapConfig.STUDENTS);

    }

//    private static void compareSidConverterImplementations() {
//        ArrayList<LDAPUser> users = LDAPUtilsGeneric.getUsersFromLdapByAll(LdapConfig.NOT_STUDENTS);
//        System.out.println("|user|sidApache|sidRoller");
//        for (LDAPUser user : users) {
//            System.out.println("|" + user.getName() + "|" + SidConverter.convertToString(user.getObjectSid()) + "|"
//                    + SidConverter.getSIDAsString(user.getObjectSid()));
//        }
//    }

    private static void addMember(final String groupId, final String memberSid, final LdapConfig ldapConfig) {
//        String userDistinguishedName = "CN=" + memberSid + ",CN=ForeignSecurityPrincipals,DC=iul,DC=intra";
        String userDistinguishedName = memberSid;

        LDAPUtilsGeneric.addMemberOfByDistinguishedNameAndGroupSamAccountName(userDistinguishedName, groupId, ldapConfig);

    }

    private static void showMembers(final String groupId, final LdapConfig ldapConfig) {
        List<String> members = LDAPUtilsGeneric.getGroupUsersByGroupId(groupId, ldapConfig);
        for (String member : members) {
            System.out.println(member);
        }
    }

    private static void testFind(String username, final LdapConfig ldapConfig) {
        LDAPUser user = LDAPUtilsGeneric.findUserById(username, ldapConfig);
        showData(user);
    }

    private static void showData(LDAPUser user) {
        if (user == null) {
            System.out.println(user);
            return;
        }
        System.out.println(user.getDisplayName());
        System.out.println(user.getObjectSid());
        System.out.println("sAMAccountName:" + user.getUsernameWithoutSuffix());
        System.out.println("DistinguishedName: " + user.getDistinguishedName());
        System.out.println("convertToString: " + SidConverter.convertToString(user.getObjectSid()));
        System.out.println("easyVistaOu:" + user.getEasyVistaOu());
        System.out.println("easyVistaOffice:" + user.getEasyVistaOffice());
        System.out.println(LDAPAttribute.HOME_PHONE + ":" + user.getDocumentIdNumber());

        System.out.println(LDAPAttribute.STUDENT_ID_ISCTE + ":" + user.getStudentNumber());
        System.out.println(LDAPAttribute.IP_PHONE + ":" + user.getMobilePhoneMFA());

        System.out.println(LDAPAttribute.ID_DOCUMENT_NUMBER + ":" + user.getDocumentIdNumber());
        System.out.println(LDAPAttribute.EMPLOYEE_NUMBER + ":" + user.getEmployeeNumber());
        System.out.println(LDAPAttribute.SECOND_EMAIL + ":" + user.getSecondEmail());
    }

    private static void testObjectSid(String sid, final LdapConfig ldapConfig) {
        LDAPUser user = LDAPUtilsGeneric.findUserByObjectSid(sid, ldapConfig);
        showData(user);
    }

    private static void testCreate() {
        LDAPUser ldapUser = new LDAPUser();
        ldapUser.setName("palhaco12");
        ldapUser.setUsername("batata12@iscte.pt");
        ldapUser.setUsernameWithoutSuffix("batata12");
        ldapUser.setCommonName("palhaco12");
        ldapUser.setInstitutionalEmail("palhaco.batatinha2@iscte.pt");

        String password = "BatataX23d.";
        LDAPUtilsGeneric.createLdapUser(ldapUser, password, null, LdapConfig.SERVICES);
    }

//    private static void updateEmailsFromFile(String filename) throws IOException {
//        List<String> lines = FileUtils.readLines(new File(filename));
//
//        for (String line : lines) {
//            String[] parts = line.split(",");
//            String username = parts[0];
//            String email = parts[1];
//
//            try {
//                String usernameWithoutDomainSuffix = username.split("@")[0];
//                LDAPUtilsGeneric.updateLdapUserEmailBySamAccountName(usernameWithoutDomainSuffix, email, LdapConfig.NOT_STUDENTS);
//            } catch (Throwable t) {
//                System.out.println("");
//                System.out.println(line);
//                System.out.println("");
//                System.out.println("");
//                t.printStackTrace();
//            }
//        }
//    }

}
