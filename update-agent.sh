#!/bin/bash
# -xv
#
# -------------------------------------
# Disable SVN, because Google Code was discontinued and now we are using GIT in BitBucket.
# Antonio Casqueiro - 2015-09-17
# -------------------------------------
#
# Fenix Update Agent
# Author: Ivo Branco 
# Company: ISCTE-IUL (www.iscte-iul.pt)
# Contact: ivo.branco@iscte-iul.pt
# 
# 05 - Sep - 2013
#
# Arguments:
#	1. agent project name like 'google-apps-agent'
#	2. revision (optional)
#
######################################
# CONFIGURATION VARIABLES START HERE #
######################################
#
# Agents Source Variables
SOURCE_BASE_DIR=/srv/agentes/agents-source
PROJ=$1
SOURCE_PROJ_DIR=$SOURCE_BASE_DIR/$PROJ
SOURCE_PROJ_TARGET_DIR=${SOURCE_PROJ_DIR}/target
#
# Svn Variables
SVN_SERVER=fenix-iscte-iul.googlecode.com
SVN_PATH=svn/agents
SVN_USERNAME=
SVN_REVISION=$2
#
####################################
# CONFIGURATION VARIABLES END HERE #
####################################
#
#
# Unless you're pretty sure about what you're doing
# DO NOT edit anything below this comment.
#
function updateOrCreateCheckout {
#	ARGS="";
#	if [ -n "$SVN_REVISION" ]; then
#		ARGS="-r $SVN_REVISION";
#	fi
#	if [ ! -d $SOURCE_BASE_DIR ]; then
#		svn co https://$SVN_USERNAME@$SVN_SERVER/$SVN_PATH $SOURCE_BASE_DIR $ARGS;
#	else 
#		cd $SOURCE_PROJ_DIR;
#		svn update $ARGS;
#	fi

	echo "SVN DISABLED - Using GIT to update the code";
	cd $SOURCE_BASE_DIR;
	git pull
}

function build {
	cd $SOURCE_PROJ_DIR
	mvn clean package
	chown -R agentes:agentes $SOURCE_BASE_DIR
}

updateOrCreateCheckout $SVN_REVISION;
echo "$PROJ was updated.";

build;
echo "$PROJ was built.";

echo 0
