package pt.iscte.dsi.webclient.utils;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.YearMonthDay;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public final class Utilities {

    private static HashMap<String, String> dsiDegreeCodes = new HashMap<String, String>(1);

    private static HashMap<String, Integer> dsiOrganizationalCodes = new HashMap<String, Integer>(1);

    static {
        loadDSIDegreeCodes();
        loadDSIOrganizationalCodes();
    }

    private static void loadDSIDegreeCodes() {
        // ("Fénix code", "DSI code");
        dsiDegreeCodes.put("25", "825");
        dsiDegreeCodes.put("B25", "825");
        dsiDegreeCodes.put("79", "864");
        dsiDegreeCodes.put("009", "911");
        dsiDegreeCodes.put("B009", "911");
        dsiDegreeCodes.put("9448", "701");
        dsiDegreeCodes.put("016", "701");
        dsiDegreeCodes.put("17", "817");
        dsiDegreeCodes.put("32", "832");
        dsiDegreeCodes.put("67", "901");
        dsiDegreeCodes.put("11", "811");
        dsiDegreeCodes.put("48", "848");
        dsiDegreeCodes.put("2D", "871");
        dsiDegreeCodes.put("49", "849");
        dsiDegreeCodes.put("B67", "901");
        dsiDegreeCodes.put("B11", "811");
        dsiDegreeCodes.put("0022", "702");
        dsiDegreeCodes.put("022", "702");
        dsiDegreeCodes.put("52", "852");
        dsiDegreeCodes.put("BPG002", "937");
        dsiDegreeCodes.put("9019", "915");
        dsiDegreeCodes.put("B31", "831");
        dsiDegreeCodes.put("31", "831");
        dsiDegreeCodes.put("58", "887");
        dsiDegreeCodes.put("B58", "887");
        dsiDegreeCodes.put("19", "819");
        dsiDegreeCodes.put("66", "921");
        dsiDegreeCodes.put("21", "821");
        dsiDegreeCodes.put("105", "918");
        dsiDegreeCodes.put("01", "801");
        dsiDegreeCodes.put("B01", "801");
        dsiDegreeCodes.put("37", "837");
        dsiDegreeCodes.put("B27", "827");
        dsiDegreeCodes.put("27", "827");
        dsiDegreeCodes.put("BPG017", "948");
        dsiDegreeCodes.put("BPG014", "944");
        dsiDegreeCodes.put("20", "820");
        dsiDegreeCodes.put("09", "809");
        dsiDegreeCodes.put("51", "883");
        dsiDegreeCodes.put("46", "846");
        dsiDegreeCodes.put("B46", "846");
        dsiDegreeCodes.put("BPG019", "947");
        dsiDegreeCodes.put("107", "927");
        dsiDegreeCodes.put("B412", "933");
        dsiDegreeCodes.put("9081", "703");
        dsiDegreeCodes.put("156", "703");
        dsiDegreeCodes.put("50", "850");
        dsiDegreeCodes.put("B68", "894");
        dsiDegreeCodes.put("68", "894");
        dsiDegreeCodes.put("71", "930");
        dsiDegreeCodes.put("114", "924");
        dsiDegreeCodes.put("72", "865");
        dsiDegreeCodes.put("69", "898");
        dsiDegreeCodes.put("B72", "865");
        dsiDegreeCodes.put("34", "834");
        dsiDegreeCodes.put("B34", "834");
        dsiDegreeCodes.put("103", "843");
        dsiDegreeCodes.put("9098", "704");
        dsiDegreeCodes.put("B103", "843");
        dsiDegreeCodes.put("249", "704");
        dsiDegreeCodes.put("9119", "715");
        dsiDegreeCodes.put("292", "715");
        dsiDegreeCodes.put("B104", "917");
        dsiDegreeCodes.put("104", "917");
        dsiDegreeCodes.put("07", "807");
        dsiDegreeCodes.put("411", "929");
        dsiDegreeCodes.put("B07", "807");
        dsiDegreeCodes.put("38", "838");
        dsiDegreeCodes.put("B38", "838");
        dsiDegreeCodes.put("B002", "904");
        dsiDegreeCodes.put("002", "904");
        dsiDegreeCodes.put("40", "840");
        dsiDegreeCodes.put("B40", "840");
        dsiDegreeCodes.put("078", "889");
        dsiDegreeCodes.put("BPG012", "932");
        dsiDegreeCodes.put("24", "824");
        dsiDegreeCodes.put("B24", "824");
        dsiDegreeCodes.put("9140", "705");
        dsiDegreeCodes.put("654", "705");
        dsiDegreeCodes.put("8029", "916");
        dsiDegreeCodes.put("B012", "932");
        dsiDegreeCodes.put("1PG", "882");
        dsiDegreeCodes.put("9147", "711");
        dsiDegreeCodes.put("416", "711");
        dsiDegreeCodes.put("B113", "928");
        dsiDegreeCodes.put("4D1", "872");
        dsiDegreeCodes.put("4D2", "872");
        dsiDegreeCodes.put("4D", "872");
        dsiDegreeCodes.put("4D3", "872");
        dsiDegreeCodes.put("4D5", "872");
        dsiDegreeCodes.put("4D4", "872");
        dsiDegreeCodes.put("4D6", "872");
        dsiDegreeCodes.put("2PG", "902");
        dsiDegreeCodes.put("16", "816");
        dsiDegreeCodes.put("77", "931");
        dsiDegreeCodes.put("63C", "863");
        dsiDegreeCodes.put("62", "862");
        dsiDegreeCodes.put("BPG005", "950");
        dsiDegreeCodes.put("BPG010", "943");
        dsiDegreeCodes.put("BPG009", "942");
        dsiDegreeCodes.put("9157", "707");
        dsiDegreeCodes.put("54", "854");
        dsiDegreeCodes.put("41", "841");
        dsiDegreeCodes.put("15", "815");
        dsiDegreeCodes.put("B15", "815");
        dsiDegreeCodes.put("23", "823");
        dsiDegreeCodes.put("B112", "914");
        dsiDegreeCodes.put("433", "706");
        dsiDegreeCodes.put("9167", "706");
        dsiDegreeCodes.put("BPG007", "934");
        dsiDegreeCodes.put("BPG008", "941");
        dsiDegreeCodes.put("405", "907");
        dsiDegreeCodes.put("BPG006", "940");
        dsiDegreeCodes.put("13", "813");
        dsiDegreeCodes.put("42", "842");
        dsiDegreeCodes.put("B42", "842");
        dsiDegreeCodes.put("9181", "708");
        dsiDegreeCodes.put("70", "867");
        dsiDegreeCodes.put("44", "844");
        dsiDegreeCodes.put("45", "845");
        dsiDegreeCodes.put("008", "908");
        dsiDegreeCodes.put("B008", "908");
        dsiDegreeCodes.put("467", "708");
        dsiDegreeCodes.put("08", "808");
        dsiDegreeCodes.put("005", "905");
        dsiDegreeCodes.put("B005", "905");
        dsiDegreeCodes.put("9189", "709");
        dsiDegreeCodes.put("492", "709");
        dsiDegreeCodes.put("B007", "906");
        dsiDegreeCodes.put("22", "881");
        dsiDegreeCodes.put("26", "826");
        dsiDegreeCodes.put("55", "855");
        dsiDegreeCodes.put("9205", "710");
        dsiDegreeCodes.put("562", "710");
        dsiDegreeCodes.put("BPG021", "935");
        dsiDegreeCodes.put("76", "869");
        dsiDegreeCodes.put("BPG004", "939");
        dsiDegreeCodes.put("73", "891");
        dsiDegreeCodes.put("61", "861");
        dsiDegreeCodes.put("B73", "891");
        dsiDegreeCodes.put("B60", "860");
        dsiDegreeCodes.put("60", "860");
        dsiDegreeCodes.put("59", "859");
        dsiDegreeCodes.put("56", "856");
        dsiDegreeCodes.put("36", "836");
        dsiDegreeCodes.put("B12", "812");
        dsiDegreeCodes.put("12", "812");
        dsiDegreeCodes.put("05", "805");
        dsiDegreeCodes.put("47", "847");
        dsiDegreeCodes.put("9219", "712");
        dsiDegreeCodes.put("B4", "923");
        dsiDegreeCodes.put("697", "712");
        dsiDegreeCodes.put("04", "804");
        dsiDegreeCodes.put("75", "892");
        dsiDegreeCodes.put("BPG003", "938");
        dsiDegreeCodes.put("65", "895");
        dsiDegreeCodes.put("3D", "875");
        dsiDegreeCodes.put("001", "949");
        dsiDegreeCodes.put("B001", "949");
        dsiDegreeCodes.put("57", "857");
        dsiDegreeCodes.put("B57", "857");
        dsiDegreeCodes.put("03", "803");
        dsiDegreeCodes.put("9240", "713");
        dsiDegreeCodes.put("1D", "873");
        dsiDegreeCodes.put("759", "713");
        dsiDegreeCodes.put("B1D", "873");
        dsiDegreeCodes.put("30", "830");
        dsiDegreeCodes.put("B30", "830");
        dsiDegreeCodes.put("B101", "927");
        dsiDegreeCodes.put("29", "829");
        dsiDegreeCodes.put("02", "802");
        dsiDegreeCodes.put("10", "810");
        dsiDegreeCodes.put("763", "714");
        dsiDegreeCodes.put("B006", "920");
        dsiDegreeCodes.put("9241", "714");
        dsiDegreeCodes.put("006", "920");
        dsiDegreeCodes.put("BPG016", "945");
        dsiDegreeCodes.put("33", "833");
    }

    private static void loadDSIOrganizationalCodes() {
        dsiOrganizationalCodes.put("ACEA", 28);
        dsiOrganizationalCodes.put("ADETTI", 4);
        dsiOrganizationalCodes.put("AUDAX", 59);
        dsiOrganizationalCodes.put("CAV", 41);
        dsiOrganizationalCodes.put("CC", 53);
        dsiOrganizationalCodes.put("CEA", 20);
        dsiOrganizationalCodes.put("CEAS", 7);
        dsiOrganizationalCodes.put("CEHCP", 17);
        dsiOrganizationalCodes.put("CEMAF", 8);
        dsiOrganizationalCodes.put("CET", 9);
        dsiOrganizationalCodes.put("CEUA", 18);
        dsiOrganizationalCodes.put("CI", 30);
        dsiOrganizationalCodes.put("CIES", 11);
        dsiOrganizationalCodes.put("CIS", 10);
        dsiOrganizationalCodes.put("CP", 54);
        dsiOrganizationalCodes.put("DA", 22);
        dsiOrganizationalCodes.put("DAFP", 48);
        dsiOrganizationalCodes.put("DC", 75);
        dsiOrganizationalCodes.put("DCG", 25);
        dsiOrganizationalCodes.put("DCTI", 26);
        dsiOrganizationalCodes.put("DE", 2);
        dsiOrganizationalCodes.put("DF", 74);
        dsiOrganizationalCodes.put("DGRH", 44);
        dsiOrganizationalCodes.put("DH", 3);
        dsiOrganizationalCodes.put("DINÂMIA", 12);
        dsiOrganizationalCodes.put("DMQ", 24);
        dsiOrganizationalCodes.put("DP", 46);
        dsiOrganizationalCodes.put("DPSO", 1);
        dsiOrganizationalCodes.put("DS", 23);
        dsiOrganizationalCodes.put("DSA", 46);
        dsiOrganizationalCodes.put("DSAD", 48);
        dsiOrganizationalCodes.put("DSBD", 40);
        dsiOrganizationalCodes.put("DSRH", 44);
        dsiOrganizationalCodes.put("GAA", 33);
        dsiOrganizationalCodes.put("GAQ", 68);
        dsiOrganizationalCodes.put("GARE", 45);
        dsiOrganizationalCodes.put("GEP", 69);
        dsiOrganizationalCodes.put("GESP", 43);
        dsiOrganizationalCodes.put("GIEM", 13);
        dsiOrganizationalCodes.put("GIESTA", 19);
        dsiOrganizationalCodes.put("GIRE", 33);
        dsiOrganizationalCodes.put("GJ", 58);
        dsiOrganizationalCodes.put("GP", 58);
        dsiOrganizationalCodes.put("GPGA", 51);
        dsiOrganizationalCodes.put("GPRF", 48);
        dsiOrganizationalCodes.put("IN OUT GLOBAL", 70);
        dsiOrganizationalCodes.put("INDEG", 15);
        dsiOrganizationalCodes.put("NAC", 46);
        dsiOrganizationalCodes.put("NAT", 46);
        dsiOrganizationalCodes.put("NRI", 32);
        dsiOrganizationalCodes.put("OBSERVA", 38);
        dsiOrganizationalCodes.put("OVERGEST", 14);
        dsiOrganizationalCodes.put("SAAU", 29);
        dsiOrganizationalCodes.put("SACG", 46);
        dsiOrganizationalCodes.put("SACS", 46);
        dsiOrganizationalCodes.put("SACT", 46);
        dsiOrganizationalCodes.put("SAD", 21);
        dsiOrganizationalCodes.put("SC", 48);
        dsiOrganizationalCodes.put("SD", 44);
        dsiOrganizationalCodes.put("SEA", 44);
        dsiOrganizationalCodes.put("SEP", 48);
        dsiOrganizationalCodes.put("SM", 46);
        dsiOrganizationalCodes.put("SVA", 44);
        dsiOrganizationalCodes.put("T", 48);
        dsiOrganizationalCodes.put("UNIDE", 5);
        dsiOrganizationalCodes.put("NOT_DEFINED", 99);
    }

    public static String getDSIDegreeCode(final String degreeCode) {
        return dsiDegreeCodes.get(degreeCode);
    }

    public static Integer getDSIUnitID(final String unitAcronym) {
        return dsiOrganizationalCodes.get(unitAcronym);
    }

    public static String getRegistrationHour(final YearMonthDay yearMonthDay) {
        int hourOfDay = yearMonthDay.toDateTimeAtCurrentTime().getHourOfDay();
        int minutesOfDay = yearMonthDay.toDateTimeAtCurrentTime().getMinuteOfHour();
        String hourOfDay_ = String.valueOf(hourOfDay);
        String minutesOfDay_ = String.valueOf(minutesOfDay);

        if (hourOfDay < 10) {
            hourOfDay_ = "0" + String.valueOf(hourOfDay);
        }

        if (minutesOfDay < 10) {
            minutesOfDay_ = "0" + String.valueOf(minutesOfDay);
        }

        return hourOfDay_ + ":" + minutesOfDay_;
    }

    public static String getRegistrationDate(final YearMonthDay yearMonthDay) {
        String day = String.valueOf(yearMonthDay.getDayOfMonth());
        String month = String.valueOf(yearMonthDay.getMonthOfYear());
        String year = String.valueOf(yearMonthDay.getYear());

        if (day.length() == 1)
            day = "0" + day;

        if (month.length() == 1)
            month = "0" + month;

        return year.substring(2, year.length()) + month + day;
    }

    public static String getRegistrationHour(final DateTime dateTime) {
        return dateTime.toString("HH:mm");
    }

    public static String getRegistrationDate(final DateTime dateTime) {
        return dateTime.toString("yyyy-MM-dd");
    }

    // Method used to deal with differences between DSI and Fénix databases
    // concerning with encoding
    public static String convertUTFEncodingToISO88591(final String toConvert) {
        String stringConverted = toConvert.toLowerCase();
        stringConverted = stringConverted.replace('á', 'a');
        stringConverted = stringConverted.replace('à', 'a');
        stringConverted = stringConverted.replace('â', 'a');
        stringConverted = stringConverted.replace('ã', 'a');
        stringConverted = stringConverted.replace('ä', 'o');
        stringConverted = stringConverted.replace('ç', 'c');
        stringConverted = stringConverted.replace('é', 'e');
        stringConverted = stringConverted.replace('è', 'e');
        stringConverted = stringConverted.replace('ê', 'e');
        stringConverted = stringConverted.replace('í', 'i');
        stringConverted = stringConverted.replace('ì', 'i');
        stringConverted = stringConverted.replace('ï', 'i');
        stringConverted = stringConverted.replace('ó', 'o');
        stringConverted = stringConverted.replace('ò', 'o');
        stringConverted = stringConverted.replace('ô', 'o');
        stringConverted = stringConverted.replace('õ', 'o');
        stringConverted = stringConverted.replace('ú', 'u');
        stringConverted = stringConverted.replace('ù', 'u');
        stringConverted = stringConverted.replace('ü', 'u');
        return stringConverted;
    }
}