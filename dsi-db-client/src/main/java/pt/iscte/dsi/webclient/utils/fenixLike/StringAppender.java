package pt.iscte.dsi.webclient.utils.fenixLike;

public class StringAppender {
    public static final String append(String string1, String string2, String... strings) {
        StringBuilder builder = new StringBuilder();
        builder.append(string1);
        builder.append(string2);
        for (String s : strings) {
            builder.append(s);
        }
        return builder.toString();
    }
}