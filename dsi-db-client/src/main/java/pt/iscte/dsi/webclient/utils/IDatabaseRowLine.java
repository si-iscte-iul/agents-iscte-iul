package pt.iscte.dsi.webclient.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public interface IDatabaseRowLine {

    public boolean fillWithDatabaseRowData(ResultSet rs) throws SQLException;

    public String getUniqueKey();

}