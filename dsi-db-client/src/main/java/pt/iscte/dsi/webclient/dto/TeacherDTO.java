package pt.iscte.dsi.webclient.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import pt.iscte.dsi.webclient.utils.IDatabaseRowLine;

public class TeacherDTO implements IDatabaseRowLine {

    String teacherUsername;

    String teacherFullName;
    String teacherUsedFullName;
    String teacherFirstName;
    String teacherUsedFirstName;
    String teacherLastName;
    String teacherUsedLastName;

    Integer teacherUserType;

    Integer teacherOrganId;

    String teacherOfficeExtension;

    String teacherPhone;

    String teacherOffice;

    String teacherCellphone;

    Integer teacherVisibility;

    String teacherPersonalEmail;

    Integer teacherUid;

    String teacherHomeUnix;

    String teacherInstituionalEmail;

    String teacherObservations;

    String teacherAliases;

    String teacherLockerID;

    String teacherDDI;

    String teacherMechanographicalNumber;

    String teacherFaxNumber;

    String teacherGid;

    String teacherIdentificationNumber;

    Integer teacherCategoryId;

    String teacherKeywords;

    String teacherExternalCVURL;

    String teacherLDAPName;

    public boolean fillWithDatabaseRowData(ResultSet rs) throws SQLException {
        setTeacherUsername(StringUtils.getStringInUtf8Format(rs.getBytes("LOGIN")));
        setTeacherFullName(StringUtils.getStringInUtf8Format(rs.getBytes("NOME")));
        setTeacherUsedFullName(StringUtils.getStringInUtf8Format(rs.getBytes("NOME_USADO")));
        setTeacherFirstName(StringUtils.getStringInUtf8Format(rs.getBytes("NOMES_PROPRIOS")));
        setTeacherUsedFirstName(StringUtils.getStringInUtf8Format(rs.getBytes("NOMES_PROPRIOS_USADOS")));
        setTeacherLastName(StringUtils.getStringInUtf8Format(rs.getBytes("APELIDOS")));
        setTeacherUsedLastName(StringUtils.getStringInUtf8Format(rs.getBytes("APELIDOS_USADOS")));
        setTeacherUserType(rs.getInt("TIPO_USER"));
        setTeacherOrganId(rs.getInt("ORGAO_ISCTE"));
        setTeacherOfficeExtension(StringUtils.getStringInUtf8Format(rs.getBytes("EXT_GABINETE")));
        setTeacherPhone(StringUtils.getStringInUtf8Format(rs.getBytes("TELEFONE")));
        setTeacherOffice(StringUtils.getStringInUtf8Format(rs.getBytes("GABINETE")));
        setTeacherCellphone(StringUtils.getStringInUtf8Format(rs.getBytes("TELEMOVEL")));
        setTeacherVisibility(rs.getInt("VISIBILIDADE"));
        setTeacherPersonalEmail(StringUtils.getStringInUtf8Format(rs.getBytes("E_MAIL_PESSOAL")));
        setTeacherUid(rs.getInt("UID"));
        setTeacherHomeUnix(StringUtils.getStringInUtf8Format(rs.getBytes("HOME_UNIX")));
        setTeacherInstituionalEmail(StringUtils.getStringInUtf8Format(rs.getBytes("E_MAIL_ISCTE")));
        setTeacherObservations(StringUtils.getStringInUtf8Format(rs.getBytes("OBSERVACOES")));
        setTeacherAliases(StringUtils.getStringInUtf8Format(rs.getBytes("ALIASES")));
        setTeacherLockerID(StringUtils.getStringInUtf8Format(rs.getBytes("ID_CACIFO")));
        setTeacherDDI(StringUtils.getStringInUtf8Format(rs.getBytes("DDI")));
        setTeacherMechanographicalNumber(StringUtils.getStringInUtf8Format(rs.getBytes("N_MECANOGRAFICO")));
        setTeacherFaxNumber(StringUtils.getStringInUtf8Format(rs.getBytes("FAX")));
        setTeacherGid(StringUtils.getStringInUtf8Format(rs.getBytes("GID")));
        setTeacherIdentificationNumber(StringUtils.getStringInUtf8Format(rs.getBytes("N_DOCUMENTO_IDENTIFICACAO")));
        setTeacherCategoryId(rs.getInt("ID_CATEGORIA"));
        setTeacherKeywords(StringUtils.getStringInUtf8Format(rs.getBytes("KEYWORDS")));
        setTeacherExternalCVURL(StringUtils.getStringInUtf8Format(rs.getBytes("EXTERNAL_CV_URL")));
        setTeacherLDAPName(StringUtils.getStringInUtf8Format(rs.getBytes("NOME_LDAP")));
        return true;
    }

    public String getTeacherAliases() {
        return teacherAliases;
    }

    public void setTeacherAliases(String teacherAliases) {
        this.teacherAliases = teacherAliases;
    }

    public Integer getTeacherCategoryId() {
        return teacherCategoryId;
    }

    public void setTeacherCategoryId(Integer teacherCategoryId) {
        this.teacherCategoryId = teacherCategoryId;
    }

    public String getTeacherCellphone() {
        return teacherCellphone;
    }

    public void setTeacherCellphone(String teacherCellphone) {
        this.teacherCellphone = teacherCellphone;
    }

    public String getTeacherDDI() {
        return teacherDDI;
    }

    public void setTeacherDDI(String teacherDDI) {
        this.teacherDDI = teacherDDI;
    }

    public String getTeacherExternalCVURL() {
        return teacherExternalCVURL;
    }

    public void setTeacherExternalCVURL(String teacherExternalCVURL) {
        this.teacherExternalCVURL = teacherExternalCVURL;
    }

    public String getTeacherFaxNumber() {
        return teacherFaxNumber;
    }

    public void setTeacherFaxNumber(String teacherFaxNumber) {
        this.teacherFaxNumber = teacherFaxNumber;
    }

    public String getTeacherFirstName() {
        return teacherFirstName;
    }

    public void setTeacherFirstName(String teacherFirstName) {
        this.teacherFirstName = teacherFirstName;
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName) {
        this.teacherFullName = teacherFullName;
    }

    public String getTeacherUsedFullName() {
        return teacherUsedFullName;
    }

    public void setTeacherUsedFullName(String teacherFullUsedName) {
        this.teacherUsedFullName = teacherFullUsedName;
    }

    public String getTeacherGid() {
        return teacherGid;
    }

    public void setTeacherGid(String teacherGid) {
        this.teacherGid = teacherGid;
    }

    public String getTeacherHomeUnix() {
        return teacherHomeUnix;
    }

    public void setTeacherHomeUnix(String teacherHomeUnix) {
        this.teacherHomeUnix = teacherHomeUnix;
    }

    public String getTeacherIdentificationNumber() {
        return teacherIdentificationNumber;
    }

    public void setTeacherIdentificationNumber(String teacherIdentificationNumber) {
        this.teacherIdentificationNumber = teacherIdentificationNumber;
    }

    public String getTeacherInstituionalEmail() {
        return teacherInstituionalEmail;
    }

    public void setTeacherInstituionalEmail(String teacherInstituionEmail) {
        this.teacherInstituionalEmail = teacherInstituionEmail;
    }

    public String getTeacherKeywords() {
        return teacherKeywords;
    }

    public void setTeacherKeywords(String teacherKeywords) {
        this.teacherKeywords = teacherKeywords;
    }

    public String getTeacherLastName() {
        return teacherLastName;
    }

    public void setTeacherLastName(String teacherLastName) {
        this.teacherLastName = teacherLastName;
    }

    public String getTeacherLDAPName() {
        return teacherLDAPName;
    }

    public void setTeacherLDAPName(String teacherLDAPName) {
        this.teacherLDAPName = teacherLDAPName;
    }

    public String getTeacherLockerID() {
        return teacherLockerID;
    }

    public void setTeacherLockerID(String teacherLockerID) {
        this.teacherLockerID = teacherLockerID;
    }

    public String getTeacherMechanographicalNumber() {
        return teacherMechanographicalNumber;
    }

    public void setTeacherMechanographicalNumber(String teacherMechanographicalNumber) {
        this.teacherMechanographicalNumber = teacherMechanographicalNumber;
    }

    public String getTeacherObservations() {
        return teacherObservations;
    }

    public void setTeacherObservations(String teacherObservations) {
        this.teacherObservations = teacherObservations;
    }

    public String getTeacherOffice() {
        return teacherOffice;
    }

    public void setTeacherOffice(String teacherOffice) {
        this.teacherOffice = teacherOffice;
    }

    public String getTeacherOfficeExtension() {
        return teacherOfficeExtension;
    }

    public void setTeacherOfficeExtension(String teacherOfficeExtension) {
        this.teacherOfficeExtension = teacherOfficeExtension;
    }

    public Integer getTeacherOrganId() {
        return teacherOrganId;
    }

    public void setTeacherOrganId(Integer teacherOrganId) {
        this.teacherOrganId = teacherOrganId;
    }

    public String getTeacherPersonalEmail() {
        return teacherPersonalEmail;
    }

    public void setTeacherPersonalEmail(String teacherPersonalEmail) {
        this.teacherPersonalEmail = teacherPersonalEmail;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public Integer getTeacherUid() {
        return teacherUid;
    }

    public void setTeacherUid(Integer teacherUid) {
        this.teacherUid = teacherUid;
    }

    public String getTeacherUsedFirstName() {
        return teacherUsedFirstName;
    }

    public void setTeacherUsedFirstName(String teacherUsedFirstName) {
        this.teacherUsedFirstName = teacherUsedFirstName;
    }

    public String getTeacherUsedLastName() {
        return teacherUsedLastName;
    }

    public void setTeacherUsedLastName(String teacherUsedLastName) {
        this.teacherUsedLastName = teacherUsedLastName;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public Integer getTeacherUserType() {
        return teacherUserType;
    }

    public void setTeacherUserType(Integer teacherUserType) {
        this.teacherUserType = teacherUserType;
    }

    public Integer getTeacherVisibility() {
        return teacherVisibility;
    }

    public void setTeacherVisibility(Integer teacherVisibility) {
        this.teacherVisibility = teacherVisibility;
    }

    public String getUniqueKey() {
        return getTeacherUid().toString();
    }
}
