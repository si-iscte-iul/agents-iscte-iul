package pt.iscte.dsi.webclient;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import pt.iscte.agents.dsiDb.DsiStudentDataService;
import pt.iscte.agents.dsiDb.RegistrationAgreement;
import pt.iscte.agents.dsiDb.StudentData;
import pt.iscte.dsi.webclient.dto.Aluno;
import pt.iscte.dsi.webclient.utils.AlunoDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.DegreeType;
import pt.iscte.dsi.webclient.utils.MappingDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.fenixLike.LogSender;
import pt.iscte.dsi.webclient.utils.fenixLike.Pair;
import pt.iscte.dsi.webclient.utils.fenixLike.PropertiesManager;

/**
 * This class loads the aluno table in the ciiscte_wp database. It calls a Fénix
 * webservice to get the data.
 * 
 * @author António Casqueiro
 */
public class ImportNewStudents {
    private final static Logger logger = Logger.getLogger(ImportNewStudents.class);

    // The process updates value is hardcoded because no updates
    // should be performed, until explicitly being told!
    private static final boolean PROCESS_UPDATES = false;

    private static final SimpleDateFormat sdfSmallDate = new SimpleDateFormat("yyMMdd");
    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");

    private AlunoDataLoaderFromDatabase<Aluno> loader = null;
    private Collection<Aluno> listaAlunos = null;
    private Collection<Aluno> listaAlunosInsert = null;
    private Collection<Aluno> listaAlunosUpdate = null;

    private Date sysdate;

    private List<Pair<StudentData, String>> errors;
    private List<Pair<StudentData, String>> warnings;

    private static final String ERROR_USER_WITHOUT_USERNAME = "User without username";
    private static final String ERROR_INVALID_USERNAME = "Invalid username";
    private static final String ERROR_UNSUPPORTED_DEGREE_CODE = "Unsupported degree code";

    // private static final String
    // WARNING_INSTITUTIONAL_DEGREE_STUDENT_BUT_NOT_ERASMUS =
    // " Institutional degree student, but not Erasmus.";

    private List<Pair<Aluno, String>> actions;
    private static final String ACTION_INSERT = "I";
    private static final String ACTION_UPDATE = "U";

    // private static final String ACTION_DELETE = "D";

    @SuppressWarnings("rawtypes")
    private MappingDataLoaderFromDatabase database = null;

    @SuppressWarnings("rawtypes")
    public ImportNewStudents() {
        super();
        this.loader = new AlunoDataLoaderFromDatabase<Aluno>();
        this.listaAlunosInsert = new ArrayList<Aluno>();
        this.listaAlunosUpdate = new ArrayList<Aluno>();

        this.actions = new ArrayList<Pair<Aluno, String>>();
        this.errors = new ArrayList<Pair<StudentData, String>>();
        this.warnings = new ArrayList<Pair<StudentData, String>>();

        database = new MappingDataLoaderFromDatabase();
        database.connectDatabase();
    }

    // ------------------------------------------------------------------------
    // FILTER RECORDS CODE - start
    // ------------------------------------------------------------------------
    /*
     * private static HashMap<String, String> filter = new HashMap<String,
     * String>(); static { filter.put("p7758@iscte.pt", "x");
     * filter.put("p7759@iscte.pt", "x"); filter.put("p7760@iscte.pt", "x");
     * filter.put("p7761@iscte.pt", "x"); filter.put("p7762@iscte.pt", "x");
     * filter.put("p7763@iscte.pt", "x"); filter.put("p7764@iscte.pt", "x");
     * filter.put("p7765@iscte.pt", "x"); filter.put("p7766@iscte.pt", "x");
     * filter.put("p7767@iscte.pt", "x"); filter.put("p7768@iscte.pt", "x");
     * filter.put("p7769@iscte.pt", "x"); filter.put("p7770@iscte.pt", "x");
     * filter.put("p7746@iscte.pt", "x"); filter.put("p7747@iscte.pt", "x");
     * filter.put("p7748@iscte.pt", "x"); filter.put("p7749@iscte.pt", "x");
     * filter.put("p7750@iscte.pt", "x"); filter.put("p7751@iscte.pt", "x");
     * filter.put("p7752@iscte.pt", "x"); filter.put("p7753@iscte.pt", "x");
     * filter.put("p7754@iscte.pt", "x"); filter.put("p7755@iscte.pt", "x");
     * filter.put("p7756@iscte.pt", "x"); }
     */
    private boolean isToProcess(String username) {
        // return filter.get(username) != null;
        // return "dhcoh@iscte.pt".equals(username);
        return true;
    }

    private boolean isToProcess(Aluno aluno) {
        // if (aluno == null) {
        // logger.debug("Skipping new!");
        // return;
        // }
        //
        // if (!"system".equals(aluno.getUtil())) {
        // logger.debug("Skipping not mine!");
        // return;
        // }

        // // ajsco - Remove filter
        // if (!"12870063".equals(studentData.getIdDocumentNumber())) {
        // return;
        // }

        // if (!studentData.getUsername().trim().endsWith("oeapa@iscte.pt")) {
        // // skip it!
        // return;
        // }

        return true;
    }

    // ------------------------------------------------------------------------
    // FILTER RECORDS CODE - end
    // ------------------------------------------------------------------------

    public static void main(String[] args) throws UnsupportedEncodingException, RemoteException {
        DsiStudentDataService service = DsiStudentDataServiceUtil.getService();

        int start = getStartValue();
        final int STEP = getStepValue();
        final int STOP = getStopValue();

        List<StudentData> list = null;
        ImportNewStudents agent = new ImportNewStudents();
        agent.loadAll();

        try {
            String[] staticList = getStaticList();
            if (staticList != null && staticList.length > 0) {
                agent.updateSysdate();
                for (String elem : staticList) {
                    String username = elem.trim();
                    logger.info("Processing: " + username);
                    StudentData studentData = service.getOneByUsernameStudentData(username);
                    agent.process(studentData);
                }

                // COMMIT changes
                agent.commit();
                agent.cleanUp();
            } else {
                do {
                    int end = start + STEP;
                    logger.info("(" + start + "," + end + ")");
                    list = buildList(service.getAllInIntervalStudentData(start, end));
                    start += STEP + 1;

                    agent.updateSysdate();
                    for (StudentData studentData : list) {
                        agent.process(studentData);
                    }

                    // COMMIT changes
                    agent.commit();
                    agent.cleanUp();
                } while (start < STOP);
                logger.info("The END");
            }

            // Report
            agent.report();
        } finally {
            agent.releaseDatabaseConnection();
        }
    }

    private static List<StudentData> buildList(List<StudentData> interval) {
        if (interval == null || interval.size() == 0) {
            return new ArrayList<StudentData>(0);
        }

        return interval;
    }

    private void releaseDatabaseConnection() {
        database.closeConnection();
    }

    private void report() throws UnsupportedEncodingException {
        final int countOK = actions.size();
        final int countErrors = errors.size();

        // Build message
        String loggingMessage = buildMessage();

        // Send email
        int totalRecords = countOK + countErrors;
        LogSender.sendLoggingByEmail("Webservice client ImportNewStudents", countErrors == 0, countErrors, countOK, totalRecords,
                loggingMessage);

        logger.info(loggingMessage);
    }

    private String buildMessage() {
        StringBuilder sb = new StringBuilder();

        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Errors"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Degree code", "Degree acronym",
                "Error"));
        for (Pair<StudentData, String> error : errors) {
            StudentData studentData = error.getFirst();
            String msg = error.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getName(), studentData.getIdDocumentNumber(),
                    studentData.getUsername(), studentData.getDegreeCode(), studentData.getDegreeAcronym(), msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Warnings"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Degree code", "Degree acronym",
                "Warning"));
        for (Pair<StudentData, String> warning : warnings) {
            StudentData studentData = warning.getFirst();
            String msg = warning.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getName(), studentData.getIdDocumentNumber(),
                    studentData.getUsername(), studentData.getDegreeCode(), studentData.getDegreeAcronym(), msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Actions performed"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Degree code", "Action"));
        for (Pair<Aluno, String> action : actions) {
            Aluno studentData = action.getFirst();
            String actionPerformed = action.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getNome(), studentData.getBi(),
                    studentData.getLogin(), studentData.getCod_area(), studentData.getCod_area(), actionPerformed));
        }

        return sb.toString();
    }

    private static int getStopValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.stop");
    }

    private static int getStepValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.step");
    }

    private static int getStartValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.start");
    }

    // private static String getEndpointAddress() {
    // String p =
    // PropertiesManager.getProperty("DsiStudentIntegration.endpointAddress");
    // if (p != null && p.trim().length() > 0) {
    // return p.trim();
    // }
    // return null;
    // }

    private void updateSysdate() {
        sysdate = new Date();
    }

    private void loadAll() {
        listaAlunos = loader.load(Aluno.class, "SELECT * FROM aluno");
    }

    private void process(StudentData studentData) {
        if (studentData.getUsername() == null) {
            errors.add(new Pair<StudentData, String>(studentData, ERROR_USER_WITHOUT_USERNAME));
            return;
        }

        if (!studentData.getUsername().trim().endsWith("@iscte.pt")) {
            errors.add(new Pair<StudentData, String>(studentData, ERROR_INVALID_USERNAME));
            return;
        }

        // Filter by data in Fénix
        if (!isToProcess(studentData.getUsername())) {
            // Skip
            return;
        }

        // 1- Check if student already exists, by:
        // * Login
        Aluno aluno = findPersonByUsername(studentData);

        // Filter by data in DSI
        if (!isToProcess(aluno)) {
            // Skip
            return;
        }

        // Skip students from "Unidades curriculares avulsas", because some
        // support is required.
        // A new "Tipo Matricula" must be created, otherwise the
        // getTipoMatricula method fails!
        //
        // if
        // (DegreeType.INSTITUTIONAL_DEGREE.equals(studentData.getDegreeType())
        // &&
        // !RegistrationAgreement.ERASMUS.equals(studentData.getRegistrationAgreement()))
        // {
        // warnings.add(new Pair<StudentData, String>(studentData,
        // WARNING_INSTITUTIONAL_DEGREE_STUDENT_BUT_NOT_ERASMUS));
        // return;
        // }

        // Skip students from a degree not mapped
        //
        if (getCodigoArea(studentData.getDegreeAcronym(), studentData.getDegreeCode()) == -1) {
            errors.add(new Pair<StudentData, String>(studentData, ERROR_UNSUPPORTED_DEGREE_CODE));
            return;
        }

        boolean isNew = false;
        if (aluno == null) {
            boolean alreadyExists = findPersonByBi(studentData) != null;

            // 2- Create student if he does not exists
            aluno = createStudent(studentData, alreadyExists);
            isNew = true;
        } else {
            // 2- Update student data
            if (PROCESS_UPDATES) {
                updateStudent(aluno, studentData);
            }
        }

        // 3- INSERT or UPDATE (memory)
        if (isNew) {
            addInsert(aluno);
        } else {
            if (PROCESS_UPDATES) {
                addUpdate(aluno);
            }
        }

        // Feedback
        if (isNew || PROCESS_UPDATES) {
            logger.debug((isNew ? "I" : "U") + ": Processed " + studentData.getName() + " | id document: "
                    + studentData.getIdDocumentNumber() + " | username: " + studentData.getUsername());
        }
    }

    private void addInsert(Aluno aluno) {
        listaAlunosInsert.add(aluno);
        listaAlunos.add(aluno);
        actions.add(new Pair<Aluno, String>(aluno, ACTION_INSERT));
    }

    private void addUpdate(Aluno aluno) {
        listaAlunosUpdate.add(aluno);
        actions.add(new Pair<Aluno, String>(aluno, ACTION_UPDATE));
    }

    private Aluno findPersonByUsername(StudentData studentData) {
        for (Aluno aluno : listaAlunos) {
            if (aluno.getLogin().equals(getLoginForDSI(studentData.getUsername()))) {
                return aluno;
            }
        }
        return null;
    }

    private Aluno findPersonByBi(StudentData studentData) {
        for (Aluno aluno : listaAlunos) {
            if (aluno.getBi().equals(studentData.getIdDocumentNumber())) {
                logger.debug(studentData.getName() + ":" + studentData.getUsername() + ":" + studentData.getIdDocumentNumber());
                return aluno;
            }
        }
        return null;
    }

    private Aluno createStudent(StudentData studentData, boolean alreadyExists) {
        Aluno aluno = new Aluno();

        updateStudent(aluno, studentData);
        aluno.setData_alteracao(null);
        aluno.setData(getSmallDate(sysdate));

        if (alreadyExists && !isUniversalUsername(aluno.getLogin())) {
            aluno.setNome_ldap(aluno.getNome() + " (" + aluno.getLogin() + ")");
        }

        // Add non updatable fields
        aluno.setPassword_p(studentData.getPassword());

        aluno.setActivo(true);
        aluno.setNt(1);
        aluno.setMail(1);
        aluno.setWww(1);
        aluno.setLinux(1);
        aluno.setAutorizado(true);
        aluno.setHora(getTime(sysdate));
        aluno.setUtil("system");
        aluno.setAutor_data(getSmallDate(sysdate));
        aluno.setElearning(1);
        aluno.setMsdnaa(0);
        aluno.setR_e_mail(0);

        return aluno;
    }

    private void updateStudent(Aluno aluno, StudentData studentData) {
        aluno.setNome(studentData.getName());
        aluno.setNome_usado(studentData.getNickname());
        aluno.setNomes_proprios(studentData.getGivenNames());
        aluno.setNomes_proprios_usados(studentData.getUsedGivenNames());
        aluno.setApelidos(studentData.getFamilyNames());
        aluno.setApelidos_usados(studentData.getUsedFamilyNames());
        aluno.setLogin(getLoginForDSI(studentData.getUsername()));
        aluno.setBi(studentData.getIdDocumentNumber());
        aluno.setTipo_matricula(getTipoMatricula(studentData.getDegreeType(), studentData.getRegistrationAgreement()));
        aluno.setAno(studentData.getCurricularYear());
        aluno.setCod_area(getCodigoArea(studentData.getDegreeAcronym(), studentData.getDegreeCode()));
        aluno.setInsc_data(getSmallDate(studentData.getRegistrationStartDate().toGregorianCalendar()));
        aluno.setN_aluno(studentData.getNumber().toString());
        aluno.setE_mail_iscte(studentData.getInstitutionalEmail());
        aluno.setE_mail_pessoal(studentData.getPersonalEmail());
        aluno.setTelefone(studentData.getPhoneNumber());
        aluno.setTelemovel(studentData.getMobilePhoneNumber());
        aluno.setData_alteracao(getSmallDate(sysdate));
    }

    private void commit() {
        List<String> sqlStatements = new ArrayList<String>(listaAlunosInsert.size() + listaAlunosUpdate.size());

        // INSERTS
        for (Aluno aluno : listaAlunosInsert) {
            sqlStatements.add(generateInsert(aluno));
        }
        // UPDATES
        for (Aluno aluno : listaAlunosUpdate) {
            sqlStatements.add(generateUpdate(aluno));
        }

        // TODO ajsco - Será que é mesmo isto que se pretende!?!?
        // Because, autocommit is not set to false,
        // if one fails, the ones that sucessed
        // will not be rolledback
        loader.executeUpdate(sqlStatements);
    }

    private String generateUpdate(Aluno aluno) {
        // PK (login=? AND cod_curso=?)
        // Possible optimization - Use PreparedStatements
        StringBuilder sb = new StringBuilder(1000);
        sb.append("UPDATE aluno SET ");

        sb.append("nome=").append(getString(aluno.getNome())).append(",");
        sb.append("nome_usado=").append(getString(aluno.getNome_usado())).append(",");
        sb.append("nomes_proprios=").append(getString(aluno.getNomes_proprios())).append(",");
        sb.append("nomes_proprios_usados=").append(getString(aluno.getNomes_proprios_usados())).append(",");
        sb.append("apelidos=").append(getString(aluno.getApelidos())).append(",");
        sb.append("apelidos_usados=").append(getString(aluno.getApelidos_usados())).append(",");
        sb.append("activo='").append(aluno.isActivo()).append("',");
        sb.append("obs=").append(getString(aluno.getObs())).append(",");
        sb.append("bi=").append(getString(aluno.getBi())).append(",");
        sb.append("nt=").append(aluno.getNt()).append(",");
        sb.append("mail=").append(aluno.getMail()).append(",");
        sb.append("www=").append(aluno.getWww()).append(",");
        sb.append("linux=").append(aluno.getLinux()).append(",");
        sb.append("tipo_matricula=").append(aluno.getTipo_matricula()).append(",");
        sb.append("ano=").append(aluno.getAno()).append(",");
        sb.append("cod_area=").append(aluno.getCod_area()).append(",");
        sb.append("autorizado=").append(aluno.isAutorizado()).append(",");
        sb.append("hora=").append(getString(aluno.getHora())).append(",");
        sb.append("util=").append(getString(aluno.getUtil())).append(",");
        sb.append("insc_data=").append(getString(aluno.getInsc_data())).append(",");
        sb.append("autor_data=").append(getString(aluno.getAutor_data())).append(",");
        sb.append("n_aluno=").append(getString(aluno.getN_aluno())).append(",");
        sb.append("elearning=").append(aluno.getElearning()).append(",");
        sb.append("msdnaa=").append(aluno.getMsdnaa()).append(",");
        sb.append("msdnaa_autorizacao=").append(getString(aluno.getMsdnaa_autorizacao())).append(",");
        sb.append("r_e_mail=").append(aluno.getR_e_mail()).append(",");
        sb.append("data_alteracao=").append(getString(aluno.getData_alteracao())).append("");

        sb.append(" WHERE ");
        sb.append("login=").append(getString(aluno.getLogin())).append(" AND ");
        sb.append("cod_area=").append(aluno.getCod_area());
        sb.append(";");

        return sb.toString();
    }

    private String generateInsert(Aluno aluno) {
        // PK (login=? AND cod_curso=?)
        // Possible optimization - Use PreparedStatements
        StringBuilder sb = new StringBuilder(1000);
        sb.append("INSERT INTO aluno (");
        sb.append("nome,");
        sb.append("nome_usado,");
        sb.append("nomes_proprios,");
        sb.append("nomes_proprios_usados,");
        sb.append("apelidos,");
        sb.append("apelidos_usados,");
        sb.append("login,");
        sb.append("e_mail_iscte,");
        sb.append("e_mail_pessoal,");
        sb.append("telefone,");
        sb.append("telemovel,");
        sb.append("activo,");
        sb.append("obs,");
        sb.append("bi,");
        sb.append("nt,");
        sb.append("mail,");
        sb.append("www,");
        sb.append("linux,");
        sb.append("password,");
        sb.append("password_p,");
        sb.append("tipo_matricula,");
        sb.append("ano,");
        sb.append("cod_area,");
        sb.append("data,");
        sb.append("autorizado,");
        sb.append("hora,");
        sb.append("util,");
        sb.append("insc_data,");
        sb.append("autor_data,");
        sb.append("n_aluno,");
        sb.append("elearning,");
        sb.append("msdnaa,");
        sb.append("msdnaa_autorizacao,");
        sb.append("r_e_mail,");
        sb.append("data_alteracao,");
        sb.append("nome_ldap");
        sb.append(") ");

        sb.append("VALUES (");
        sb.append(getString(aluno.getNome())).append(",");
        sb.append(getString(aluno.getNome_usado())).append(",");
        sb.append(getString(aluno.getNomes_proprios())).append(",");
        sb.append(getString(aluno.getNomes_proprios_usados())).append(",");
        sb.append(getString(aluno.getApelidos())).append(",");
        sb.append(getString(aluno.getApelidos_usados())).append(",");
        sb.append(getString(aluno.getLogin())).append(",");
        sb.append(getString(aluno.getE_mail_iscte())).append(",");
        sb.append(getString(aluno.getE_mail_pessoal())).append(",");
        sb.append(getString(aluno.getTelefone())).append(",");
        sb.append(getString(aluno.getTelemovel())).append(",");
        sb.append(aluno.isActivo()).append(",");
        sb.append(getString(aluno.getObs())).append(",");
        sb.append(getString(aluno.getBi())).append(",");
        sb.append(aluno.getNt()).append(",");
        sb.append(aluno.getMail()).append(",");
        sb.append(aluno.getWww()).append(",");
        sb.append(aluno.getLinux()).append(",");
        if (aluno.getPassword_p() != null) {
            sb.append("'cipasswd',");
            sb.append(getString(aluno.getPassword_p())).append(",");
        } else {
            sb.append("null,");
            sb.append("null,");
        }
        sb.append(aluno.getTipo_matricula()).append(",");
        sb.append(aluno.getAno()).append(",");
        sb.append(aluno.getCod_area()).append(",");
        sb.append(getString(aluno.getData())).append(",");
        sb.append(aluno.isAutorizado()).append(",");
        sb.append(getString(aluno.getHora())).append(",");
        sb.append(getString(aluno.getUtil())).append(",");
        sb.append(getString(aluno.getInsc_data())).append(",");
        sb.append(getString(aluno.getAutor_data())).append(",");
        sb.append(getString(aluno.getN_aluno())).append(",");
        sb.append(aluno.getElearning()).append(",");
        sb.append(aluno.getMsdnaa()).append(",");
        sb.append(getString(aluno.getMsdnaa_autorizacao())).append(",");
        sb.append(aluno.getR_e_mail()).append(",");
        sb.append(getString(aluno.getData_alteracao())).append(",");
        if (aluno.getNome_ldap() != null) {
            sb.append(getString(aluno.getNome_ldap()));
        } else {
            sb.append("''");
        }
        sb.append(");");

        return sb.toString();

    }

    private void cleanUp() {
        listaAlunosInsert.clear();
        listaAlunosUpdate.clear();
    }

    private int getTipoMatricula(String degreeType, RegistrationAgreement ingressionType) {
        if (DegreeType.BOLONHA_DEGREE.name().equals(degreeType) || DegreeType.DEGREE.name().equals(degreeType)
                || DegreeType.BOLONHA_INTEGRATED_MASTER_DEGREE.name().equals(degreeType)) {
            return 0;
        }

        if (DegreeType.BOLONHA_MASTER_DEGREE.name().equals(degreeType) || DegreeType.MASTER_DEGREE.name().equals(degreeType)) {
            return 1;
        }

        if (DegreeType.BOLONHA_PHD_PROGRAM.name().equals(degreeType) || DegreeType.PHD_DEGREE.name().equals(degreeType)) {
            return 2;
        }

        if (DegreeType.BOLONHA_POST_GRADUATION_DEGREE.name().equals(degreeType) || DegreeType.POST_GRADUATION_DEGREE.name().equals(degreeType)
                || DegreeType.POST_GRADUATE_SEMINAR.name().equals(degreeType)) {
            return 3;
        }

        // Erasmus: 4
        if (RegistrationAgreement.ERASMUS.equals(ingressionType)) {
            return 4;
        }

        if (DegreeType.BOLONHA_SPECIALIZATION_DEGREE.name().equals(degreeType) || DegreeType.SPECIALIZATION_DEGREE.name().equals(degreeType)) {
            return 5;
        }

        if (DegreeType.SUMMER_COURSE.name().equals(degreeType)) {
            return 6;
        }

        if (DegreeType.BOLONHA_CET_DEGREE.name().equals(degreeType)) {
            return 8;
        }

        if (DegreeType.BOLONHA_ADVANCED_FORMATION_DIPLOMA.name().equals(degreeType)) {
            return 9;
        }

        if (DegreeType.INSTITUTIONAL_DEGREE.name().equals(degreeType) && !RegistrationAgreement.ERASMUS.equals(ingressionType)) {
            // TODO Criar novo tipo matricula.
            return 0;
        }

        if (DegreeType.JOINT_PROGRAMMES.name().equals(degreeType) || DegreeType.OTHER_PROGRAMMES.name().equals(degreeType)) {
            // TODO Criar novo tipo matricula.
            return 0;
        }

        throw new IllegalArgumentException("Unknown mapping for degree type " + degreeType);
    }

    private int getCodigoArea(String degreeAcronym, String degreeCode) {
        Integer code = database.getCodAreaFromAreaTable(degreeAcronym);

        if (code == null) {
            // ajsco: 2009-10-11
            // I started using "sigla_nova" column from "area" table to identify
            // the degree.
            // However some degrees in the DSI database do not honor this
            // rule...
            // So I must still call the CourseMapping class for the
            // exceptions...

            String codeTemp = CourseMapping.getCodigoCursoDSI(degreeCode);
            if (codeTemp == null && degreeCode.startsWith("B")) {
                codeTemp = CourseMapping.getCodigoCursoDSI(degreeCode.substring(1));
            }

            if (codeTemp == null) {
                return -1;
                // throw new
                // IllegalArgumentException("Unknown mapping for degree code " +
                // degreeCode);
            }

            code = new Integer(codeTemp);
        }

        return code;
    }

    // 071028
    private static String getSmallDate(Calendar date) {
        if (date != null) {
            return getSmallDate(date.getTime());
        }
        return "";
    }

    private static String getSmallDate(Date date) {
        if (date != null) {
            return sdfSmallDate.format(date);
        }
        return "";
    }

    // 18:23
    private static String getTime(Date date) {
        if (date != null) {
            return sdfTime.format(date);
        }
        return "";
    }

    private static String getString(String str) {
        if (str == null || str.trim().length() == 0) {
            return null;
        }

        return "'" + str.replaceAll("'", "") + "'";
    }

    /**
     * Remove "@iscte.pt" suffix.
     * 
     * @param login
     * @return
     */
    private static String getLoginForDSI(String login) {
        return login.substring(0, login.length() - 9);
    }

    private static boolean isUniversalUsername(String username) {
        if (username != null) {
            int counter = 0;
            for (char c : username.toCharArray()) {
                if (Character.isDigit(c))
                    return false;
                if (counter > 3)
                    return true;
                counter++;
            }
            return true;
        }
        return false;
    }

    private static String[] getStaticList() {
        String p = PropertiesManager.getProperty("DsiStudentIntegration.staticList");
        if (p != null && p.trim().length() > 0) {
            return p.split(",");
        }
        return null;
    }

}
