package pt.iscte.dsi.webclient.utils.fenixLike;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

import org.joda.time.DateTime;

/**
 * This class is used to send emails with logging information.
 * 
 * @author António Casqueiro
 */
public class LogSender {
    private static final String SCRIPTS_FROM_EMAIL_NAME_KEY = "logSender.from.email.name";

    private static final String SCRIPTS_FROM_EMAIL_ADDRESS_KEY = "logSender.from.email.address";

    private static final String SCRIPTS_TO_EMAIL_ADDRESSES_KEY = "logSender.to.email.addresses";

    private static final String FILE_SEPARATOR;

    private static final String SCRIPTS_FROM_EMAIL_NAME;

    private static final String SCRIPTS_FROM_EMAIL_ADDRESS;

    private static final String SCRIPTS_TO_EMAIL_ADDRESSES;

    private static final Properties properties = new Properties();

    private static final String FILE_SEPARATOR_KEY = "file.separator";

    static {
        PropertiesManager.loadProperties(properties, "/configuration.properties");
        FILE_SEPARATOR = System.getProperty(FILE_SEPARATOR_KEY);
        SCRIPTS_FROM_EMAIL_NAME = (String) properties.get(SCRIPTS_FROM_EMAIL_NAME_KEY);
        SCRIPTS_FROM_EMAIL_ADDRESS = (String) properties.get(SCRIPTS_FROM_EMAIL_ADDRESS_KEY);
        SCRIPTS_TO_EMAIL_ADDRESSES = (String) properties.get(SCRIPTS_TO_EMAIL_ADDRESSES_KEY);
        checkBuildProperties();
    }

    private static void checkBuildProperties() {
        if (SCRIPTS_FROM_EMAIL_NAME == null) {
            throw new RuntimeException(SCRIPTS_FROM_EMAIL_NAME_KEY + " property cannot be null " + SCRIPTS_FROM_EMAIL_NAME);
        } else if (SCRIPTS_FROM_EMAIL_ADDRESS == null) {
            throw new RuntimeException(SCRIPTS_FROM_EMAIL_ADDRESS_KEY + " property cannot be null " + SCRIPTS_FROM_EMAIL_ADDRESS);
        } else if (SCRIPTS_TO_EMAIL_ADDRESSES == null) {
            throw new RuntimeException(SCRIPTS_TO_EMAIL_ADDRESSES_KEY + " property cannot be null " + SCRIPTS_TO_EMAIL_ADDRESSES);
        }
    }

    public static Collection<String> getListOfAddressesFromToAddressesProperty() {
        return SCRIPTS_TO_EMAIL_ADDRESSES.contains(",") ? Arrays.asList(SCRIPTS_TO_EMAIL_ADDRESSES.split(",")) : Collections
                .singleton(SCRIPTS_TO_EMAIL_ADDRESSES);
    }

    private static String getRegistrationHour(final DateTime dateTime) {
        return dateTime.toString("HH:mm");
    }

    private static String getRegistrationDate(final DateTime dateTime) {
        return dateTime.toString("yyyy-MM-dd");
    }

    public static void sendLoggingByEmail(final String subject, final boolean wasScriptResultOk, final int hardFailures,
            final int softFailures, final int successRecords, final int totalRecords, final String loggingMessage) {
        final DateTime currentDateTime = new DateTime();
        final String date = getRegistrationDate(currentDateTime);
        final String hour = getRegistrationHour(currentDateTime);

        sendLoggingByEmail(subject + " - " + date + "T" + hour + " - " + (wasScriptResultOk ? "ok" : "failed") + " - "
                + hardFailures + " hard fails, " + softFailures + " soft fails, " + successRecords + " success, " + totalRecords
                + " total", loggingMessage);

    }

    /**
     * 
     * @param subject
     *            The subject to be presented in the e-mail sent. To this
     *            subject it will be appended both the current date and hour, as
     *            well as the result of the script, which is configured through
     *            the parameter <code>wasScriptResultOk</code>
     * @param wasScriptResultOk
     *            When this parameter is set to true, the subject will present
     *            the string 'ok' so that, just looking at the subject of the
     *            e-mail, it is possible to understand that no problems were
     *            found. It will present 'failed' otherwise.
     * @param errorRecords
     *            The number of error records found by this script.
     * @param successRecords
     *            The number of success records found by this script.
     * @param totalRecords
     *            The total number of records processed by this script.
     * @param loggingMessage
     */
    public static void sendLoggingByEmail(final String subject, final boolean wasScriptResultOk, final int errorRecords,
            final int successRecords, final int totalRecords, final String loggingMessage) {
        final DateTime currentDateTime = new DateTime();
        final String date = getRegistrationDate(currentDateTime);
        final String hour = getRegistrationHour(currentDateTime);

        sendLoggingByEmail(subject + " - " + date + "T" + hour + " - " + (wasScriptResultOk ? "ok" : "failed") + " - "
                + errorRecords + " failed, " + successRecords + " success, " + totalRecords + " total", loggingMessage);

    }

    @SuppressWarnings("unchecked")
    public static void sendLoggingByEmail(final String subject, final String loggingMessage) {
        EmailSender.send(SCRIPTS_FROM_EMAIL_NAME, SCRIPTS_FROM_EMAIL_ADDRESS, new String[] {}, Collections.EMPTY_LIST,
                Collections.EMPTY_LIST, getListOfAddressesFromToAddressesProperty(), subject, loggingMessage);
    }

    public static final String getEmailLineFromArgumentsInTsv(final Object... parameters) {
        return getEmailLineFromArguments("\t", parameters);
    }

    private static final String getEmailLineFromArguments(final String argumentSeparator, final Object... parameters) {
        final StringBuilder result = new StringBuilder();
        for (final Object object : parameters) {
            if (object == null || !object.getClass().isArray()) {
                result.append(object).append(argumentSeparator);
            } else {
                Object[] objects = (Object[]) object;
                for (final Object o : objects) {
                    result.append(o).append("#");
                }
                if (objects.length > 0) {
                    result.deleteCharAt(result.length() - 1);
                }
                result.append(argumentSeparator);
            }
        }
        return result.toString().trim() + "\n";
    }
}
