package pt.iscte.dsi.webclient.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class Aluno extends UnifiedUser {

    private int nt;

    private int mail;

    private int www;

    private int linux;

    private int tipo_matricula;

    private int ano;

    private int cod_area;

    private String data;

    private boolean autorizado;

    private String hora;

    private String util;

    private String insc_data;

    private String autor_data;

    private String n_aluno;

    private int elearning;

    private int msdnaa;

    private String msdnaa_autorizacao;

    private int r_e_mail;

    private String data_alteracao;

    private String bi;

    private boolean activo;

    private String obs;

    private String password_p;

    /**
     * @return the ano
     */
    public int getAno() {
        return ano;
    }

    /**
     * @param ano
     *            the ano to set
     */
    public void setAno(int ano) {
        this.ano = ano;
    }

    /**
     * @return the autor_data
     */
    public String getAutor_data() {
        return autor_data;
    }

    /**
     * @param autor_data
     *            the autor_data to set
     */
    public void setAutor_data(String autor_data) {
        this.autor_data = autor_data;
    }

    /**
     * @return the autorizado
     */
    public boolean isAutorizado() {
        return autorizado;
    }

    /**
     * @param autorizado
     *            the autorizado to set
     */
    public void setAutorizado(boolean autorizado) {
        this.autorizado = autorizado;
    }

    /**
     * @return the cod_area
     */
    public int getCod_area() {
        return cod_area;
    }

    /**
     * @param cod_area
     *            the cod_area to set
     */
    public void setCod_area(int cod_area) {
        this.cod_area = cod_area;
    }

    /**
     * @return the data
     */
    public String getData() {
        return data;
    }

    /**
     * @param data
     *            the data to set
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * @return the data_alteracao
     */
    public String getData_alteracao() {
        return data_alteracao;
    }

    /**
     * @param data_alteracao
     *            the data_alteracao to set
     */
    public void setData_alteracao(String data_alteracao) {
        this.data_alteracao = data_alteracao;
    }

    /**
     * @return the elearning
     */
    public int getElearning() {
        return elearning;
    }

    /**
     * @param elearning
     *            the elearning to set
     */
    public void setElearning(int elearning) {
        this.elearning = elearning;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora
     *            the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the insc_data
     */
    public String getInsc_data() {
        return insc_data;
    }

    /**
     * @param insc_data
     *            the insc_data to set
     */
    public void setInsc_data(String insc_data) {
        this.insc_data = insc_data;
    }

    /**
     * @return the linux
     */
    public int getLinux() {
        return linux;
    }

    /**
     * @param linux
     *            the linux to set
     */
    public void setLinux(int linux) {
        this.linux = linux;
    }

    /**
     * @return the mail
     */
    public int getMail() {
        return mail;
    }

    /**
     * @param mail
     *            the mail to set
     */
    public void setMail(int mail) {
        this.mail = mail;
    }

    /**
     * @return the msdnaa
     */
    public int getMsdnaa() {
        return msdnaa;
    }

    /**
     * @param msdnaa
     *            the msdnaa to set
     */
    public void setMsdnaa(int msdnaa) {
        this.msdnaa = msdnaa;
    }

    /**
     * @return the msdnaa_autorizacao
     */
    public String getMsdnaa_autorizacao() {
        return msdnaa_autorizacao;
    }

    /**
     * @param msdnaa_autorizacao
     *            the msdnaa_autorizacao to set
     */
    public void setMsdnaa_autorizacao(String msdnaa_autorizacao) {
        this.msdnaa_autorizacao = msdnaa_autorizacao;
    }

    /**
     * @return the n_aluno
     */
    public String getN_aluno() {
        return n_aluno;
    }

    /**
     * @param n_aluno
     *            the n_aluno to set
     */
    public void setN_aluno(String n_aluno) {
        this.n_aluno = n_aluno;
    }

    /**
     * @return the nt
     */
    public int getNt() {
        return nt;
    }

    /**
     * @param nt
     *            the nt to set
     */
    public void setNt(int nt) {
        this.nt = nt;
    }

    /**
     * @return the r_e_mail
     */
    public int getR_e_mail() {
        return r_e_mail;
    }

    /**
     * @param r_e_mail
     *            the r_e_mail to set
     */
    public void setR_e_mail(int r_e_mail) {
        this.r_e_mail = r_e_mail;
    }

    /**
     * @return the tipo_matricula
     */
    public int getTipo_matricula() {
        return tipo_matricula;
    }

    /**
     * @param tipo_matricula
     *            the tipo_matricula to set
     */
    public void setTipo_matricula(int tipo_matricula) {
        this.tipo_matricula = tipo_matricula;
    }

    /**
     * @return the util
     */
    public String getUtil() {
        return util;
    }

    /**
     * @param util
     *            the util to set
     */
    public void setUtil(String util) {
        this.util = util;
    }

    /**
     * @return the www
     */
    public int getWww() {
        return www;
    }

    /**
     * @param www
     *            the www to set
     */
    public void setWww(int www) {
        this.www = www;
    }

    @Override
    public String getBi() {
        return bi;
    }

    @Override
    public void setBi(String bi) {
        this.bi = bi;
    }

    @Override
    public boolean isActivo() {
        return activo;
    }

    @Override
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String getObs() {
        return obs;
    }

    @Override
    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getPassword_p() {
        return password_p;
    }

    public void setPassword_p(String password_p) {
        this.password_p = password_p;
    }

    @Override
    public boolean fillWithDatabaseRowData(ResultSet rs) throws SQLException {
        final boolean superResult = super.fillWithDatabaseRowData(rs);

        setActivo(rs.getBoolean("activo"));
        setObs(StringUtils.getStringInUtf8Format(rs.getBytes("obs")));
        setBi(StringUtils.getStringInUtf8Format(rs.getBytes("bi")));
        setNt(rs.getInt("nt"));
        setMail(rs.getInt("mail"));
        setWww(rs.getInt("www"));
        setLinux(rs.getInt("linux"));
        setTipo_matricula(rs.getInt("tipo_matricula"));
        setAno(rs.getInt("ano"));
        setCod_area(rs.getInt("cod_area"));
        setData(StringUtils.getStringInUtf8Format(rs.getBytes("data")));
        setAutorizado(rs.getBoolean("autorizado"));
        setHora(StringUtils.getStringInUtf8Format(rs.getBytes("hora")));
        setUtil(StringUtils.getStringInUtf8Format(rs.getBytes("util")));
        setInsc_data(StringUtils.getStringInUtf8Format(rs.getBytes("insc_data")));
        setAutor_data(StringUtils.getStringInUtf8Format(rs.getBytes("autor_data")));
        setN_aluno(StringUtils.getStringInUtf8Format(rs.getBytes("n_aluno")));
        setElearning(rs.getInt("elearning"));
        setMsdnaa(rs.getInt("msdnaa"));
        setMsdnaa_autorizacao(StringUtils.getStringInUtf8Format(rs.getBytes("msdnaa_autorizacao")));
        setR_e_mail(rs.getInt("r_e_mail"));
        setData_alteracao(StringUtils.getStringInUtf8Format(rs.getBytes("data_alteracao")));

        return superResult && true;
    }

    public String getUniqueKey() {
        // TODO Auto-generated method stub
        return null;
    }
}