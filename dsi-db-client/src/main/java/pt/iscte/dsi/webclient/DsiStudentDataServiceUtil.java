package pt.iscte.dsi.webclient;

import java.net.MalformedURLException;
import com.sun.xml.ws.developer.JAXWSProperties;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import pt.iscte.agents.dsiDb.DsiStudentDataService;
import pt.iscte.agents.dsiDb.DsiStudentDataService_Service;
import pt.iscte.dsi.webclient.utils.fenixLike.PropertiesManager;

public class DsiStudentDataServiceUtil {

    private static final String DSI_STUDENT_DATA_SERVICE_WSDL_LOCATION = "DsiStudentIntegration.ws.client.wsdl.location";
    private static final String DSI_STUDENT_DATA_SERVICE_ADDRESS = "DsiStudentIntegration.ws.client.address";
    private static final String DSI_STUDENT_DATA_SERVICE_CONNECT_TIMEOUT = "DsiStudentIntegration.ws.client.request.timeout";
    private static final String DSI_STUDENT_DATA_SERVICE_REQUEST_TIMEOUT = "DsiStudentIntegration.ws.connect.timeout";

    private final static Logger logger = Logger.getLogger(DsiStudentDataServiceUtil.class);
    private static URL wsdlLocation = null;

    public static DsiStudentDataService getService() throws RemoteException {
        URL wsdlLocation = getWsdlLocation();
        DsiStudentDataService_Service service = new DsiStudentDataService_Service(wsdlLocation);
        DsiStudentDataService webService = service.getPort(DsiStudentDataService.class);
        Map<String, Object> context = ((BindingProvider) webService).getRequestContext();
        String address = getProperty(DSI_STUDENT_DATA_SERVICE_ADDRESS);
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, address);
        String connectTimeoutStr = getProperty(DSI_STUDENT_DATA_SERVICE_CONNECT_TIMEOUT);
        Integer connectTimeout = new Integer(connectTimeoutStr) * 1000;
        context.put(JAXWSProperties.CONNECT_TIMEOUT, connectTimeout);
        String requestTimeoutStr = getProperty(DSI_STUDENT_DATA_SERVICE_REQUEST_TIMEOUT);
        Integer requestTimeout = new Integer(requestTimeoutStr) * 1000;
        context.put(JAXWSProperties.REQUEST_TIMEOUT, requestTimeout);
        return webService;
    }

    private static URL getWsdlLocation() throws RemoteException {
        if (wsdlLocation == null) {
            String wsdlLocationStr = getProperty(DSI_STUDENT_DATA_SERVICE_WSDL_LOCATION);
            try {
                wsdlLocation = new URL(wsdlLocationStr);
            } catch (MalformedURLException e) {
                logger.error("Wrong wsdl location for dsi student data service ws client.", e);
                e.printStackTrace();
                throw new RemoteException("Dsi student data service web services is down", e);
            }
        }
        return wsdlLocation;
    }

    private static String getProperty(String propertyName) {
        String propertyValue = PropertiesManager.getProperty(propertyName);
        return propertyValue;
    }

}
