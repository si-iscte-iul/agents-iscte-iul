package pt.iscte.dsi.webclient.dto;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public enum Tipo_user {

    DOCENTE(1),

    FUNCIONARIO(2),

    INVESTIGADOR(3),

    CONVIDADO(4),

    DIVERSOS(5),

    ALUNO(6),

    COLABORADOR(7),

    EDITOR_EXTERNO(8),

    CONSELHEIRO(9),

    SEGURANCA(10),

    DOCENTE_EXTERNO(11),

    INSPECTOR(12);

    private int code;

    private Tipo_user(int code) {
        this.code = code;
    }

    public static Tipo_user getByCode(final int code) {
        for (final Tipo_user value : Tipo_user.values()) {
            if (value.code == code) {
                return value;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        switch (this) {
        case DOCENTE:
            return "Docente";
        case FUNCIONARIO:
            return "Funcionário(a)";
        case INVESTIGADOR:
            return "Investigador(a)";
        case CONVIDADO:
            return "Convidado(a)";
        case DIVERSOS:
            return "Diversos";
        case ALUNO:
            return "Aluno";
        case COLABORADOR:
            return "Colaborador(a)";
        case EDITOR_EXTERNO:
            return "Editor(Externo)";
        case CONSELHEIRO:
            return "Conselheiro";
        case SEGURANCA:
            return "Segurança";
        case DOCENTE_EXTERNO:
            return "Docente de escola externa";
        case INSPECTOR:
            return "Inspector";
        }
        return "UNKNOWN";
    }
}