/* 
 * ################################################################
 * 
 * 
 * Copyright (C) 2006-2007 ISCTE Computing Services (CI)
 * Contact: heliopolis@iscte.pt
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Initial developer(s): Heliopolis Team
 * Contributor(s): 
 *
 * ################################################################
 */
package pt.iscte.dsi.webclient.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * @author Diogo Henriques & Paulo Zenida
 * 
 */
public abstract class DatabaseManager {

    private final static Logger logger = Logger.getLogger(DatabaseManager.class);

    protected Connection con = null;

    protected Statement stm = null;

    protected ResultSet rs;

    protected final static String DATABASE_DRIVER_PROP = "database.driver";

    protected final static String DATABASE_URL_PROP = "database.url";

    protected final static String DATABASE_NAME_PROP = "database.name";

    protected final static String DATABASE_USERNAME_PROP = "database.username";

    protected final static String DATABASE_PASSWORD_PROP = "database.password";

    protected String databaseDriver;

    protected String databaseUrl;

    protected String databaseName;

    protected String databaseUsername;

    protected String databasePassword;

    public DatabaseManager() {
        initializeDatabaseParameters();
    }

    protected void initializeDriverProperty() {
        try {
            Properties databaseProperties = getDatabaseProperties();
            this.databaseDriver = databaseProperties.getProperty(DATABASE_DRIVER_PROP);
        } catch (IOException e) {
            System.err.println("It was not possible to initialize the driver property");
        }
    }

    protected void initializeDatabaseParameters() {
        initializeDriverProperty();
        try {
            Properties databaseProperties = getDatabaseProperties();
            this.databaseUrl = databaseProperties.getProperty(DATABASE_URL_PROP);
            this.databaseName = databaseProperties.getProperty(DATABASE_NAME_PROP);
            this.databaseUsername = databaseProperties.getProperty(DATABASE_USERNAME_PROP);
            this.databasePassword = databaseProperties.getProperty(DATABASE_PASSWORD_PROP);
        } catch (IOException e) {
            logger.error("It was not possible to initialize the database parameters");
        }
    }

    protected void connectDatabase() {
        try {
            Class.forName(this.databaseDriver);
            con = DriverManager.getConnection(this.databaseUrl + this.databaseName, this.databaseUsername, this.databasePassword);
        } catch (SQLException e) {
            System.err.println("SQLException: " + e);
            con = null;
        } catch (ClassNotFoundException e) {
            System.err.println("ClassNotFoundException " + e);
        }
    }

    protected void connectDatabase(final String databaseUrl, final String databaseUsername, final String databasePassword) {
        try {
            Class.forName(this.databaseDriver);
            con = DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException e) {
            System.err.println("SQLException: " + e);
            con = null;
        } catch (ClassNotFoundException e) {
            System.err.println("ClassNotFoundException " + e);
        }
    }

    protected void closeConnection() throws SQLException {
        con.close();
    }

    public abstract String getDatabaseFilename();

    public Properties getDatabaseProperties() throws IOException {
        Properties props = new Properties();
        props.load(this.getClass().getClassLoader().getResourceAsStream(getDatabaseFilename()));
        return props;
    }
}