package pt.iscte.dsi.webclient.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class to access Fénix database directly. This implementation is not the best
 * one... I did it this way to take advantage of the <code>BaseDataLoaderFromDatabase</code> logic and speed up the development.
 * 
 * @author António Casqueiro
 */
public final class MappingDataLoaderFromDatabase<T> extends BaseDataLoaderFromDatabase<T> {

    public String getDatabaseFilename() {
        return "configuration.properties";
    }

    public void connectDatabase() {
        super.connectDatabase();
    }

    public void closeConnection() {
        try {
            super.closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
    }

    public boolean executeStatement(final String sql) {
        try {
            stm = con.createStatement();
            stm.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error("Exception: " + e);
            return false;
        } finally {
            try {
                if (stm != null) {
                    stm.close();
                }
            } catch (SQLException e) {
                logger.error("Exception: " + e);
            }
        }

        return true;
    }

    public Boolean checkIfExists(final String sql) {
        ResultSet rs = null;
        try {
            stm = con.createStatement();
            rs = stm.executeQuery(sql);
            if (!rs.next()) {
                return Boolean.FALSE;
            }
        } catch (SQLException e) {
            logger.error("Exception: " + e);
            return null;
        } finally {
            try {
                if (stm != null) {
                    stm.close();
                }
            } catch (SQLException e) {
                logger.error("Exception: " + e);
            }
        }

        return Boolean.TRUE;
    }

    public Integer getCodAreaFromAreaTable(final String sigla_nova) {
        Integer codArea = null;
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT cod_area FROM area WHERE LOWER(sigla_nova) = LOWER('" + sigla_nova + "');");

            if (rs.next()) {
                codArea = rs.getInt(1);
                return codArea;
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return codArea;
    }
}