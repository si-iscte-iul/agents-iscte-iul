package pt.iscte.dsi.webclient.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class Utilizador_iscte extends UnifiedUser {

    private Tipo_user tipo_user;

    private int ext_gabinete;

    private String gabinete;

    private int visibilidade;

    private String home_unix;

    private String aliases;

    private int id_cacifo;

    private String ddi;

    private String n_mecanografico;

    private String fax;

    private boolean modificado;

    private String removido_por;

    private String data_accao;

    private int id_categoria;

    private String keywords;

    private String external_cv_url;

    private String document_id_number;

    private boolean removido;

    private String observacoes;

    private Orgao_iscte orgao_iscte;

    private String password;

    /**
     * @return the aliases
     */
    public String getAliases() {
        return aliases;
    }

    /**
     * @param aliases
     *            the aliases to set
     */
    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    /**
     * @return the external_cv_url
     */
    public String getExternal_cv_url() {
        return external_cv_url;
    }

    /**
     * @param external_cv_url
     *            the external_cv_url to set
     */
    public void setExternal_cv_url(String external_cv_url) {
        this.external_cv_url = external_cv_url;
    }

    /**
     * @return the data_accao
     */
    public String getData_accao() {
        return data_accao;
    }

    /**
     * @param data_accao
     *            the data_accao to set
     */
    public void setData_accao(String data_accao) {
        this.data_accao = data_accao;
    }

    /**
     * @return the ddi
     */
    public String getDdi() {
        return ddi;
    }

    /**
     * @param ddi
     *            the ddi to set
     */
    public void setDdi(String ddi) {
        this.ddi = ddi;
    }

    /**
     * @return the ext_gabinete
     */
    public int getExt_gabinete() {
        return ext_gabinete;
    }

    /**
     * @param ext_gabinete
     *            the ext_gabinete to set
     */
    public void setExt_gabinete(int ext_gabinete) {
        this.ext_gabinete = ext_gabinete;
    }

    /**
     * @return the fax
     */
    public String getFax() {
        return fax;
    }

    /**
     * @param fax
     *            the fax to set
     */
    public void setFax(String fax) {
        this.fax = fax;
    }

    /**
     * @return the gabinete
     */
    public String getGabinete() {
        return gabinete;
    }

    /**
     * @param gabinete
     *            the gabinete to set
     */
    public void setGabinete(String gabinete) {
        this.gabinete = gabinete;
    }

    /**
     * @return the home_unix
     */
    public String getHome_unix() {
        return home_unix;
    }

    /**
     * @param home_unix
     *            the home_unix to set
     */
    public void setHome_unix(String home_unix) {
        this.home_unix = home_unix;
    }

    /**
     * @return the id_cacifo
     */
    public int getId_cacifo() {
        return id_cacifo;
    }

    /**
     * @param id_cacifo
     *            the id_cacifo to set
     */
    public void setId_cacifo(int id_cacifo) {
        this.id_cacifo = id_cacifo;
    }

    /**
     * @return the id_categoria
     */
    public int getId_categoria() {
        return id_categoria;
    }

    /**
     * @param id_categoria
     *            the id_categoria to set
     */
    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }

    /**
     * @return the keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords
     *            the keywords to set
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * @return the modificado
     */
    public boolean isModificado() {
        return modificado;
    }

    /**
     * @param modificado
     *            the modificado to set
     */
    public void setModificado(boolean modificado) {
        this.modificado = modificado;
    }

    /**
     * @return the n_mecanografico
     */
    public String getN_mecanografico() {
        return n_mecanografico;
    }

    /**
     * @param n_mecanografico
     *            the n_mecanografico to set
     */
    public void setN_mecanografico(String n_mecanografico) {
        this.n_mecanografico = n_mecanografico;
    }

    /**
     * @return the orgao_iscte
     */
    public Orgao_iscte getOrgao_iscte() {
        return orgao_iscte;
    }

    /**
     * @param orgao_iscte
     *            the orgao_iscte to set
     */
    public void setOrgao_iscte(Orgao_iscte orgao_iscte) {
        this.orgao_iscte = orgao_iscte;
    }

    /**
     * @return the removido_por
     */
    public String getRemovido_por() {
        return removido_por;
    }

    /**
     * @param removido_por
     *            the removido_por to set
     */
    public void setRemovido_por(String removido_por) {
        this.removido_por = removido_por;
    }

    /**
     * @return the tipo_user
     */
    public Tipo_user getTipo_user() {
        return tipo_user;
    }

    /**
     * @param tipo_user
     *            the tipo_user to set
     */
    public void setTipo_user(Tipo_user tipo_user) {
        this.tipo_user = tipo_user;
    }

    /**
     * @return the visibilidade
     */
    public int getVisibilidade() {
        return visibilidade;
    }

    /**
     * @param visibilidade
     *            the visibilidade to set
     */
    public void setVisibilidade(int visibilidade) {
        this.visibilidade = visibilidade;
    }

    @Override
    public String getBi() {
        return document_id_number;
    }

    @Override
    public void setBi(String bi) {
        this.document_id_number = bi;
    }

    @Override
    public String getObs() {
        return observacoes;
    }

    @Override
    public void setObs(String obs) {
        this.observacoes = obs;
    }

    @Override
    public boolean isActivo() {
        return !removido;
    }

    @Override
    public void setActivo(boolean activo) {
        this.removido = activo;
    }

    @Override
    public boolean fillWithDatabaseRowData(ResultSet rs) throws SQLException {
        final boolean superResult = super.fillWithDatabaseRowData(rs);

        setActivo(rs.getBoolean("removido"));
        setObs(StringUtils.getStringInUtf8Format(rs.getBytes("observacoes")));
        setBi(StringUtils.getStringInUtf8Format(rs.getBytes("n_documento_identificacao")));

        setTipo_user(Tipo_user.getByCode(rs.getInt("tipo_user")));
        setOrgao_iscte(new Orgao_iscte(rs));
        setExt_gabinete(rs.getInt("ext_gabinete"));
        setGabinete(StringUtils.getStringInUtf8Format(rs.getBytes("gabinete")));
        setVisibilidade(rs.getInt("visibilidade"));
        setHome_unix(StringUtils.getStringInUtf8Format(rs.getBytes("home_unix")));
        setAliases(StringUtils.getStringInUtf8Format(rs.getBytes("aliases")));
        setId_cacifo(rs.getInt("id_cacifo"));
        setDdi(StringUtils.getStringInUtf8Format(rs.getBytes("ddi")));
        setN_mecanografico(StringUtils.getStringInUtf8Format(rs.getBytes("n_mecanografico")));
        setFax(StringUtils.getStringInUtf8Format(rs.getBytes("fax")));
        setModificado(rs.getBoolean("modificado"));
        setRemovido_por(StringUtils.getStringInUtf8Format(rs.getBytes("removido_por")));
        setData_accao(StringUtils.getStringInUtf8Format(rs.getBytes("data_accao")));
        setId_categoria(rs.getInt("id_categoria"));
        setKeywords(StringUtils.getStringInUtf8Format(rs.getBytes("keywords")));
        setExternal_cv_url(StringUtils.getStringInUtf8Format(rs.getBytes("external_cv_url")));
        return superResult && true;
    }

    @Override
    public String toString() {
        return super.toString() + ",Tipo=" + getTipo_user() + ",Orgao ISCTE=" + getOrgao_iscte();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDocumentIdNumber() {
        return document_id_number;
    }

    public void setDocumentIdNumber(String document_id_number) {
        this.document_id_number = document_id_number;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public boolean isRemovido() {
        return removido;
    }

    public void setRemovido(boolean removido) {
        this.removido = removido;
    }
}