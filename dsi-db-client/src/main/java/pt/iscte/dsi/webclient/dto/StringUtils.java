package pt.iscte.dsi.webclient.dto;

import java.io.UnsupportedEncodingException;

public class StringUtils {

    public static String getStringInUtf8ToInsert(final byte[] bytes) {
        return getStringInFormat(bytes, "UTF8");
    }

    public static String getStringInUtf8Format(final byte[] bytes) {
        return getStringInFormat(bytes, "ISO-8859-1");
    }

    private static String getStringInFormat(final byte[] bytes, final String charset) {
        if (bytes == null) {
            return null;
        }
        try {
            return new String(bytes, charset);
        } catch (UnsupportedEncodingException e) {
            return new String(bytes);
        }
    }
}