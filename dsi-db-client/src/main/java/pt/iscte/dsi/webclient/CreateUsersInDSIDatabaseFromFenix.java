package pt.iscte.dsi.webclient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import pt.iscte.dsi.webclient.dto.Aluno;
import pt.iscte.dsi.webclient.dto.Utilizador_iscte;
import pt.iscte.dsi.webclient.utils.DataLoaderFromDatabase;

public class CreateUsersInDSIDatabaseFromFenix {
    private static final String ALL_NO_STUDENT_USERS_DSI_QUERY = "SELECT * FROM utilizador_iscte, orgao_iscte "
            + "where utilizador_iscte.orgao_iscte = orgao_iscte.id_orgao_iscte " + "ORDER BY login";

    private static DataLoaderFromDatabase<Object> database = null;

    private List<Utilizador_iscte> allNoStudentUsersFromFenix = new ArrayList<Utilizador_iscte>(0);

    private Collection<Utilizador_iscte> allNoStudentUsersFromDSI = new ArrayList<Utilizador_iscte>(0);

    private List<Utilizador_iscte> noStudentUsersFromFenixToCreateInDSI = new ArrayList<Utilizador_iscte>(0);

    private static final String USERNAME_SUFFIX = "@iscte.pt";

    private List<Aluno> studentUsersFromFenix = new ArrayList<Aluno>(0);

    private Collection<Aluno> studentUsersFromDSI = new ArrayList<Aluno>(0);

    private static final String AT = "@";

    private static int UID = 0;

    private static int REGISTRATION_ID = 0;

    private List<String> errorMessagesRelatedWithContracts = new ArrayList<String>(0);

    private List<String> errorMessagesRelatedWithUserNames = new ArrayList<String>(0);

    private List<String> createdUserMessages = new ArrayList<String>(0);

    private static final String FILE_NAME_PATH = "snotra.iscte.intranet:/home/fenix_scripts_production/data/export/dsi/";

    private static final String FILE_NAME_TYPE = ".xls";

    private static final String FILE_NAME_PREFIX = "docentes-e-funcionarios-criados-na-DSI-pelo-fenix-";

    private static String outputFileName = null;

    static {
        database = new DataLoaderFromDatabase<Object>();
    }
/*
    @Override
    protected void run() throws Exception {
	loadAllUsersFromDSI();
	loadAllPotentialUsersFromFenix();
	findNoStudentUsersToCreateInDSIDatabase();
	createUsersIfNeeded();
	sendLoggingByEmail("Teacher and Employee migration process from Fénix@iscte to DSI database",
		false, getLogging());
	resetDataStructures();
    }

    private void resetDataStructures() {
	allNoStudentUsersFromDSI.clear();
	allNoStudentUsersFromFenix.clear();
	noStudentUsersFromFenixToCreateInDSI.clear();
	studentUsersFromFenix.clear();
	studentUsersFromDSI.clear();
    }

    private void loadAllPotentialUsersFromFenix() {
	loadActiveTeachers();
	loadActiveEmployees();
    }

    private void createUsersIfNeeded() throws Exception {
	int counter = 0;
	final Spreadsheet spreadsheet = new Spreadsheet("Credenciais de utilizadores");
	String processingMessage = null;
	for (Utilizador_iscte utilizadorIscte : noStudentUsersFromFenixToCreateInDSI) {
	    if (!utilizadorIscte.getOrgao_iscte().getSigla().equals("NOT_DEFINED")) {
		createNoStudentUserInDSIDatabase(utilizadorIscte, spreadsheet);
		++counter;
	    } else {
		processingMessage = "Employee with name " + utilizadorIscte.getNome() + "("
			+ utilizadorIscte.getLogin() + ")" + " does not have an associated unit.";
		errorMessagesRelatedWithContracts.add(processingMessage);
	    }
	}

	if (counter > 0) {
	    spreadsheet.exportToXLSSheet(new File(outputFileName));
	}
    }

    private void createNoStudentUserInDSIDatabase(final Utilizador_iscte utilizadorIscte,
	    final Spreadsheet spreadsheet) {
	int i = 0;
	spreadsheet.setHeader(i++, "Login");
	spreadsheet.setHeader(i++, "Nome");
	spreadsheet.setHeader(i++, "Password");
	spreadsheet.setHeader(i++, "E_Mail");
	spreadsheet.setHeader(i++, "Orgao");

	final Row row = spreadsheet.addRow();
	i = 0;

	outputFileName = EXPORT_DIR_PATH + FILE_SEPARATOR + FILE_NAME_PREFIX + getDate()
		+ FILE_NAME_TYPE;

	final List<String> sqlStatements = new ArrayList<String>(1);
	final String login = utilizadorIscte.getLogin().substring(0,
		utilizadorIscte.getLogin().indexOf(AT));

	if (UID == 0) {
	    UID = getUID() + 1;
	} else {
	    UID += 1;
	}

	if (REGISTRATION_ID == 0) {
	    REGISTRATION_ID = database
		    .getLastAttributeValueFromTable("SELECT MAX(id_inscricao) FROM inscricao") + 1;
	} else {
	    REGISTRATION_ID += 1;
	}

	final String insertRegistration = "INSERT INTO inscricao (estado,login_ciiscte,login_utilizador,id_inscricao,hora_inscricao,hora_autorizacao"
		+ ",data_inscricao,data_autorizacao)"
		+ " VALUES (2,'jpps',"
		+ "'"
		+ login
		+ "',"
		+ REGISTRATION_ID
		+ ",'"
		+ getHour()
		+ "','"
		+ getHour()
		+ "','"
		+ getDate()
		+ "','"
		+ getDate() + "');";

	final String insertSystemWorkingArea1 = "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
		+ "('1','1','" + login + "'," + REGISTRATION_ID + ",'" + getDate() + "');";

	final String insertSystemWorkingArea2 = "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
		+ "('8','3','" + login + "'," + REGISTRATION_ID + ",'" + getDate() + "');";

	final String insertSystemWorkingArea3 = "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
		+ "('3','1','" + login + "'," + REGISTRATION_ID + ",'" + getDate() + "');";

	final String insertSystemWorkingArea4 = "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
		+ "('2','1','" + login + "'," + REGISTRATION_ID + ",'" + getDate() + "');";

	final String insertSystemWorkingArea5 = "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
		+ "('1','2','" + login + "'," + REGISTRATION_ID + ",'" + getDate() + "');";

	final String insertNoStudentUser = "INSERT INTO utilizador_iscte (login,nome,tipo_user,orgao_iscte,ext_gabinete,telefone,"
		+ "gabinete,telemovel,visibilidade,e_mail_pessoal,password,uid,"
		+ "home_unix,e_mail_iscte,observacoes,id_cacifo,n_mecanografico,removido,"
		+ "n_documento_identificacao,external_cv_url) VALUES ("
		+ "'"
		+ login
		+ "',"
		+ "'"
		+ Utilities.convertUTFEncodingToISO88591(utilizadorIscte.getNome())
		+ "',"
		+ (utilizadorIscte.getTipo_user().equals(Tipo_user.FUNCIONARIO) ? 2 : 1)
		+ ","
		+ (utilizadorIscte.getOrgao_iscte().getSigla() != null ? Utilities
			.getDSIUnitID(utilizadorIscte.getOrgao_iscte().getSigla()) : 999)
		+ ","
		+ "999999,"
		+ (utilizadorIscte.getTelefone() == null ? 999999999 : utilizadorIscte.getTelefone())
		+ ","
		+ "'00.00',"
		+ (utilizadorIscte.getTelemovel() == null ? 999999999 : utilizadorIscte.getTelemovel())
		+ ",1,"
		+ "'"
		+ (utilizadorIscte.getE_mail_pessoal() == null ? "" : utilizadorIscte
			.getE_mail_pessoal())
		+ "',"
		+ "'"
		+ utilizadorIscte.getPassword()
		+ "',"
		+ UID
		+ ","
		+ "'/home/"
		+ login
		+ "',"
		+ "'"
		+ (utilizadorIscte.getE_mail_iscte() == null ? login + USERNAME_SUFFIX : utilizadorIscte
			.getE_mail_iscte())
		+ "',"
		+ "'Imported from Fenix - "
		+ getDate()
		+ "',427,'"
		+ (utilizadorIscte.getN_mecanografico() == null ? "" : utilizadorIscte
			.getN_mecanografico())
		+ "',"
		+ "'f',"
		+ "'"
		+ (utilizadorIscte.getDocumentIdNumber() == null ? "" : utilizadorIscte
			.getDocumentIdNumber())
		+ "',"
		+ "'"
		+ (utilizadorIscte.getExternal_cv_url() == null ? "" : utilizadorIscte
			.getExternal_cv_url()) + "');\n";

	sqlStatements.add(insertNoStudentUser);
	sqlStatements.add(insertRegistration);
	sqlStatements.add(insertSystemWorkingArea1);
	sqlStatements.add(insertSystemWorkingArea2);
	sqlStatements.add(insertSystemWorkingArea3);
	sqlStatements.add(insertSystemWorkingArea4);
	sqlStatements.add(insertSystemWorkingArea5);
	database.executeUpdate(sqlStatements);

	row.setCell(i++, utilizadorIscte.getLogin());
	row.setCell(i++, utilizadorIscte.getNome());
	row.setCell(i++, utilizadorIscte.getPassword());
	row.setCell(i++, (utilizadorIscte.getE_mail_iscte() == null ? login + USERNAME_SUFFIX
		: utilizadorIscte.getE_mail_iscte()));
	row.setCell(i++, utilizadorIscte.getOrgao_iscte().getDesignacao());
	createdUserMessages.add("User created: " + utilizadorIscte.getNome() + ", "
		+ utilizadorIscte.getLogin());
    }

    private String getLogging() {
	String loggingMessage = "Teacher and Employee migration process from Fenix@iscte to DSI database at "
		+ getDate() + " - " + getHour() + "\n\n" + "Problems related with usernames:\n";
	for (String message : errorMessagesRelatedWithUserNames) {
	    loggingMessage += message + "\n";
	}

	loggingMessage += "\n\nProblems related with contracts:\n";
	for (String message : errorMessagesRelatedWithContracts) {
	    loggingMessage += message + "\n";
	}

	loggingMessage += "\n\nOther messages:\n";
	for (String message : processErrorMessages) {
	    loggingMessage += message + "\n";
	}

	loggingMessage += "\n\nUsers created in DSI database:\n";
	if (createdUserMessages.size() > 0) {
	    for (String message : createdUserMessages) {
		loggingMessage += message + "\n";
	    }
	    loggingMessage += "\n\nFile with teacher and employee information to process credentials: "
		    + FILE_NAME_PATH + outputFileName;
	}

	return loggingMessage;
    }

    private void findNoStudentUsersToCreateInDSIDatabase() {
	boolean userAlreadyExistsInDSI;
	for (Utilizador_iscte utilizadorIscteFromFenix : allNoStudentUsersFromFenix) {
	    userAlreadyExistsInDSI = false;
	    for (Utilizador_iscte utilizadorIscteFromDSI : allNoStudentUsersFromDSI) {
		if (utilizadorIscteFromFenix.getLogin().equals(utilizadorIscteFromDSI.getLogin())) {
		    userAlreadyExistsInDSI = true;
		}

		if (utilizadorIscteFromFenix.getE_mail_iscte() != null
			&& utilizadorIscteFromDSI.getE_mail_iscte() != null) {
		    if (utilizadorIscteFromFenix.getE_mail_iscte().toLowerCase().equals(
			    utilizadorIscteFromDSI.getE_mail_iscte().toLowerCase())) {
			userAlreadyExistsInDSI = true;
		    }
		}

		if (utilizadorIscteFromFenix.getN_mecanografico() != null
			&& utilizadorIscteFromDSI.getN_mecanografico() != null) {
		    if (utilizadorIscteFromFenix.getN_mecanografico().equals(
			    utilizadorIscteFromDSI.getN_mecanografico())) {
			userAlreadyExistsInDSI = true;
		    }
		}

		if (userAlreadyExistsInDSI) {
		    break;
		}
	    }

	    if (!userAlreadyExistsInDSI) {
		noStudentUsersFromFenixToCreateInDSI.add(utilizadorIscteFromFenix);
	    }
	}
    }

    private void loadActiveEmployees() {
	String processingMessage = null;
	for (Employee employee : rootDomainObject.getEmployees()) {
	    if (employee.getActive()) {

		final Utilizador_iscte utilizadorIscte = new Utilizador_iscte();
		final Orgao_iscte orgaoIscte = new Orgao_iscte();
		final String userPrincipalName = employee.getPerson().getIstUsername();

		if (userPrincipalName != null
			&& !employee.getPerson().getIstUsername().toLowerCase().matches("pt[0-9]*")
			&& !employee.getPerson().getIstUsername().toLowerCase().matches(".*[0-9]+.*")) {

		    utilizadorIscte.setLogin(employee.getPerson().getIstUsername());
		    utilizadorIscte.setNome(employee.getPerson().getName());
		    utilizadorIscte.setN_mecanografico(String.valueOf(employee.getEmployeeNumber()));
		    utilizadorIscte.setTipo_user(Tipo_user.FUNCIONARIO);

		    final Contract contract = getCurrentWorkingContract(employee);
		    if (contract != null && contract.getUnit() != null) {
			orgaoIscte.setSigla(contract.getUnit().getAcronym());
			orgaoIscte.setDesignacao(contract.getUnit().getName());
		    } else {
			orgaoIscte.setSigla("NOT_DEFINED");
		    }

		    utilizadorIscte.setOrgao_iscte(orgaoIscte);

		    utilizadorIscte.setTelefone(employee.getPerson().getPhone());
		    utilizadorIscte.setTelemovel(employee.getPerson().getMobile());
		    utilizadorIscte.setE_mail_pessoal(employee.getPerson().getPersonalEmail());
		    utilizadorIscte.setPassword(GeneratePassword.getInstance().generatePassword(
			    employee.getPerson()));

		    String instututionalEmail = employee.getPerson().getInstitutionalEmail();

		    if (instututionalEmail == null) {
			instututionalEmail = InstitutionalEMail.getInstitutionalEmail(employee
				.getPerson());
		    }

		    utilizadorIscte.setE_mail_iscte(instututionalEmail);

		    utilizadorIscte.setDocumentIdNumber(employee.getPerson().getDocumentIdNumber());
		    utilizadorIscte.setExternal_cv_url(employee.getPerson().getExternalCurriculumUrl());

		    allNoStudentUsersFromFenix.add(utilizadorIscte);
		} else if (userPrincipalName == null) {
		    processingMessage = "Employee with name " + employee.getPerson().getName()
			    + " does not have an username.";
		    errorMessagesRelatedWithUserNames.add(processingMessage);
		} else if (employee.getPerson().getIstUsername().toLowerCase().matches("pt[0-9]*")) {
		    processingMessage = "Username of employee with name "
			    + employee.getPerson().getName() + " ("
			    + employee.getPerson().getIstUsername() + ")" + " starts with \"pt\".";
		    errorMessagesRelatedWithUserNames.add(processingMessage);
		} else if (employee.getPerson().getIstUsername().toLowerCase().matches(".*[0-9]+.*")) {
		    processingMessage = "Username of employee with name "
			    + employee.getPerson().getName() + " ("
			    + employee.getPerson().getIstUsername() + ")"
			    + " appears to be a student user name.";
		    errorMessagesRelatedWithUserNames.add(processingMessage);
		} else if (employee.getCurrentWorkingContract() == null) {
		    processingMessage = "Employee with name " + employee.getPerson().getName() + " ("
			    + employee.getPerson().getIstUsername() + ")"
			    + " does not have an associated unit.";
		    errorMessagesRelatedWithContracts.add(processingMessage);
		}
	    }
	}
    }

    private void loadActiveTeachers() {
	String processingMessage = null;
	for (Teacher teacher : rootDomainObject.getTeachers()) {
	    if (!teacher.isDeceased() && !teacher.isInactive(ExecutionYear.readCurrentExecutionYear())) {

		if (!teacher.getPerson().getIstUsername().toLowerCase().startsWith("pt")
			&& !teacher.getPerson().getIstUsername().toLowerCase().matches(".*[0-9].*")
			&& teacher.getCurrentWorkingDepartment() != null) {
		    final Utilizador_iscte utilizadorIscte = new Utilizador_iscte();
		    final Orgao_iscte orgaoIscte = new Orgao_iscte();

		    utilizadorIscte.setLogin(teacher.getPerson().getIstUsername());
		    utilizadorIscte.setNome(Utilities.convertUTFEncodingToISO88591(teacher.getPerson()
			    .getName()));
		    utilizadorIscte.setN_mecanografico(String.valueOf(teacher.getTeacherNumber()));
		    utilizadorIscte.setTipo_user(Tipo_user.DOCENTE);

		    if (teacher.getCurrentWorkingUnit() != null) {
			orgaoIscte.setSigla(teacher.getCurrentWorkingUnit().getAcronym());
			orgaoIscte.setDesignacao(teacher.getCurrentWorkingUnit().getName());
		    } else {
			orgaoIscte.setSigla("NOT_DEFINED");
		    }

		    utilizadorIscte.setOrgao_iscte(orgaoIscte);

		    utilizadorIscte.setTelefone(teacher.getPerson().getPhone());
		    utilizadorIscte.setTelemovel(teacher.getPerson().getMobile());
		    utilizadorIscte.setE_mail_pessoal(teacher.getPerson().getPersonalEmail());
		    utilizadorIscte.setPassword(GeneratePassword.getInstance().generatePassword(
			    teacher.getPerson()));

		    String instututionalEmail = teacher.getPerson().getInstitutionalEmail();

		    if (instututionalEmail == null) {
			instututionalEmail = InstitutionalEMail.getInstitutionalEmail(teacher
				.getPerson());
		    }

		    utilizadorIscte.setE_mail_iscte(instututionalEmail);

		    utilizadorIscte.setDocumentIdNumber(teacher.getPerson().getDocumentIdNumber());
		    utilizadorIscte.setDocumentIdNumber(teacher.getPerson().getExternalCurriculumUrl());

		    allNoStudentUsersFromFenix.add(utilizadorIscte);
		} else if (teacher.getPerson().getIstUsername().toLowerCase().startsWith("pt")) {
		    processingMessage = "Username of teacher with name " + teacher.getPerson().getName()
			    + " (" + teacher.getPerson().getIstUsername() + ")" + " starts with \"pt\".";
		    errorMessagesRelatedWithUserNames.add(processingMessage);
		} else if (teacher.getPerson().getIstUsername().toLowerCase().matches(".*[0-9].*")) {
		    processingMessage = "Username of teacher with name " + teacher.getPerson().getName()
			    + " (" + teacher.getPerson().getIstUsername() + ")"
			    + " appears to be a student username.";
		    errorMessagesRelatedWithUserNames.add(processingMessage);
		} else if (teacher.getCurrentWorkingDepartment() == null) {
		    processingMessage = "Teacher with name " + teacher.getPerson().getName() + " ("
			    + teacher.getPerson().getIstUsername() + ")"
			    + " does not have an associated unit.";
		    errorMessagesRelatedWithContracts.add(processingMessage);
		}
	    }
	}
    }

    public Contract getCurrentWorkingContract(final Employee employee) {
	YearMonthDay current = new YearMonthDay();
	for (final Contract accountability : employee.getWorkingContracts()) {
	    if (isActive(accountability, current)) {
		return accountability;
	    }
	}
	return null;
    }

    public boolean belongsToPeriod(final Contract contract, final YearMonthDay begin) {
	return ((contract.getBeginDate().isAfter(begin) || contract.getBeginDate().equals(begin)) && (contract
		.getEndDate() == null || contract.getEndDate().isAfter(begin)));
    }

    public boolean isActive(final Contract contract, final YearMonthDay currentDate) {
	return belongsToPeriod(contract, currentDate);
    }

    private int getUID() {
	int userUid = 0;
	final int maxUidFromISCTEUser = database
		.getLastAttributeValueFromTable("SELECT max(uid) FROM utilizador_iscte WHERE uid < 50000;");
	final int maxUidFromStudentUser = database
		.getLastAttributeValueFromTable("SELECT max(uid) from aluno WHERE uid < 50000;");

	if (maxUidFromISCTEUser >= maxUidFromISCTEUser)
	    userUid = maxUidFromISCTEUser;

	if (maxUidFromISCTEUser < maxUidFromISCTEUser)
	    userUid = maxUidFromStudentUser;

	return userUid;
    }

    private void loadAllUsersFromDSI() {
	allNoStudentUsersFromDSI = new DataLoaderFromDatabase<Utilizador_iscte>().load(
		Utilizador_iscte.class, ALL_NO_STUDENT_USERS_DSI_QUERY);
    }

    public static void main(String[] args) {
	processWriteTransaction(new CreateUsersInDSIDatabaseFromFenix());
    }
*/
}