package pt.iscte.dsi.webclient;

import java.net.MalformedURLException;
import com.sun.xml.ws.developer.JAXWSProperties;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import pt.iscte.agents.dsiDb.DsiNotStudentDataService;
import pt.iscte.agents.dsiDb.DsiNotStudentDataService_Service;
import pt.iscte.dsi.webclient.utils.fenixLike.PropertiesManager;

public class DsiNotStudentDataServiceUtil {

    private static final String DSI_NOT_STUDENT_DATA_SERVICE_WSDL_LOCATION = "DsiNotStudentIntegration.ws.client.wsdl.location";
    private static final String DSI_NOT_STUDENT_DATA_SERVICE_ADDRESS = "DsiNotStudentIntegration.ws.client.address";
    private static final String DSI_NOT_STUDENT_DATA_SERVICE_CONNECT_TIMEOUT =
            "DsiNotStudentIntegration.ws.client.request.timeout";
    private static final String DSI_NOT_STUDENT_DATA_SERVICE_REQUEST_TIMEOUT = "DsiNotStudentIntegration.ws.connect.timeout";

    private final static Logger logger = Logger.getLogger(DsiNotStudentDataServiceUtil.class);
    private static URL wsdlLocation = null;

    public static DsiNotStudentDataService getService() throws RemoteException {
        URL wsdlLocation = getWsdlLocation();
        DsiNotStudentDataService_Service service = new DsiNotStudentDataService_Service(wsdlLocation);
        DsiNotStudentDataService webService = service.getPort(DsiNotStudentDataService.class);
        Map<String, Object> context = ((BindingProvider) webService).getRequestContext();
        String address = getProperty(DSI_NOT_STUDENT_DATA_SERVICE_ADDRESS);
        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, address);
        String connectTimeoutStr = getProperty(DSI_NOT_STUDENT_DATA_SERVICE_CONNECT_TIMEOUT);
        Integer connectTimeout = new Integer(connectTimeoutStr) * 1000;
        context.put(JAXWSProperties.CONNECT_TIMEOUT, connectTimeout);
        String requestTimeoutStr = getProperty(DSI_NOT_STUDENT_DATA_SERVICE_REQUEST_TIMEOUT);
        Integer requestTimeout = new Integer(requestTimeoutStr) * 1000;
        context.put(JAXWSProperties.REQUEST_TIMEOUT, requestTimeout);
        return webService;
    }

    private static URL getWsdlLocation() throws RemoteException {
        if (wsdlLocation == null) {
            String wsdlLocationStr = getProperty(DSI_NOT_STUDENT_DATA_SERVICE_WSDL_LOCATION);
            try {
                wsdlLocation = new URL(wsdlLocationStr);
            } catch (MalformedURLException e) {
                logger.error("Wrong wsdl location for dsi not student data service ws client.", e);
                e.printStackTrace();
                throw new RemoteException("Dsi not student data service web services is down", e);
            }
        }
        return wsdlLocation;
    }

    private static String getProperty(String propertyName) {
        String propertyValue = PropertiesManager.getProperty(propertyName);
        return propertyValue;
    }

}
