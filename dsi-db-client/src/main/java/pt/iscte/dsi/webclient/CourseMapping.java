package pt.iscte.dsi.webclient;

import java.util.Hashtable;
import java.util.Map;

public class CourseMapping {

    private static Map<String, String> mapeamentoCursos = new Hashtable<String, String>();

    static {
        loadMapeamentoCursos();
    }

    private static void loadMapeamentoCursos() {
        //Licenciaturas
        mapeamentoCursos.put("016", "701");
        mapeamentoCursos.put("022", "702");
        mapeamentoCursos.put("156", "703");
        mapeamentoCursos.put("249", "704");
        mapeamentoCursos.put("292", "715");
        mapeamentoCursos.put("416", "711");
        mapeamentoCursos.put("605", "711");
        mapeamentoCursos.put("433", "706");
        mapeamentoCursos.put("455", "707");
        mapeamentoCursos.put("467", "708");
        mapeamentoCursos.put("562", "710");
        mapeamentoCursos.put("492", "709");
        mapeamentoCursos.put("654", "705");
        mapeamentoCursos.put("697", "712");
        mapeamentoCursos.put("759", "713");
        mapeamentoCursos.put("763", "714");
        mapeamentoCursos.put("9098", "704");
        mapeamentoCursos.put("9019", "915");
        mapeamentoCursos.put("9167", "706");
        mapeamentoCursos.put("0022", "702");
        mapeamentoCursos.put("9240", "713");
        mapeamentoCursos.put("9147", "711");
        mapeamentoCursos.put("9157", "707");
        mapeamentoCursos.put("9219", "712");
        mapeamentoCursos.put("9119", "715");
        mapeamentoCursos.put("9140", "705");
        mapeamentoCursos.put("9241", "714");
        mapeamentoCursos.put("8029", "916");
        mapeamentoCursos.put("9081", "703");
        mapeamentoCursos.put("9181", "708");
        mapeamentoCursos.put("9448", "701");
        mapeamentoCursos.put("9205", "710");
        mapeamentoCursos.put("9189", "709");
        mapeamentoCursos.put("703", "949");
        mapeamentoCursos.put("9257", "702");

        //Outros graus
        mapeamentoCursos.put("01", "801");
        mapeamentoCursos.put("02", "802");
        mapeamentoCursos.put("002", "904");
        mapeamentoCursos.put("005", "905");
        mapeamentoCursos.put("006", "830");
        mapeamentoCursos.put("007", "906");
        mapeamentoCursos.put("008", "908");
        mapeamentoCursos.put("009", "911");
        mapeamentoCursos.put("04", "804");
        mapeamentoCursos.put("07", "807");
        mapeamentoCursos.put("11", "811");
        mapeamentoCursos.put("12", "812");
        mapeamentoCursos.put("13", "813");
        mapeamentoCursos.put("15", "815");
        mapeamentoCursos.put("16", "816");
        mapeamentoCursos.put("20", "820");
        mapeamentoCursos.put("21", "821");
        mapeamentoCursos.put("23", "823");
        mapeamentoCursos.put("24", "825");
        mapeamentoCursos.put("25", "825");
        mapeamentoCursos.put("26", "826");
        mapeamentoCursos.put("27", "827");
        mapeamentoCursos.put("30", "830");
        mapeamentoCursos.put("34", "834");
        mapeamentoCursos.put("38", "838");
        mapeamentoCursos.put("40", "840");
        mapeamentoCursos.put("40", "841");
        mapeamentoCursos.put("42", "842");
        mapeamentoCursos.put("43", "843");
        mapeamentoCursos.put("45", "845");
        mapeamentoCursos.put("46", "846");
        mapeamentoCursos.put("47", "847");
        mapeamentoCursos.put("49", "849");
        mapeamentoCursos.put("51", "883");
        mapeamentoCursos.put("52", "852");
        mapeamentoCursos.put("54", "854");
        mapeamentoCursos.put("55", "855");
        mapeamentoCursos.put("57", "857");
        mapeamentoCursos.put("58", "887");
        mapeamentoCursos.put("60", "860");
        mapeamentoCursos.put("62", "816");
        mapeamentoCursos.put("63C", "863");
        mapeamentoCursos.put("65", "895");
        mapeamentoCursos.put("66", "921");
        mapeamentoCursos.put("67", "901");
        mapeamentoCursos.put("68", "894");
        mapeamentoCursos.put("71", "930");
        mapeamentoCursos.put("72", "865");
        mapeamentoCursos.put("73", "891");
        mapeamentoCursos.put("76", "869");
        mapeamentoCursos.put("77", "931");
        mapeamentoCursos.put("1D", "873");
        mapeamentoCursos.put("2D", "871");
        mapeamentoCursos.put("4D", "872");
        mapeamentoCursos.put("4D1", "872");
        mapeamentoCursos.put("4D2", "872");
        mapeamentoCursos.put("4D3", "872");
        mapeamentoCursos.put("4D4", "872");
        mapeamentoCursos.put("4D5", "872");
        mapeamentoCursos.put("4D6", "872");
        mapeamentoCursos.put("405", "907");
        mapeamentoCursos.put("101", "926");
        mapeamentoCursos.put("103", "843");
        mapeamentoCursos.put("104", "917");
        mapeamentoCursos.put("105", "918");
        mapeamentoCursos.put("106", "922");
        mapeamentoCursos.put("107", "927");
        mapeamentoCursos.put("109", "919");
        mapeamentoCursos.put("110", "953");
        mapeamentoCursos.put("112", "914");
        mapeamentoCursos.put("113", "928");
        mapeamentoCursos.put("114", "924");
        mapeamentoCursos.put("4", "923");
        mapeamentoCursos.put("411", "929");
        mapeamentoCursos.put("012", "932");
        mapeamentoCursos.put("412", "933");
        mapeamentoCursos.put("PG007", "934");
        mapeamentoCursos.put("PG20", "935");
        mapeamentoCursos.put("PG001", "936");
        mapeamentoCursos.put("PG002", "937");
        mapeamentoCursos.put("PG003", "938");
        mapeamentoCursos.put("PG004", "939");
        mapeamentoCursos.put("PG006", "940");
        mapeamentoCursos.put("PG008", "941");
        mapeamentoCursos.put("PG009", "942");
        mapeamentoCursos.put("PG010", "943");
        mapeamentoCursos.put("PG013", "954");
        mapeamentoCursos.put("PG014", "944");
        mapeamentoCursos.put("PG016", "945");
        mapeamentoCursos.put("PG017", "948");
        mapeamentoCursos.put("PG018", "941");
        mapeamentoCursos.put("PG019", "947");
        mapeamentoCursos.put("X03", "946");
        mapeamentoCursos.put("M106", "922");
        mapeamentoCursos.put("001", "949");
        mapeamentoCursos.put("PG005", "950");
        mapeamentoCursos.put("703", "949");
        mapeamentoCursos.put("406", "951");
        mapeamentoCursos.put("W002", "955");
        mapeamentoCursos.put("BX006", "956");
        mapeamentoCursos.put("X005", "957");
        mapeamentoCursos.put("75", "892");
        mapeamentoCursos.put("7", "807");
        mapeamentoCursos.put("B109", "919");
        mapeamentoCursos.put("09", "809");
        mapeamentoCursos.put("32", "832");
        mapeamentoCursos.put("DFA002", "960");
        mapeamentoCursos.put("BX004", "961");
        mapeamentoCursos.put("B7", "807");
        mapeamentoCursos.put("BPG009", "942");
        mapeamentoCursos.put("B25", "825");
        mapeamentoCursos.put("B1D", "873");
        mapeamentoCursos.put("B009", "911");
        mapeamentoCursos.put("B01", "801");
        mapeamentoCursos.put("BPG013", "954");
        mapeamentoCursos.put("B411", "929");
        mapeamentoCursos.put("B30", "830");
        mapeamentoCursos.put("B16", "816");
        mapeamentoCursos.put("BPG018", "941");
        mapeamentoCursos.put("B42", "842");
        mapeamentoCursos.put("B4", "923");
        mapeamentoCursos.put("B23", "823");

        mapeamentoCursos.put("BPG019", "947");
        mapeamentoCursos.put("B001", "949");
        mapeamentoCursos.put("B005", "905");
        mapeamentoCursos.put("B006", "830");
        mapeamentoCursos.put("B012", "932");
        mapeamentoCursos.put("B101", "926");
        mapeamentoCursos.put("B105", "918");
        mapeamentoCursos.put("B106", "922");
        mapeamentoCursos.put("B107", "927");
        mapeamentoCursos.put("B112", "914");
        mapeamentoCursos.put("B113", "928");
        mapeamentoCursos.put("9927", "708");
        mapeamentoCursos.put("BPG012", "932");

        mapeamentoCursos.put("B115", "963");
        mapeamentoCursos.put("B124", "964");
        mapeamentoCursos.put("B119", "965");
        mapeamentoCursos.put("B188", "966");
        mapeamentoCursos.put("B117", "967");

        mapeamentoCursos.put("CET002", "968");
        mapeamentoCursos.put("B702", "969");
        mapeamentoCursos.put("B121", "970");

        mapeamentoCursos.put("6800", "6800");
        mapeamentoCursos.put("1D4", "962");
        mapeamentoCursos.put("W001", "971");
        mapeamentoCursos.put("B471", "972");

        mapeamentoCursos.put("05", "805");
        mapeamentoCursos.put("06", "973");
        mapeamentoCursos.put("64", "975");
        mapeamentoCursos.put("74", "976");
        mapeamentoCursos.put("413", "974");

        mapeamentoCursos.put("DFA001", "977");
        mapeamentoCursos.put("BDFA02", "978");

        mapeamentoCursos.put("3D", "979");
        mapeamentoCursos.put("1CE", "980");

        mapeamentoCursos.put("BX007", "981");

        mapeamentoCursos.put("CET001", "958");

        mapeamentoCursos.put("08", "808");
        mapeamentoCursos.put("18", "983");
        mapeamentoCursos.put("22", "881");
        mapeamentoCursos.put("31", "831");
        mapeamentoCursos.put("37", "837");
        mapeamentoCursos.put("41", "841");
        mapeamentoCursos.put("078", "889");
        mapeamentoCursos.put("410", "962");
        mapeamentoCursos.put("10", "810");
        mapeamentoCursos.put("19", "819");

        mapeamentoCursos.put("1PG", "882");
        mapeamentoCursos.put("408", "408");
        mapeamentoCursos.put("BPG024", "993");
        mapeamentoCursos.put("5601", "5601");

        mapeamentoCursos.put("BX008", "992");

        mapeamentoCursos.put("5562", "994");
        mapeamentoCursos.put("5563", "995");
        mapeamentoCursos.put("5564", "996");
        mapeamentoCursos.put("5602", "997");
        mapeamentoCursos.put("5624", "998");
        mapeamentoCursos.put("9341", "999");

        mapeamentoCursos.put("BS001", "1000");
        mapeamentoCursos.put("409", "1001");

        mapeamentoCursos.put("B122", "1002");
        mapeamentoCursos.put("013", "1003");
        mapeamentoCursos.put("414", "952");

        mapeamentoCursos.put("8365", "1004");  // ETI - PL
        mapeamentoCursos.put("8366", "1005");  // IGE - PL
        mapeamentoCursos.put("9156", "1006");  // Gestão de Marketing

        mapeamentoCursos.put("034", "1007");
        mapeamentoCursos.put("037", "1008");
        mapeamentoCursos.put("6069", "1009");
        mapeamentoCursos.put("B120", "1010");
        mapeamentoCursos.put("415", "1011");

        mapeamentoCursos.put("03", "803");
        mapeamentoCursos.put("14", "814");
        mapeamentoCursos.put("28", "828");
        mapeamentoCursos.put("17", "970");
        mapeamentoCursos.put("48", "848");
        mapeamentoCursos.put("56", "856");
    }

    // ajsco: 2009-10-11
    // I started using "sigla_nova" column from "area" table to identify the degree.
    // However some degrees in the DSI database do not honor this rule...
    // So this class stays as a fallback mapping
    public static String getCodigoCursoDSI(String key) {
        return mapeamentoCursos.get(key);
    }

}