package pt.iscte.dsi.webclient.dto;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public enum Tipo_orgao {

    DEPARTAMENTO(1),

    SERVICO(2),

    CENTRO_INVESTIGACAO(3),

    ASSOCIACAO(4),

    PROJECTO(5),

    DIVERSOS(6),

    INSTITUICAO(0);

    private int code;

    private Tipo_orgao(final int code) {
        this.code = code;
    }

    public static Tipo_orgao getByCode(final int code) {
        for (final Tipo_orgao value : Tipo_orgao.values()) {
            if (value.code == code) {
                return value;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        switch (this) {
        case DEPARTAMENTO:
            return "Departamento";
        case SERVICO:
            return "Serviço";
        case CENTRO_INVESTIGACAO:
            return "Centro de Investigação";
        case ASSOCIACAO:
            return "Associação";
        case PROJECTO:
            return "Projecto";
        case DIVERSOS:
            return "Diversos";
        case INSTITUICAO:
            return "Instituição";
        }
        return "UNKNOWN";
    }
}