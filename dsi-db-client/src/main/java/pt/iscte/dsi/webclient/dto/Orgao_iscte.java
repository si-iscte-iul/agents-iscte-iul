package pt.iscte.dsi.webclient.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public class Orgao_iscte {

    private String designacao;

    private String gid;

    private Tipo_orgao tipo_orgao;

    private String sigla;

    private int id_orgao_iscte;

    private String e_mail_iscte;

    private boolean activo;

    private String designacao_gare;

    private String sigla_oficial;

    private String grupo_oficial;

    private String dominio_oficial;

    private String designacao_oficial;

    public Orgao_iscte() {
    }

    Orgao_iscte(final ResultSet rs) throws SQLException {
        setDesignacao(StringUtils.getStringInUtf8Format(rs.getBytes("designacao")));
        setGid(StringUtils.getStringInUtf8Format(rs.getBytes("gid")));
        setTipo_orgao(Tipo_orgao.getByCode(rs.getInt("id_tipo_orgao")));
        setSigla(StringUtils.getStringInUtf8Format(rs.getBytes("sigla")));
        setId_orgao_iscte(rs.getInt("id_orgao_iscte"));
        setE_mail_iscte(StringUtils.getStringInUtf8Format(rs.getBytes("e_mail_iscte")));
        setActivo(rs.getBoolean("activo"));
        setDesignacao_gare(StringUtils.getStringInUtf8Format(rs.getBytes("designacao_gare")));
        setSigla_oficial(StringUtils.getStringInUtf8Format(rs.getBytes("sigla_oficial")));
        setGrupo_oficial(StringUtils.getStringInUtf8Format(rs.getBytes("grupo_oficial")));
        setDominio_oficial(StringUtils.getStringInUtf8Format(rs.getBytes("dominio_oficial")));
        setDesignacao_oficial(StringUtils.getStringInUtf8Format(rs.getBytes("designacao_oficial")));
    }

    /**
     * @return the activo
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * @param activo
     *            the activo to set
     */
    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    /**
     * @return the designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * @param designacao
     *            the designacao to set
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * @return the designacao_gare
     */
    public String getDesignacao_gare() {
        return designacao_gare;
    }

    /**
     * @param designacao_gare
     *            the designacao_gare to set
     */
    public void setDesignacao_gare(String designacao_gare) {
        this.designacao_gare = designacao_gare;
    }

    /**
     * @return the designacao_oficial
     */
    public String getDesignacao_oficial() {
        return designacao_oficial;
    }

    /**
     * @param designacao_oficial
     *            the designacao_oficial to set
     */
    public void setDesignacao_oficial(String designacao_oficial) {
        this.designacao_oficial = designacao_oficial;
    }

    /**
     * @return the dominio_oficial
     */
    public String getDominio_oficial() {
        return dominio_oficial;
    }

    /**
     * @param dominio_oficial
     *            the dominio_oficial to set
     */
    public void setDominio_oficial(String dominio_oficial) {
        this.dominio_oficial = dominio_oficial;
    }

    /**
     * @return the e_mail_iscte
     */
    public String getE_mail_iscte() {
        return e_mail_iscte;
    }

    /**
     * @param e_mail_iscte
     *            the e_mail_iscte to set
     */
    public void setE_mail_iscte(String e_mail_iscte) {
        this.e_mail_iscte = e_mail_iscte;
    }

    /**
     * @return the gid
     */
    public String getGid() {
        return gid;
    }

    /**
     * @param gid
     *            the gid to set
     */
    public void setGid(String gid) {
        this.gid = gid;
    }

    /**
     * @return the grupo_oficial
     */
    public String getGrupo_oficial() {
        return grupo_oficial;
    }

    /**
     * @param grupo_oficial
     *            the grupo_oficial to set
     */
    public void setGrupo_oficial(String grupo_oficial) {
        this.grupo_oficial = grupo_oficial;
    }

    /**
     * @return the id_orgao_iscte
     */
    public int getId_orgao_iscte() {
        return id_orgao_iscte;
    }

    /**
     * @param id_orgao_iscte
     *            the id_orgao_iscte to set
     */
    public void setId_orgao_iscte(int id_orgao_iscte) {
        this.id_orgao_iscte = id_orgao_iscte;
    }

    /**
     * @return the tipo_orgao
     */
    public Tipo_orgao getTipo_orgao() {
        return tipo_orgao;
    }

    /**
     * @param tipo_orgao
     *            the tipo_orgao to set
     */
    public void setTipo_orgao(Tipo_orgao tipo_orgao) {
        this.tipo_orgao = tipo_orgao;
    }

    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla
     *            the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    /**
     * @return the sigla_oficial
     */
    public String getSigla_oficial() {
        return sigla_oficial;
    }

    /**
     * @param sigla_oficial
     *            the sigla_oficial to set
     */
    public void setSigla_oficial(String sigla_oficial) {
        this.sigla_oficial = sigla_oficial;
    }

    @Override
    public String toString() {
        return "ORGAO: Designacação oficial=" + getDesignacao_oficial() + ",Tipo" + getTipo_orgao();
    }
}