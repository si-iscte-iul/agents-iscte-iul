package pt.iscte.dsi.webclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import pt.iscte.agents.dsiDb.DsiNotStudentDataService;
import pt.iscte.agents.dsiDb.DsiNotStudentDataService_Service;
import pt.iscte.agents.dsiDb.NotStudentData;
import pt.iscte.dsi.webclient.dto.Orgao_iscte;
import pt.iscte.dsi.webclient.dto.Tipo_user;
import pt.iscte.dsi.webclient.dto.Utilizador_iscte;
import pt.iscte.dsi.webclient.utils.DataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.Utilizador_iscteDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.fenixLike.LogSender;
import pt.iscte.dsi.webclient.utils.fenixLike.Pair;
import pt.iscte.dsi.webclient.utils.fenixLike.PropertiesManager;

/**
 * Imports not students from the fenix system to the dsi system It fills the
 * utilizador_iscte, area_trabalho and the inscricao tables. Data to fill is
 * gotten from the NotStudentsData webservice and is put in a Utilizador_iscte
 * object. From there it is later put into the tables. If the iscte user already
 * exists in the dsi database and the PROCESS_UPDATES constant is set to true it
 * will update all values of the iscte users. Otherwise only new users are
 * imported.
 * 
 * @author Benjamin Sick
 * 
 */
public class ImportNewNotStudents {

    private final static Logger logger = Logger.getLogger(ImportNewNotStudents.class);

    // The process updates value is hardcoded because no updates
    // should be performed, until explicitly being told!
    //
    // should the script only create new records or also alter data which
    // already exists
    // ATTENTION: If set to true data in the dsi database gets overwritten!
    private static final boolean PROCESS_UPDATES = false;

    private static DataLoaderFromDatabase<Object> database = null;

    private static final String USERNAME_SUFFIX = "@iscte.pt";

    private static int UID = 0;

    private static int REGISTRATION_ID = 0;

    static {
        database = new DataLoaderFromDatabase<Object>();
    }

    // how many students should be read out of the fenix system
    // most of them are students, so they got not returned here,
    // that is why the number should be quite high.
    private static final int STOP = getStopValue();

    // interval from which to get users from webservice
    private static final int STEP = getStepValue();

    private static final SimpleDateFormat sdfSmallDate = new SimpleDateFormat("yyMMdd");

    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");

    private Utilizador_iscteDataLoaderFromDatabase<Utilizador_iscte> loader = null;

    // already existent users in dsi database are saved here
    private Collection<Utilizador_iscte> listaUtilizador_iscte = null;

    // new users which have to be created
    private Collection<Utilizador_iscte> listaUtilizador_iscteInsert = null;

    // users to be updated
    private Collection<Utilizador_iscte> listaUtilizador_iscteUpdate = null;

    private Date sysdate;

    private int userTypeNumber;

    private Tipo_user tipo_user = Tipo_user.DIVERSOS;

    private Orgao_iscte orgao_iscte = new Orgao_iscte();

    private static int inserts = 0;

    private List<Pair<NotStudentData, String>> errors;
    private List<Pair<Utilizador_iscte, String>> warnings;

    private static final String ERROR_USER_WITHOUT_USERNAME = "User without username";
    private static final String ERROR_INVALID_USERNAME = "Invalid username";

    // Cannot create accounts without a known department, otherwise it will fail
    // creating the account in areas and other systems.
    private static final String ERROR_USER_WITHOUT_KNOWN_DEPARTMENT = "User without a known department.";

    private static final String WARN_SETTING_DEPARTMENT_TO_DIVERSOS =
            "Could not get corresponding department. Setting department to Diversos";
    private static final String WARN_INVALID_PHONE = "Invalid phone";
    private static final String WARN_INVALID_MOBILE = "Invalid mobile";

    private List<Pair<Utilizador_iscte, String>> actions;
    private static final String ACTION_INSERT = "I";
    private static final String ACTION_UPDATE = "U";

    // private static final String ACTION_DELETE = "D";

    public ImportNewNotStudents() {
        super();
        this.loader = new Utilizador_iscteDataLoaderFromDatabase<Utilizador_iscte>();
        this.listaUtilizador_iscteInsert = new ArrayList<Utilizador_iscte>();
        this.listaUtilizador_iscteUpdate = new ArrayList<Utilizador_iscte>();

        this.actions = new ArrayList<Pair<Utilizador_iscte, String>>();
        this.errors = new ArrayList<Pair<NotStudentData, String>>();
        this.warnings = new ArrayList<Pair<Utilizador_iscte, String>>();
    }

    private void updateSysdate() {
        sysdate = new Date();
    }

    private void loadAll() {
        listaUtilizador_iscte =
                loader.load(Utilizador_iscte.class,
                        "SELECT * FROM utilizador_iscte ui, orgao_iscte oi WHERE ui.orgao_iscte = oi.id_orgao_iscte");
    }

    private void process(NotStudentData notStudentData) {
        if (notStudentData.getUsername() == null) {
            errors.add(new Pair<NotStudentData, String>(notStudentData, ERROR_USER_WITHOUT_USERNAME));
            return;
        }

        if (!notStudentData.getUsername().trim().endsWith("@iscte.pt")) {
            errors.add(new Pair<NotStudentData, String>(notStudentData, ERROR_INVALID_USERNAME));
            return;
        }

        // 1- Verifica se existe, por:
        // * Login
        Utilizador_iscte utilizador_iscte = findPersonByUsername(notStudentData);

        /*
         * There are 12 types of users in dsi tipo_user table:
         * 
         * 1 | Docente
         * 
         * 2 | Funcionário(a)
         * 
         * 3 | Investigador(a)
         * 
         * 4 | Convidado(a)
         * 
         * 5 | Diversos
         * 
         * 6 | Aluno
         * 
         * 7 | Colaborador(a)
         * 
         * 8 | Editor(Externo)
         * 
         * 9 | Conselheiro
         * 
         * 10 | Segurança
         * 
         * 11 | Docente de escola externa
         * 
         * 12 | Inspector(a)
         */

        tipo_user = Tipo_user.DIVERSOS;

        if (notStudentData.isKeyTeacher()) {
            tipo_user = Tipo_user.DOCENTE;
        } else if (notStudentData.isKeyExternalTeacher()) {
            tipo_user = Tipo_user.DOCENTE_EXTERNO;
        } else if (notStudentData.isKeyEmployee()) {
            tipo_user = Tipo_user.FUNCIONARIO;
        } else if (notStudentData.isKeyResearcher()) {
            tipo_user = Tipo_user.INVESTIGADOR;
        } else if (notStudentData.isKeyCollaborator()) {
            tipo_user = Tipo_user.COLABORADOR;
        } else if (notStudentData.isKeyAdvisor()) {
            tipo_user = Tipo_user.CONSELHEIRO;
        } else if (notStudentData.isKeySecurity()) {
            tipo_user = Tipo_user.SEGURANCA;
        } else if (notStudentData.isKeyInspector()) {
            tipo_user = Tipo_user.INSPECTOR;
        } else if (notStudentData.isKeyExternalEditor()) {
            tipo_user = Tipo_user.EDITOR_EXTERNO;
        } else if (notStudentData.isKeyStudent()) {
            tipo_user = Tipo_user.ALUNO;
        } else if (notStudentData.isKeyGuest()) {
            tipo_user = Tipo_user.CONVIDADO;
        } else if (notStudentData.isKeyDiverse()) {
            tipo_user = Tipo_user.DIVERSOS;
        }

        if (notStudentData.getDepartmentUnit() != null && mapFenixToDsiDepartment(notStudentData.getDepartmentUnit()) != -1) {
            orgao_iscte.setSigla_oficial(notStudentData.getDepartmentUnit());
            orgao_iscte.setId_orgao_iscte(mapFenixToDsiDepartment(notStudentData.getDepartmentUnit()));
        } else {
            // orgao_iscte.setSigla_oficial("Diversos");
            // orgao_iscte.setId_orgao_iscte(31);
            errors.add(new Pair<NotStudentData, String>(notStudentData, ERROR_USER_WITHOUT_KNOWN_DEPARTMENT));
            return;
        }

        // create student or update student
        boolean isNew = false;
        if (utilizador_iscte == null) {
            utilizador_iscte = createNotStudent(notStudentData);
            addInsert(utilizador_iscte);
            isNew = true;
        } else {
            if (PROCESS_UPDATES && updateRequired(utilizador_iscte, notStudentData)) {
                updateNotStudent(utilizador_iscte, notStudentData);
                addUpdate(utilizador_iscte);
            }
        }

        // Feedback
        if (isNew || PROCESS_UPDATES) {
            logger.debug((isNew ? "I" : "U") + ": Processed " + notStudentData.getName() + " | id document: "
                    + notStudentData.getIdDocumentNumber() + " | username: " + notStudentData.getUsername());
        }
    }

    private void fixPhones(Utilizador_iscte data) {
        String phone = data.getTelefone();
        if (phone != null && (phone.length() > 9 || !NumberUtils.isNumber(phone))) {
            data.setTelefone(null);
            warnings.add(new Pair<Utilizador_iscte, String>(data, WARN_INVALID_PHONE));
        }

        String mobile = data.getTelemovel();
        if (mobile != null && (mobile.length() > 9 || !NumberUtils.isNumber(mobile))) {
            data.setTelemovel(null);
            warnings.add(new Pair<Utilizador_iscte, String>(data, WARN_INVALID_MOBILE));
        }
    }

    private void addInsert(Utilizador_iscte utilizador_iscte) {
        listaUtilizador_iscteInsert.add(utilizador_iscte);
        listaUtilizador_iscte.add(utilizador_iscte);
        actions.add(new Pair<Utilizador_iscte, String>(utilizador_iscte, ACTION_INSERT));
    }

    private void addUpdate(Utilizador_iscte utilizador_iscte) {
        listaUtilizador_iscteUpdate.add(utilizador_iscte);
        actions.add(new Pair<Utilizador_iscte, String>(utilizador_iscte, ACTION_UPDATE));
    }

    private boolean updateRequired(Utilizador_iscte utilizador_iscte, NotStudentData notStudentData) {
        if (utilizador_iscte.getOrgao_iscte() != orgao_iscte || utilizador_iscte.getVisibilidade() != 1
                || utilizador_iscte.getTipo_user() != tipo_user
                || utilizador_iscte.getLogin() != getLoginForDSI(notStudentData.getUsername())
                || utilizador_iscte.getNome() != notStudentData.getName()
                || utilizador_iscte.getNome_usado() != notStudentData.getNickname()
                || utilizador_iscte.getNomes_proprios() != notStudentData.getGivenNames()
                || utilizador_iscte.getNomes_proprios_usados() != notStudentData.getUsedGivenNames()
                || utilizador_iscte.getApelidos() != notStudentData.getFamilyNames()
                || utilizador_iscte.getApelidos_usados() != notStudentData.getUsedFamilyNames()
                || utilizador_iscte.getTelefone() != notStudentData.getPhoneNumber()
                || utilizador_iscte.getTelemovel() != notStudentData.getMobilePhoneNumber()
                || utilizador_iscte.getE_mail_pessoal() != notStudentData.getPersonalEmail()
                || utilizador_iscte.getPassword() != notStudentData.getPassword()
                || utilizador_iscte.getE_mail_iscte() != notStudentData.getInstitutionalEmail()
                || utilizador_iscte.getDocumentIdNumber() != notStudentData.getIdDocumentNumber()) {
            return true;
        }
        return false;
    }

    private Utilizador_iscte findPersonByUsername(NotStudentData notStudentData) {
        for (Utilizador_iscte utilizador_iscte : listaUtilizador_iscte) {
            if (utilizador_iscte.getLogin().equals(getLoginForDSI(notStudentData.getUsername()))) {
                return utilizador_iscte;
            }
        }
        return null;
    }

    private Utilizador_iscte createNotStudent(NotStudentData notStudentData) {

        Utilizador_iscte utilizador_iscte = new Utilizador_iscte();

        updateNotStudent(utilizador_iscte, notStudentData);

        utilizador_iscte.setModificado(false);

        return utilizador_iscte;
    }

    private void updateNotStudent(Utilizador_iscte utilizador_iscte, NotStudentData notStudentData) {

        Orgao_iscte orgI = new Orgao_iscte();
        orgI.setId_orgao_iscte(orgao_iscte.getId_orgao_iscte());

        utilizador_iscte.setData_accao(getSmallDate(sysdate));
        utilizador_iscte.setOrgao_iscte(orgI);
        utilizador_iscte.setVisibilidade(1);
        utilizador_iscte.setObservacoes("fenix_import_" + getSmallDate(sysdate));
        utilizador_iscte.setRemovido(false);
        utilizador_iscte.setModificado(true);
        utilizador_iscte.setTipo_user(tipo_user);
        utilizador_iscte.setLogin(getLoginForDSI(notStudentData.getUsername()));
        utilizador_iscte.setNome(notStudentData.getName());
        utilizador_iscte.setNome_usado(notStudentData.getNickname());
        utilizador_iscte.setNomes_proprios(notStudentData.getGivenNames());
        utilizador_iscte.setNomes_proprios_usados(notStudentData.getUsedGivenNames());
        utilizador_iscte.setApelidos(notStudentData.getFamilyNames());
        utilizador_iscte.setApelidos_usados(notStudentData.getUsedFamilyNames());
        if (notStudentData.getPhoneNumber() != null) {
            utilizador_iscte.setTelefone(notStudentData.getPhoneNumber().replaceAll("\\W", ""));
        }
        if (notStudentData.getMobilePhoneNumber() != null) {
            utilizador_iscte.setTelemovel(notStudentData.getMobilePhoneNumber().replaceAll("\\W", ""));
        }
        utilizador_iscte.setE_mail_pessoal(notStudentData.getPersonalEmail());
        utilizador_iscte.setPassword(notStudentData.getPassword());
        utilizador_iscte.setE_mail_iscte(notStudentData.getInstitutionalEmail());
        utilizador_iscte.setDocumentIdNumber(notStudentData.getIdDocumentNumber());
    }

    private List<String> generateUpdate(final Utilizador_iscte utilizadorIscte) {

        final List<String> sqlStatements = new ArrayList<String>(1);

        if (UID == 0) {
            UID = getUID() + 1;
        } else {
            UID += 1;
        }

        if (REGISTRATION_ID == 0) {
            REGISTRATION_ID = database.getLastAttributeValueFromTable("SELECT MAX(id_inscricao) FROM inscricao") + 1;
        } else {
            REGISTRATION_ID += 1;
        }

        if (utilizadorIscte.getTipo_user().equals(Tipo_user.DOCENTE)) {
            userTypeNumber = 1;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.FUNCIONARIO)) {
            userTypeNumber = 2;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.INVESTIGADOR)) {
            userTypeNumber = 3;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.CONVIDADO)) {
            userTypeNumber = 4;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.DIVERSOS)) {
            userTypeNumber = 5;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.ALUNO)) {
            userTypeNumber = 6;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.COLABORADOR)) {
            userTypeNumber = 7;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.EDITOR_EXTERNO)) {
            userTypeNumber = 8;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.CONSELHEIRO)) {
            userTypeNumber = 9;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.SEGURANCA)) {
            userTypeNumber = 10;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.DOCENTE_EXTERNO)) {
            userTypeNumber = 11;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.INSPECTOR)) {
            userTypeNumber = 12;
        }

        final String updateRegistration =
                "UPDATE inscricao SET " + "estado=" + 2 + ","
                        + "login_ciiscte="
                        + "'sys'"
                        + ","
                        // + "login_utilizador='" + utilizadorIscte.getLogin() + "',"
                        // + "id_inscricao=" + REGISTRATION_ID + ","
                        + "hora_inscricao='" + getTime(sysdate) + "'," + "hora_autorizacao='" + getTime(sysdate) + "',"
                        + "data_inscricao='" + getSmallDate(sysdate) + "'," + "data_autorizacao='" + getSmallDate(sysdate) + "' "
                        + "WHERE " + "login_utilizador='" + utilizadorIscte.getLogin() + "';";

        // Adding following values to working area:
        // ISCTE NT
        // ISCTE Mail
        // ISCTE Linux
        // Geral Wireless
        // Alunos NT
        final String updateSystemWorkingArea1 =
                "UPDATE area_trabalho SET " + "tipo_area='1'," + "tipo_rede='1'" + " WHERE " + "login_utilizador='"
                        + utilizadorIscte.getLogin() + "';";

        final String updateSystemWorkingArea2 =
                "UPDATE area_trabalho SET " + "tipo_area='8'," + "tipo_rede='3'" + " WHERE " + "login_utilizador='"
                        + utilizadorIscte.getLogin() + "';";

        final String updateSystemWorkingArea3 =
                "UPDATE area_trabalho SET " + "tipo_area='3'," + "tipo_rede='1'" + " WHERE " + "login_utilizador='"
                        + utilizadorIscte.getLogin() + "';";

        final String updateSystemWorkingArea4 =
                "UPDATE area_trabalho SET " + "tipo_area='2'," + "tipo_rede='1'" + " WHERE " + "login_utilizador='"
                        + utilizadorIscte.getLogin() + "';";

        final String updateSystemWorkingArea5 =
                "UPDATE area_trabalho SET " + "tipo_area='1'," + "tipo_rede='2'" + " WHERE " + "login_utilizador='"
                        + utilizadorIscte.getLogin() + "';";

        final String updateNoStudentUser =
                "UPDATE utilizador_iscte SET " + "login='"
                        + utilizadorIscte.getLogin()
                        + "',"
                        + "nome='"
                        + utilizadorIscte.getNome()
                        + "',"
                        + "nome_usado='"
                        + utilizadorIscte.getNome_usado()
                        + "',"
                        + "nomes_proprios='"
                        + utilizadorIscte.getNomes_proprios()
                        + "',"
                        + "nomes_proprios_usados='"
                        + utilizadorIscte.getNomes_proprios_usados()
                        + "',"
                        + "apelidos='"
                        + utilizadorIscte.getApelidos()
                        + "',"
                        + "apelidos_usados='"
                        + utilizadorIscte.getApelidos_usados()
                        + "',"
                        + "tipo_user="
                        + userTypeNumber
                        + ","
                        + "orgao_iscte="
                        + (utilizadorIscte.getOrgao_iscte().getId_orgao_iscte() != 0 ? utilizadorIscte.getOrgao_iscte()
                                .getId_orgao_iscte() : 31)
                        + ","
                        + "ext_gabinete="
                        + 999999
                        + ","
                        + "telefone="
                        + ((utilizadorIscte.getTelefone() == null || utilizadorIscte.getTelefone().equals("0") || utilizadorIscte
                                .getTelefone().equals("")) ? 999999999 : utilizadorIscte.getTelefone())
                        + ","
                        + "gabinete="
                        + "'00.00'"
                        + ","
                        + "telemovel="
                        + ((utilizadorIscte.getTelemovel() == null || utilizadorIscte.getTelemovel().equals("0") || utilizadorIscte
                                .getTelemovel().equals("")) ? 999999999 : utilizadorIscte.getTelemovel())
                        + ","
                        + "visibilidade="
                        + 1
                        + ","
                        + "e_mail_pessoal='"
                        + (utilizadorIscte.getE_mail_pessoal() == null ? "" : utilizadorIscte.getE_mail_pessoal())
                        + "',"
                        + "password='"
                        + utilizadorIscte.getPassword()
                        + "',"
                        + "uid="
                        + UID
                        + ","
                        + "home_unix="
                        + "'/home/"
                        + utilizadorIscte.getLogin()
                        + "',"
                        + "e_mail_iscte='"
                        + (utilizadorIscte.getE_mail_iscte() == null ? utilizadorIscte.getLogin() + USERNAME_SUFFIX : utilizadorIscte
                                .getE_mail_iscte()) + "'," + "observacoes='" + utilizadorIscte.getObservacoes() + "',"
                        + "id_cacifo=" + 427 + "," + "n_mecanografico='"
                        + (utilizadorIscte.getN_mecanografico() == null ? "" : utilizadorIscte.getN_mecanografico()) + "',"
                        + "removido=" + "'f'" + "," + "n_documento_identificacao='"
                        + (utilizadorIscte.getDocumentIdNumber() == null ? "" : utilizadorIscte.getDocumentIdNumber()) + "',"
                        + "external_cv_url='"
                        + (utilizadorIscte.getExternal_cv_url() == null ? "" : utilizadorIscte.getExternal_cv_url()) + "' "
                        + "WHERE " + "login='" + utilizadorIscte.getLogin() + "';";

        sqlStatements.add(updateNoStudentUser);
        sqlStatements.add(updateRegistration);
        sqlStatements.add(updateSystemWorkingArea1);
        sqlStatements.add(updateSystemWorkingArea2);
        sqlStatements.add(updateSystemWorkingArea3);
        sqlStatements.add(updateSystemWorkingArea4);
        sqlStatements.add(updateSystemWorkingArea5);

        return sqlStatements;
    }

    private List<String> generateInsert(final Utilizador_iscte utilizadorIscte) {

        final List<String> sqlStatements = new ArrayList<String>(1);

        if (UID == 0) {
            UID = getUID() + 1;
        } else {
            UID += 1;
        }

        if (REGISTRATION_ID == 0) {
            REGISTRATION_ID = database.getLastAttributeValueFromTable("SELECT MAX(id_inscricao) FROM inscricao") + 1;
        } else {
            REGISTRATION_ID += 1;
        }

        if (utilizadorIscte.getTipo_user().equals(Tipo_user.DOCENTE)) {
            userTypeNumber = 1;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.FUNCIONARIO)) {
            userTypeNumber = 2;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.INVESTIGADOR)) {
            userTypeNumber = 3;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.CONVIDADO)) {
            userTypeNumber = 4;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.DIVERSOS)) {
            userTypeNumber = 5;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.ALUNO)) {
            userTypeNumber = 6;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.COLABORADOR)) {
            userTypeNumber = 7;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.EDITOR_EXTERNO)) {
            userTypeNumber = 8;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.CONSELHEIRO)) {
            userTypeNumber = 9;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.SEGURANCA)) {
            userTypeNumber = 10;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.DOCENTE_EXTERNO)) {
            userTypeNumber = 11;
        } else if (utilizadorIscte.getTipo_user().equals(Tipo_user.INSPECTOR)) {
            userTypeNumber = 12;
        }

        // for all notstudents the same
        final String insertRegistration =
                "INSERT INTO inscricao (estado,login_ciiscte,login_utilizador,id_inscricao,hora_inscricao,hora_autorizacao"
                        + ",data_inscricao,data_autorizacao)" + " VALUES (2,'sys'," + "'" + utilizadorIscte.getLogin() + "',"
                        + REGISTRATION_ID + ",'" + getTime(sysdate) + "','" + getTime(sysdate) + "','" + getSmallDate(sysdate)
                        + "','" + getSmallDate(sysdate) + "');";

        // Adding following values to working area:
        // ISCTE NT
        // ISCTE Mail
        // ISCTE Linux
        // Geral Wireless
        // Alunos NT
        final String insertSystemWorkingArea1 =
                "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
                        + "('1','1','" + utilizadorIscte.getLogin() + "'," + REGISTRATION_ID + ",'" + getSmallDate(sysdate)
                        + "');";

        final String insertSystemWorkingArea2 =
                "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
                        + "('8','3','" + utilizadorIscte.getLogin() + "'," + REGISTRATION_ID + ",'" + getSmallDate(sysdate)
                        + "');";

        final String insertSystemWorkingArea3 =
                "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
                        + "('3','1','" + utilizadorIscte.getLogin() + "'," + REGISTRATION_ID + ",'" + getSmallDate(sysdate)
                        + "');";

        final String insertSystemWorkingArea4 =
                "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
                        + "('2','1','" + utilizadorIscte.getLogin() + "'," + REGISTRATION_ID + ",'" + getSmallDate(sysdate)
                        + "');";

        final String insertSystemWorkingArea5 =
                "INSERT INTO area_trabalho (tipo_area,tipo_rede,login_utilizador,id_inscricao,data_abertura) VALUES "
                        + "('1','2','" + utilizadorIscte.getLogin() + "'," + REGISTRATION_ID + ",'" + getSmallDate(sysdate)
                        + "');";

        // depending on the type of the iscte user, it will fill in the
        // appropriate values
        final String insertNoStudentUser =
                "INSERT INTO utilizador_iscte (login,nome,nome_usado,nomes_proprios,nomes_proprios_usados,apelidos,apelidos_usados,tipo_user,orgao_iscte,ext_gabinete,telefone,"
                        + "gabinete,telemovel,visibilidade,e_mail_pessoal,password,uid,"
                        + "home_unix,e_mail_iscte,observacoes,id_cacifo,n_mecanografico,removido,"
                        + "n_documento_identificacao,external_cv_url) VALUES (" + "'"
                        + utilizadorIscte.getLogin()
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getNome())
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getNome_usado())
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getNomes_proprios())
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getNomes_proprios_usados())
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getApelidos())
                        + "',"
                        + "'"
                        + fixString(utilizadorIscte.getApelidos_usados())
                        + "',"
                        + "'"
                        + userTypeNumber
                        + "'"
                        + ","
                        + (utilizadorIscte.getOrgao_iscte().getId_orgao_iscte() != 0 ? utilizadorIscte.getOrgao_iscte()
                                .getId_orgao_iscte() : 31)
                        + ","
                        + "999999,"
                        + ((utilizadorIscte.getTelefone() == null || utilizadorIscte.getTelefone().equals("0") || utilizadorIscte
                                .getTelefone().equals("")) ? 999999999 : utilizadorIscte.getTelefone())
                        + ","
                        + "'00.00',"
                        + ((utilizadorIscte.getTelemovel() == null || utilizadorIscte.getTelemovel().equals("0") || utilizadorIscte
                                .getTelemovel().equals("")) ? 999999999 : utilizadorIscte.getTelemovel())
                        + (utilizadorIscte.getTipo_user() == Tipo_user.DOCENTE_EXTERNO ? ",0," : ",1,")
                        + "'"
                        + (utilizadorIscte.getE_mail_pessoal() == null ? "" : utilizadorIscte.getE_mail_pessoal())
                        + "',"
                        + "'"
                        + fixPassword(utilizadorIscte.getPassword())
                        + "',"
                        + UID
                        + ","
                        + "'/home/"
                        + utilizadorIscte.getLogin()
                        + "',"
                        + "'"
                        + (utilizadorIscte.getE_mail_iscte() == null ? utilizadorIscte.getLogin() + USERNAME_SUFFIX : utilizadorIscte
                                .getE_mail_iscte())
                        + "','"
                        + utilizadorIscte.getObservacoes()
                        + "',427,'"
                        + (utilizadorIscte.getN_mecanografico() == null ? "" : utilizadorIscte.getN_mecanografico())
                        + "',"
                        + "'f',"
                        + "'"
                        + (utilizadorIscte.getDocumentIdNumber() == null ? "" : utilizadorIscte.getDocumentIdNumber())
                        + "',"
                        + "'"
                        + (utilizadorIscte.getExternal_cv_url() == null ? "" : utilizadorIscte.getExternal_cv_url())
                        + "');\n";

        sqlStatements.add(insertNoStudentUser);
        sqlStatements.add(insertRegistration);
        sqlStatements.add(insertSystemWorkingArea1);
        sqlStatements.add(insertSystemWorkingArea2);
        sqlStatements.add(insertSystemWorkingArea3);
        sqlStatements.add(insertSystemWorkingArea4);
        sqlStatements.add(insertSystemWorkingArea5);

        return sqlStatements;
    }

    private String fixString(String s) {
        if (s != null) {
            return s.replaceAll("'", "");
        }
        return s;
    }

    private String fixPassword(String password) {
        if (password != null && password.length() > 15) {
            return password.substring(0, 14);
        }
        return password;
    }

    private void commit() {
        List<String> sqlStatements =
                new ArrayList<String>(listaUtilizador_iscteInsert.size() + listaUtilizador_iscteUpdate.size());

        // INSERTS
        for (Utilizador_iscte utilizador_iscte : listaUtilizador_iscteInsert) {
            fixPhones(utilizador_iscte);
            for (String sqlStatement : generateInsert(utilizador_iscte)) {
                sqlStatements.add(sqlStatement);
            }
            if (utilizador_iscte.getOrgao_iscte().getId_orgao_iscte() == 31) {
                warnings.add(new Pair<Utilizador_iscte, String>(utilizador_iscte, WARN_SETTING_DEPARTMENT_TO_DIVERSOS));
            }

            inserts++;
        }

        // UPDATES
        for (Utilizador_iscte utilizador_iscte : listaUtilizador_iscteUpdate) {
            fixPhones(utilizador_iscte);
            for (String sqlStatement : generateUpdate(utilizador_iscte)) {
                sqlStatements.add(sqlStatement);
            }
        }

        // commit it
        loader.executeUpdate(sqlStatements);

    }

    private void cleanUp() {
        listaUtilizador_iscteInsert.clear();
        listaUtilizador_iscteUpdate.clear();
    }

    private String getSmallDate(Date date) {
        if (date != null) {
            return sdfSmallDate.format(date);
        }
        return "";
    }

    /**
     * Remove "@iscte.pt" suffix.
     * 
     * @param login
     * @return
     */
    private static String getLoginForDSI(String login) {
        return login.substring(0, login.length() - 9);
    }

    private int mapFenixToDsiDepartment(String department) {
        Integer code = DepartmentMapping.getDSIDepartment(department);
        if (code == null) {
            return -1;
            // throw new
            // IllegalArgumentException("Unknown mapping for department " +
            // department);
        }
        return code.intValue();
    }

    private int getUID() {
        int userUid = 0;
        final int maxUidFromISCTEUser =
                database.getLastAttributeValueFromTable("SELECT max(uid) FROM utilizador_iscte WHERE uid < 50000;");
        final int maxUidFromStudentUser =
                database.getLastAttributeValueFromTable("SELECT max(uid) from aluno WHERE uid < 50000;");

        if (maxUidFromISCTEUser >= maxUidFromISCTEUser)
            userUid = maxUidFromISCTEUser;

        if (maxUidFromISCTEUser < maxUidFromISCTEUser)
            userUid = maxUidFromStudentUser;

        return userUid;
    }

    // 18:23:15
    private String getTime(Date date) {
        if (date != null) {
            return sdfTime.format(date);
        }
        return "";
    }

    public static void main(String[] args) throws RemoteException {
        DsiNotStudentDataService service = DsiNotStudentDataServiceUtil.getService();
        int start = getStartValue();

        List<NotStudentData> list = null;
        ImportNewNotStudents agent = new ImportNewNotStudents();
        agent.loadAll();

        String[] staticList = getStaticList();
        if (staticList != null && staticList.length > 0) {
            agent.updateSysdate();
            for (String elem : staticList) {
                String username = elem.trim();
                logger.info("Processing: " + username);
                NotStudentData notStudentData = service.getOneByUsernameNotStudentData(username);
                agent.process(notStudentData);
            }
            // COMMIT changes
            agent.commit();
            agent.cleanUp();
        } else {
            do {
                int end = start + STEP;
                // .........
                logger.info("(" + start + "," + end + ")");
                list = buildList(service.getAllInIntervalNotStudentData(start, end));
                start += STEP + 1;

                agent.updateSysdate();
                for (NotStudentData notStudentData : list) {
                    agent.process(notStudentData);
                }

                // COMMIT changes
                agent.commit();
                agent.cleanUp();
            } while (start < STOP);
        }
        System.out.println("Inserts: " + inserts);

        // Process Result
        logger.info("The END");

        // Report
        agent.report();
    }

    private static List<NotStudentData> buildList(List<NotStudentData> interval) {
        if (interval == null || interval.size() == 0) {
            return new ArrayList<NotStudentData>(0);
        }

        return interval;
    }

    private void report() {
        final int countOK = actions.size();
        final int countErrors = errors.size();

        // Build message
        String loggingMessage = buildMessage();

        // Send email
        int totalRecords = countOK + countErrors;
        LogSender.sendLoggingByEmail("Webservice client ImportNewNotStudents", countErrors == 0, countErrors, countOK,
                totalRecords, loggingMessage);
        logger.info(loggingMessage);
    }

    private String buildMessage() {
        StringBuilder sb = new StringBuilder();

        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Errors"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Department", "Error"));
        for (Pair<NotStudentData, String> error : errors) {
            NotStudentData data = error.getFirst();
            String msg = error.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(data.getName(), data.getIdDocumentNumber(), data.getUsername(),
                    data.getDepartmentUnit(), msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Warnings"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Orgao ISCTE", "Warning"));
        for (Pair<Utilizador_iscte, String> warning : warnings) {
            Utilizador_iscte data = warning.getFirst();
            String msg = warning.getSecond();
            String orgaoIscte = data.getOrgao_iscte().getSigla();
            if (orgaoIscte == null) {
                final Integer key = data.getOrgao_iscte().getId_orgao_iscte();
                orgaoIscte = DepartmentMapping.getSiglaOrgaoIscte(key);
            }
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(data.getNome(), data.getBi(), data.getLogin(), orgaoIscte, msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Actions performed"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Orgao ISCTE", "Action"));
        for (Pair<Utilizador_iscte, String> action : actions) {
            Utilizador_iscte data = action.getFirst();
            String actionPerformed = action.getSecond();
            String orgaoIscte = data.getOrgao_iscte().getSigla();

            if (orgaoIscte == null) {
                final Integer key = data.getOrgao_iscte().getId_orgao_iscte();
                orgaoIscte = DepartmentMapping.getSiglaOrgaoIscte(key);
            }
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(data.getNome(), data.getBi(), data.getLogin(), orgaoIscte,
                    actionPerformed));
        }

        return sb.toString();
    }

    private static int getStopValue() {
        return PropertiesManager.getIntegerProperty("DsiNotStudentIntegration.stop");
    }

    private static int getStepValue() {
        return PropertiesManager.getIntegerProperty("DsiNotStudentIntegration.step");
    }

    private static int getStartValue() {
        return PropertiesManager.getIntegerProperty("DsiNotStudentIntegration.start");
    }

    // private static String getEndpointAddress() {
    // String p =
    // PropertiesManager.getProperty("DsiNotStudentIntegration.endpointAddress");
    // if (p != null && p.trim().length() > 0) {
    // return p.trim();
    // }
    // return null;
    // }

    private static String[] getStaticList() {
        String p = PropertiesManager.getProperty("DsiNotStudentIntegration.staticList");
        if (p != null && p.trim().length() > 0) {
            return p.split(",");
        }
        return null;
    }

}
