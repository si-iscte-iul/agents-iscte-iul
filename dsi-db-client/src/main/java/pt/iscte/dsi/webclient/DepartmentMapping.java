package pt.iscte.dsi.webclient;

import java.util.HashMap;
import java.util.Map;

public class DepartmentMapping {

    private static Map<String, Integer> departmentMapping = new HashMap<String, Integer>(80);
    private static Map<Integer, String> orgaoIscteSiglaMapping = new HashMap<Integer, String>(80);

    static {
        loadDepartmentMapping();
        loadOrgaoIscteSiglaMapping();
    }

    private static void loadDepartmentMapping() {
        departmentMapping.put("ACEA", 28);
        departmentMapping.put("ADETTI", 4);
        departmentMapping.put("AUDAX", 59);
        departmentMapping.put("CAV", 41);
        departmentMapping.put("CC", 53);
        departmentMapping.put("CEA", 20);
        departmentMapping.put("CEAS", 7);
        departmentMapping.put("CEHCP", 17);
        departmentMapping.put("CEHC", 17);
        departmentMapping.put("CEMAF", 8);
        departmentMapping.put("CET", 9);
        departmentMapping.put("CEUA", 18);
        departmentMapping.put("CI", 30);
        departmentMapping.put("CIES", 11);
        departmentMapping.put("CIES-IUL", 11);
        departmentMapping.put("CIS", 10);
        departmentMapping.put("CIS-IUL", 10);
        departmentMapping.put("CP", 54);
        departmentMapping.put("DA", 22);
        departmentMapping.put("DAFP", 48);
        departmentMapping.put("DC", 75);
        departmentMapping.put("DSI", 30);
        departmentMapping.put("DCG", 25);
        departmentMapping.put("DCTI", 26);
        departmentMapping.put("DE", 2);
        departmentMapping.put("DF", 74);
        departmentMapping.put("DGRH", 44);
        departmentMapping.put("DH", 3);
        departmentMapping.put("DINAMIA", 12);
        departmentMapping.put("DINAMIACET-IUL", 12);
        departmentMapping.put("DMQ", 24);
        departmentMapping.put("DP", 46);
        departmentMapping.put("DPSO", 1);
        departmentMapping.put("DS", 23);
        departmentMapping.put("DSA", 46);
        departmentMapping.put("SA", 46);
        departmentMapping.put("DSAD", 48);
        departmentMapping.put("UF", 48);
        departmentMapping.put("GPRF", 48);
        departmentMapping.put("SC", 48);
        departmentMapping.put("SEP", 48);
        departmentMapping.put("DSFP", 48);
        departmentMapping.put("T", 48);
        departmentMapping.put("UER", 48);
        departmentMapping.put("DSBD", 40);
        departmentMapping.put("DSRH", 44);
        departmentMapping.put("URH", 44);
        departmentMapping.put("SFPRH", 44);
        departmentMapping.put("GAA", 33);
        departmentMapping.put("GAQ", 68);
        departmentMapping.put("GARE", 45);
        departmentMapping.put("NT", 45);
        departmentMapping.put("GEP", 69);
        departmentMapping.put("GESP", 43);
        departmentMapping.put("GIEM", 13);
        departmentMapping.put("GIESTA", 19);
        departmentMapping.put("GIRE", 33);
        departmentMapping.put("GA", 58);
        departmentMapping.put("NRIA", 58);
        departmentMapping.put("GJ", 58);
        departmentMapping.put("GP", 58);
        departmentMapping.put("SP", 58);
        departmentMapping.put("GAR", 58);
        departmentMapping.put("GPGA", 51);
        departmentMapping.put("IN OUT GLOBAL", 70);
        departmentMapping.put("ISCTE", 71);
        departmentMapping.put("iscte", 71);
        departmentMapping.put("ISCTE-IUL", 71);
        departmentMapping.put("INDEG", 15);
        departmentMapping.put("NAC", 46);
        departmentMapping.put("NAT", 46);
        departmentMapping.put("NRI", 32);
        departmentMapping.put("GRI", 32);
        departmentMapping.put("OBSERVA", 38);
        departmentMapping.put("OVERGEST", 14);
        departmentMapping.put("SAAU", 29);
        departmentMapping.put("DAU", 29);
        departmentMapping.put("SACG", 46);
        departmentMapping.put("SACS", 46);
        departmentMapping.put("SACT", 46);
        departmentMapping.put("SAD", 21);
        departmentMapping.put("SD", 44);
//  departmentMapping.put("SEA", 44);
        departmentMapping.put("SM", 46);
        departmentMapping.put("SVA", 44);
        departmentMapping.put("UNIDE", 5);
        departmentMapping.put("NOT_DEFINED", 99);
        departmentMapping.put("GGC", 78);
        departmentMapping.put("GPDGA", 79);

        departmentMapping.put("DRHCO", 81);
        departmentMapping.put("DMOG", 82);

        departmentMapping.put("SEA", 83);
        departmentMapping.put("SAS", 84);
        departmentMapping.put("AAA", 84);
        departmentMapping.put("GAI", 85);
        departmentMapping.put("GAIP", 85);
        departmentMapping.put("DEP", 86);
        departmentMapping.put("GCI", 87);
        departmentMapping.put("DCPPP", 88);

        // Add non matching mappings.
        // In order to change the mappings in DSI,
        // we must check the mailing lists process.

        // CRIA
        departmentMapping.put("CRIA", 7);
        departmentMapping.put("CRIA-IUL", 7);

        // GMIVA
        departmentMapping.put("GMIVA", 33);

        // Fundação ISCTE
        departmentMapping.put("FI", 62);

        // GAQE
        departmentMapping.put("GAQE", 68);
        departmentMapping.put("GEAPQ", 68);
        departmentMapping.put("GPSQ", 68);
        departmentMapping.put("NEA", 68);
        departmentMapping.put("NPQ", 68);
        departmentMapping.put("GEPQ", 68);

        // DSBD -> Serviços de Informação e Documentação (SID)
        departmentMapping.put("SID", 40);

        // DSBD -> Unidade de Informação e Formação  (UIF)
        departmentMapping.put("UIF", 40);

        // DSBB -> Núcleo de Biblioteconomia (NB) 
        departmentMapping.put("NB", 40);

        // SVA -> Núcleo de Gestão Técnica (NGT)
        departmentMapping.put("NGT", 44);

        // DSFP -> Núcleo de Contabilidade e Orçamento (NCO)
        departmentMapping.put("NCO", 44);

        // NT -> Nucleo de Apoio Geral (NAG)
        departmentMapping.put("NAG", 45);

        // SM -> Unidade de Estudados Graduados (SA)
        departmentMapping.put("UEG", 46);

        // 
        departmentMapping.put("UD", 30);
        departmentMapping.put("URCS", 30);
        departmentMapping.put("UMTE", 30);
        departmentMapping.put("NAU", 30);
        departmentMapping.put("SI", 30);
        departmentMapping.put("NMTE", 30);
        departmentMapping.put("EM-ATIEC", 30);  // SIIC since 2019

        // Equipa Tec. Ap. Téc. Informático Esp.Com
        departmentMapping.put("ETAPTIEC", 30);

        // Escolas
        departmentMapping.put("ECSH", 89);
        departmentMapping.put("IBS", 90);
        departmentMapping.put("EG", 90);
        departmentMapping.put("ESPP", 91);
        departmentMapping.put("ISTA", 92);

        // NATA
        departmentMapping.put("NATA-ECSH", 89);
        departmentMapping.put("NATA-IBS", 90);
        departmentMapping.put("NATA-EG", 90);
        departmentMapping.put("NATA-ESPP", 91);
        departmentMapping.put("NATA-ISTA", 92);

        departmentMapping.put("UATA-ISTA", 92);
        departmentMapping.put("UATA-ECSH", 89);
        departmentMapping.put("UATA-ESPP", 91);
        departmentMapping.put("UATA-IBS", 90);
        departmentMapping.put("UATA-EG", 90);

        departmentMapping.put("IPPS", 93);
        departmentMapping.put("IUL-G", 94);
        departmentMapping.put("IULG", 94);

        // Departamento de Métodos de Pesquisa Social
        departmentMapping.put("DMPS", 95);

        // Projectos
        departmentMapping.put("PROJ", 80);
        departmentMapping.put("PROJ-GAI", 80);

        // GESP - Gabinete de Inserção Profissional (GIP)
        departmentMapping.put("GIP", 43);

        // RPJPP - Residência Prof. José Pinto Peixoto
        departmentMapping.put("RPJPP", 96);

        // DIPPS - Departamento da IPPS
        departmentMapping.put("DIPPS", 97);

        // DM - Departamento de Matemática
        departmentMapping.put("DM", 98);

        // DMQGE - Departamento de Métodos Quantitativos para Gestão e Economia
        departmentMapping.put("DMQGE", 99);

        // --------------------------------------------------------------------
        // Unidades organicas "não oficiais", mas reais da IBS.
        // --------------------------------------------------------------------
        // - Career Services
        departmentMapping.put("IBSCS", 90);
        // - Communication Office
        departmentMapping.put("IBSCO", 90);
        // - Internacional Office
        departmentMapping.put("IBSIO", 90);
        // - Accreditations and Rankings
        departmentMapping.put("IBSAR", 90);
        // - Marketing Office
        departmentMapping.put("IBSMO", 90);

        // - EG-Accreditations & Rankings (GEAPQ), mapped to NATA-EG_OLD
        departmentMapping.put("NATA-EG_OLD", 90);
        // --------------------------------------------------------------------

        // CEI - Centro Estudos Internacionais
        departmentMapping.put("CEI-IUL", 100);

        // IT-IUL - Instituto de Telecomunicações-IUL
        departmentMapping.put("IT-IUL", 101);

        // DI - Departamento INDEG
        departmentMapping.put("DI", 15);

        // DSFP -> Núcleo de Compras (NC)
        departmentMapping.put("NC", 44);

        // UCOMP - Unidade de Compras
        departmentMapping.put("UCOMP", 44);

        // SGE -> 46 : SA
        departmentMapping.put("SGE", 46);
        departmentMapping.put("AA3C", 46);
        departmentMapping.put("AGCAD", 46);
        departmentMapping.put("UOE", 46);
        departmentMapping.put("A2CECSH", 46);
        departmentMapping.put("A2CESPP", 46);
        departmentMapping.put("UIBS", 46);
        departmentMapping.put("A1CIBS", 46);
        departmentMapping.put("A2CIBS", 46);
        departmentMapping.put("UISTA", 46);
        departmentMapping.put("UGC", 46);
        departmentMapping.put("UA3C", 46);
        departmentMapping.put("U12C-ISTA", 46);
        departmentMapping.put("U12C-ECSH", 46);
        departmentMapping.put("U12C-ESPP", 46);
        departmentMapping.put("U12C-IBS", 46);
        departmentMapping.put("N1C-IBS", 46);
        departmentMapping.put("N2C-IBS", 46);
        departmentMapping.put("NAG", 46);
        departmentMapping.put("U1C", 46);
        departmentMapping.put("N1C-ISCTA-IBS", 46);
        departmentMapping.put("N1C-ESPP-ECSH", 46);
        departmentMapping.put("U2C", 46);
        departmentMapping.put("N2C-ISCTA-IBS", 46);
        departmentMapping.put("N2C-ESPP-ECSH", 46);

        // SIIC -> 30 : SI 
        departmentMapping.put("SIIC", 30);

        // Area de Apoio Logistico (AAL) -> 48 : UER
        departmentMapping.put("AAL", 48);

        // Unidade de Património e Compras (UPC) -> 48 : DSFP
        departmentMapping.put("UPC", 48);

        // Equipa Planeam. Gestão Informação BIM
        departmentMapping.put("EPGIB", 48);

        departmentMapping.put("SRHCP", 48);
        departmentMapping.put("NM", 48);
        departmentMapping.put("NGC", 48);
        departmentMapping.put("NPO", 48);
        departmentMapping.put("UGE", 48);
        departmentMapping.put("NEVENT", 48);
        departmentMapping.put("NI", 48);
        departmentMapping.put("NT", 48);
        departmentMapping.put("NGP", 48);
        departmentMapping.put("NC", 48);
        departmentMapping.put("NGT", 48);
        departmentMapping.put("NCG", 48);
        departmentMapping.put("SRHCE", 48);
        departmentMapping.put("UESPACOS", 48);
        departmentMapping.put("NESPACOS", 48);
        departmentMapping.put("SIEQUIP", 48);
        
        // GRI
        departmentMapping.put("URI", 32);
        departmentMapping.put("NCI", 32);
        departmentMapping.put("NCOOP", 32);
        departmentMapping.put("NE", 32);

        departmentMapping.put("GC", 87);

        // AB -> 40 : DSBD
        departmentMapping.put("AB", 40);

        // NB -> 40 : DSBD
        departmentMapping.put("NB", 40);

        // GCM -> 87 : GCI
        departmentMapping.put("GCM", 87);

        // Clube ISCTE
        departmentMapping.put("CBISCTE", 76);

        // AJ - Assessoria Juridica
        departmentMapping.put("AJ", 58);

        // Gabinete de Apoio aos Orgaos Univers.
        departmentMapping.put("GAOU", 102);

        // Gabinete de Career Services e Alumni
        departmentMapping.put("GCSA", 103);

        // Gabinete de Desen. de Sistemas de Inf.
        departmentMapping.put("GDSI", 104);

        // ECSH-Laboratorio de Antropologia Visual
        departmentMapping.put("LAV", 105);

        // ESPP-Lab. de Ciencias da Comunicacao
        departmentMapping.put("LCC", 106);

        // Lab DCTI
        departmentMapping.put("LDCTI", 107);

        // Lab FABLAB
        departmentMapping.put("LFABLAB", 108);

        // Conselho Geral
        departmentMapping.put("CGERAL", 109);

        // Conselho Curadores
        departmentMapping.put("CCURA", 110);

        // ISTAR-IUL
        departmentMapping.put("ISTAR-IUL", 111);

        // BGI
        departmentMapping.put("BGI", 112);

        // REITORIA
        departmentMapping.put("REITORIA", 113);

        // Nucleo de competencias transversais
        departmentMapping.put("NCT", 114);

        // Nucleo de linguas
        departmentMapping.put("NL", 115);

        // Administrador(a)
        departmentMapping.put("ADMIN", 116);

        // UGDU - Unidade de Gestao do Desporto Universitario
        departmentMapping.put("UGDU", 117);
        departmentMapping.put("UCDU", 117);
        
        // NATS - Nucleo de Apoio Tecnico e de Secretariado
        departmentMapping.put("NATS", 118);

        // NEA - Nucleo de Expediente e Arquivo
        departmentMapping.put("NEA", 119);

        // GAB-R - Gabinete da Reitora
        departmentMapping.put("GAB-R", 120);

        // EM - Equipa de missao
        departmentMapping.put("EM", 121);
        departmentMapping.put("EM-AC", 121);
        departmentMapping.put("EM-EIP", 121);
        departmentMapping.put("EM-DCD", 121);
        departmentMapping.put("EM-QVDI", 121);
        departmentMapping.put("EM-ERSEARECE", 121);
        departmentMapping.put("EM-ERSE", 121);
        departmentMapping.put("EM-RE", 121);
        departmentMapping.put("EM-ISAUDE", 121);

        // LLCT - Laboratório de Línguas e Competências Transversais
        departmentMapping.put("LLCT", 122);
        departmentMapping.put("LCT", 122);
        
        // Centro de Emergencia COVID
        departmentMapping.put("CE-COVID", 123);
    }

    private static void loadOrgaoIscteSiglaMapping() {
        orgaoIscteSiglaMapping.put(1, "DepPsico");
        orgaoIscteSiglaMapping.put(2, "DepEconomia");
        orgaoIscteSiglaMapping.put(3, "SecHistoria");
        orgaoIscteSiglaMapping.put(4, "adetti");
        orgaoIscteSiglaMapping.put(5, "unide");
        orgaoIscteSiglaMapping.put(6, "unics");
        orgaoIscteSiglaMapping.put(7, "ceas");   // CRIA
        orgaoIscteSiglaMapping.put(8, "cemaf");
        orgaoIscteSiglaMapping.put(9, "cet");
        orgaoIscteSiglaMapping.put(10, "cis");
        orgaoIscteSiglaMapping.put(11, "cies");
        orgaoIscteSiglaMapping.put(12, "dinamia");
        orgaoIscteSiglaMapping.put(13, "giem");
        orgaoIscteSiglaMapping.put(14, "overgest");
        orgaoIscteSiglaMapping.put(15, "INDEG");   // DI
        orgaoIscteSiglaMapping.put(16, "PROACT");
        orgaoIscteSiglaMapping.put(17, "CEHC");    // CEHCP
        orgaoIscteSiglaMapping.put(18, "Urban");
        orgaoIscteSiglaMapping.put(19, "giesta");
        orgaoIscteSiglaMapping.put(20, "cea");
        orgaoIscteSiglaMapping.put(21, "SecDireito");
        orgaoIscteSiglaMapping.put(22, "DepAntrop");
        orgaoIscteSiglaMapping.put(23, "DepSocio");
        orgaoIscteSiglaMapping.put(24, "DepMetodos");
        orgaoIscteSiglaMapping.put(25, "DepGestao");
        orgaoIscteSiglaMapping.put(26, "DepCTI");
        orgaoIscteSiglaMapping.put(27, "DepFinancas");
        orgaoIscteSiglaMapping.put(28, "AreaEA");
        orgaoIscteSiglaMapping.put(29, "DAU"); // AreaArquitectura
        orgaoIscteSiglaMapping.put(30, "ciiscte");
        orgaoIscteSiglaMapping.put(31, "div");
        orgaoIscteSiglaMapping.put(32, "GRI");     // NRI
        orgaoIscteSiglaMapping.put(33, "GIRE");    // GMIVA
        orgaoIscteSiglaMapping.put(34, "inesla");
        orgaoIscteSiglaMapping.put(35, "AAAISCTE");
        orgaoIscteSiglaMapping.put(36, "AEDG");
        orgaoIscteSiglaMapping.put(37, "convidados");
        orgaoIscteSiglaMapping.put(38, "observa");
        orgaoIscteSiglaMapping.put(39, "CCL");
        orgaoIscteSiglaMapping.put(40, "DSBD");    // SID; UIF; NB
        orgaoIscteSiglaMapping.put(41, "CAV");
        orgaoIscteSiglaMapping.put(43, "GESP");    // GIP
        orgaoIscteSiglaMapping.put(44, "DSRH");    // URH, SFPRH; NGT; NCO; NC
        orgaoIscteSiglaMapping.put(45, "GARE");    // NT; NAG
        orgaoIscteSiglaMapping.put(46, "DSA");     // SA; UEG; SGE
        orgaoIscteSiglaMapping.put(48, "DSFP");    // DSAD; UF; GPRF; SC; SEP; UER; UPC
        orgaoIscteSiglaMapping.put(49, "Administracao");
        orgaoIscteSiglaMapping.put(51, "GPGA");
        orgaoIscteSiglaMapping.put(53, "CC");
        orgaoIscteSiglaMapping.put(54, "CP");
        orgaoIscteSiglaMapping.put(55, "SSH");
        orgaoIscteSiglaMapping.put(56, "app");
        orgaoIscteSiglaMapping.put(57, "PFPP2013");
        orgaoIscteSiglaMapping.put(58, "SP");           // GPresidente; GA; NRIA; GAR
        orgaoIscteSiglaMapping.put(59, "audax");    // Centro de Investigação em Empreendedorismo e Empresas Familiares
        orgaoIscteSiglaMapping.put(60, "GPA");
        orgaoIscteSiglaMapping.put(61, "gestin");
        orgaoIscteSiglaMapping.put(62, "CIFFA");   // Fundação
        orgaoIscteSiglaMapping.put(63, "editores");
        orgaoIscteSiglaMapping.put(64, "CRESP");
        orgaoIscteSiglaMapping.put(65, "cco.iscte");
        orgaoIscteSiglaMapping.put(66, "segurancas.iscte");
        orgaoIscteSiglaMapping.put(67, "SARTRE4");
        orgaoIscteSiglaMapping.put(68, "gaq.iscte"); // GAQE; GEAPQ; NPQ; NEA
        orgaoIscteSiglaMapping.put(69, "gep.iscte");
        orgaoIscteSiglaMapping.put(70, "inout.iscte");
        orgaoIscteSiglaMapping.put(71, "iscte"); // ISCTE; ISCTE-IUL
        orgaoIscteSiglaMapping.put(72, "EQUAL");
        orgaoIscteSiglaMapping.put(73, "MRC");
        orgaoIscteSiglaMapping.put(74, "df");
        orgaoIscteSiglaMapping.put(75, "dc");
        orgaoIscteSiglaMapping.put(76, "clube");
        orgaoIscteSiglaMapping.put(77, "LAPSO");
        orgaoIscteSiglaMapping.put(78, "GGC");
        orgaoIscteSiglaMapping.put(79, "GPDGA");
        orgaoIscteSiglaMapping.put(80, "PROJ");  // Projectos da FCT, GAI Projectos
        orgaoIscteSiglaMapping.put(81, "DRHCO");
        orgaoIscteSiglaMapping.put(82, "DMOG");
        orgaoIscteSiglaMapping.put(83, "SEA");
        orgaoIscteSiglaMapping.put(84, "SAS");
        orgaoIscteSiglaMapping.put(85, "GAI");
        orgaoIscteSiglaMapping.put(86, "DEP");
        orgaoIscteSiglaMapping.put(87, "GCI");
        orgaoIscteSiglaMapping.put(88, "DCPPP");
        orgaoIscteSiglaMapping.put(89, "ECSH");
        orgaoIscteSiglaMapping.put(90, "IBS");
        orgaoIscteSiglaMapping.put(91, "ESPP");
        orgaoIscteSiglaMapping.put(92, "ISTA");

        orgaoIscteSiglaMapping.put(93, "IPPS");
        orgaoIscteSiglaMapping.put(94, "IUL-G");
        orgaoIscteSiglaMapping.put(95, "DMPS");
        orgaoIscteSiglaMapping.put(96, "RPJPP");

        orgaoIscteSiglaMapping.put(97, "DIPPS");
        orgaoIscteSiglaMapping.put(98, "DM");
        orgaoIscteSiglaMapping.put(99, "DMQGE");

        orgaoIscteSiglaMapping.put(100, "CEI-IUL");
        orgaoIscteSiglaMapping.put(101, "IT-IUL");

        orgaoIscteSiglaMapping.put(102, "GAOU");
        orgaoIscteSiglaMapping.put(103, "GCSA");
        orgaoIscteSiglaMapping.put(104, "GDSI");
        orgaoIscteSiglaMapping.put(105, "LAV");
        orgaoIscteSiglaMapping.put(106, "LCC");
        orgaoIscteSiglaMapping.put(107, "LDCTI");
        orgaoIscteSiglaMapping.put(108, "LFABLAB");

        orgaoIscteSiglaMapping.put(109, "CGERAL");
        orgaoIscteSiglaMapping.put(110, "CCURA");

        orgaoIscteSiglaMapping.put(111, "ISTAR-IUL");
        orgaoIscteSiglaMapping.put(112, "BGI");

        orgaoIscteSiglaMapping.put(113, "REITORIA");
        orgaoIscteSiglaMapping.put(114, "NCT");
        orgaoIscteSiglaMapping.put(115, "NL");
        orgaoIscteSiglaMapping.put(116, "ADMIN");

        orgaoIscteSiglaMapping.put(117, "UGDU");
        orgaoIscteSiglaMapping.put(118, "NATS");
        orgaoIscteSiglaMapping.put(119, "NEA");
        orgaoIscteSiglaMapping.put(120, "GAB-R");
        orgaoIscteSiglaMapping.put(121, "EM");
        orgaoIscteSiglaMapping.put(122, "LLCT");   // LCT
        orgaoIscteSiglaMapping.put(123, "CE-COVID");
    }

    public static Integer getDSIDepartment(String key) {
        return departmentMapping.get(key);
    }

    public static String getSiglaOrgaoIscte(Integer key) {
        return orgaoIscteSiglaMapping.get(key);
    }
}