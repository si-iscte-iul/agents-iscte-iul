package pt.iscte.dsi.webclient.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import pt.iscte.dsi.webclient.dto.Utilizador_iscte;

/**
 * 
 * @author Paulo Zenida
 */
public final class Utilizador_iscteDataLoaderFromDatabase<T> extends BaseDataLoaderFromDatabase<T> {

    protected final static Logger logger = Logger.getLogger(DatabaseManager.class);

    public String getDatabaseFilename() {
        return "configuration.properties";
    }

    public void executeUpdate(Utilizador_iscte uIscte) {
        connectDatabase();
        try {
            PreparedStatement pstm = con.prepareStatement("UPDATE utilizador_iscte SET nome = ? WHERE login = ?");

            pstm.setBytes(1, uIscte.getNome().getBytes());
            pstm.setString(2, uIscte.getLogin());

            pstm.executeUpdate();

            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
    }

    public void executeUpdate(final List<String> sqlStatements) {
        connectDatabase();

        String sqlDebug = null;
        try {
            stm = con.createStatement();
            for (final String sqlStatement : sqlStatements) {
                sqlDebug = sqlStatement;
                stm.executeUpdate(sqlStatement);
            }
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            System.out.println(sqlDebug);
            logger.error("Exception: " + e);
        }
    }

    public boolean executeQuery(String sqlStatement) {
        boolean searchedRecordAlreadyExistsInDatabase = false;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);
            int i = 0;

            while (rs.next()) {
                ++i;
            }

            if (i > 0) {
                searchedRecordAlreadyExistsInDatabase = true;
            }

            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return searchedRecordAlreadyExistsInDatabase;
    }

    public int getLastAttributeValueFromTable(final String sqlStatement) {
        int lastIdInternal = 0;;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);

            while (rs.next()) {
                lastIdInternal = rs.getInt(1);
            }
            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return lastIdInternal;
    }
}