package pt.iscte.dsi.webclient.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import pt.iscte.dsi.webclient.dto.Aluno;

/**
 * 
 * @author Paulo Zenida and maintained by António Casqueiro
 */
public final class AlunoDataLoaderFromDatabase<T> extends BaseDataLoaderFromDatabase<T> {

    protected final static Logger logger = Logger.getLogger(DatabaseManager.class);

    private static String INSERT_STATEMENT = null;
    static {
        StringBuilder sb = new StringBuilder(1000);
        sb.append("INSERT INTO aluno (");
        sb.append("nome,");
        sb.append("nome_usado,");
        sb.append("nomes_proprios,");
        sb.append("nomes_proprios_usados,");
        sb.append("apelidos,");
        sb.append("apelidos_usados,");
        sb.append("login,");
        sb.append("e_mail_iscte,");
        sb.append("e_mail_pessoal,");
        sb.append("telefone,");
        sb.append("telemovel,");
        sb.append("activo,");
        sb.append("obs,");
        sb.append("bi,");
        sb.append("nt,");
        sb.append("mail,");
        sb.append("www,");
        sb.append("linux,");
        sb.append("password,");
        sb.append("password_p,");
        sb.append("tipo_matricula,");
        sb.append("ano,");
        sb.append("cod_area,");
        sb.append("data,");
        sb.append("autorizado,");
        sb.append("hora,");
        sb.append("util,");
        sb.append("insc_data,");
        sb.append("autor_data,");
        sb.append("n_aluno,");
        sb.append("elearning,");
        sb.append("msdnaa,");
        sb.append("msdnaa_autorizacao,");
        sb.append("r_e_mail,");
        sb.append("data_alteracao,");
        sb.append("nome_ldap");
        sb.append(") ");
        sb.append("VALUES (");
        sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ");
        sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ");
        sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ");
        sb.append("?, ?, ?, ?, ?, ?");
        sb.append(");");
        INSERT_STATEMENT = sb.toString();
    }

    private static String UPDATE_STATEMENT = null;
    static {
        StringBuilder sb = new StringBuilder(1000);
        sb.append("UPDATE aluno SET ");
        sb.append("nome=?,");
        sb.append("nome_usado=?,");
        sb.append("nomes_proprios=?,");
        sb.append("nomes_proprios_usados=?,");
        sb.append("apelidos=?,");
        sb.append("apelidos_usados=?,");
        sb.append("activo=?,");
        sb.append("obs=?,");
        sb.append("bi=?,");
        sb.append("nt=?,");
        sb.append("mail=?,");
        sb.append("www=?,");
        sb.append("linux=?,");
        sb.append("tipo_matricula=?,");
        sb.append("ano=?,");
        sb.append("cod_area=?,");
        sb.append("autorizado=?,");
        sb.append("hora=?,");
        sb.append("util=?,");
        sb.append("insc_data=?,");
        sb.append("autor_data=?,");
        sb.append("n_aluno=?,");
        sb.append("elearning=?,");
        sb.append("msdnaa=?,");
        sb.append("msdnaa_autorizacao=?,");
        sb.append("r_e_mail=?,");
        sb.append("data_alteracao=?");
        sb.append(" WHERE ");
        sb.append("login=? AND cod_area=?;");
        UPDATE_STATEMENT = sb.toString();
    }

    public String getDatabaseFilename() {
        return "configuration.properties";
    }

    public void executeUpdate(Aluno aluno) {
        connectDatabase();
        try {
            PreparedStatement pstm = con.prepareStatement("UPDATE aluno SET nome = ? WHERE login = ?");

            pstm.setBytes(1, aluno.getNome().getBytes());
            pstm.setString(2, aluno.getLogin());

            pstm.executeUpdate();

            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
    }

    public void executeUpdate(final List<String> sqlStatements) {
        connectDatabase();

        String sqlDebug = null;
        try {
            stm = con.createStatement();
            for (final String sqlStatement : sqlStatements) {
                sqlDebug = sqlStatement;
                stm.executeUpdate(sqlStatement);
            }
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            System.out.println(sqlDebug);
            logger.error("Exception: " + e);
        }
    }

    public boolean executeQuery(String sqlStatement) {
        boolean searchedRecordAlreadyExistsInDatabase = false;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);
            int i = 0;

            while (rs.next()) {
                ++i;
            }

            if (i > 0) {
                searchedRecordAlreadyExistsInDatabase = true;
            }

            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return searchedRecordAlreadyExistsInDatabase;
    }

    public int getLastAttributeValueFromTable(final String sqlStatement) {
        int lastIdInternal = 0;;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);

            while (rs.next()) {
                lastIdInternal = rs.getInt(1);
            }
            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return lastIdInternal;
    }

    public boolean executeUpdatePs(Aluno aluno) {
        connectDatabase();
        int i = 0;
        try {
            PreparedStatement pstm = con.prepareStatement(UPDATE_STATEMENT);

            pstm.setString(i++, aluno.getNome());
            pstm.setString(i++, aluno.getNome_usado());
            pstm.setString(i++, aluno.getNomes_proprios());
            pstm.setString(i++, aluno.getNomes_proprios_usados());
            pstm.setString(i++, aluno.getApelidos());
            pstm.setString(i++, aluno.getApelidos_usados());
            pstm.setBoolean(i++, aluno.isActivo());
            pstm.setString(i++, aluno.getObs());
            pstm.setString(i++, aluno.getBi());
            pstm.setInt(i++, aluno.getNt());
            pstm.setInt(i++, aluno.getMail());
            pstm.setInt(i++, aluno.getWww());
            pstm.setInt(i++, aluno.getLinux());
            pstm.setInt(i++, aluno.getTipo_matricula());
            pstm.setInt(i++, aluno.getAno());
            pstm.setInt(i++, aluno.getCod_area());
            pstm.setBoolean(i++, aluno.isAutorizado());
            pstm.setString(i++, aluno.getHora());
            pstm.setString(i++, aluno.getUtil());
            pstm.setString(i++, aluno.getInsc_data());
            pstm.setString(i++, aluno.getAutor_data());
            pstm.setString(i++, aluno.getN_aluno());
            pstm.setInt(i++, aluno.getElearning());
            pstm.setInt(i++, aluno.getMsdnaa());
            pstm.setString(i++, aluno.getMsdnaa_autorizacao());
            pstm.setInt(i++, aluno.getR_e_mail());
            pstm.setString(i++, aluno.getData_alteracao());

            // WHERE
            pstm.setString(i++, aluno.getLogin());
            pstm.setInt(i++, aluno.getCod_area());

            int changed = pstm.executeUpdate();

            stm.close();
            closeConnection();
            return changed >= 1;
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
        return false;
    }

    public boolean executeInsertPs(Aluno aluno) {
        connectDatabase();
        int i = 0;
        try {
            PreparedStatement pstm = con.prepareStatement(INSERT_STATEMENT);

            pstm.setString(i++, aluno.getNome());
            pstm.setString(i++, aluno.getNome_usado());
            pstm.setString(i++, aluno.getNomes_proprios());
            pstm.setString(i++, aluno.getNomes_proprios_usados());
            pstm.setString(i++, aluno.getApelidos());
            pstm.setString(i++, aluno.getApelidos_usados());
            pstm.setString(i++, aluno.getLogin());
            pstm.setString(i++, aluno.getE_mail_iscte());
            pstm.setString(i++, aluno.getE_mail_pessoal());
            pstm.setString(i++, aluno.getTelefone());
            pstm.setString(i++, aluno.getTelemovel());
            pstm.setBoolean(i++, aluno.isActivo());
            pstm.setString(i++, aluno.getObs());
            pstm.setString(i++, aluno.getBi());
            pstm.setInt(i++, aluno.getNt());
            pstm.setInt(i++, aluno.getMail());
            pstm.setInt(i++, aluno.getWww());
            pstm.setInt(i++, aluno.getLinux());
            pstm.setString(i++, "cipasswd");
            pstm.setString(i++, aluno.getPassword_p());
            pstm.setInt(i++, aluno.getTipo_matricula());
            pstm.setInt(i++, aluno.getAno());
            pstm.setInt(i++, aluno.getCod_area());
            pstm.setString(i++, aluno.getData());
            pstm.setBoolean(i++, aluno.isAutorizado());
            pstm.setString(i++, aluno.getHora());
            pstm.setString(i++, aluno.getUtil());
            pstm.setString(i++, aluno.getInsc_data());
            pstm.setString(i++, aluno.getAutor_data());
            pstm.setString(i++, aluno.getN_aluno());
            pstm.setInt(i++, aluno.getElearning());
            pstm.setInt(i++, aluno.getMsdnaa());
            pstm.setString(i++, aluno.getMsdnaa_autorizacao());
            pstm.setInt(i++, aluno.getR_e_mail());
            pstm.setString(i++, aluno.getData_alteracao());
            pstm.setString(i++, aluno.getNome_ldap());

            int changed = pstm.executeUpdate();

            stm.close();
            closeConnection();
            return changed >= 1;
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
        return false;
    }
}