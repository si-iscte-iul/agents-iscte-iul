package pt.iscte.dsi.webclient.utils;

/**
 * Esta classe serve para guardar subquerys SQL correspondentes a um parâmetro IN.
 */
final class InQueryInformation {

    /**
     * Devolve o tamanho que ocupa a clausula IN.
     * 
     * @param column nome da coluna sobre o qual se vai fazer o IN
     * @param isString true caso os valores a incluir dentro do IN sejam strings
     * @param textValuesRepresentation representação dos valores a incluir na cláusula IN em texto
     * @param in true caso seja para IN, false caso seja IN NOT
     * @return o tamanho que ocupa a cláusula IN
     */
    static int length(String column, boolean isString, String[] textValuesRepresentation, boolean in) {
        // Length da coluna + " IN (" + ")" + "," dentro do IN
        int length = column.length() + (in ? 6 : 10) + textValuesRepresentation.length - 1;

        // Length do texto dentro do IN
        for (int i = 0; i < textValuesRepresentation.length; i++) {
            length += textValuesRepresentation[i].length();
        }

        if (isString) {
            // Length das plicas dentro do IN
            length += textValuesRepresentation.length * 2;
        }

        return length;
    }

    /**
     * Adiciona a cláusula IN.
     * 
     * @param sb onde se irá adicionar a cláusula IN
     * @param column nome da coluna sobre o qual se vai fazer o IN
     * @param isString true caso os valores a incluir dentro do IN sejam strings
     * @param textValuesRepresentation representação dos valores a incluir na cláusula IN em texto
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    static void addQuery(StringBuffer sb, String column, boolean isString, String[] textValuesRepresentation, boolean in) {
        sb.append(column).append(in ? " IN (" : " NOT IN (");

        if (isString) {
            sb.append('\'').append(textValuesRepresentation[0]).append('\'');

            for (int i = 1; i < textValuesRepresentation.length; i++) {
                sb.append(",'").append(textValuesRepresentation[i]).append('\'');
            }
        } else {
            sb.append(textValuesRepresentation[0]);

            for (int i = 1; i < textValuesRepresentation.length; i++) {
                sb.append(",").append(textValuesRepresentation[i]);
            }
        }

        sb.append(")");
    }
}
