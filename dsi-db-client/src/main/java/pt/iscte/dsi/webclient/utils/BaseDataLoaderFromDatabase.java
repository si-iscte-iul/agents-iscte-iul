package pt.iscte.dsi.webclient.utils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public abstract class BaseDataLoaderFromDatabase<T> extends DatabaseManager {

    protected final static Logger logger = Logger.getLogger(DatabaseManager.class);

    /**
     * This method should not be used for a MS SQL Server database.
     * 
     * @param clazz
     * @param sql
     * @return
     */
    public Collection<T> load(Class<T> clazz, String sql) {
        Collection<T> result = new ArrayList<T>();
        try {
            connectDatabase();
            stm = con.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                T t = instanciateType(clazz);
                boolean shouldAdd = ((IDatabaseRowLine) t).fillWithDatabaseRowData(rs);
                if (shouldAdd)
                    result.add(t);
            }
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
        return result;
    }

    public Collection<T> loadForMsSQLServer(Class<T> clazz, String sql) {
        Collection<T> result = new ArrayList<T>();
        try {
            connectDatabase(databaseUrl, databaseUsername, databasePassword);
            stm = con.createStatement();
            rs = stm.executeQuery(sql);
            while (rs.next()) {
                T t = instanciateType(clazz);
                boolean shouldAdd = ((IDatabaseRowLine) t).fillWithDatabaseRowData(rs);
                if (shouldAdd)
                    result.add(t);
            }
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
        return result;
    }

    private T instanciateType(Class<T> clazz) {
        T t = null;
        try {
            t = clazz.newInstance();
        } catch (InstantiationException e) {
            logger.fatal(e);
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            logger.fatal(e);
            throw new RuntimeException(e);
        }
        return t;
    }
}