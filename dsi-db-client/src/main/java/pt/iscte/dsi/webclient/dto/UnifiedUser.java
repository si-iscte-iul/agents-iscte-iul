package pt.iscte.dsi.webclient.dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import pt.iscte.dsi.webclient.utils.IDatabaseRowLine;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public abstract class UnifiedUser implements IDatabaseRowLine {

    private String nome;

    private String nome_usado;

    private String nomes_proprios;

    private String nomes_proprios_usados;

    private String apelidos;

    private String apelidos_usados;

    private String login;

    private String telefone;

    private String telemovel;

    private String e_mail_pessoal;

    private String e_mail_iscte;

    private String nome_ldap;

    private String gid;

    private String uid;

    /**
     * @return the apelidos
     */
    public String getApelidos() {
        return apelidos;
    }

    /**
     * @param apelidos
     *            the apelidos to set
     */
    public void setApelidos(String apelidos) {
        this.apelidos = apelidos;
    }

    /**
     * @return the apelidos_usados
     */
    public String getApelidos_usados() {
        return apelidos_usados;
    }

    /**
     * @param apelidos_usados
     *            the apelidos_usados to set
     */
    public void setApelidos_usados(String apelidos_usados) {
        this.apelidos_usados = apelidos_usados;
    }

    /**
     * @return the e_mail_iscte
     */
    public String getE_mail_iscte() {
        return e_mail_iscte;
    }

    /**
     * @param e_mail_iscte
     *            the e_mail_iscte to set
     */
    public void setE_mail_iscte(String e_mail_iscte) {
        this.e_mail_iscte = e_mail_iscte;
    }

    /**
     * @return the e_mail_pessoal
     */
    public String getE_mail_pessoal() {
        return e_mail_pessoal;
    }

    /**
     * @param e_mail_pessoal
     *            the e_mail_pessoal to set
     */
    public void setE_mail_pessoal(String e_mail_pessoal) {
        this.e_mail_pessoal = e_mail_pessoal;
    }

    /**
     * @return the gid
     */
    public String getGid() {
        return gid;
    }

    /**
     * @param gid
     *            the gid to set
     */
    public void setGid(String gid) {
        this.gid = gid;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login
     *            the login to set
     */
    public void setLogin(String login) {
        // Removed by ajsco 2008-09-23
        // In the context of the DSI Web Service client, the
        // suffix @iscte should not be added.
        //	this.login = (login.contains("@iscte") ? login : login + "@iscte.pt");

        this.login = login;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome
     *            the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the nome_ldap
     */
    public String getNome_ldap() {
        return nome_ldap;
    }

    /**
     * @param nome_ldap
     *            the nome_ldap to set
     */
    public void setNome_ldap(String nome_ldap) {
        this.nome_ldap = nome_ldap;
    }

    /**
     * @return the nome_usado
     */
    public String getNome_usado() {
        return nome_usado;
    }

    /**
     * @param nome_usado
     *            the nome_usado to set
     */
    public void setNome_usado(String nome_usado) {
        this.nome_usado = nome_usado;
    }

    /**
     * @return the nomes_proprios
     */
    public String getNomes_proprios() {
        return nomes_proprios;
    }

    /**
     * @param nomes_proprios
     *            the nomes_proprios to set
     */
    public void setNomes_proprios(String nomes_proprios) {
        this.nomes_proprios = nomes_proprios;
    }

    /**
     * @return the nomes_proprios_usados
     */
    public String getNomes_proprios_usados() {
        return nomes_proprios_usados;
    }

    /**
     * @param nomes_proprios_usados
     *            the nomes_proprios_usados to set
     */
    public void setNomes_proprios_usados(String nomes_proprios_usados) {
        this.nomes_proprios_usados = nomes_proprios_usados;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone
     *            the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the telemovel
     */
    public String getTelemovel() {
        return telemovel;
    }

    /**
     * @param telemovel
     *            the telemovel to set
     */
    public void setTelemovel(String telemovel) {
        this.telemovel = telemovel;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid
     *            the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @param activo
     *            the activo to set
     */
    public abstract void setActivo(boolean activo);

    /**
     * @return the activo
     */
    public abstract boolean isActivo();

    /**
     * @return the bi
     */
    public abstract String getBi();

    /**
     * @param bi
     *            the bi to set
     */
    public abstract void setBi(String bi);

    /**
     * @return the obs
     */
    public abstract String getObs();

    /**
     * @param obs
     *            the obs to set
     */
    public abstract void setObs(String obs);

    public boolean fillWithDatabaseRowData(ResultSet rs) throws SQLException {
        setNome(StringUtils.getStringInUtf8Format(rs.getBytes("nome")));
        setNome_usado(StringUtils.getStringInUtf8Format(rs.getBytes("nome_usado")));
        setNomes_proprios(StringUtils.getStringInUtf8Format(rs.getBytes("nomes_proprios")));
        setNomes_proprios_usados(StringUtils.getStringInUtf8Format(rs.getBytes("nomes_proprios_usados")));
        setApelidos(StringUtils.getStringInUtf8Format(rs.getBytes("apelidos")));
        setApelidos_usados(StringUtils.getStringInUtf8Format(rs.getBytes("apelidos_usados")));
        setLogin(StringUtils.getStringInUtf8Format(rs.getBytes("login")));
        setTelefone(StringUtils.getStringInUtf8Format(rs.getBytes("telefone")));
        setTelemovel(StringUtils.getStringInUtf8Format(rs.getBytes("telemovel")));
        setE_mail_pessoal(StringUtils.getStringInUtf8Format(rs.getBytes("e_mail_pessoal")));
        setE_mail_iscte(StringUtils.getStringInUtf8Format(rs.getBytes("e_mail_iscte")));
        setNome_ldap(StringUtils.getStringInUtf8Format(rs.getBytes("nome_ldap")));
        setGid(StringUtils.getStringInUtf8Format(rs.getBytes("gid")));
        setUid(StringUtils.getStringInUtf8Format(rs.getBytes("uid")));
        return true;
    }

    public String getUniqueKey() {
        return getLogin();
    }

    @Override
    public String toString() {
        return "UNIFIED USER: Login=" + getLogin() + ",Name=" + getNome() + ",BI=" + getBi() + ",E-mail ISCTE="
                + getE_mail_iscte();
    }
}