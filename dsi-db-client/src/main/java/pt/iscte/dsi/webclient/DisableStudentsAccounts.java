package pt.iscte.dsi.webclient;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import pt.iscte.agents.dsiDb.DsiStudentDataService;
import pt.iscte.agents.dsiDb.StudentData;
import pt.iscte.dsi.webclient.dto.Aluno;
import pt.iscte.dsi.webclient.dto.Utilizador_iscte;
import pt.iscte.dsi.webclient.utils.AlunoDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.MappingDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.Utilizador_iscteDataLoaderFromDatabase;
import pt.iscte.dsi.webclient.utils.fenixLike.LogSender;
import pt.iscte.dsi.webclient.utils.fenixLike.Pair;
import pt.iscte.dsi.webclient.utils.fenixLike.PropertiesManager;

/**
 * This class updates or deletes records from aluno table in the ciiscte_wp
 * database, in order to remove a student account.<br/>
 * It calls a Fénix webservice to get the data.
 * 
 * @author António Casqueiro
 */
public class DisableStudentsAccounts {
    private final static Logger logger = Logger.getLogger(DisableStudentsAccounts.class);

    private static final boolean COMMIT = true;

    private static final SimpleDateFormat sdfSmallDate = new SimpleDateFormat("yyMMdd");

    private Collection<Aluno> listaAlunos = null;
    private Collection<Utilizador_iscte> listaUtilizador_iscte = null;
    private Collection<Aluno> listaAlunosUpdate = null;
    private Collection<Aluno> listaAlunosDelete = null;

    private Date sysdate;

    private List<Pair<StudentData, String>> errors;
    private List<Pair<StudentData, String>> warnings;

    private static final String ERROR_USER_WITHOUT_USERNAME = "User without username";
    private static final String ERROR_INVALID_USERNAME = "Invalid username";

    private List<Pair<Aluno, String>> actions;
    private static final String ACTION_UPDATE = "U";
    private static final String ACTION_DELETE = "D";

    private AlunoDataLoaderFromDatabase<Aluno> loaderStudent = null;
    private Utilizador_iscteDataLoaderFromDatabase<Utilizador_iscte> loaderNotStudent = null;

    @SuppressWarnings("rawtypes")
    private MappingDataLoaderFromDatabase database = null;

    @SuppressWarnings("rawtypes")
    public DisableStudentsAccounts() {
        super();
        this.loaderStudent = new AlunoDataLoaderFromDatabase<Aluno>();
        this.loaderNotStudent = new Utilizador_iscteDataLoaderFromDatabase<Utilizador_iscte>();

        this.listaAlunosDelete = new ArrayList<Aluno>();
        this.listaAlunosUpdate = new ArrayList<Aluno>();

        this.actions = new ArrayList<Pair<Aluno, String>>();
        this.errors = new ArrayList<Pair<StudentData, String>>();
        this.warnings = new ArrayList<Pair<StudentData, String>>();

        database = new MappingDataLoaderFromDatabase();
        database.connectDatabase();
    }

    private boolean isToProcess(String username) {
        // return filter.get(username) != null;
        // return "dhcoh@iscte.pt".equals(username);
        return true;
    }

    // ------------------------------------------------------------------------
    // FILTER RECORDS CODE - end
    // ------------------------------------------------------------------------

    public static void main(String[] args) throws RemoteException, UnsupportedEncodingException {
        DsiStudentDataService service = DsiStudentDataServiceUtil.getService();

        int start = getStartValue();
        final int STEP = getStepValue();
        final int STOP = getStopValue();

        List<StudentData> list = null;
        DisableStudentsAccounts agent = new DisableStudentsAccounts();
        agent.loadAll();

        try {
            String[] staticList = getStaticList();
            if (staticList != null && staticList.length > 0) {
                agent.updateSysdate();
                for (String elem : staticList) {
                    String username = elem.trim();
                    logger.info("Processing: " + username);
                    StudentData studentData = service.getOneByUsernameStudentData(username);
                    agent.process(studentData);
                }

                // COMMIT changes
                agent.commit();
                agent.cleanUp();
            } else {
                do {
                    int end = start + STEP;
                    logger.info("(" + start + "," + end + ")");
                    list = buildList(service.getInactiveStudentsInInterval(start, end));
                    start += STEP + 1;

                    agent.updateSysdate();
                    for (StudentData studentData : list) {
                        agent.process(studentData);
                    }

                    // COMMIT changes
                    agent.commit();
                    agent.cleanUp();
                } while (start < STOP);
                logger.info("The END");
            }

            // Report
            agent.report();
        } finally {
            agent.releaseDatabaseConnection();
        }
    }

    private static List<StudentData> buildList(List<StudentData> interval) {
        if (interval == null || interval.size() == 0) {
            return new ArrayList<StudentData>(0);
        }

        return interval;
    }

    private void releaseDatabaseConnection() {
        database.closeConnection();
    }

    private void report() throws UnsupportedEncodingException {
        final int countOK = actions.size();
        final int countErrors = errors.size();

        // Build message
        String loggingMessage = buildMessage();

        // Send email
        int totalRecords = countOK + countErrors;
        LogSender.sendLoggingByEmail("Webservice client DisableStudentsAccounts", countErrors == 0, countErrors, countOK,
                totalRecords, loggingMessage);

        logger.info(loggingMessage);
    }

    private String buildMessage() {
        StringBuilder sb = new StringBuilder();

        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Errors"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Error"));
        for (Pair<StudentData, String> error : errors) {
            StudentData studentData = error.getFirst();
            String msg = error.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getName(), studentData.getIdDocumentNumber(),
                    studentData.getUsername(), msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Warnings"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Warning"));
        for (Pair<StudentData, String> warning : warnings) {
            StudentData studentData = warning.getFirst();
            String msg = warning.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getName(), studentData.getIdDocumentNumber(),
                    studentData.getUsername(), msg));
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Actions performed"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Name", "Document ID", "Username", "Action"));
        int countDeleted = 0;
        int countUpdated = 0;
        for (Pair<Aluno, String> action : actions) {
            Aluno studentData = action.getFirst();
            String actionPerformed = action.getSecond();
            sb.append(LogSender.getEmailLineFromArgumentsInTsv(studentData.getNome(), studentData.getBi(),
                    studentData.getLogin(), actionPerformed));

            if (ACTION_DELETE.equals(actionPerformed)) {
                countDeleted++;
            }
            if (ACTION_UPDATE.equals(actionPerformed)) {
                countUpdated++;
            }
        }

        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv(""));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Statistics"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv("Deleted", "Updated", "Total"));
        sb.append(LogSender.getEmailLineFromArgumentsInTsv(countDeleted, countUpdated, countDeleted + countUpdated));

        return sb.toString();
    }

    private static int getStopValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.stop");
    }

    private static int getStepValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.step");
    }

    private static int getStartValue() {
        return PropertiesManager.getIntegerProperty("DsiStudentIntegration.start");
    }

    // private static String getEndpointAddress() {
    // String p =
    // PropertiesManager.getProperty("DsiStudentIntegration.endpointAddress");
    // if (p != null && p.trim().length() > 0) {
    // return p.trim();
    // }
    // return null;
    // }

    private void updateSysdate() {
        sysdate = new Date();
    }

    private void loadAll() {
        listaAlunos = loaderStudent.load(Aluno.class, "SELECT * FROM aluno");
        listaUtilizador_iscte =
                loaderNotStudent.load(Utilizador_iscte.class,
                        "SELECT * FROM utilizador_iscte ui, orgao_iscte oi WHERE ui.orgao_iscte = oi.id_orgao_iscte");
    }

    private void process(StudentData studentData) {
        if (studentData.getUsername() == null) {
            errors.add(new Pair<StudentData, String>(studentData, ERROR_USER_WITHOUT_USERNAME));
            return;
        }

        if (!studentData.getUsername().trim().endsWith("@iscte.pt")) {
            errors.add(new Pair<StudentData, String>(studentData, ERROR_INVALID_USERNAME));
            return;
        }

        // Filter by data in Fénix
        final String username = studentData.getUsername();
        if (!isToProcess(username)) {
            // Skip
            return;
        }

        // 1- Check if student exists, by:
        // * Login
        Aluno aluno = findStudentByUsername(username);

        if (aluno == null) {
            // Skip
            return;
        }

        // 2- Check if student exists as a non student
        Utilizador_iscte utilizador_iscte = findNonStudentByUsername(username);

        // 3- Update if its only a student, delete otherwise.
        boolean isToDelete = false;
        if (utilizador_iscte == null) {
            if (aluno.isActivo()) {
                addUpdate(aluno);
            } else {
                // Skip
                return;
            }
        } else {
            addDelete(aluno);
            isToDelete = true;
        }

        // Feedback
        logger.debug((isToDelete ? "D" : "U") + ": Processed " + studentData.getName() + " | id document: "
                + studentData.getIdDocumentNumber() + " | username: " + studentData.getUsername());
    }

    private void addDelete(Aluno aluno) {
        listaAlunosDelete.add(aluno);
        listaAlunos.add(aluno);
        actions.add(new Pair<Aluno, String>(aluno, ACTION_DELETE));
    }

    private void addUpdate(Aluno aluno) {
        listaAlunosUpdate.add(aluno);
        actions.add(new Pair<Aluno, String>(aluno, ACTION_UPDATE));
    }

    private Aluno findStudentByUsername(String username) {
        for (Aluno aluno : listaAlunos) {
            if (aluno.getLogin().equals(getLoginForDSI(username))) {
                return aluno;
            }
        }
        return null;
    }

    private Utilizador_iscte findNonStudentByUsername(String username) {
        for (Utilizador_iscte utilizador_iscte : listaUtilizador_iscte) {
            if (utilizador_iscte.getLogin().equals(getLoginForDSI(username))) {
                return utilizador_iscte;
            }
        }
        return null;
    }

    private void commit() {
        List<String> sqlStatements = new ArrayList<String>(listaAlunosDelete.size() + listaAlunosUpdate.size());

        // DELETES
        for (Aluno aluno : listaAlunosDelete) {
            sqlStatements.add(generateDelete(aluno));
        }
        // UPDATES
        for (Aluno aluno : listaAlunosUpdate) {
            sqlStatements.add(generateUpdate(aluno));
        }

        // EXECUTE
        for (String sqlStatement : sqlStatements) {
            if (COMMIT) {
                database.executeStatement(sqlStatement);
            }
            // logger.debug(sqlStatement);
        }

    }

    private String generateUpdate(Aluno aluno) {
        // Possible optimization - Use PreparedStatements
        StringBuilder sb = new StringBuilder(300);
        sb.append("UPDATE aluno SET ");

        sb.append("activo='false").append("',");
        sb.append("obs='Desabilitado pelo script " + this.getClass().getSimpleName()).append("',");
        sb.append("data_alteracao=").append(getString(getSmallDate(sysdate))).append("");

        sb.append(" WHERE ");
        sb.append("login=").append(getString(aluno.getLogin()));
        sb.append(";");

        return sb.toString();
    }

    private String generateDelete(Aluno aluno) {
        // Possible optimization - Use PreparedStatements
        StringBuilder sb = new StringBuilder(100);
        sb.append("DELETE FROM aluno WHERE login =");
        sb.append(getString(aluno.getLogin()));
        sb.append(";");

        return sb.toString();

    }

    private void cleanUp() {
        listaAlunosDelete.clear();
        listaAlunosUpdate.clear();
    }

    // 071028
    private static String getSmallDate(Date date) {
        if (date != null) {
            return sdfSmallDate.format(date);
        }
        return "";
    }

    private static String getString(String str) {
        if (str == null || str.trim().length() == 0) {
            return null;
        }

        return "'" + str.replaceAll("'", "\\\\'") + "'";
    }

    /**
     * Remove "@iscte.pt" suffix.
     * 
     * @param login
     * @return
     */
    private static String getLoginForDSI(String login) {
        return login.substring(0, login.length() - 9);
    }

    private static String[] getStaticList() {
        String p = PropertiesManager.getProperty("DsiStudentIntegration.staticList");
        if (p != null && p.trim().length() > 0) {
            return p.split(",");
        }
        return null;
    }

}
