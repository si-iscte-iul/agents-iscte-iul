package pt.iscte.dsi.webclient.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * 
 * @author Paulo Zenida
 * 
 */
public final class DataLoaderFromDatabase<T> extends BaseDataLoaderFromDatabase<T> {

    protected final static Logger logger = Logger.getLogger(DatabaseManager.class);

    public String getDatabaseFilename() {
        return "configuration.properties";
    }

    public void executeUpdate(final List<String> sqlStatements) {
        connectDatabase();
        try {
            stm = con.createStatement();
            for (final String sqlStatement : sqlStatements) {
                stm.executeUpdate(sqlStatement);
            }
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }
    }

    public boolean executeQuery(String sqlStatement) {
        boolean searchedRecordAlreadyExistsInDatabase = false;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);
            int i = 0;

            while (rs.next()) {
                ++i;
            }

            if (i > 0) {
                searchedRecordAlreadyExistsInDatabase = true;
            }

            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return searchedRecordAlreadyExistsInDatabase;
    }

    public int getLastAttributeValueFromTable(final String sqlStatement) {
        int lastIdInternal = 0;;
        connectDatabase();
        try {
            stm = con.createStatement();
            ResultSet rs = stm.executeQuery(sqlStatement);

            while (rs.next()) {
                lastIdInternal = rs.getInt(1);
            }
            rs.close();
            stm.close();
            closeConnection();
        } catch (SQLException e) {
            logger.error("Exception: " + e);
        }

        return lastIdInternal;
    }
}