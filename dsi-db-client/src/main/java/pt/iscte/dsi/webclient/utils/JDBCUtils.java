package pt.iscte.dsi.webclient.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Esta classe contém um conjunto de utilitários diversos para manipulação de
 * estruturas relacionadas com JDBC.
 */
final class JDBCUtils {
    /**
     * Constructor.
     */
    private JDBCUtils() {
    }

    /**
     * Devolve o inteiro de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o inteiro de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Integer getInteger(ResultSet rs, int columnIndex) throws SQLException {
        final int tmp = rs.getInt(columnIndex);

        if (!rs.wasNull()) {
            return new Integer(tmp);
        }

        return null;
    }

    /**
     * Devolve o long de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o long de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Long getLong(ResultSet rs, int columnIndex) throws SQLException {
        final long tmp = rs.getLong(columnIndex);

        if (!rs.wasNull()) {
            return new Long(tmp);
        }

        return null;
    }

    /**
     * Devolve o float de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o float de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Float getFloat(ResultSet rs, int columnIndex) throws SQLException {
        final float tmp = rs.getFloat(columnIndex);

        if (!rs.wasNull()) {
            return new Float(tmp);
        }

        return null;
    }

    /**
     * Devolve o double de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o double de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Double getDouble(ResultSet rs, int columnIndex) throws SQLException {
        final double tmp = rs.getDouble(columnIndex);

        if (!rs.wasNull()) {
            return new Double(tmp);
        }

        return null;
    }

    /**
     * Devolve o booleano de um ResultSet de uma determinada coluna tendo em conta uma representação
     * em formato String ('S' = true e 'N' = false).
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o booleano de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Boolean getBoolean(ResultSet rs, int columnIndex) throws SQLException {
        String tmp = rs.getString(columnIndex);

        if (tmp != null) {
            return "S".equals(tmp) ? Boolean.TRUE : Boolean.FALSE;
        }

        return null;
    }

    /**
     * Devolve a data de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return a data de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Date getDate(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getDate(columnIndex);
    }

    /**
     * Devolve a data de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return a data de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Date getDateTime(ResultSet rs, int columnIndex) throws SQLException {
        Timestamp t = rs.getTimestamp(columnIndex);

        return t == null ? null : new Date(t.getTime());
    }

    /**
     * Devolve o BigDecimal de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o BigDecimal de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static BigDecimal getBigDecimal(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getBigDecimal(columnIndex);
    }

    /**
     * Devolve o BigInteger de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o BigInteger de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static BigInteger getBigInteger(ResultSet rs, int columnIndex) throws SQLException {
        final BigDecimal tmp = rs.getBigDecimal(columnIndex);

        if (tmp != null) {
            return tmp.toBigInteger();
        }

        return null;
    }

    /**
     * Devolve a data com nanosegundos de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return a data com horas de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Timestamp getTimestamp(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getTimestamp(columnIndex);
    }

    /**
     * Devolve a string de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return a string de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static String getString(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getString(columnIndex);
    }

    /**
     * Devolve o byte de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o byte de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Byte getByte(ResultSet rs, int columnIndex) throws SQLException {
        final byte tmp = rs.getByte(columnIndex);

        if (!rs.wasNull()) {
            return new Byte(tmp);
        }

        return null;
    }

    /**
     * Devolve o short de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o short de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Short getShort(ResultSet rs, int columnIndex) throws SQLException {
        final short tmp = rs.getShort(columnIndex);

        if (!rs.wasNull()) {
            return new Short(tmp);
        }

        return null;
    }

    /**
     * Devolve o time de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return o time de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Time getTime(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getTime(columnIndex);
    }

    /**
     * Devolve a data (ano/mes) de um ResultSet de uma determinada coluna.
     * 
     * @param rs ResultSet com os dados
     * @param columnIndex indice da coluna
     * @return a data de um ResultSet de uma determinada coluna
     * @throws SQLException caso ocorra alguma excepção no acesso ao ResultSet
     * @throws NullPointerException caso o ResultSet seja null
     */
    public static Date getYearMonth(ResultSet rs, int columnIndex) throws SQLException {
        String str = rs.getString(columnIndex);
        if (str == null) {
            return null;
        }

        return fromYearMonth(str);
    }

    /**
     * Permite colocar um Integer no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Integer
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setInteger(PreparedStatement ps, Integer value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.NUMERIC);
        } else {
            setInt(ps, value.intValue(), columnIndex);
        }
    }

    /**
     * Permite colocar um int no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O int
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setInt(PreparedStatement ps, int value, int columnIndex) throws SQLException {
        ps.setInt(columnIndex, value);
    }

    /**
     * Permite colocar um Long no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Long
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setLong(PreparedStatement ps, Long value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.NUMERIC);
        } else {
            setLong(ps, value.longValue(), columnIndex);
        }
    }

    /**
     * Permite colocar um Long no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O long
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setLong(PreparedStatement ps, long value, int columnIndex) throws SQLException {
        ps.setLong(columnIndex, value);
    }

    /**
     * Permite colocar um Float no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Float
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setFloat(PreparedStatement ps, Float value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.NUMERIC);
        } else {
            setFloat(ps, value.floatValue(), columnIndex);
        }
    }

    /**
     * Permite colocar um float no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O float
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setFloat(PreparedStatement ps, float value, int columnIndex) throws SQLException {
        ps.setFloat(columnIndex, value);
    }

    /**
     * Permite colocar um Double no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Double
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setDouble(PreparedStatement ps, Double value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.NUMERIC);
        } else {
            setDouble(ps, value.doubleValue(), columnIndex);
        }
    }

    /**
     * Permite colocar um double no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O double
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setDouble(PreparedStatement ps, double value, int columnIndex) throws SQLException {
        ps.setDouble(columnIndex, value);
    }

    /**
     * Permite colocar um BigDecimal no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O BigDecimal
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setBigDecimal(PreparedStatement ps, BigDecimal value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.DECIMAL);
        } else {
            ps.setBigDecimal(columnIndex, value);
        }
    }

    /**
     * Permite colocar um BigInteger no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O BigInteger
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setBigInteger(PreparedStatement ps, BigInteger value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.DECIMAL);
        } else {
            ps.setBigDecimal(columnIndex, new BigDecimal(value));
        }
    }

    /**
     * Permite colocar um Boolean no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Boolean
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setBoolean(PreparedStatement ps, Boolean value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.VARCHAR);
        } else {
            setBoolean(ps, value.booleanValue(), columnIndex);
        }
    }

    /**
     * Permite colocar um boolean no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O boolean
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setBoolean(PreparedStatement ps, boolean value, int columnIndex) throws SQLException {
        ps.setString(columnIndex, value ? "S" : "N");
    }

    /**
     * Permite colocar um Date no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Date
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setDate(PreparedStatement ps, Date value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.DATE);
        } else {
            ps.setDate(columnIndex, new java.sql.Date(value.getTime()));
        }
    }

    /**
     * Permite colocar um Date no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Date
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setDate(PreparedStatement ps, java.sql.Date value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.DATE);
        } else {
            ps.setDate(columnIndex, value);
        }
    }

    /**
     * Permite colocar um Timestamp no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Timestamp
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setTimestamp(PreparedStatement ps, Timestamp value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.TIMESTAMP);
        } else {
            ps.setTimestamp(columnIndex, value);
        }
    }

    /**
     * Permite colocar uma data com hh:mm:ss no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Timestamp
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setDateTime(PreparedStatement ps, Date value, int columnIndex) throws SQLException {
        setTimestamp(ps, value == null ? null : new Timestamp(value.getTime()), columnIndex);
    }

    /**
     * Permite colocar uma String no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value A String
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setString(PreparedStatement ps, String value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.VARCHAR);
        } else {
            ps.setString(columnIndex, value);
        }
    }

    /**
     * Permite colocar um Byte no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Byte
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setByte(PreparedStatement ps, Byte value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.SMALLINT);
        } else {
            ps.setByte(columnIndex, value.byteValue());
        }
    }

    /**
     * Permite colocar um byte no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O byte
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setByte(PreparedStatement ps, byte value, int columnIndex) throws SQLException {
        ps.setByte(columnIndex, value);
    }

    /**
     * Permite colocar um Short no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Short
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setShort(PreparedStatement ps, Short value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.SMALLINT);
        } else {
            ps.setShort(columnIndex, value.shortValue());
        }
    }

    /**
     * Permite colocar um short no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O short
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setShort(PreparedStatement ps, short value, int columnIndex) throws SQLException {
        ps.setShort(columnIndex, value);
    }

    /**
     * Permite colocar um Time no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Time
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setTime(PreparedStatement ps, Time value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.TIME);
        } else {
            ps.setTime(columnIndex, value);
        }
    }

    /**
     * Permite colocar um Time no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Time
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setTime(PreparedStatement ps, Date value, int columnIndex) throws SQLException {
        setTime(ps, value == null ? null : new Time(value.getTime()), columnIndex);
    }

    /**
     * Permite colocar um Date (Ano/Mês) no PreparedStatment.
     * 
     * @param ps O PreparedStatment
     * @param value O Date
     * @param columnIndex O Indice da coluna
     * @throws SQLException Em caso de excepção no acesso ao PreparedStatment
     * @throws NullPointerException caso o PreparedStatement seja null
     */
    public static void setYearMonth(PreparedStatement ps, Date value, int columnIndex) throws SQLException {
        if (value == null) {
            ps.setNull(columnIndex, Types.VARCHAR);
        } else {
            ps.setString(columnIndex, toYearMonth(value));
        }
    }

    /**
     * Adicionada a um StringBuffer um array de strings para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     */
    public static void getInClauseSQL(StringBuffer sb, String column, String[] elementos) {
        getInClauseSQL(sb, column, elementos, true);
    }

    /**
     * Adicionada a um StringBuffer um array de strings para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    public static void getInClauseSQL(StringBuffer sb, String column, String[] elementos, boolean in) {
        if (elementos == null || elementos.length == 0) {
            return;
        }

        if (elementos.length == 1) {
            sb.append(column).append(in ? "='" : "<>'").append(elementos[0]).append("'");

            return;
        }

        int length = InQueryInformation.length(column, true, elementos, in);

        sb.ensureCapacity(sb.length() + length);

        InQueryInformation.addQuery(sb, column, true, elementos, in);
    }

    /**
     * Adicionada a um StringBuffer um array de Integers para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     */
    public static void getInClauseSQL(StringBuffer sb, String column, Integer[] elementos) {
        getInClauseSQL(sb, column, elementos, true);
    }

    /**
     * Adicionada a um StringBuffer um array de Integers para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    public static void getInClauseSQL(StringBuffer sb, String column, Integer[] elementos, boolean in) {
        if (elementos == null || elementos.length == 0) {
            return;
        }

        if (elementos.length == 1) {
            sb.append(column).append(in ? "=" : "<>").append(elementos[0]);

            return;
        }

        String[] textValuesRepresentation = new String[elementos.length];
        for (int i = 0; i < textValuesRepresentation.length; i++) {
            textValuesRepresentation[i] = elementos[i].toString();
        }

        int length = InQueryInformation.length(column, false, textValuesRepresentation, in);

        sb.ensureCapacity(sb.length() + length);

        InQueryInformation.addQuery(sb, column, false, textValuesRepresentation, in);
    }

    /**
     * Adicionada a um StringBuffer um array de Integers para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     */
    public static void getInClauseSQL(StringBuffer sb, String column, int[] elementos) {
        getInClauseSQL(sb, column, elementos, true);
    }

    /**
     * Adicionada a um StringBuffer um array de Integers para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    public static void getInClauseSQL(StringBuffer sb, String column, int[] elementos, boolean in) {
        if (elementos == null || elementos.length == 0) {
            return;
        }

        if (elementos.length == 1) {
            sb.append(column).append(in ? "=" : "<>").append(elementos[0]);

            return;
        }

        String[] textValuesRepresentation = new String[elementos.length];
        for (int i = 0; i < textValuesRepresentation.length; i++) {
            textValuesRepresentation[i] = String.valueOf(elementos[i]);
        }

        int length = InQueryInformation.length(column, false, textValuesRepresentation, in);

        sb.ensureCapacity(sb.length() + length);

        InQueryInformation.addQuery(sb, column, false, textValuesRepresentation, in);
    }

    /**
     * Adicionada a um StringBuffer um array de Longs para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     */
    public static void getInClauseSQL(StringBuffer sb, String column, Long[] elementos) {
        getInClauseSQL(sb, column, elementos, true);
    }

    /**
     * Adicionada a um StringBuffer um array de Longs para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    public static void getInClauseSQL(StringBuffer sb, String column, Long[] elementos, boolean in) {
        if (elementos == null || elementos.length == 0) {
            return;
        }

        if (elementos.length == 1) {
            sb.append(column).append(in ? "=" : "<>").append(elementos[0]);

            return;
        }

        String[] textValuesRepresentation = new String[elementos.length];
        for (int i = 0; i < textValuesRepresentation.length; i++) {
            textValuesRepresentation[i] = elementos[i].toString();
        }

        int length = InQueryInformation.length(column, false, textValuesRepresentation, in);

        sb.ensureCapacity(sb.length() + length);

        InQueryInformation.addQuery(sb, column, false, textValuesRepresentation, in);
    }

    /**
     * Adicionada a um StringBuffer um array de Longs para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     */
    public static void getInClauseSQL(StringBuffer sb, String column, long[] elementos) {
        getInClauseSQL(sb, column, elementos, true);
    }

    /**
     * Adicionada a um StringBuffer um array de Longs para uma lista passível de ser passada à cláusula IN de um comando SQL.
     * 
     * @param sb buffer onde se irá colocar os dados
     * @param column nome da coluna sobre o qual se fazer o IN
     * @param elementos array de strings com os elementos da lista
     * @param in true caso seja para IN, false caso seja IN NOT
     */
    public static void getInClauseSQL(StringBuffer sb, String column, long[] elementos, boolean in) {
        if (elementos == null || elementos.length == 0) {
            return;
        }

        if (elementos.length == 1) {
            sb.append(column).append(in ? "=" : "<>").append(elementos[0]);

            return;
        }

        String[] textValuesRepresentation = new String[elementos.length];
        for (int i = 0; i < textValuesRepresentation.length; i++) {
            textValuesRepresentation[i] = String.valueOf(elementos[i]);
        }

        int length = InQueryInformation.length(column, false, textValuesRepresentation, in);

        sb.ensureCapacity(sb.length() + length);

        InQueryInformation.addQuery(sb, column, false, textValuesRepresentation, in);
    }

    /**
     * Converte um <code>java.util.Date</code> num <code>java.sql.Timestamp</code>.
     * <P>
     * Deve ser usado quando se quer passar uma data com Horas, Minutos e Segundos num <code>OpcionalParameters</code>. <BR/>
     * Quando se trabalha directamente com <code>Resulset</code>, usar o <code>setOrdinaryDate</code>.
     * </P>
     * 
     * @param value data a converter
     * @return data retirnada como Timestamp
     */
    public static Timestamp convertDateToTimestamp(Date value) {
        if (value == null) {
            return null;
        }

        return new Timestamp(value.getTime());
    }

    // -----------------------------------------------------------------------
    private static final SimpleDateFormat sdf = new SimpleDateFormat("YYYYMM");

    /**
     * Converte uma data numa string com o formato da base de dados (YYYYMM).
     * 
     * @param date data a converter
     * @return a representaçao em string da data passada
     */
    private static String toYearMonth(Date date) {
        return sdf.format(date);
    }

    /**
     * Converte uma string com o formato da base de dados (YYYYMM), numa data .
     * 
     * @param date data a converter
     * @return a representaçao em <code>java.util.Date</code> da data passada
     */
    private static Date fromYearMonth(String date) {
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
